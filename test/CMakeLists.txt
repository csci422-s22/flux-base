# add the unit tests in the subdirectories
add_test_files( TEST_FILES "ut" core )
add_test_files( TEST_FILES "ut" . )
add_test_files( TEST_FILES "ut" ${CMAKE_SOURCE_DIR}/test )

list( REMOVE_DUPLICATES TEST_FILES )

set( unit_skip )

# the main unit test file
set( DRIVER_UNIT driver.cpp )
set( COMMON_UNIT common.cpp )

add_library( ut_common_lib STATIC ${COMMON_UNIT} )
target_link_libraries( ut_common_lib flux_shared )

# needed for the directory class definitions
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_SOURCE_DIR}/src )

# set the directory for the executables
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/test/bin )

# create a target for each test source
foreach( test_src ${TEST_FILES} )

  list (FIND unit_skip ${test_src} _index)

  # replace the '/' with '_' and "../" with ""
  string( REPLACE "../" "" test_src_underscored ${test_src} )
  string( REPLACE "/" "_" test_src_underscored ${test_src_underscored} )

  # remove the extension
  get_filename_component( test_bin ${test_src_underscored} NAME_WE )

  # assign the name of the executable
  set( test_exe ${test_bin}_exe )

  # the actual executable: target for simply building
  flux_add_executable( ${test_exe} ${test_src} )
  target_link_libraries( ${test_exe} ut_common_lib )
  target_compile_definitions( ${test_exe} PUBLIC -DSTANDALONE -DFLUX_FULL_UNIT_TEST=false )

  # create the target
  add_custom_target( ${test_bin} command $<TARGET_FILE:${test_exe}> 1 WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/test )

  # targets for memory & optionally stack checking & gdb
  ADD_DEBUG_TARGETS( ${test_bin} ${CMAKE_SOURCE_DIR}/test )

endforeach()

add_definitions( -DFLUX_SOURCE_DIR="${CMAKE_SOURCE_DIR}" )

# create the full unit executable
flux_add_executable( unit_exe ${DRIVER_UNIT} ${TEST_FILES} )
target_compile_definitions( unit_exe PUBLIC -DTEST_NUM_PROCS=4 )
target_link_libraries( unit_exe flux_shared )
target_compile_definitions( unit_exe PUBLIC -DSTDOUT_REDIRECT="${CMAKE_BINARY_DIR}/unit_tests_output.txt" -DFLUX_FULL_UNIT_TEST=true )

# target for running all unit tests from a single executable
add_custom_target( unit command $<TARGET_FILE:unit_exe> WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/test )

ADD_DEBUG_TARGETS( unit ${CMAKE_SOURCE_DIR}/test )
ADD_COVERAGE_UT( unit_coverage unit )

# reset the directory for the executables
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin )
