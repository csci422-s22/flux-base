/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/

#include "tester.hpp"

#include "readwrite.h"
#include "mesh.h"

#include "webgl.h"

using namespace flux;

UT_TEST_SUITE(readwrite_suite)

UT_TEST_CASE(readwrite_obj_test)
{

  std::unique_ptr<MeshBase> mesh1 = read_obj( std::string(FLUX_SOURCE_DIR) + "/data/spot.obj",true);
  std::unique_ptr<MeshBase> mesh2 = read_obj( std::string(FLUX_SOURCE_DIR) + "/data/spot.obj",false);

  // whether duplicating vertices or not, the meshes should have the same number of triangles
  UT_ASSERT_EQUALS( mesh1->nb() , mesh2->nb() );

  write_obj( *mesh2.get() , "tmp/test.obj" );

  std::unique_ptr<MeshBase> mesh3 = read_obj( "tmp/test.obj" , false );
  UT_ASSERT_EQUALS( mesh2->nb() , mesh3->nb() );
  UT_ASSERT_EQUALS( mesh2->vertices().nb() , mesh3->vertices().nb() );

  Viewer viewer;
  viewer.add( *mesh1.get() );
  //viewer.run();

}
UT_TEST_CASE_END(readwrite_obj_test)

UT_TEST_CASE(readwrite_off_test)
{

  Viewer viewer;

  std::vector< std::string > meshes = {"beetle.off" , "fandisk.off" , "fandisk_quads.off" , "bunny.off" };

  std::unique_ptr<MeshBase> mesh = nullptr;
  for (size_t i = 0; i < meshes.size(); i++) {
    mesh = read_off( std::string(FLUX_SOURCE_DIR) + "/data/" + meshes[i] );
  }
  viewer.add( *mesh.get() );
  viewer.run();

}
UT_TEST_CASE_END(readwrite_off_test)

UT_TEST_SUITE_END(readwrite_suite)
