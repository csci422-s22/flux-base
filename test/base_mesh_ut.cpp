/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/

#include "tester.hpp"

#include "element.h"
#include "field.h"
#include "mesh.h"

using namespace flux;

UT_TEST_SUITE( mesh_test_suite )

UT_TEST_CASE( line_mesh_tests )
{
  double tol = 1e-12;

  Mesh<Line> mesh(3);

  UT_ASSERT_EQUALS( mesh.vertices().dim() , 3 );

  double x0[3] = {0,0,0};
  double x1[3] = {1,0,0};

  mesh.vertices().add(x0);
  mesh.vertices().add(x1);
  UT_ASSERT_EQUALS( mesh.vertices().nb() , 2 );

  int line[4] = {0,1};
  mesh.add(line);

  std::vector<int> triangles;
  mesh.get_triangles( triangles , nullptr );

  UT_ASSERT_EQUALS( triangles.size() , 0 );

  std::vector<int> edges;
  mesh.get_edges( edges );
  UT_ASSERT_EQUALS( edges.size()/2 , 1  );

  array2d<double> normals(3);
  mesh.calculate_vertex_normals(normals);
  UT_ASSERT_EQUALS( normals.nb() , mesh.vertices().nb() );

  for (int i = 0; i < mesh.vertices().nb(); i++) {
    UT_ASSERT_NEAR( normals(i,0) , 0.0 , tol );
    UT_ASSERT_NEAR( normals(i,1) , 1.0 , tol );
    UT_ASSERT_NEAR( normals(i,2) , 0.0 , tol );
  }
}
UT_TEST_CASE_END( line_mesh_tests )

UT_TEST_CASE( triangle_mesh_tests )
{
  double tol = 1e-12;

  Mesh<Triangle> mesh(3);

  UT_ASSERT_EQUALS( mesh.vertices().dim() , 3 );

  double x0[3] = {0,0,0};
  double x1[3] = {1,0,0};
  double x2[3] = {0,1,0};

  mesh.vertices().add(x0);
  mesh.vertices().add(x1);
  mesh.vertices().add(x2);
  UT_ASSERT_EQUALS( mesh.vertices().nb() , 3 );

  int t[3] = {0,1,2};
  mesh.add(t);

  std::vector<int> triangles, parents;
  mesh.get_triangles( triangles , &parents );

  UT_ASSERT_EQUALS( int(triangles.size()/3) , mesh.nb()  );
  UT_ASSERT_EQUALS( int(parents.size()) , mesh.nb() );

  std::vector<int> edges;
  mesh.get_edges( edges );
  UT_ASSERT_EQUALS( edges.size()/2 , 3  );

  std::vector<double> normal(3);
  get_normal<Triangle>( mesh.vertices() , mesh[0] , 3 , normal );
  UT_ASSERT_NEAR( normal[0] , 0.0 , tol );
  UT_ASSERT_NEAR( normal[1] , 0.0 , tol );
  UT_ASSERT_NEAR( normal[2] , 1.0 , tol );

  array2d<double> normals(3);
  mesh.calculate_vertex_normals(normals);
  UT_ASSERT_EQUALS( normals.nb() , mesh.vertices().nb() );

  for (int i = 0; i < mesh.vertices().nb(); i++) {
    UT_ASSERT_NEAR( normals(i,0) , 0.0 , tol );
    UT_ASSERT_NEAR( normals(i,1) , 0.0 , tol );
    UT_ASSERT_NEAR( normals(i,2) , 1.0 , tol );
  }

  // attach some fields to the triangle mesh
  std::vector<double> f = {100,20,50};
  UT_ASSERT_EQUALS( mesh.nb_fields() , 0 );
  mesh.create_vertex_field( "test-field" , f );
  UT_ASSERT_EQUALS( mesh.nb_fields() , 1 );
  UT_ASSERT_EQUALS( mesh.field(0).type() , FieldType_VertexBased );
  UT_ASSERT_EQUALS( mesh.field(0).name() , "test-field" );

  const std::vector<double>& data = mesh.field(0).data();
  UT_ASSERT_EQUALS( int(data.size()) , mesh.vertices().nb() );
  for (size_t i = 0; i < f.size(); i++)
    UT_ASSERT_NEAR( f[i] , data[i] , tol );

  // test the constructor from complete vertex and element data
  std::vector<double> p = { 0,0,0 , 1,0,0, 0,1,0 };
  Mesh<Triangle> mesh2( 3 , 3 , p.data() , 1 , t );
  UT_ASSERT_EQUALS( mesh2.vertices().nb() , mesh.vertices().nb() );
  UT_ASSERT_EQUALS( mesh2.nb() , mesh.nb() );
  for (int k = 0; k < mesh2.vertices().nb(); k++) {
    for (int d = 0; d < 3; d++)
      UT_ASSERT_NEAR( mesh2.vertices()(k,d) , mesh.vertices()(k,d) , tol );
  }

  for (int k = 0; k < mesh2.nb(); k++) {
    for (int j = 0; j < mesh2.count(k); j++)
      UT_ASSERT_EQUALS( mesh2(k,j) , mesh(k,j) );
  }
}
UT_TEST_CASE_END( triangle_mesh_tests )

UT_TEST_CASE( quad_mesh_tests )
{
  double tol = 1e-12;

  Mesh<Quad> mesh(3);

  UT_ASSERT_EQUALS( mesh.vertices().dim() , 3 );

  double x0[3] = {0,0,0};
  double x1[3] = {1,0,0};
  double x2[3] = {1,1,0};
  double x3[3] = {0,1,0};

  mesh.vertices().add(x0);
  mesh.vertices().add(x1);
  mesh.vertices().add(x2);
  mesh.vertices().add(x3);
  UT_ASSERT_EQUALS( mesh.vertices().nb() , 4 );

  int q[4] = {0,1,2,3};
  mesh.add(q);

  std::vector<int> triangles, parents;
  mesh.get_triangles( triangles , &parents );

  UT_ASSERT_EQUALS( triangles.size()/3 , 2 );
  UT_ASSERT_EQUALS( parents.size() , 2 );

  // both triangles should point to the same parent (0)
  UT_ASSERT_EQUALS( parents[0] , 0 );
  UT_ASSERT_EQUALS( parents[1] , 0 );

  std::vector<int> edges;
  mesh.get_edges( edges );
  UT_ASSERT_EQUALS( edges.size()/2 , 4  );

  std::vector<double> normal(3);
  get_normal<Triangle>( mesh.vertices() , mesh[0] , 3 , normal );
  UT_ASSERT_NEAR( normal[0] , 0.0 , tol );
  UT_ASSERT_NEAR( normal[1] , 0.0 , tol );
  UT_ASSERT_NEAR( normal[2] , 1.0 , tol );

  array2d<double> normals(3);
  mesh.calculate_vertex_normals(normals);
  UT_ASSERT_EQUALS( normals.nb() , mesh.vertices().nb() );

  for (int i = 0; i < mesh.vertices().nb(); i++) {
    UT_ASSERT_NEAR( normals(i,0) , 0.0 , tol );
    UT_ASSERT_NEAR( normals(i,1) , 0.0 , tol );
    UT_ASSERT_NEAR( normals(i,2) , 1.0 , tol );
  }
}
UT_TEST_CASE_END( quad_mesh_tests )

UT_TEST_CASE( polygon_mesh_tests )
{
  double tol = 1e-12;

  Mesh<Polygon> mesh(3);

  UT_ASSERT_EQUALS( mesh.vertices().dim() , 3 );

  double x0[3] = {0,0,0};
  double x1[3] = {1,0,0};
  double x2[3] = {1,1,0};
  double x3[3] = {0,1,0};

  mesh.vertices().add(x0);
  mesh.vertices().add(x1);
  mesh.vertices().add(x2);
  mesh.vertices().add(x3);
  UT_ASSERT_EQUALS( mesh.vertices().nb() , 4 );

  int p[4] = {0,1,2,3};
  mesh.add(p,4);

  std::vector<int> triangles, parents;
  mesh.get_triangles( triangles , &parents );

  UT_ASSERT_EQUALS( triangles.size()/3 , 2 );
  UT_ASSERT_EQUALS( parents.size() , 2 );

  // both triangles should point to the same parent (0)
  UT_ASSERT_EQUALS( parents[0] , 0 );
  UT_ASSERT_EQUALS( parents[1] , 0 );

  std::vector<int> edges;
  mesh.get_edges( edges );
  UT_ASSERT_EQUALS( edges.size()/2 , 4  );

  std::vector<double> normal(3);
  get_normal<Triangle>( mesh.vertices() , mesh[0] , 3 , normal );
  UT_ASSERT_NEAR( normal[0] , 0.0 , tol );
  UT_ASSERT_NEAR( normal[1] , 0.0 , tol );
  UT_ASSERT_NEAR( normal[2] , 1.0 , tol );

  array2d<double> normals(3);
  mesh.calculate_vertex_normals(normals);
  UT_ASSERT_EQUALS( normals.nb() , mesh.vertices().nb() );

  for (int i = 0; i < mesh.vertices().nb(); i++) {
    UT_ASSERT_NEAR( normals(i,0) , 0.0 , tol );
    UT_ASSERT_NEAR( normals(i,1) , 0.0 , tol );
    UT_ASSERT_NEAR( normals(i,2) , 1.0 , tol );
  }
}
UT_TEST_CASE_END( polygon_mesh_tests )

UT_TEST_CASE( tet_mesh_tests )
{
  double tol = 1e-12;

  Mesh<Tet> mesh(3);

  UT_ASSERT_EQUALS( mesh.vertices().dim() , 3 );

  double x0[3] = {0,0,0};
  double x1[3] = {1,0,0};
  double x2[3] = {0,1,0};
  double x3[3] = {0,0,1};

  mesh.vertices().add(x0);
  mesh.vertices().add(x1);
  mesh.vertices().add(x2);
  mesh.vertices().add(x3);
  UT_ASSERT_EQUALS( mesh.vertices().nb() , 4 );

  int t[4] = {0,1,2,3};
  mesh.add(t);

  // retrieval of the parents of the triangles is not supported
  std::vector<int> triangles;
  mesh.get_triangles( triangles , nullptr );

  UT_ASSERT_EQUALS( triangles.size()/3 , 4 );

  std::vector<int> edges;
  mesh.get_edges( edges );
  UT_ASSERT_EQUALS( edges.size()/2 , 6  );

  // the normal vector will not be calculated correctly - it will be a vector of zeros.
  std::vector<double> normal(3);
  get_normal<Tet>( mesh.vertices() , mesh[0] , 3 , normal );
  UT_ASSERT_NEAR( normal[0] , 0.0 , tol );
  UT_ASSERT_NEAR( normal[1] , 0.0 , tol );
  UT_ASSERT_NEAR( normal[2] , 0.0 , tol );
}
UT_TEST_CASE_END( tet_mesh_tests )

UT_TEST_SUITE_END( mesh_test_suite )
