/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/

#include "tester.hpp"

#include "halfedges.h"
#include "mesh.h"
#include "readwrite.h"
#include "size.h"
#include "vec.hpp"
#include "webgl.h"

using namespace flux;

UT_TEST_SUITE(size_test_suite)

UT_TEST_CASE(image_test)
{
  ImageSizingField size( std::string(FLUX_SOURCE_DIR) + "/data/giraffe.jpg" , 0.1 , 0.5 );

  Grid<Quad>& grid = size.grid();
  std::vector<double> s( grid.vertices().nb() );
  for (int k = 0; k < grid.vertices().nb(); k++) {
    s[k] = size( grid.vertices()[k] );
  }
  grid.create_vertex_field( "size" , s );

  for (int k = 0; k < grid.nb(); k++) {

    // calculate the centeroid
    vec2d p0( grid.vertices()[ grid(k,0) ] );
    vec2d p1( grid.vertices()[ grid(k,1) ] );
    vec2d p2( grid.vertices()[ grid(k,2) ] );
    vec2d p3( grid.vertices()[ grid(k,3) ] );

    vec2d c = 0.25 * (p0 + p1 + p2 + p3);

    // ensure the evaluated size is the same as the one stored in the pixel
    double h = size(c.data());
    UT_ASSERT_NEAR( h , size.size(k) , 1e-12 );
  }

  Viewer viewer;
  viewer.add( size.grid() );
  //viewer.run();
}
UT_TEST_CASE_END(image_test)

UT_TEST_CASE( constant_size_test )
{
  double tol = 1e-12;

  // a sizing field that always evaluates to 0.5
  class ConstantSizingField : public SizingField<2> {
  public:
    double operator()( const double* x ) const {
      return 0.5;
    }
  };

  ConstantSizingField size;

  vec2d x = {0,0};
  vec2d y = {1,1};

  double hx = size(x.data());
  double hy = size(y.data());

  UT_ASSERT_NEAR( hx , 0.5 , tol );
  UT_ASSERT_NEAR( hy , 0.5 , tol );

  HalfVertex vx( 2 , x.data() );
  HalfVertex vy( 2 , y.data() );

  double l = size.length( &vx , &vy );
  UT_ASSERT_NEAR( l , norm(y - x) / 0.5 , tol );

  Grid<Quad> grid({20,20});
  int nb_expected = size.nb_expected_triangles(grid);
  UT_ASSERT_EQUALS( nb_expected , 9 );
}
UT_TEST_CASE_END( constant_size_test )

UT_TEST_SUITE_END(size_test_suite)
