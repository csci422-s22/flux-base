#include "tester.hpp"

#include "element.h"
#include "grid.h"
#include "kdtree.h"
#include "vec.hpp"

#include <set>
#include <vector>

using namespace flux;

UT_TEST_SUITE(kdtree_test_suite)

UT_TEST_CASE(kdtree_test_2d)
{
  // rosetta code test case: https://rosettacode.org/wiki/K-d_tree
  static const int dim = 2;
  std::vector<double> points = { 2, 3 , 5, 4 , 9, 6, 4, 7, 8, 1, 7, 2 };

  kdtree<dim> tree(points);
  double point[2] = {9,2};
  int n = tree.nearest_neighbor(point);
  printf("nearest = %d: (%g,%g)\n",n,points[2*n],points[2*n+1]);
  UT_ASSERT_EQUALS( n , 4 );

  for (int k = 0; k < points.size()/dim; k++) {
    int n = tree.nearest_neighbor(&points[dim*k]);
    printf("nearest = %d: (%g,%g)\n",n,points[2*n],points[2*n+1]);
    UT_ASSERT_EQUALS( n , k );
  }
}
UT_TEST_CASE_END(kdtree_test_2d)

void
get_random_points( int nb_points , int dim, std::vector<double>& points ) {
  points.resize(dim*nb_points);
  for (int k = 0; k < nb_points*dim; k++)
    points[k] = double(rand())/double(RAND_MAX);
}

UT_TEST_CASE(kdtree_test_nd)
{
  int nb_points = 1e4;

  {
    static const int dim = 2;

    std::vector<double> points;
    get_random_points(nb_points,dim,points);

    kdtree<dim> tree(points);
    for (int k = 0; k < points.size()/dim; k++) {
      int n = tree.nearest_neighbor(&points[dim*k]);
      UT_ASSERT_EQUALS( n , k );
    }
  }

  {
    static const int dim = 3;

    std::vector<double> points;
    get_random_points(nb_points,dim,points);

    kdtree<dim> tree(points);
    for (int k = 0; k < points.size()/dim; k++) {
      int n = tree.nearest_neighbor(&points[dim*k]);
      UT_ASSERT_EQUALS( n , k );
    }
  }
}
UT_TEST_CASE_END(kdtree_test_nd)


UT_TEST_CASE(kdtree_grid_test)
{

  int N = 10;
  Grid<Tet> mesh({N,N,N});
  kdtree<3> search( mesh.vertices().data() );
  double h = 1.0 / N;

  // determine vertex-to-vertex information
  std::vector< std::set<int> > vertex2vertex( mesh.vertices().nb() );
  std::vector<int> edges;
  mesh.get_edges(edges);
  for (int i = 0; i < edges.size()/2; i++) {
    int p = edges[2*i  ];
    int q = edges[2*i+1];
    vertex2vertex[p].insert(q);
    vertex2vertex[q].insert(p);
  }

  std::vector<int> neighbors;
  for (int k = 0; k < mesh.vertices().nb(); k++) {

    neighbors.resize( vertex2vertex[k].size() +1 ); // request +1 because the first nearest neighbor is the point itself
    search.get_nearest_neighbors( mesh.vertices()[k] , neighbors );

    // ensure the sizes still match and that the first neighbor is the point itself
    UT_ASSERT_EQUALS( neighbors.size(), vertex2vertex[k].size()+1 );
    UT_ASSERT_EQUALS( neighbors[0] , k );

    vec3d p( mesh.vertices()[k] );
    double d = 0.0;

    // ensure the neighbours exist (in any order) in the vertex-to-vertex information
    // also ensure the distance is non-decreasing
    for (int i = 1; i < neighbors.size(); i++) {

      vec3d q( mesh.vertices()[neighbors[i]]);
      double distance = norm( q - p );
      UT_ASSERT( distance >= d );
      d = distance;

      // only check the neighbours that are directly connected to this grid point with a distance of h
      // diagonals (distance > h) may not necessarily be connected to this vertex (in vertex2vertex)
      // despite being returned as a nearest neighbor
      if (d > h) continue;

      UT_ASSERT( vertex2vertex[k].find( neighbors[i] ) != vertex2vertex[k].end() );
    }
  }

}
UT_TEST_CASE_END(kdtree_grid_test)

UT_TEST_SUITE_END(kdtree_test_suite)
