/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/

#include "tester.hpp"

#include "error.h"

#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>

#ifndef _WIN32
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#else
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
typedef int socklen_t;
#endif

#include <thread>

using namespace flux;

static int port = 7681;

namespace websockets {
int shakehands(int, const std::string&);
int sendmessage(int, const std::string&,int);
int write(int,const std::vector<std::string>&,bool);
}

UT_TEST_SUITE(websockets_test_suite)

void
client_thread() {

  // sleep so the server has a chance to start up
  sleep(2);

  struct sockaddr_in server;
  int client_fd = -1;

  #ifdef _WIN32
  WSADATA wsadata;
  int error = WSAStartup(0x0202,&wsadata);
  if (error) {
    printf("error starting up windows sockets\n");
    return;
  }
  #endif

  // create a socket
  client_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (client_fd < 0) {
    printf("failed to create socket\n");
    return;
  }

  server.sin_family      = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port        = htons(port);

  // allow the port to be re-used so that we don't have to keep changing the port number when re-running
  #ifndef _WIN32
  int enable = 1;
  if (setsockopt(client_fd,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(int)) < 0) {
     printf("error setting socket options\n");
  }
  #endif

  // connect to the server
  if (connect( client_fd , (struct sockaddr*)&server , sizeof(server) ) < 0)
    printf("error connecting\n");

  // handshake message
  std::string request = "GET / HTTP/1.1\n \
    Host: localhost:7681\n \
    Connection: Upgrade\n \
    Pragma: no-cache\n \
    Cache-Control: no-cache\n \
    User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36\n \
    Upgrade: websocket\n \
    Origin: null\n \
    Sec-WebSocket-Version: 13\n \
    Accept-Encoding: gzip, deflate, br\n \
    Accept-Language: en-US,en;q=0.9,fr-CA;q=0.8,fr;q=0.7,de;q=0.6\n \
    Sec-WebSocket-Key: pg8GIED9JHeSHzrpLMzKew==\n \
    Sec-WebSocket-Extensions: permessage-deflate; client_max_window_bits\n";

  // send the handshake request
  send( client_fd , &request[0] , request.size() , 0 );
}

void
server_thread() {
  std::vector<std::string> messages(3);
  messages[0].resize(100);
  messages[1].resize(150);
  messages[2].resize(70000);
  websockets::write( port , messages , false );
}

UT_TEST_CASE(connection_test)
{
  std::thread tclient(&client_thread);
  std::thread tserver(&server_thread);

  tclient.join();
  tserver.join();
}
UT_TEST_CASE_END(connection_test)

UT_TEST_SUITE_END(websockets_test_suite)
