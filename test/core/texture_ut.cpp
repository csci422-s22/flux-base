#include "tester.hpp"

#include "texture.h"

using namespace flux;

UT_TEST_SUITE( texture_test_suite )

UT_TEST_CASE( read_test )
{
  Texture texture( std::string(FLUX_SOURCE_DIR) + "/data/fox.png" );

  UT_ASSERT_EQUALS( texture.width()    , 1024 );
  UT_ASSERT_EQUALS( texture.height()   , 1024 );
  UT_ASSERT_EQUALS( texture.channels() ,    3 );
}
UT_TEST_CASE_END( read_test )

UT_TEST_SUITE_END( texture_test_suite )
