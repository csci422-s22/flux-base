/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/

#include "tester.hpp"

#include "element.h"
#include "mesh.h"
#include "readwrite.h"
#include "texture.h"
#include "webgl.h"

using namespace flux;

UT_TEST_SUITE(webgl_test_suite)

UT_TEST_CASE(write_test)
{
  std::unique_ptr<MeshBase> mesh = read_obj( std::string(FLUX_SOURCE_DIR) + "/data/fox.obj",true);

  Texture texture( std::string(FLUX_SOURCE_DIR) + "/data/fox.png" );

  // create a vertex field that has the y value of every vertex
  std::vector<double> y( mesh->vertices().nb() );
  for (int k = 0; k < mesh->vertices().nb(); k++)
    y[k] = mesh->vertices()[k][1];
  mesh->create_vertex_field( "y" , y );

  // attach a 2d mesh as well so we can make sure the normals are computed for a 2d mesh
  double x[8] = {0,0 , 1,0 , 1,1 , 0,1 };
  int q[4] = {0,1,2,3};
  Mesh<Quad> mesh2( 2 , 4 , x , 1 , q);

  Viewer viewer;
  viewer.add( *mesh.get() , &texture );
  viewer.add( mesh2 );
  viewer.run();

}
UT_TEST_CASE_END(write_test)

UT_TEST_CASE(cell_field_test)
{
  std::unique_ptr<MeshBase> mesh = read_obj( std::string(FLUX_SOURCE_DIR) + "/data/spot.obj",true);

  // create a vertex field that has the y value of every vertex
  std::vector<double> y( mesh->vertices().nb() );
  for (int k = 0; k < mesh->vertices().nb(); k++)
    y[k] = mesh->vertices()[k][1];
  mesh->create_vertex_field( "y" , y );

  // create a vertex field that has the z value of every vertex
  std::vector<double> z( mesh->vertices().nb() );
  for (int k = 0; k < mesh->vertices().nb(); k++)
    z[k] = mesh->vertices()[k][2];
  mesh->create_vertex_field( "z" , z );

  // create a cell field that holds the element number
  std::vector<double> number( mesh->nb() );
  for (int k = 0; k < mesh->nb(); k++)
    number[k] = k;
  mesh->create_cell_field( "number" , number );

  Viewer viewer;
  viewer.add( *mesh.get() );
  viewer.run();

}
UT_TEST_CASE_END(cell_field_test)

UT_TEST_SUITE_END(webgl_test_suite)
