/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
 
#include "tester.hpp"

#include "element.h"

using namespace flux;

UT_TEST_SUITE( element_test_suite )

UT_TEST_CASE( random_barycentric_test )
{
  double tol = 1e-8;
  int ntests = 100;

  std::vector<double> alpha;
  for (int i = 0; i < ntests; i++) {

    random_barycentric<Triangle>(alpha);

    UT_ASSERT_NEAR( (alpha[0] + alpha[1] + alpha[2]) , 1.0 , tol );

    UT_ASSERT( alpha[0] >= 0.0 );
    UT_ASSERT( alpha[0] <= 1.0 );

    UT_ASSERT( alpha[1] >= 0.0 );
    UT_ASSERT( alpha[1] <= 1.0 );

    UT_ASSERT( alpha[2] >= 0.0 );
    UT_ASSERT( alpha[2] <= 1.0 );

  }

}
UT_TEST_CASE_END( random_barycentric_test )

UT_TEST_SUITE_END( element_test_suite )
