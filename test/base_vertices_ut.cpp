/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/

#include "tester.hpp"

#include "vertices.h"

#include <random>

using namespace flux;

UT_TEST_SUITE( vertices_test_suite )

UT_TEST_CASE( test1 )
{
  double tol = 1e-12;

  Vertices vertices(3);
  UT_ASSERT_EQUALS( vertices.dim() , 3 );

  double x0[3] = {0,0,0};
  double x1[3] = {1,0,0};
  double x2[3] = {0,1,0};

  UT_ASSERT_EQUALS( vertices.nb() , 0 );

  vertices.add(x0);
  vertices.add(x1);
  vertices.add(x2);

  UT_ASSERT_EQUALS( vertices.nb() , 3 );
  UT_ASSERT_EQUALS( vertices.normals().nb() , 0 );
  UT_ASSERT_EQUALS( vertices.uv().nb() , 0 );

  double u0[2] = {0,0};
  double u1[2] = {1,0};
  double u2[2] = {0,1};
  vertices.uv().add( u0 );
  vertices.uv().add( u1 );
  vertices.uv().add( u2 );
  UT_ASSERT_EQUALS( vertices.uv().nb() , 3 );

  Vertices vertices2(3);
  vertices2.copy(vertices);
  UT_ASSERT_EQUALS( vertices2.nb() , vertices.nb() );
  for (int k = 0; k < vertices2.nb(); k++) {
    for (int d = 0; d  < vertices2.dim(); d++)
      UT_ASSERT_NEAR( vertices(k,d) , vertices2(k,d) , tol );
  }

  vertices.clear();
  UT_ASSERT_EQUALS( vertices.dim() , 3 );
  UT_ASSERT_EQUALS( vertices.nb() , 0 );
  UT_ASSERT_EQUALS( vertices.uv().nb() , 0 );


  // create vertices to test the removal (via array2d)
  double* x[3] = {x0,x1,x2};
  for (int k = 0; k < 100; k++) {
    // pick one of x0, x1 or x2
    // (it doesn't really matter what the coordinates are for this test)
    vertices.add( x[ k % 3 ] );
  }

  // now remove
  std::random_device rd;
  std::mt19937 rng(rd());
  int nb_vertices = vertices.nb();
  for (int k = 0; k < nb_vertices; k++) {
    // pick a random integer within [0,current # vertices-1]
    std::uniform_int_distribution<int> uni(0,vertices.nb()-1);
    int idx = uni(rng);
    printf("removing vertex %d\n",idx);
    vertices.remove(idx);
    UT_ASSERT_EQUALS( vertices.nb() , nb_vertices-k-1 );
    UT_ASSERT_EQUALS( vertices.data().size() , (nb_vertices-k-1) * 3 );
  }
  UT_ASSERT_EQUALS( vertices.nb() , 0 );

}
UT_TEST_CASE_END( test1 )

UT_TEST_SUITE_END( vertices_test_suite )
