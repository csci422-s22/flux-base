#include "element.h"
#include "grid.h"
#include "halfedges.h"
#include "webgl.h"

#include <cstdio>

using namespace flux;

/**
 * \brief Flips (swaps) an edge (p,q), i.e. connecting it to the left and right
 *        vertices of the adjacent triangles instead of (p,q).
 *        Assumes all faces are triangles and that the overall mesh is closed (no boundary).
 *        This means all left and right faces are non-null.
 */
void
flip( HalfEdge* edge ) {

  HalfVertex* p = edge->vertex;
  HalfVertex* q = edge->twin->vertex;

  flux_assert( p != q );

  HalfVertex* vL = edge->next->next->vertex;
  HalfVertex* vR = edge->twin->next->next->vertex;

  HalfEdge* twin = edge->twin;

  // get the faces adjacent to this edge
  HalfFace* fL = edge->face;
  HalfFace* fR = twin->face;

  // we are assuming a closed mesh
  flux_assert( fL != nullptr );
  flux_assert( fR != nullptr );

  // get the lower edges
  HalfEdge* eLR = twin->next;
  HalfEdge* eLL = edge->next->next;

  // get the upper edges
  HalfEdge* eUR = twin->next->next;
  HalfEdge* eUL = edge->next;

  // top
  edge->next = eUR;
  edge->vertex = vL;
  edge->face = fL;
  edge->twin = twin;

  eUR->next = eUL;
  eUR->vertex = vR;
  eUR->face = fL;
  //eUR->twin (same)

  eUL->next = edge;
  eUL->vertex = q;
  eUL->face = fL;
  //eUL->twin (same)

  // bottom
  twin->next = eLL;
  twin->vertex = vR;
  twin->face = fR;
  twin->twin = edge;

  eLL->next = eLR;
  eLL->vertex = vL;
  eLL->face = fR;
  //eLL->twin (same)

  eLR->next = twin;
  eLR->vertex = p;
  eLR->face = fR;
  //eLR->twin (same)

  // vertices
  vL->edge = edge;
  vR->edge = eUR;
  p->edge = eLR;
  q->edge = eUL;

  // faces
  fL->edge = edge;
  fR->edge = twin;
}

int
main( int argc, char** argv ) {

  Grid<Triangle> input( {3,3} );

  int p = 5;
  int q = 10;

  HalfEdgeMesh<Triangle> half(input);

  HalfEdge *edge = nullptr;
  for (auto& e : half.edges()) {
    if (e->face == nullptr) continue;
    if (e->vertex->index == p && e->twin->vertex->index == q) {
      edge = e.get();
      break;
    }
  }
  flux_assert( edge != nullptr );

  flip(edge);

  Viewer viewer;
  viewer.add( input );

  Mesh<Triangle> output( input.vertices().dim() );
  half.extract(output);
  viewer.add( output );

  viewer.run();

  return 0;
}
