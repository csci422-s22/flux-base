#include "element.h"
#include "grid.h"
#include "halfedges.h"
#include "webgl.h"

#include <cstdio>

using namespace flux;

/**
 * \brief Flips (swaps) an edge (p,q), i.e. connecting it to the left and right
 *        vertices of the adjacent triangles instead of (p,q).
 *        Assumes all faces are triangles and that the overall mesh is closed (no boundary).
 *        This means all left and right faces are non-null.
 */
void
flip( HalfEdge* edge ) {
  flux_implement;
}

int
main( int argc, char** argv ) {

  Grid<Triangle> input( {3,3} , 3 );

  // we will flip edge (5,10)
  int p = 5;
  int q = 10;

  HalfEdgeMesh<Triangle> half(input);

  // first find this edge in the mesh
  HalfEdge *edge = nullptr;
  for (auto& e : half.edges()) {
    if (e->face == nullptr) continue;
    // check if the indices of the endpoint vertices are p and q

  }
  flux_assert( edge != nullptr );

  // flip the edge
  flip(edge);

  // extract the modified mesh from the halfedge representation
  Mesh<Triangle> output( input.vertices().dim() );
  half.extract(output);

  // add the original and modified meshes to the viewer
  Viewer viewer;
  viewer.add( input );
  viewer.add( output );

  viewer.run();

  return 0;
}
