### lecture 14: remeshing

#### objectives:

By the end of this lecture, you will be able to:

- implement a remeshing algorithm to achieve a target element size and quality by using splits, collapses, swaps and smoothing,
- practice implementing a local mesh modification operator using a half-edge mesh representation.

#### remeshing

The goal of remeshing is to improve the overal quality of a mesh in order to use it for simulations, visualization or other applications downstream of the modeling process.

There are several techniques for remeshing, which can be roughly divided into two categories. The first category completely regenerates a new mesh, and may or may not require an input mesh to get started. If you think back to our lecture on Delaunay triangulations, this would be a technique which completely regenerates a mesh from an input set of points (no input mesh is required). The second technique starts with a valid initial mesh and maintains a valid mesh throughout the remeshing procedure. This has the advantage of being very robust - if, at any point in the remeshing process, you detect that an invalid mesh would be created, then you would not perform the proposed modification operator.

#### regularity

Regularity refers to whether the vertices in a mesh have roughly the same **valency** (the number of neighboring vertices). A triangle mesh is regular if the valency of interior vertices is 6, and that of boundary vertices is 4. One goal of the remeshing procedure is to improve the regularity of the vertices. In the figure below, the left-most mesh contains several irregular vertices, whereas the middle figure is called *semiregular* because it was obtained from a subdivision of an initially coarse, regular mesh, and contains only a few irregular vertices. The right-most mesh consists of vertices which are all regular.

![](../images/regularity.png)
(Figure 6.2 from *Polygon Mesh Processing*)

#### variational techniques

Variational techniques aim to minimize some energy functional. A simple technique would be to maintain mesh connectivity, and purely optimize vertex coordinates. However, we can also optimize some functional that simultaneously provides the mesh connectivity and vertex coordinates. Recall the Voronoi diagram from Lectures 9-10, and how it is dual to the Delaunay triangulation. To obtain a regular distribution of points, we can construct a *centroidal Voronoi tessellation* (CVT), which is a Voronoi diagram in which each generating site is at the centroid (or close to it) of the corresponding Voronoi cell. In fact, the CVT minimizes the **CVT energy functional**:

```math
E(\mathbf{p}_1,\mathbf{p}_2,\dots,\mathbf{p}_n) = \sum\limits_{i=1}^n \int_{V_i} \rho(\mathbf{x})\lvert| \mathbf{x} - \mathbf{p}_i\rvert|^2 \mathrm{d}\mathbf{x}
```
where $`\mathbf{p}_i`$ are the Voronoi sites (seeds) and $`V_i`$ is the Voronoi cell associated with site $`\mathbf{p}_i`$; $`\rho(\mathbf{x})`$ is some density which charaterizes the desired size of the cells. Recall the Lloyd relaxation  algorithm for constructing a CVT::
1. Randomly initialize Voronoi sites
2. Iterate until convergence:
  a. Compute the Voronoi diagram of the current sites
  b. Compute the centroids of the Voronoi cell
  c. Move the sites to the centroids

This algorithm has the property that the CVT energy monotonically decreases at every iteration. This iteration sequence is shown from left-to-right in the figure below.

![](../images/lloyd-relaxation.png)
(Figure 6.8 from *Polygon Mesh Processing*)

#### incremental remeshing

A simpler remeshing technique consists of locally modifying the mesh to achieve some desired target criteria. These target criteria need not be a single objective and may be driven by mesh edge lengths, triangle quality or vertex valency. To improve the mesh with respect to the target criteria, we can use *local operators*, including edge collapses (as we saw with simplification), edge splitting, edge flipping (or swapping) and vertex smoothing. This remeshing procedure has the advantage of always maintaining a valid mesh, thereby providing a *robust* algorithm. The local operators are sketched below.

![](../images/local-operators.png)
(Figure 6.12 from *Polygon Mesh Processing*)

#### overall `remesh` algorithm

Given some initial surface mesh, the `remesh` algorithm takes in a single parameter `target_edge_length` which is the desired edge length for all edges in the mesh. In general it is not possible to exactly achieve the target edge length, so we will strive to create edges that are within some range `[low,high]`. The overall `remesh` algorithm is outlined below - each procedure is further described in the following subsections:

```c++
remesh( target_edge_length )
  low = (4/5) * target_edge_length
  hi  = (4/3) * target_edge_length
  for i = 0 to nb_iter do
    split_long_edges(high)
    collapse_short_edges(low,high)
    equalize_valences()
    tangential_relaxation()
    project_to_surface()
```

Using 5-10 iterations (`nb_iter`) is usually good enough. You don't have to order the subprocedures in this way, for example you might want to switch the order of `equalize_valences` with `tangential_relaxation`, and you can also interleave additional relaxation steps in between the split and collapse procedures. This algorithm is very heuristic, so feel free to experiment!

##### `split_long_edges` algorithm

The `split_long_edges` algorithm continues to visit all long edges in the current mesh and introduces a new vertex at the midpoint of a long edge, while updating the connectivity of the mesh (i.e. updating the half-edge data structures). The algorithm is outlined as follows:

```c++
split_long_edges(high)
  while exists edge e with length(e) > high do
    split e at midpoint(e)
```

I would recommend maintaining a queue of edges (via `std::queue`) which contains all edges that have a length greater than the target. When performing a split, remember to re-evaluate the edge lengths after the split is performed and add any newly introduced edges (that are still longer than `high`) to the queue. I would also recommend only maintaining the unique edges in the queue - i.e. if `edge` is too long, add it to the queue, but don't add `edge->twin`.


##### `collapse_short_edges` algorithm

Since the `collapse_short_edges` algorithm follows the `split_long_edges`, we would ideally like to collapse edges without recreating additional long edges to split later on. This is why this algorithm takes in both the `low` and the `high`. When checking if an edge can be collapsed, you should check the one-ring of the removed vertex and see if the edge between a vertex in this one-ring with the receiving vertex would create a long edge.

```c++
collapse_short_edges(low,high)
  while exists edge e with length(e) < low do

    // we will collapse p onto q
    e = (p,q)

    // retrieve the one-ring of p
    ring = one_ring(p)

    collapse_ok = true
    for v in ring do // loop through the one-ring of p
      if length(p,v) > high
        collapse_ok = false

    if collapse_ok
      collapse p onto q along e
```
When we studied mesh simplification, we collapsed an edge onto the optimal coordinates $`\bar{\mathbf{v}}`$. In this case, you can just collapse the edge onto the receiving vertex `q`. The implementation of the collapse operator is otherwise the same as the one we saw with mesh simplification.

You can maintain a priority queue of edges that need to be collapsed (those shorter than `low`) and remember to update this set whenever an edge is collapsed, since there might still be some short edges in the vicinity of the collapse.

##### `equalize_valences` algorithm

The goal of the `equalize_valences` algorithm is to perform edge flips (a.k.a edge swaps) between two triangles to equalize the valences of the vertices around an edge. Ideally, an internal mesh vertex will have a valency of `6` and a boundary vertex will have a valency of `4`. The criteria we will improve will be called the `deviation`. Here is the algorithm:

```c++
equalize_valences()
  for each edge e do
    let a, b, c, d be the vertices of the two triangles adjacent to e

    deviation_pre = abs( valence(a) - target_val(a) )
                  + abs( valence(b) - target_val(b) )
                  + abs( valence(c) - target_val(c) )
                  + abs( valence(d) - target_val(d) )

    // attempt the flip
    flip(e)

    deviation_post = abs( valence(a) - target_val(a) )
                   + abs( valence(b) - target_val(b) )
                   + abs( valence(c) - target_val(c) )
                   + abs( valence(d) - target_val(d) )

   if deviation_pre <= deviation_post
    flip(e) // flip back since the deviation did not improve
```

You don't necessarily need to flip the edge and then flip back since this incurs some unnecessary computation. You can instead calculate `deviation_post` analytically from `deviation_pre` to determine if the flip should be performed. Here, `target_val` returns the target valency of a vertex (equal to 6 for interior vertices and 4 for boundary vertices), and `valence` is a function that returns the number of vertices in the one-ring of a vertex.

##### `tangential_relaxation` algorithm

The `tangential_relaxation` function is really a vertex smoothing algorithm, which constrains vertex movement to the tangent plane of the surface at the vertex. As in *Polygon Mesh Processing*, let $`\mathbf{p}`$ be the coordinates of the vertex we would like to smooth, and let $`\mathbf{n}`$ be the normal at $`\mathbf{p}`$ (perhaps, obtained by averaging the face normals surrounding the vertex). Let $`\mathbf{q}`$ be the new position of the vertex calculated as the centroid of the one-ring of the vertex:
```math
\mathbf{q} = \frac{1}{\mathcal{N}(\mathbf{p})} \sum\limits_{\mathbf{p}_j \in \mathcal{N}(\mathbf{p})} \mathbf{p}_j
```
where $`\mathcal{N}(\mathbf{p})`$ is the one-ring of $`\mathbf{p}`$. The new position $`\mathbf{p}^\prime`$ can be computed by projecting $`\mathbf{q}`$ onto the tangent plane at $`\mathbf{p}`$:
```math
  \mathbf{p}^{\prime} = \mathbf{q} + ( \mathbf{n} \cdot (\mathbf{p} - \mathbf{q}) )\mathbf{n}.
```

Ideally, you would compute the updated coordinates (the $`\mathbf{p}^\prime`$s) for every vertex and store them in a separate array, and then do a final update of all the coordinates. However, you can also update the coordinates as you compute them, if you want.

##### `project_to_surface` algorithm

The details of the `project_to_surface` function are omitted here for brevity. Ultimately, this function projects a point to the original surface (i.e. finds the closest point on the original surface mesh to some query point). There are a few ways this can be done:

1. The simplest way is to use a kd-tree of the input surface points and maintain the vertex-to-element information of the input mesh. For a given query point $`\mathbf{x}`$, the nearest neighbor is obtained from the pre-computed kd-tree. Then we loop through the elements in the input mesh surrounding this nearest vertex, and the query point is projected to each triangle - the closest projection point is retained. This is very efficient, requiring $`\mathcal{O}(\log n)`$ to find the nearest vertex, and $`\mathcal{O}(1)`$ to loop through the one-ring of the nearest vertex. However, it does not guarantee that the closest triangle is used for the projection. Here is a nice library you can use to compute kd-trees of point clouds: [https://github.com/jlblancoc/nanoflann](https://github.com/jlblancoc/nanoflann).

2. Another option is to maintain a kd-tree which divides the input triangles (instead of the input vertices). In this case, the closest triangle will be returned and the projection can be computed. If you chose this method (in your final project), there is a triangle kd-tree implementation in the `pmp-library` (in `src/pmp/algorithms/TriangleKdTree.*`) which you may want to adapt to use in `flux` (citing the authors of the code). Here is a link to the `pmp-library` which accompanies the *Polygon Mesh Processing* book: [https://github.com/pmp-library/pmp-library](https://github.com/pmp-library/pmp-library).

Here are some results of implementing the aforementioned remeshing algorithm in `flux`. The input mesh (left) came from the surface reconstruction algorithm (from last week) - after 5 iterations, the remesher did a good job of regularizing the mesh (right).

![](../images/sphere-remesh0.png)![](../images/sphere-remesh1.png)

### alternative remeshing algorithms

In your final project, you may want to remesh a 2d domain instead of a 3d surface (still using triangles). This might be simpler to implement because it doesn't require a projection to a surface - all newly introduced vertices and smoothed vertices will directly be in the 2d mesh. However, you will need to be careful with boundaries. I would also recommend driving the remeshing procedure from a *sizing field*. This means that edge lengths should now be computed with respect to this sizing field. Suppose $`h(\mathbf{x})`$ represents the desired length at a point in the domain $`\mathbf{x}`$. To determine if an edge (with a length of $`\ell`$) should be collapsed, you should check if
```math
\frac{\ell}{h(\mathbf{x})} < 4/5.
```

Similarly, to determine if an edge with length $`\ell`$ should be split, you can check if
```math
\frac{\ell}{h(\mathbf{x})} > 4/3.
```
Since $`h(\mathbf{x})`$ is defined continuously throughout the domain, you can take the average of $`h(\mathbf{x}_1)`$ and $`h(\mathbf{x}_2)`$ in these calculations (other interpolation schemes are possible), where $`\mathbf{x}_1`$ and $`\mathbf{x}_2`$ are the endpoint coordinates of the edge. You can also use different threshold values, e.g. I often use $`\sqrt{2}`$ as the upper bound and $`\sqrt{2}/2`$ as the lower bound. Here is an example in which the desired mesh size $`h(\mathbf{x})`$ was inferred from a picture of my family's dog:

![](../images/ursa.png)

It is also possible to prescribe an anisotropic sizing field, in which the sizing and orientation is prescribed by a *metric tensor* $`\mathbf{m}(\mathbf{x})`$ throughout the domain. In other words, by prescribing a field of 2x2 symmetric, positive definite matrices. The length of an edge $`e`$ under a metric $`\mathbf{m}`$ is
```math
\ell_{\mathbf{m}}(e) = \sqrt{ (\mathbf{x}_2 - \mathbf{x}_1)^T \mathbf{m} (\mathbf{x}_2 - \mathbf{x}_1) }.
```
The `syms` and `symd` classes can be used to represent these symmetric matrices in `flux`. Note that the eigenvalues of $`\mathbf{m}`$ are $`1/h_1^2`$ and $`1/h_2^2`$ where $`h_1`$ and $`h_2`$ are the requested sizes in the directions of the two eigenvectors of $`\mathbf{m}`$. These metric tensors can be visualized as ellipses - an example of an anisotropic mesh with a prescribed metric field is shown below.

<img src="../images/rae2822_metric.png" width=400>
<img src="../images/rae2822_mesh.png" width=400>

#### exercises and unit tests

Today, we will implement the flip operator together. I will then add (after class) this `flip` function to our `HalfEdgeMesh` class, which you will be responsible for unit testing.
