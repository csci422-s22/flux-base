### lecture 10: More Voronoi diagrams

#### objectives

- extract Voronoi diagrams from Delaunay triangulations,
- interpolate values defined at vertices within triangles using barycentric coordinates,
- create a more uniform Voronoi diagram (and Delaunay triangulation) using Lloyd relaxation.

#### relationship to Delaunay triangulations

Let's first recall the relationship between Delaunay triangulations and Voronoi diagrams.

The center of the circumcircles of each Delaunay triangle is a Voronoi vertex (a vertex that bounds several Voronoi cells). There is an edge between Voronoi vertices (an edge between two Voronoi cells) if two Delaunay triangles share a face (an edge in 2d or a common triangle in 3d).

This gives us a way to indirectly construct Voronoi diagrams from a Delaunay triangulation. Similarly, each Voronoi vertex defines a Delaunay simplex. In fact, the number of Voronoi polygons sharing a vertex should be 3, which defines the three vertices in the Delaunay triangle. This duality is exhibited below - the dashed blue polygon represents a Voronoi cell for the seed $\mathbf{p}_5$.

![](../images/voronoi-1.svg) ![](../images/voronoi-2.svg)

#### calculating the circmucenters and creating a polygon mesh

Let's now write some code to convert a Delaunay triangulation to a Voronoi diagram. Ultimately, we need to (1) compute circumcenters of each triangle and (2) determine the vertices (in CCW order) of each Voronoi polytope.

There are several ways we can compute the circumcenter of a triangle. One approach consists of computing the coordinates of the circumcenter in the **barycentric coordinate system** of each Delaunay triangle.

##### barycentric coordinates
Barycentric coordinates, also called area coordinates, allow us to interpolate values defined at vertices into a value $`c`$ at a point $`\mathbf{p}`$ in a triangle:

```math
c(\mathbf{p}) = \alpha c_1 + \beta c_2 + \gamma c_3
```
The weights ($`\alpha,\ \beta,\ \gamma`$) used in the interpolation are the ratio of the areas of the triangles in the following figure to the full area of the triangle:

![](../images/barycentric.svg)

In other words:

```math
\alpha = \frac{\Delta(\mathbf{p},\mathbf{p}_2,\mathbf{p}_3)}{\Delta(\mathbf{p}_1,\mathbf{p}_2,\mathbf{p}_3)} \\
\beta = \frac{\Delta(\mathbf{p}_1,\mathbf{p},\mathbf{p}_3)}{\Delta(\mathbf{p}_1,\mathbf{p}_2,\mathbf{p}_3)} \\
\gamma = \frac{\Delta(\mathbf{p}_1,\mathbf{p}_2,\mathbf{p})}{\Delta(\mathbf{p}_1,\mathbf{p}_2,\mathbf{p}_3)}
```

Barycentric coordinates have the property that $\alpha + \beta + \gamma = 1$. A point is inside the triangle if $`0 \le \alpha,\beta,\gamma \le 1`$. For a physical interpretation (involving masses), please see [Wikipedia's description of barycentric coordinates](https://en.wikipedia.org/wiki/Barycentric_coordinate_system).

##### calculating the circumcenter

Back to Voronoi diagrams! Given a Delaunay triangle with vertex coordinates $`\mathbf{p}_1,\ \mathbf{p}_2,\ \mathbf{p}_3`$, we can first compute the lengths of the edges of the triangle:
```math
\ell_a = \lvert| \mathbf{p}_3 - \mathbf{p}_2\rvert| \\
\ell_b = \lvert| \mathbf{p}_3 - \mathbf{p}_1\rvert| \\
\ell_c = \lvert| \mathbf{p}_2 - \mathbf{p}_1\rvert|
```
The barcyentric coordinates of the triangle circumcenter (a Voronoi vertex) is
```math
A = \ell_a^2 ( -\ell_a^2 + \ell_b^2 + \ell_c^2 ) \\
B = \ell_b^2 (  \phantom{-}\ell_a^2 - \ell_b^2 + \ell_c^2 ) \\
C = \ell_c^2 ( \phantom{-} \ell_a^2 + \ell_b^2 - \ell_c^2 )
```
The full area of the triangle is $A+B+C$. Thus the barycentric coordinates are $`\alpha = A/(A+B+C),\ \beta = B/(A+B+C),\ \gamma = C/(A+B+C)`$.

By the definition of barycentric coordinates, the physical coordinates of the triangle circumcenter $`\mathbf{p}_c`$ are then:
```math
\mathbf{p}_c = \alpha \mathbf{p}_1 + \beta \mathbf{p}_2 + \gamma \mathbf{p}_3.
```

For further information (and alternatives on how to compute a triangle circumcenter), please see [here](https://en.wikipedia.org/wiki/Circumscribed_circle#Barycentric_coordinates).

##### calculating the polygon connectivity

To determine the polygon connectivity, we can build up a halfedge representation of the original Delaunay triangulation and then loop through all the vertices, extract the faces surrounding this vertex (the one-ring). The index of these faces (via `face->index`) defines the index of a Voronoi vertex that describes the polygon. Recall that our algorithm for extracting the one-ring provides the vertices, edges or faces in CW order, so we will need to reverse this order (using `std::reverse`) to define the polygon in CCW order. As an exercise, you can also try extracting the one-ring of faces directly in CCW order by traversing the one-ring via `edge->next-next->twin` until returning to the starting edge.

##### suggestion: skip creating polygons touching a Voronoi vertex (a Delaunay circumcenter) that is outside the domain

If there are really thin triangles near the domain boundary, it's possible the circumcenters will lie really far away from the domain. Including these circumcenters as vertices in our Voronoi diagram will make it hard to visualize the Voronoi diagram. Therefore, after you compute the circumcenter, I would recommend checking whether the circumcenter is within the domain. In the exericses we will do in class, our domain is $`[0,1]^2`$, so you can check whether each circumcenter coordinate is within $`[0,1]`$. If not, flag this Voronoi vertex as `outside`. Then, when you are building up the polygons, skip creating a polygon that touches an `outside` vertex.

#### smoothing the sites: Lloyd relaxation

Now that we can compute the geometry of a Voronoi cell, we can smooth the Delaunay vertices (the Voronoi sites), using an algorithm called Lloyd relaxation. This algorithm consists of iteratively moving the Delaunay vertices / Voronoi sites (**not the Voronoi vertices**) to the centroid of the generated Voronoi cell. The steps for smoothing vertices would be:

```
1. initialize the Delaunay vertices (a.k.a. Voronoi sites), e.g. randomly
2. for some number of iterations:
  a. recompute the Delaunay triangulation of the current Delaunay vertices,
  b. compute the Voronoi diagram as the dual of the Delaunay triangulation,
  c. for each Voronoi cell:
    i. if this cell touches an "outside" vertex (see previous section), don't move the generating site,
    ii. compute the centroid of this cell,
    iii. move the site to the centroid.
```

In your second project, I would recommend using 10 iterations (`Step 2`). I would also recommend fixing sites that contain an "outside" vertex (`Step 2.c.i`). The following figures illustrate the smoothing process. Here is the Voronoi diagram at the onset of the iterations (top left), after 1 iterations (top right), and after 5 (bottom left) and 10 (bottom right) iterations of Lloyd relaxation. Note that some cells are omitted because they touch a Voronoi vertex which was tagged as `outside` (and the corresponding Delaunay vertex was fixed during that iteration).

![](../images/voronoi0.png)![](../images/voronoi1.png)
![](../images/voronoi5.png)![](../images/voronoi10.png)

When computing the centroid, you can simply compute the average of the polygon vertex coordinates, if you want. If you want to compute the true centroid, you can use the formulas [here](https://en.wikipedia.org/wiki/Centroid#Of_a_polygon).

#### algorithms for directly constructing Voronoi diagrams

Instead of dualizing a Delaunay triangulation, there are algorithms for directly constructing Voronoi diagrams:

- **Fortune's sweepline algorithm:** an $\mathcal{O}(n\log n)$ algorithm for constructing the Voronoi diagram by maintaining a sweep line (straight) and a beach line (a piecewise set of parabolas) to incrementally construct the Voronoi cells to the left of the sweep line. See the figure below.

![](../images/fortune.gif)

- **Halfspace intersections:** Constructs each Voronoi cell by clipping an initial domain against the Voronoi bisectors (the lines between Voronoi seeds). If this domain is a triangle mesh (as in the example of a sphere mesh below), then we can initiate clipping with each triangle and march into adjacent triangles until no more clipping is needed. This procedure can be very efficient if the *radius of security theorem* is used (Levy, 2012), and can also be parallelized over every Voronoi seed. **This would make for an excellent final project!!**

![](../images/sphere-rvd.png)

#### why are Voronoi diagrams useful?

Voronoi diagrams appear naturally in the patterns you see on a giraffe, in the vein structures of leaves, drying soil and in the wings of dragonflies (to name a few). Their generalization, called *power diagrams* (essentially a Voronoi diagram with **weights** on the seeds) are also increasingly popular in computer graphics for blue-noise sampling and even in for simulating physical phenomena, like fracture mechanics and fluids.

#### unit testing ideas

Nothing to unit test today!
