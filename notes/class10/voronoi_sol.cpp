#include "element.h"
#include "grid.h"
#include "halfedges.h"
#include "mesh.h"
#include "readwrite.h"
#include "vec.hpp"
#include "webgl.h"

#include <algorithm>
#include <cmath>
#include <cstdio>

using namespace flux;

/**
 * \brief Computes the circumenter of a triangle.
 *
 * \param[in] points - reference to the full Points structure where coordinates are saved.
 * \param[in] triangle - pointer to the 3 indices of the triangle (into points).
 *
 * \details For example, points[t[0]] is the pointer to the coordinates for the first vertex.
 */
vec2d
get_circumcenter( const Vertices& points , const int* triangle ) {

  // see https://en.wikipedia.org/wiki/Circumscribed_circle#Barycentric_coordinates

  vec2d xa( points[ triangle[0] ] );
  vec2d xb( points[ triangle[1] ] );
  vec2d xc( points[ triangle[2] ] );

  double la = norm( xb - xc );
  double lb = norm( xa - xc );
  double lc = norm( xa - xb );

  double A = la*la*( -la*la + lb*lb + lc*lc );
  double B = lb*lb*(  la*la - lb*lb + lc*lc );
  double C = lc*lc*(  la*la + lb*lb - lc*lc );

  double s = A + B + C;
  flux_assert( s > 0.0 );

  double alpha = A/s;
  double beta = B/s;
  double gamma = C/s;

  vec2d c;
  c[0] = alpha * xa(0) + beta * xb(0) + gamma * xc(0);
  c[1] = alpha * xa(1) + beta * xb(1) + gamma * xc(1);

  return c;
}

/**
 * \brief A helper class for extracting the Voronoi diagram via the dual of a Delaunay triangulation.
 *
 */
class VoronoiDual2d : public Mesh<Polygon> {

public:
  /**
   * \brief Initializes (but does not construct) the Voronoi diagram from an input triangulation.
   *
   * \param[in] mesh - the Delaunay triangulation
   */
  VoronoiDual2d( const Mesh<Triangle>& mesh ) :
    Mesh<Polygon>(2),
    mesh_(mesh)
  {}

  /**
   * \brief Extracts the dual of the Delaunay triangulation, while limiting Voronoi vertices to be within the domain.
   *
   * \param[in] limit - Voronoi vertex coordinates will be restricted to [-limit,1+limit]^2
   *
   */
  void extract(double limit) {

    // compute the circumcenter of each Delaunay triangle
    std::vector<bool> outside( mesh_.nb() , false );
    for (int k = 0; k < mesh_.nb(); k++) {

      // calculate the circumcenter of this element
      vec2d c = get_circumcenter( mesh_.vertices() , mesh_[k] );

      // limit the circumcenter
      for (int i = 0; i < 2; i++) {
        if (c[i] <  -limit) c[i] =  -limit, outside[k] = true;
        if (c[i] > 1+limit) c[i] = 1+limit, outside[k] = true;
      }

      // add to vertices
      vertices_.add(&c[0]);
    }

    // use the half edge onerings to go in CW order
    HalfEdgeMesh<Triangle> halfedges(mesh_);
    std::vector<double> site;
    for (auto& v : halfedges.vertices()) {

      std::vector<HalfFace*> faces;
      halfedges.get_onering( v.get() , faces );

      std::vector<int> polygon( faces.size() );
      bool skip = false;
      for (int j = 0; j < faces.size(); j++) {
        polygon[j] = faces[j]->index;
        if (outside[polygon[j]]) skip = true;
      }

      // skip polygons that touch a vertex out of bounds
      if (skip) continue;

      // the half faces are ordered clockwise, so reverse the order
      std::reverse( polygon.begin() , polygon.end() );

      // add the polygon to the Voronoi diagram
      add( polygon.data() , polygon.size() );
      site.push_back( nb() );
    }

    create_cell_field("site",site);
  }

private:
  const Mesh<Triangle>& mesh_; // reference to the Delaunay triangulation
};

int
main( int argc, char** argv ) {

  std::shared_ptr<MeshBase> mesh_ptr = read_obj( "../data/delaunay1k.obj" , false );
  Mesh<Triangle>& mesh = *static_cast<Mesh<Triangle>*>(mesh_ptr.get());

  VoronoiDual2d voronoi(mesh);
  voronoi.extract(5.0);

  Viewer viewer;
  viewer.add(voronoi);
  viewer.run();

  return 0;
}
