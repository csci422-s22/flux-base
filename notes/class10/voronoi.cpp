#include "element.h"
#include "grid.h"
#include "halfedges.h"
#include "mesh.h"
#include "readwrite.h"
#include "vec.hpp"
#include "webgl.h"

#include <algorithm>
#include <cmath>
#include <cstdio>

using namespace flux;

/**
 * \brief Computes the circumenter of a triangle.
 *
 * \param[in] points - reference to the full Points structure where coordinates are saved.
 * \param[in] triangle - pointer to the 3 indices of the triangle (referencing vertex coordinates stored in points).
 *
 * \details For example, points[t[0]] is the pointer to the coordinates for the first vertex.
 */
vec2d
get_circumcenter( const Vertices& points , const int* triangle ) {

  // extract the three coordinates of the triangle vertices
  vec2d xa( points[ triangle[0] ] );
  vec2d xb( points[ triangle[1] ] );
  vec2d xc( points[ triangle[2] ] );

  // exercise 1: calculate the circumcenter
  vec2d c;
  flux_implement;

  return c;
}

/**
 * \brief A helper class for extracting the Voronoi diagram via the dual of a Delaunay triangulation.
 *
 */
class VoronoiDual2d : public Mesh<Polygon> {

public:
  /**
   * \brief Initializes (but does not construct) the Voronoi diagram from an input triangulation.
   *
   * \param[in] mesh - the Delaunay triangulation
   */
  VoronoiDual2d( const Mesh<Triangle>& mesh ) :
    Mesh<Polygon>(2), // the dimension of the vertices will be in 2d
    mesh_(mesh)
  {}

  /**
   * \brief Extracts the dual of the Delaunay triangulation, while limiting Voronoi vertices to be within the domain.
   *
   * \param[in] limit - Voronoi vertex coordinates will be restricted to [-limit,1+limit]^2
   *
   */
  void extract(double limit) {

    // compute the circumcenter of each Delaunay triangle and store as a vertex in this polygon mesh
    std::vector<bool> outside( mesh_.nb() , false ); // keep track whether a circumcenter is outside the domain
    for (int k = 0; k < mesh_.nb(); k++) {

      // calculate the circumcenter of this element
      vec2d c = get_circumcenter( mesh_.vertices() , mesh_[k] );

      // exercise 2: add Voronoi vertices to the polygon mesh
      flux_implement;

      // limit the circumcenter to be within [-limit,1+limit]^2

      // add the Voronoi vertex to the mesh
    }

    // exercise 3: create polygons (optional: create a cell field)
    flux_implement;

  }

private:
  const Mesh<Triangle>& mesh_; // reference to the Delaunay triangulation
};

int
main( int argc, char** argv ) {

  // read the delaunay mesh
  std::shared_ptr<MeshBase> mesh_ptr = read_obj( "../data/delaunay1k.obj" , false );
  Mesh<Triangle>& mesh = *static_cast<Mesh<Triangle>*>(mesh_ptr.get());

  // extract the dual
  VoronoiDual2d voronoi(mesh);
  voronoi.extract(0.0);

  // plot the voronoi diagram
  Viewer viewer;
  viewer.add(voronoi);
  viewer.run();

  return 0;
}
