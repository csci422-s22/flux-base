#include "element.h"
#include "grid.h"
#include "vec.h"
#include "webgl.h"

#include <cstdio>
#include <map>
#include <memory>
#include <set>

using namespace flux;

struct HalfEdge;

/**
 * \brief Represents a face in a half-edge based mesh.
 */
struct HalfFace {
  HalfEdge* edge; // pointer to one edge which has the face on the left
  int index = -1; // index of the face (i.e. element number)
};

/**
 * \brief Represents a vertex in a half-edge based mesh.
 */
struct HalfVertex {
  /**
   * \brief Constructs a half vertex from coordinates.
   *
   * \param[in] dim number of coordinates to copy (allows copying 2d or 3d coordiantes)
   * \param[in] x - pointer to coordinates (size = dim)
   */
  HalfVertex( int dim , const double* x ) {
    flux_assert( dim == 2 || dim == 3 );
    for (int d = 0; d < dim; d++)
      point[d] = x[d];
  }
  HalfEdge* edge; // pointer to one edge emanating from this vertex
  vec3d point;    // the coordinates of this vertex (always 3d!)
  int index = -1; // index of this vertex (i.e. vertex number)
};

/**
 * \brief Represents an edge in a half-edge based mesh.
 */
struct HalfEdge {
  HalfVertex* vertex; // pointer to the 'origin' vertex of this edge
  HalfEdge*   twin;   // pointer to the 'opposite' edge (i.e. parallel but in opposite direction)
  HalfEdge*   next;   // pointer to the next edge around the face in CCW order
  HalfEdge*   prev;   // pointer to the previous edge around the face in CCW order
  HalfFace*   face;   // pointer to the left face of this oriented edge
};

/**
 * \brief Container for half-edge entities (vertices, edges, faces)
 *        Can be used for Triangle or Quad meshes.
 */
template<typename type>
class HalfEdgeMesh {
public:

  /**
   * \brief Constructs all the half-edge entities from an incoming connectivity-based representation.
   *
   * \param[in] the mesh represented by vertices and element connectivity.
   */
  HalfEdgeMesh( const flux::Mesh<type>& mesh ) :
    mesh_(mesh) {
    build();
  }

  /**
   * \brief Returns the set of HalfVertex pointers.
   */
  const std::set< std::unique_ptr<HalfVertex> >& vertices() const { return vertices_; }

  /**
   * \brief Returns the set of HalfEdge pointers.
   */
  const std::set< std::unique_ptr<HalfEdge> >& edges() const { return edges_; }

  /**
   * \brief Returns the set of HalfFace pointers.
   */
  const std::set< std::unique_ptr<HalfFace> >& faces() const { return faces_; }

private:
  /**
   * \brief Builds all the half-edge entities from the connectivity-based mesh
   */
  void build();

  const flux::Mesh<type>& mesh_; // reference to the original mesh from which the entities were constructed

  // the half-edge vertices, edges and faces
  std::set< std::unique_ptr<HalfVertex> > vertices_;
  std::set< std::unique_ptr<HalfEdge> >   edges_;
  std::set< std::unique_ptr<HalfFace> >   faces_;
};

template<typename type>
void
HalfEdgeMesh<type>::build() {

  flux_implement;
}

/**
 * \brief Computes the vertices surrounding a vertex (v) in CW order.
 */
void
get_onering( const HalfVertex* v , std::vector<HalfVertex*>& ring ) {
  flux_implement;
}

/**
 * \brief Computes the edges surrounding a vertex (v) in CW order.
 */
void
get_onering( const HalfVertex* v , std::vector<HalfEdge*>& ring ) {
  flux_implement;
}

/**
 * \brief Computes the faces surrounding a vertex (v) in CW order.
 */
void
get_onering( const HalfVertex* v , std::vector<HalfFace*>& ring ) {
  flux_implement;
}

int
main( int argc, char** argv ) {

  Mesh<Triangle> mesh(3);

  // vertices of the cube
  double p0[3] = {0,0,0}; mesh.vertices().add( p0 );
  double p1[3] = {1,0,0}; mesh.vertices().add( p1 );
  double p2[3] = {1,1,0}; mesh.vertices().add( p2 );
  double p3[3] = {0,1,0}; mesh.vertices().add( p3 );
  double p4[3] = {0,0,1}; mesh.vertices().add( p4 );
  double p5[3] = {1,0,1}; mesh.vertices().add( p5 );
  double p6[3] = {1,1,1}; mesh.vertices().add( p6 );
  double p7[3] = {0,1,1}; mesh.vertices().add( p7 );

  // triangles of the cube
  int t0[3] = {3,1,0}; mesh.add( t0 );
  int t1[3] = {2,1,3}; mesh.add( t1 );
  int t2[3] = {1,2,6}; mesh.add( t2 );
  int t3[3] = {1,6,5}; mesh.add( t3 );
  int t4[3] = {0,1,4}; mesh.add( t4 );
  int t5[3] = {1,5,4}; mesh.add( t5 );
  int t6[3] = {2,3,7}; mesh.add( t6 );
  int t7[3] = {2,7,6}; mesh.add( t7 );
  int t8[3] = {3,0,4}; mesh.add( t8 );
  int t9[3] = {3,4,7}; mesh.add( t9 );
  int t10[3] = {4,5,6}; mesh.add( t10 );
  int t11[3] = {4,6,7}; mesh.add( t11 );

  // build up the half-edge based representation
  HalfEdgeMesh<Triangle> halfmesh( mesh );

  std::vector<HalfVertex*> vring;
  for (auto& v_ptr : halfmesh.vertices()) {
    HalfVertex* v = v_ptr.get();
    vring.clear();
    //get_onering( v , vring );
    printf("vertex %d has ring size = %lu\n",v->index,vring.size());
    for (int j = 0; j < vring.size(); j++)
      printf("\tattached to vertex %d\n",vring[j]->index);
  }

  for (auto& e_ptr : halfmesh.edges()) {
    HalfEdge* e = e_ptr.get();
    flux_assert( e->twin != nullptr );
  }

  for (auto& f_ptr : halfmesh.faces()) {
    HalfFace* f = f_ptr.get();
  }

  Viewer viewer;
  viewer.add(mesh);
  viewer.run();

  return 0;
}
