### lecture 07: half-edge data structure

#### goals
- list the components defined by a half-edge data structure,
- extract the half-edges from a mesh,
- extract the one-ring vertices, edges and faces around a vertex using half-edges.

#### why do we need a different data structure?

Imagine we need to make modifications to a mesh, each of which will be localized to a small portion of the meh (i.e. only a few elements are changed). We can certainly implement these changes with the connectivity-based operators we introduced last week, but we would have to update the underlying arrays upon every modification. Sure, we can cache empty slots in the arrays, update the values, etc., but this also creates a lot of bookkeeping work which can be prone to bugs.

Since we will primarily be working with surface meshes in our course, we can specialize data structures for our applications. Specifically, we will look at edge-based data structures and the one we will implement today is called a *half-edge data structure*.

Before getting into a half-edge based data structure, let's look at a general edge-based data structure (below), which is called a *winged-edge* data structure. Conceptually, this is similar to what we implemented in class last time by keeping track of the edge vertex indices, and which triangles (faces) each edge came from.

![](../images/edge-based.png)

The idea here is to create a single `Edge` object for each edge in the mesh. Each of these objects will then *point* to (1) endpoint vertices, (2) left & right cells and (3) the other edges of the left/right neighboring cells (you can imagine the "wings" to be these neighboring edges). In conjunction with these `Edge` objects, we will also store objects to represent a `Vertex` and a `Face`. Each `Vertex` and `Face` object would point to **one** incident `Edge` edge (note that vertices will also store coordinate information).

#### half-edge data structure

In the full winged-edge data structure we have about 32 bytes to store for each edge (8 pointers of 4 bytes each), and 4 bytes per vertex/face (we'll ignore the cost of storing the coordinates). One problem here is the amount of information we store for an edge. Not only is it quite a bit, but it's a lot of information to update when modifying the mesh (again, think bugs). Another problem, is that this data structure does not store anything about the orientation of the mesh and can make it difficult to extract information such as vertex-to-vertex connectivity, or vertex-to-edge connectivity, etc.

Instead, we'll use a half-edge based data structure. The idea here is to have two half-edges for each mesh edge, which differ in the direction they are pointing. Sure, the information is duplicated so we don't get much of a memory advantage, but the algorithms for extracting adjacency information will be simpler with these directed edges. Here's what a half-edge data structure stores:

![](../images/halfedge-based.png)

Each (directed) half-edge stores a pointer to (1) an endpoint vertex, (2) the left face, (3) the next half-edge (turning around the face CCW), (4) the "twin" (a.k.a opoosite) half-edge and [optional] (5) the previous edge (turning around the face CCW). The reason the last piece is optional is because we can always retrieve it via `edge->next->next` for a left triangle face or `edge->next->next->next` for a left quad face.

Notice in the figure that the half-edges are duplicated on each unique mesh edge, and that they point in opposite directions (and point to each other as twins/opposites).

#### building the half-edges from element connectivity

The collection of all the half-edge information will be stored in a class called `HalfEdgeMesh<type>` (templated by `type`). We could store a `set` (or even a `vector`) of all the half-edge data structures (`HalfEdge`, `HalfVertex` or `HalfFace`), but it's easier to store pointers to all of these (since some of them will point to each other). As usual, we should not use `new` to allocate memory, so we will store smart pointers (here `unique_ptr`) of these entities. These are stored in `edges_`, `vertices_` and `faces_`.

There are two basic functionalities we need to implement (1) how to convert a connectivity-based mesh representation to a half-edge based representation (`build`) and (2) how to convert a half-edge based representation to a connectivity-based representation (`extract`). I'll leave the latter as an exercise (but the solution will be available in `src/flux-base/src/halfedges.cpp`). The harder part is converting a connectivity-based mesh representation to our half-edge data structures. Here are the steps:

1. Loop through each vertex in the mesh:
  a. create a `HalfVertex` for this vertex.
  b. assign the `index` of this `HalfVertex` to the index of the vertex in the mesh.
  c. save the coordinates of the `HalfVertex`
2. Loop through the elements in the mesh:
  a. create a `HalfFace` for this element.
  b. loop through the edges of this face in CCW order:
      - create a `HalfEdge` for this edge.
      - retrieve the global indices of the endpoints of this directed edge: $`(e_0,e_1)`$.
      - retrieve the vertex at endpoint (vertex) $`e_0`$.
      - tell the created edge that its origin vertex is the vertex at $`e_0`$.
      - tell the vertex at $`e_0`$ that it's outgoing edge is the edge we created.
      - tell the edge that it's face (to the left) is the face created in step 2a.
      - look for the opposite (twin) edge by checking if the edge $`(e_1,e_0)`$ exists yet.
        - if it does, then point the created edge and the twin to each other (as twins)
        - otherwise, initialize the twin to `nullptr`
      - also set the `next` and `prev` pointers (as you circle around the element).

As an exercise, we will apply this algorithm to the mesh of a cube (which is *closed*).

#### boundary half-edges

The algorithm above works fine for closed meshes (no boundary). Some of the edges will be left with no twin, which means they are on the boundary. We could leave this twin as null, or we can create a *boundary half edge* in the opposite direction and simply set its left face to being null. This means we will have a chain of boundary edges moving clockwise (CW) around the boundary) whose left faces are all `nullptr`.

For these boundary edges, the `prev` and `next` will also be boundary edges, which will make it user-friendly to traverse this chain of boundary edges. We can construct this `prev` and `next` information for each boundary edge by starting with it's twin and then traversing to the `next->prev->twin` until the face of the current edge is not null (i.e. getting the next boundary edge in the chain). This would look like:

```c++
for (auto& it = edges_.begin(); it != edges_.end(); ++it) {

  HalfEdge* he = it->get();
  if (he->face != nullptr) continue; // not a boundary edge

  flux_assert( he->next == nullptr );

  // keep looking for another boundary edge
  HalfEdge* he_next = he->twin;
  while (he_next->face != nullptr) {
    he_next = he_next->prev->twin;
  }

  he->next      = he_next;
  he_next->prev = he;
}
```

#### exercise 1: extracting the one-ring of a vertex

Sometimes we need to know which vertices surround some other vertex. This is called the **one-ring** of the vertex. We can also have the one-ring of edges or faces that surround a vertex.

```c++
template<typename type>
void
HalfEdgeMesh<type>::get_onering( const HalfVertex* v , std::vector<HalfVertex*>& onering ) const {

  HalfEdge* first = v->edge;
  HalfEdge* edge  = first;
  do {

    // add the edge
    onering.push_back(edge->twin->vertex);

    // go to the next edge
    edge  = edge->twin->next;

  } while (edge != first);
}
```

#### exercise 2: extract the one-ring edges of a vertex

Implement a technique similar to exercise 1 for extracting a list of `HalfEdge` pointers surrounding a vertex.

#### exercise 3: extract the one-ring faces of a vertex

Implement a technique similar to exercise 1 for extracting a list of `HalfFace` pointers surrounding a vertex.

#### exercise 4: loop through the boundary of a mesh

Sometimes we'll need to know which edges are on the boundary. We can traverse this boundary by starting with some arbitrary boundary edge and traversing the linked list in CW order until we get back to the start:

```c++
template<typename type>
HalfEdge*
HalfEdgeMesh<type>::get_boundary_edge() const {

  for (auto& e_ptr = edges_.begin(); it != edges_.end(); ++it) {
    HalfEdge* e = e_ptr.get();
    if (e->face == nullptr) return e; // if the face is null, this is a boundary edge
  }
  return nullptr;  // no boundary edge
}
```

To traverse the list of boundary edges, we can then use:

```c++
HalfEdge* first = e; // some boundary edge we found from the previous algorithm
int count = 0;
do {
  count++;
  e = e->next; // go clockwise

} while (e != first);
std::cout << "there are " << count << " boundary edges" << std::endl;
```

#### unit testing ideas

We're at the point where you should really get into the habit of unit testing the code we write. Keep in mind, however, that this isn't directly the code we write in the `exercises` directory in class, since this functionality will be implemented in `flux-base`, which is the directory you need to write tests for. Here are some ideas for writing unit tests, say in `test/halfedges_ut.cpp`.

Files to unit test: `src/flux-base/src/halfedges.h` and `src/flux-base/src/halfedges.cpp`
- check that the vertex-to-vertex and vertex-to-element information extracted from the mesh connectivity matches the one-ring we get from a `HalfEdgeMesh`. For example, you can get the vertex-to-vertex information by looping through the edges (see `mesh.get_edges`) and keep a `vector` of `vectors` that tracks which vertex is connected to another vertex.
- check the number of half edges equals some expected number and check the number of boundary edges equals some expected number.

There is some functionality that you won't immediately need right now (we will need them later in the semester). For example, don't worry about unit testing the `remove` functions right now - we will only need those when we start manipulating meshes. There will also be additional functions added to our half-edge data structure later on when we talk about simplification and remeshing.
