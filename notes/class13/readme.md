## lecture 13 - mesh simplification

### objectives

- implement an algorithm for decimating a surface mesh by contracting vertices so as to minimize a quadric error metric,
- compute the optimal location of a vertex when contracting an edge,
- implement a collapse operator using a half-edge mesh data structure.

What if your mesh has too many triangles, and you want to remove some? Which ones should you remove without sacrificing the accuracy of the model? This lecture will be all about mesh simplification, particularly we will look at an algorithm call which will incrementally decimate a mesh while retaining a good representation of the original shape.

![](../images/simplification.png)

### two ingredients

The basic idea of the algorithm is to assign a "cost" to every edge and use a priority queue to determine which edges should be contracted next. The cost can be interpreted as the effect of contracting the edge on the overall surface representation. A lower cost would mean that an edge can be collapsed without affecting the representation of the surface. So we will maintain a priority queue of edges with the minimum cost edge at the beginning of the queue.

The second ingredient we need is the mesh modifications mechanics of contracting an edge. We can implement this efficiently with our half-edge based data structure. Ultimately, this is a matter of bookkeeping - you need to carefully update the relevant pointers for all half-edges affected by an edge collapse. I **strongly** recommend drawing out what this looks like on paper. If you are looking for a bug in your code for Project 3, the first thing I will ask is for a sketch of the half-edge based collapse operator.

### approximating error with quadrics

This section is a summary of Section 4 in the original paper that introduced this method called "Surface Simplification Using Quadric Error Metrics" by Michael Garland and Paul Heckbert - the paper is availalbe [here](https://csci422-s22.gitlab.io/home/docs/papers/Garland_1997_Surface_simplification_using_quadric_error_metrics.pdf).

To compute the cost of an edge, we will first compute an error for each vertex in the mesh. We will work in homogeneous coordinates for convenience - this means our 3d vertices will be represented in 4d where the fourth coordinate is 1. Homogeneous coordinates are a useful trick in computer graphics for expressing transformation matrices.

We will associate a 4x4 matrix $`\mathbf{Q}`$ with each vertex $`\mathbf{v} = [v_x\ v_y\ v_z\ 1]^T`$ where the 3d coordinates of the vertex are $`[v_x,\ v_y,\ v_z]`$. The error at this vertex will be the quadratic form $`\mathbf{v}^T \mathbf{Q} \mathbf{v}`$. In `flux`, a 4x4 matrix of doubles can be represented with a specialized `mat44d` class (which is a `mats<4,4,double>`). You will need to include `mat.h` to have access to this type.

![](../images/contraction.png)

When we contract an edge between vertices $`\mathbf{v}_1`$ and $`\mathbf{v}_2`$, we would like determine the optimal location of the receiving vertex. One option is the midpoint of the edge between $`\mathbf{v}_1`$ and $`\mathbf{v}_2`$. Another would be to pick either coordinates of the two vertices. We would like to compute the optimal location of the contracted vertex by minimizing the effect on the surface representation.

For each edge in the mesh, associate a 4x4 matrix $`\bar{\mathbf{Q}} = \mathbf{Q}_1 + \mathbf{Q}_2`$ (i.e. add the two 4x4 matrices at the endpoint vertices). Let the optimal coordinates of the contracted edge be represented by $`\bar{\mathbf{v}}`$. To minimize the error, we would like to minimize $`\bar{\mathbf{v}}^T \bar{\mathbf{Q}}\bar{\mathbf{v}}`$ while making the homogeneous coordinate of $`\bar{\mathbf{v}}`$ equal to 1. This leads to the system:

```math
\left[\begin{array}{cccc}
\bar q_{11} & \bar q_{12} & \bar q_{13} & \bar q_{14} \\
\bar q_{21} & \bar q_{22} & \bar q_{23} & \bar q_{24} \\
\bar q_{31} & \bar q_{32} & \bar q_{33} & \bar q_{34} \\
0 & 0 & 0 & 1 \\
\end{array}\right]
\bar{\mathbf{v}} =
\left[\begin{array}{c}
0 \\ 0 \\ 0 \\ 1
\end{array}\right]
```
which can be solved for $`\bar{\mathbf{v}}`$ by solving this system of equations. You can use the function `inverse` to invert a matrix in `flux` (remember to include `linear_algebra.h`). In general, you would not directly compute a matrix inverse, but it's okay for small matrices. Recall that the components of this matrix are the sum of the components of the $`\mathbf{Q}`$ matrix for the two endpoint vertices of the edge, with the 4th row modified as above. In other words, first compute some $`\bar{\mathbf{Q}} = \mathbf{Q}_1 + \mathbf{Q}_2`$, then copy it to a new 4x4 matrix, then set the fourth row of this copied matrix to $`[0\ 0\ 0\ 1]`$ and solve for $`\bar{\mathbf{v}}`$.

In practice, you would not create a new vertex when performing the contraction, but simply re-assign the coordinates of either $`\mathbf{v}_1`$ or $`\mathbf{v}_2`$ (your choice, based on your implementation) when performing the contraction.

#### computing $`\mathbf{Q}`$ for every vertex
The question remains: what should we use for $`\mathbf{Q}`$ for every vertex? We will characterize the geometric error at every vertex by looking at the planes (triangles) surrounding each vertex. Each triangle $`\Delta`$ surround a vertex defines a plane with a unit normal vector $`\mathbf{n}_\Delta`$. The plane is entirely defined by the coordinates of some vertex $`\mathbf{v}`$ (any point in the triangle) and $`\mathbf{n}_\Delta`$. Let the vector $`\mathbf{p} = [n_x,\ n_y,\ n_z,\ d]`$ where $`n_x,\ n_y,\ n_z`$ are the components of the triangle normal $`\mathbf{n}_\Delta`$ and $`d = - \mathbf{v}^T \mathbf{n}_\Delta`$. Let the 4x4 matrix $`\mathbf{K}_\Delta`$ be computed from the outer product $`\mathbf{K}_\Delta = \mathbf{p}\mathbf{p}^T`$. The error metric at vertex $`\mathbf{v}`$ is then
```math
\mathbf{Q} = \sum\limits_{\Delta \in \mathcal{T}(\mathbf{v})} \mathbf{K}_\Delta
```
where $`\mathcal{T}(\mathbf{v})`$ denotes the set of triangle faces surrounding vertex $`\mathbf{v}`$, which can be retrieved with the `get_onering` method of our `HalfEdgeMesh` class.

This derivation is done in the aforementioned paper and involves expressing the error as the sum of the squared distance from vertex $`\mathbf{v}`$ to each plane surrounding $`\mathbf{v}`$. For a review of how to compute the outer product, please see [this page](https://en.wikipedia.org/wiki/Outer_product).

### contracting an edge

When you're contracting an edge, you will need to update the relevant pointers to all `HalfEdgeMesh` entities. You should make a big drawing to help you visualize how the contraction affects all the triangles, edges and vertices in the vicinity of the collapse. The sketches below only show the entities involved in the collapse that are directly connected to the edge we are contracting (`e`) and its twin (`t`). In this example, `p` is being collapsed onto `q`, and the coordinates of `q` are being updated with the optimal coordinates saved in $`\bar{\mathbf{v}}`$ which are saved in the half-edge `e`. Keep in mind that any edge in the one-ring of the receiving vertex will be affected by the collapse (including twins).

![](../images/collapse-1.svg)![](../images/collapse-2.svg)

In the before & after sketch above, `eUL` stands for "upper-left edge", `eLL` stands for "lower-left edge", `eUR` stands for "upper-right edge" and `eLR` stands for "lower-right edge." Note that this sketch does not reflect which edges should be removed from the half-edge mesh container. In fact, `eUL`, `eUR`, `eLL` and `eLR` would no longer exist in the mesh.

When collapsing an edge, you should remove edges, vertices and faces that are no longer used from the `HalfEdgeMesh` container. You can use the `remove` method of the `HalfEdgeMesh` class to do this, which takes in a raw pointer to any of these entities, and will remove the appropriate entity.

### summary of the algorithm

Here is a summary of the algorithm that will decimate the mesh to achieve a target number of faces `nf_target`. This is what you will implement in Project 3.

1) pre-process:
  a) build up the half-edge representation of the mesh,
  b) loop through every face and calculate the vector $`\mathbf{p}`$ and the matrix $`\mathbf{K}_\Delta`$ for this face,
  c) loop through the vertices and compute $`\mathbf{Q}`$,
  d) loop through the edges and compute $`\bar{\mathbf{Q}}`$, $`\bar{\mathbf{v}}`$ as well as the cost $`\bar{\mathbf{v}}^T \bar{\mathbf{Q}}\bar{\mathbf{v}}`$ (equations above).
2) insert every edge into a priority queue using the cost of the edge to order the edges.
3) while the number of faces is greater than the target `nf_target`:
  a) retrieve the edge with the highest prority (lowest cost),
  b) collapse the edge, setting the coordinates of the receiving vertex to $`\bar{\mathbf{v}}`$,
  c) update the $`\mathbf{Q}`$ value of the receiving vertex (now equal to $`\bar{\mathbf{Q}}`$ of the collapsed edge). Also update the $`\bar{\mathbf{v}}`$, $`\bar{\mathbf{Q}}`$ and `cost` values for every edge affected by the collapse (i.e. in the new one-ring of the receiving vertex, including twins). You can remove and then re-insert these edges into your priority queue when doing this.
4) for visualization: convert the half-edge representation of the mesh to a connectivity-based representation (see the `extract` function defined for `HalfEdgeMesh`).

Implementing Step 3b requires a lot of attention, because it consists of updating the half-edge entities (vertices, edges, faces), and the pointers to surrounding entities as well. It also consists of some checks on the mesh topology, to make sure the mesh doesn't become invalid. Here are some possible checks:

1. extract the two one-rings (of `HalfVertex` entities) around the edge endpoint vertices `p` and `q`. Ensure that there are only **two** common vertices in these one-rings. Otherwise, remove the `edge` from the priority queue and reject the contraction.
2. extract the two one-rings (of `HalfFace` entities) around the edge endpoint vertices `p` and `q`. Ensure that the normals of the affected faces in these one-rings do not flip directions. If the original face normal is $`\mathbf{n}_0`$ (the first three components of $`\mathbf{p}`$ associated with the face), and the new face normal (with the proposed coordinates in $`\bar{\mathbf{v}}`$) is denoted as $`\mathbf{n}`$, then this amounts to checking if $`\mathbf{n}_0 \cdot \mathbf{n} < 0`$.

It is possible to perform a valid collapse without the second check, however, the first check above is necessary to ensure the topology of the mesh remains valid. As an example, consider the following mesh of a horse. A collapse is proposed in the highlighted region on the right (the edge is highlighted in yellow). Note that collapsing this edge is not valid because two triangles could become inverted. In fact, the number of common vertices in the one-rings of the edge endpoint vertices is three (which check #1 would detect).

<img src="../images/horse-invalid-collapse.png" width=400/><img src="../images/horse-invalid-collapse-zoom.png" width=400/>

Note that it is still possible to create invalid meshes for very complicated geometries, even with the checks above. However, the first check will work fine (but not guaranteed) for most of the meshes you will test with in Project 3. We will use closed meshes (without boundary) so, at the very least, your mesh should still be without boundary at the end of every collapse.

### exercises

We will practice with steps (1) and (2) today, and you will implement step (3) in Project 3. Step (2) can be achieved using a `std::multiset<HalfEdge*,CompareCost` where `CompareCost` is a `struct` that provides a call operator `()` which takes in two half edges and returns true if the cost of the first is lower than the second:

```c++
struct CompareCost {
  bool operator() ( const HalfEdge* e1 , const HalfEdge* e2) const {
    flux_assert( e1 != nullptr && e2 != nullptr);
    return e1->cost < e2->cost;
  }
};
```

We are using a `std::multiset` instead of a `std::set` because there could be multiple half-edges with the same error value that we want to collapse. You might also be tempted to use a `std::priority_queue`. However, you will need to remove and re-insert edges surrounding a contraction when a contraction is performed. This means you need to remove the affected edges from the priority queue, recalculate the metrics from step 1, and then re-insert the affected edges into the priority queue. While a `std::priority_queue` does allow insertion of elements, it does not allow removal of an arbitrary element (only removal of the element at the top of the queue). Therefore, we will use a `set` or `multiset`.

Note that this method supports adding a `HalfEdge` along with its `twin` into the priority queue, which means we effectively have duplicates in the edges to collapse. This is fine, since both of these edges will be removed from our `HalfEdgeMesh` container (and the priority queue) when collapsing whichever edge is chosen (the twin is one of the six edges removed during the collapse).

When we talk about remeshing next class, we will implement a "swap" operator, which will help you visualize how to implement a local mesh modification operator using half-edges - this will help with step (3).

### modifications to `flux-base`

As you may have noticed, we have some additional data that we need to keep track of throughout the simplication process. In particular, we need to compute (1) the $`\mathbf{p}`$ value of every face, (2) the 4d vector $`\bar{\mathbf{v}}`$, 4x4 matrix $`\bar{\mathbf{Q}}`$ and the scalar `cost` for every edge and (3) the 4x4 matrix $`\mathbf{Q}`$ associated with every vertex. We could keep track of all these entities using a `map`, but it is simpler to just have it directly stored in our half-edge structures. Therefore, you will notice that our half-edge based entities now look like:

```c++
/**
 * \brief Represents a face in a half-edge based mesh.
 */
struct HalfFace {
  HalfEdge* edge; // pointer to one edge which has the face on the left
  int index = -1; // index of the face (i.e. element number)
  vec4d p;        // [nx, ny, nz, -dot(x,n)] where x is a point on the face
                  // n is the face normal (used in simplification algorithm)
};

/**
 * \brief Represents a vertex in a half-edge based mesh.
 */
struct HalfVertex {
  /**
   * \brief Constructs a half vertex from coordinates.
   *
   * \param[in] dim number of coordinates to copy (allows copying 2d or 3d coordiantes)
   * \param[in] x - pointer to coordinates
   */
  HalfVertex( int dim , const double* x ) {
    flux_assert( dim == 2 || dim == 3 );
    for (int d = 0; d < dim; d++)
      point[d] = x[d];
  }
  HalfEdge* edge; // pointer to one edge emanating from this vertex
  vec3d point;    // the coordinates of this vertex (always 3d!)
  int index = -1; // index of this vertex (i.e. vertex number)
  mat44d Q;       // error quadric (used in simplification algorithm)
};

/**
 * \brief Represents an edge in a half-edge based mesh.
 */
struct HalfEdge {
  HalfVertex* vertex; // pointer to the 'origin' vertex of this edge
  HalfEdge*   twin;   // pointer to the 'opposite' edge (i.e. parallel but in opposite direction)
  HalfEdge*   next;   // pointer to the next edge around the face in CCW order
  HalfEdge*   prev;   // pointer to the previous edge around the face in CCW order
  HalfFace*   face;   // pointer to the left face of this oriented edge

  // data used in simplification algorithm
  vec4d  vbar; // optimal point (in homogeneous coordinates) where the endpoints should be collapsed
  mat44d Qbar; // sum of endpoint Q's, i.e. Qbar = Q1 + Q2
  double cost;  // the cost (error) of this edge: vbar^T * Qbar * vbar
};
```

**Note:** even though the optimal coordinates are really in 3d (with a homogeneous coordinate of 1), we are using a `vec4d` to represent `vbar` for each edge. This just makes the implementation a bit simpler since `vbar` is obtained via by solving a 4x4 system of equations. When assigning vertex coordinates during the contraction, you only need the first 3 components of `vbar`.

### unit testing

Since you will be calling `HalfEdgeMesh<Triangle>::remove` for input `HalfEdge`, `HalfVertex` and `HalfFace` pointers, you should add some tests (probably in `test/halfedges_ut.cpp`) to ensure you are familiar with calling those functions.

I have also added a <code>Sphere</code> class defined in `src/flux-base/src/sphere.[h,cpp]` which you will need to unit test. This provides a sphere mesh which is closed (recall Project 1) and will be useful for testing your algorithm in Project 3.
