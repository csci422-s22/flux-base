#include "element.h"
#include "halfedges.h"
#include "linear_algebra.h"
#include "mat.hpp"
#include "mesh.h"
#include "readwrite.h"
#include "sphere.h"
#include "vec.hpp"
#include "webgl.h"

#include <cstdio>
#include <set>

using namespace flux;

/**
 * Computes the outer product of two 4d vectors.
 */
mat44d
outer_product( const vec4d& a , const vec4d& b ) {
  mat44d X;
  for (int i = 0; i < 4; i++)
  for (int j = 0; j < 4; j++)
    X(i,j) = a(i)*b(j);
  return X;
}

/**
 * \brief Computes vbar, Qbar and the error associated with a HalfEdge e.
 */
void
calculate_edge_data( HalfEdge* e ) {

  static const vec4d rhs = {0,0,0,1};

  // calculate the Qbar matrix for the edge
  const mat44d& Q1 = e->vertex->Q;
  const mat44d& Q2 = e->twin->vertex->Q;
  e->Qbar = Q1 + Q2;

  // solve for the optimal point
  mat44d A = e->Qbar; // copy Qbar from the edge
  A(3,0) = 0;
  A(3,1) = 0;
  A(3,2) = 0;
  A(3,3) = 1;

  double detA = det(A);
  if (fabs(detA) > 1e-12) {
    // use the optimal point
    e->vbar = inverse(A) * rhs;
  }
  else {
    // use the midpoint
    const vec3d& xp = e->vertex->point;
    const vec3d& xq = e->twin->vertex->point;
    vec3d xm = 0.5 * (xp + xq);
    e->vbar = { xm[0] , xm[1] , xm[2] , 1.0 };
  }
  e->cost = transpose(e->vbar) *  e->Qbar * e->vbar;
}

/**
 * \brief Calculates the "Q" matrix associated with a HalfVertex v.
 *        Also takes in the full HalfEdgeMesh container so we can retrieve the onering of v.
 */
void
calculate_vertex_data( HalfVertex* v , const HalfEdgeMesh<Triangle>& halfedges ) {
  std::vector<HalfFace*> faces;
  halfedges.get_onering(v,faces);
  v->Q.zero();
  for (int k = 0; k < faces.size(); k++) {
    v->Q = v->Q + outer_product( faces[k]->p , faces[k]->p );
  }
}

/**
 * \brief Calculates the "p" vector associated with a HalfFace f
 */
void
calculate_face_data( HalfFace* f ) {

  HalfVertex* v0 = f->edge->vertex;
  HalfVertex* v1 = f->edge->next->vertex;
  HalfVertex* v2 = f->edge->next->next->vertex;

  vec3d u, v, x;
  for (int d = 0; d < 3; d++) {
    u[d] = v1->point[d] - v0->point[d];
    v[d] = v2->point[d] - v0->point[d];
    x[d] = v0->point[d];
  }

  vec3d n = cross(u,v);
  normalize(n);
  double d = -dot(x,n);

  // compute the face normal
  f->p = {n[0],n[1],n[2],d};
}

int
main( int argc, char** argv ) {

  // step 1: pre-process - import the initial mesh
  #if 1
  std::unique_ptr<MeshBase> mesh_ptr = read_obj( "../data/spot.obj" , false );
  Mesh<Triangle>& mesh = *static_cast<Mesh<Triangle>*>(mesh_ptr.get());
  #else
  int N = 10;
  Sphere<Triangle> mesh(N,N);
  #endif

  // 1a: build up the half edges
  HalfEdgeMesh<Triangle> halfmesh( mesh );

  // 1b: compute the face data
  for (auto& f_ptr : halfmesh.faces()) {
    calculate_face_data( f_ptr.get() );
  }

  // 1c: compute the initial matrices for each vertex
  for (auto& v_ptr : halfmesh.vertices()) {
    calculate_vertex_data(v_ptr.get(),halfmesh);
  }

  // 1d: compute the optimal point location (vbar) and matrix Qbar, as
  // well as the cost of every edge
  for (auto& e_ptr : halfmesh.edges()) {
    HalfEdge* e = e_ptr.get();
    calculate_edge_data(e);
  }

  // step 2: insert each edge into a priority queue
  struct CompareCost {
    bool operator() ( const HalfEdge* e1 , const HalfEdge* e2) const {
      return e1->cost < e2->cost;
    }
  };
  typedef std::multiset<HalfEdge*,CompareCost> CollapsePriorityQueue;

  CollapsePriorityQueue edges;
  for (auto& e_ptr : halfmesh.edges()) {
    edges.insert( e_ptr.get() );
  }

  while (edges.size() > 0) {
    HalfEdge* e = * edges.begin();
    printf("edge cost = %g\n",e->cost);
    edges.erase(e);
  }

  return 0;
}
