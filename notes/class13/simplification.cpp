#include "element.h"
#include "halfedges.h"
#include "linear_algebra.h"
#include "mat.hpp"
#include "mesh.h"
#include "readwrite.h"
#include "sphere.h"
#include "vec.hpp"
#include "webgl.h"

#include <cstdio>
#include <set>

using namespace flux;

/**
 * Computes the outer product of two 4d vectors.
 */
mat44d
outer_product( const vec4d& a , const vec4d& b ) {
  mat44d X;
  for (int i = 0; i < 4; i++)
  for (int j = 0; j < 4; j++)
    X(i,j) = a(i)*b(j);
  return X;
}

/**
 * \brief Computes vbar, Qbar and the error associated with a HalfEdge e.
 */
void
calculate_edge_data( HalfEdge* e ) {

  // the right-hand-side of the system of equations we want to solve
  // to determine vbar
  static const vec4d rhs = {0,0,0,1};

}

/**
 * \brief Calculates the "Q" matrix associated with a HalfVertex v.
 *        Also takes in the full HalfEdgeMesh container so we can retrieve the onering of v.
 */
void
calculate_vertex_data( HalfVertex* v , const HalfEdgeMesh<Triangle>& halfedges ) {

}

/**
 * \brief Calculates the "p" vector associated with a HalfFace f
 */
void
calculate_face_data( HalfFace* f ) {


}

int
main( int argc, char** argv ) {

  // step 1: pre-process - import the initial mesh
  #if 1
  std::unique_ptr<MeshBase> mesh_ptr = read_obj( "../data/spot.obj" , false );
  Mesh<Triangle>& mesh = *static_cast<Mesh<Triangle>*>(mesh_ptr.get());
  #else
  int N = 4;
  Sphere<Triangle> mesh(N,N);
  #endif

  // 1a: build up the half edges
  HalfEdgeMesh<Triangle> halfmesh( mesh );

  // 1b: compute the face data
  for (auto& f_ptr : halfmesh.faces()) {
    calculate_face_data( f_ptr.get() );
  }

  // 1c: compute the initial matrices for each vertex
  for (auto& v_ptr : halfmesh.vertices()) {
    calculate_vertex_data(v_ptr.get(),halfmesh);
  }

  // 1d: compute the optimal point location (vbar) and matrix Qbar, as
  // well as the cost of every edge
  for (auto& e_ptr : halfmesh.edges()) {
    HalfEdge* e = e_ptr.get();
    calculate_edge_data(e);
  }

  // step 2: insert each edge into a priority queue


  return 0;
}
