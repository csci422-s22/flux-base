#include "element.h"
#include "grid.h"
#include "mesh.h"
#include "readwrite.h"
#include "vec.hpp"
#include "webgl.h"

#include <cstdio>
#include <limits>

using namespace flux;

int
main( int argc, char** argv ) {

  // exercise 1
  Mesh<Polygon> hexagon(2);
  // complete exercise 1 here

  // exercise 2
  int dim = 3;
  int n = 40;
  Grid<Quad> grid( {n,n} , dim );

  // create sites with coordinates that are within [0,1]
  int nb_sites = 100;
  Vertices sites(dim);
  std::vector<double> x(dim,0.0);
  for (int k = 0; k < nb_sites; k++) {
    for (int d = 0; d < 2; d++)
      x[d] = double(rand()) / double(RAND_MAX);
    sites.add(x.data());
  }

  std::vector<double> cell_to_site( grid.nb() );
  for (int k = 0; k < grid.nb(); k++) {

    int closest_site = 0;

    // complete exercise 2 here

    cell_to_site[k] = closest_site;

  }
  grid.create_cell_field("cell",cell_to_site);

  Viewer viewer;
  viewer.add(hexagon);
  viewer.add(grid);
  viewer.run();

  return 0;
}
