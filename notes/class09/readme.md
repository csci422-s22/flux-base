### lecture 09: Voronoi diagrams

#### objectives

- manipulate polygon meshes in `flux`,
- define Voronoi diagrams and their relationship to Delaunay triangulations,
- compute discrete ("pixelized") Voronoi diagrams.

##### polytope meshes in `flux`

Voronoi diagrams are meshes of polytopes (i.e. polygons in 2d, polyhedra in 3d), so before we start with Voronoi diagrams, let's practice with representing these types of meshes. Since we don't know beforehand how many vertices each cell will have, we need to be careful in how we store the mesh. The `array2d` class provides an alternative storage layout, called `Layout_Jagged` (in contrast to `Layout_Rectangular` used for triangle or quad meshes in which we know exactly how many vertices each cell has). The connectivity is still stored as a one-dimensional array, but we keep track of the (1) first index of each cell (stored in `first_`) and (2) the number of vertices in each cell (stored in `count_`).

Thus, given a polytopal mesh (`Mesh<Polygon>`) called `mesh`, we can loop through the vertices of each element using:

```c++
for (int k = 0; k < mesh.nb(); k++) {
  for (int j = 0; j < mesh.count(k); j++) {

    // retrieve the j-th vertex index in element k
    int idx = mesh(k,j);

  }
}
```

Adding a cell to a polytopal mesh can be done similar to the way we add elements to a triangular or quad mesh, with the difference that we need to tell the `array2d` container how many vertices this cell contains (since there is no uniform `stride`):

```c++
int cell[5] = {5,7,2,98,3}; // a polygon defined by 5 vertices in CCW order
mesh.add( cell , 5 );
```

How do we extract the edges of a polytope? And how do we visualize a polygon mesh? At the end of the day, visualization tools (like OpenGL and WebGL, which we are using in `flux360`) render triangles. So we need a way to decompose a polytope into "visualization triangles". We will focus on polygonal meshes (but a final project might involve extending the framework to handle polyhedral meshes). In the example above, notice the comment about ordering the vertices in counterclockwise (CCW) order. This means that we can produce the edges by starting at some vertex and looping around the cell vertices until we get back to the starting vertex. A polygon with $n$ vertices thus has $n$ edges.

A similar approach can be done to triangulate a polygon. Pick a vertex $p$ to start off with and connect it to all the other vertices. In other words, create a triangle that connects every edge (which does not touch $p$) to $p$ itself. Thus a polygon with $n$ vertices can be triangulated with $n-2$ triangles (since there are two edges which touch $p$). In the diagram below, $p$ was selected to be vertex $3$.

![](../images/polygons-1.svg) ![](../images/polygons-2.svg)

Does this work for all polygons? No. In fact, it only works for **convex polygons**. A convex region is the set of all points, such that a line drawn between any two points is entirely contained within the region.

#### definition of Voronoi diagrams

A Voronoi diagram is a partition of space given a set of input points, which are also called seeds or generators. Each cell $V_i$ in the partition is associated with a particular seed $\mathbf{z}_i$, and contains the set of points that are closest to the seed than to any other seed:

```math
V_i = \{ \mathbf{x} \subset \mathcal{D} : \lvert| \mathbf{x} - \mathbf{z}_i \rvert| \le \lvert| \mathbf{x} - \mathbf{z}_j\rvert|, \forall j \}.
```

Here $\mathcal{D}$ is the domain we are partitioning, which we will usually take to be $\mathbb{R}^2$ (the two-dimensional Euclidean plane). It is also possible to take $\mathcal{D}$ to be a surface (which may be represented as a mesh), in which case we would obtain the *restricted Voronoi diagram*.

As an example, consider dividing a city into regions such that each region is the set of points closest to a particular cafe. Dividing the city in such a way creates the regions outlined below. Two cafes that have regions which share a border have the same distance to the border.

![](../images/cafe-sdot.png)

##### relationship to Delaunay triangulations

Voronoi diagrams are dual to the Delaunay triangulations we saw last time. This means that we can compute either a Voronoi diagram or a Delaunay triangulation, and convert it to it's dual.

In fact, the center of the circumcircles of each Delaunay triangle is a Voronoi vertex (a vertex that bounds several Voronoi cells). There is an edge between Voronoi vertices (an edge between two Voronoi cells) if two Delaunay triangles share a face (an edge in 2d or a common triangle in 3d).

This gives us a way to indirectly construct Voronoi diagrams from a Delaunay triangulation. Similarly, each Voronoi vertex defines a Delaunay simplex. In fact, the number of Voronoi polygons sharing a vertex should always be 3, which defines the three vertices in the Delaunay triangle. This duality is exhibited below - the dashed blue polygon represents a Voronoi cell for the seed $\mathbf{p}_5$.

![](../images/voronoi-1.svg) ![](../images/voronoi-2.svg)


#### exercise: "pixelized" Voronoi diagrams

Next time, we will practice converting a Delaunay triangulation to a Voronoi diagram by computing the *geometry* of the Voronoi cells. For now, let's practice with the definition of a Voronoi diagram. Roughly speaking, the mathematical definition above can be translated into the following:

*A Voronoi diagram is a collection of cells such that each point in a cell is closest to the site that "generates" the cell, than to any other site.*

Therefore, given a set of randomly placed sites, we could consider a domain represented by a really really really fine mesh, loop over the elements of this mesh and determine which site that element is closest to. When doing this calculation, you can use the centroid of the element.

![](../images/voronoi-pixelated.png) ![](../images/voronoi-pixelated-zoom.png)

When we want to visualize the Voronoi diagram, we will create a "cell field" (one value for every element), that will contain the site number of the closest site. This will allow us to visualize the Voronoi diagram as in the image above.


#### unit testing ideas

Nothing will be added to `flux-base` that you will need to unit test!
