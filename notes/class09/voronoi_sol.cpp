#include "element.h"
#include "grid.h"
#include "mesh.h"
#include "readwrite.h"
#include "vec.hpp"
#include "webgl.h"

#include <cstdio>
#include <limits>

using namespace flux;

int
main( int argc, char** argv ) {

  // exercise 1
  Mesh<Polygon> hexagon(2);
  double a = std::cos( M_PI / 3.0 );
  double b = std::sin( M_PI / 3.0 );
  double x0[2] = {1,0};
  double x1[2] = {a,b};
  double x2[2] = {-a,b};
  double x3[2] = {-1,0};
  double x4[2] = {-a,-b};
  double x5[2] = {a,-b};
  hexagon.vertices().add(x0);
  hexagon.vertices().add(x1);
  hexagon.vertices().add(x2);
  hexagon.vertices().add(x3);
  hexagon.vertices().add(x4);
  hexagon.vertices().add(x5);

  int p[6] = {0,1,2,3,4,5};
  hexagon.add(p,6);

  // exercise 2
  int dim = 3;
  int n = 500;
  Grid<Quad> grid( {n,n} , dim );

  // create sites with coordinates that are within [0,1]
  int nb_sites = 100;
  Vertices sites(dim);
  std::vector<double> x(dim,0.0);
  for (int k = 0; k < nb_sites; k++) {
    for (int d = 0; d < 2; d++)
      x[d] = double(rand()) / double(RAND_MAX);
    sites.add(x.data());
  }

  std::vector<double> cell_to_site( grid.nb() );
  for (int k = 0; k < grid.nb(); k++) {

    // compute the centroid of this element
    vec3d p;
    for (int j = 0; j < grid.count(k); j++) {

      vec3d q( grid.vertices()[grid(k,j)] , dim );
      p = p + q;
    }
    p = p / grid.count(k);

    // find the closest site
    int closest = 0;
    double min_distance = std::numeric_limits<double>::max();
    for (int j = 0; j < nb_sites; j++) {

      // calculate the distance to site j
      vec3d q(sites[j],dim);

      double distance = dot( p - q , p - q ); // this is actually the square distance
      if (distance < min_distance) {
        closest = j;
        min_distance = distance;
      }
    }
    cell_to_site[k] = double(closest) / double(nb_sites);
  }
  grid.create_cell_field("cell",cell_to_site);

  Viewer viewer;
  viewer.add(hexagon);
  viewer.add(grid);
  viewer.run();

  return 0;
}
