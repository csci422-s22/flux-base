#include "element.h"
#include "grid.h"
#include "mesh.h"
#include "vec.hpp"
#include "vertices.h"
#include "webgl.h"

#include <cstdio>
#include <map>
#include <set>
#include <queue>
#include <vector>

using namespace flux;

// we will search for an element that has a vertex
// with a distance within R of a query point q
vec3d q = { 0.6, 0.6 , 0.0 };
double R = 0.01; // the search radius

/**
 * \brief Determines whether some element satisfies our search property.
 *        In this example, we are looking for an element with a vertex
 *        that is within a distance of R to some query vertex (q).
 *        The point 'q' and radius 'R' are defined above.
 *
 * \param[in] elem - the element to check
 * \param[in] mesh - a reference to the full triangle mesh object
 *
 * \return whether the element satisfies the property (true) or not (false).
 */
bool
element_property( int elem , const Mesh<Triangle>& mesh ) {

  // exercise 2: loop through the vertices in the element
  // and find a vertex (if any) that is within a distance of R
  // to the query vertex q (defined above)
  flux_implement;

  return false;
}

/**
 * \brief Performs a breadth-first-search on the mesh adjacency graph
 *        described by the neighbors in order to find an element that
 *        satisfies the function element_property (above).
 *
 * \param[in] mesh - a triangle mesh
 * \param[in] neighbors - the triangle-to-triangle neighbor information
 *                         an entry of -1 denotes a boundary
 *
 * \return the element that satisfies the element_property function (above)
 *         returns -1 if no element was found
 */
int
search( const Mesh<Triangle>& mesh , const array2d<int>& neighbors ) {

  // mark which elements have been visited
  std::vector<bool> visited(mesh.nb(),false);

  // initialize the queue of elements to visit, and add some initial guess
  std::queue<int> elements;
  elements.push(0); // our initial guess is triangle 0

  // iterate through the queue, keeping track of the
  // number of elements that have been visited
  int nb_visited = 0;
  while (!elements.empty()) {

    // get the next triangle (t) in the queue
    int t = elements.front();
    elements.pop();
    nb_visited++;

    // check if this element satisfies the search criteria
    if (element_property(t,mesh)) {
      printf("visited %d triangles.\n",nb_visited);
      return t;
    }

    // exercise 3
    // add the neighbors to the queue (but not boundaries)
    // loop through the three edges (faces) of the triangle
    flux_implement;
  }

  return -1; // no element was found
}

int
main( int argc, char** argv ) {

  Triangle element;
  int dim = 3;

  // create a 10x10 triangle grid
  int n = 10;
  Grid<Triangle> mesh( {n,n} , dim );

  // initialize the neighbors
  // we can pass in a second parameter to array2d to initialize the number of elements
  // the third parameter is the initial value of all the data (here, -1, which we will interpret as being undefined)
  array2d<int> neighbors( element.nb_edges , mesh.nb() , -1 );

  // each edge will point to the element it came from, and the local edge index in that element
  std::map< std::pair<int,int> , std::pair<int,int> > edge2element;

  // the full list of unique edges we will populate
  std::vector<int> edges;

  // exercise 1
  flux_implement;

  // initialize the viewer
  Viewer viewer;

  // add the mesh to the viewer
  viewer.add(mesh);

  // send the data to the browser
  // this function will return once a connection is established
  //viewer.run();

  return 0;
}
