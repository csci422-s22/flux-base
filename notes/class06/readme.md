### lecture 06: searching

#### objectives

- practice with some `flux` conventions,
- calculate element-to-element adjacency information,
- extract the unique list of edges of a mesh,
- practice with linear algebra in `flux`,
- search for a triangle in a mesh with some property.

#### getting started

To copy the template from `flux-base` to your repository (remember to run this from a build directory, e.g. `build/debug` or `build/release`):

```bash
make update
cmake .
make template_class06_searching
```

To compile and run the exercise:


```bash
make class06_searching
```

To run the solution (once it is added to `flux-base` after class):
```bash
make class06_searching_sol
```

#### some notation and conventions

Last time, we saw the `array2d` class, which allowed us to store vertices and elements (specifically, triangles), as well as any other type of two-dimensional data. Here are some important functions we will frequently use.

- `nb()`: returns the total **N**um**B**er of elements (i.e. the number of vertices or the number of triangles).
- `count(k)`: returns the number of entries stored for element `k`. For data which is stored in a rectangular manner, such as vertex coordinates or triangle indices, this will always be the `stride` (the dimension for vertices, and 3 for triangles).
- `operator()(k,j)`: allows you to read/write the `j`-th entry in element `k`. For example, for vertices stored in `vertices`, then `vertices(20,2)` will access the `z`-coordinate of the 21st vertex.
- `operator[](k)`: allows you to read/write the data stored at the memory location for element `k`. In other words, this function returns a pointer to the first entry of element `k`.

#### elements

We will mostly work with triangles in our course, but we will also see quadrilaterals, polygons and tetrahedra. Each of these primitive shapes is defined in `src/flux-base/src/element.h` and `src/flux-base/src/element.cpp`. The conventions used for representing a triangle are:

```c++
/**
* \brief Triangle element:
*
*        2
*        |
*        | \
* face/  |   \     face/edge 1
* edge   |     \
*  2     |  t0   \
*        0 ------- 1
*         face/edge 0
*/
struct Triangle {
  static const int dimension = 2;
  static const int nb_vertices = 3;
  static const int nb_edges = 3;
  static const int nb_faces = 3;
  static const int nb_triangles = 1;
  static int edges[6];
  static int triangles[3];
  static int faces[6];
  typedef Line face_type;
};
```

The `dimension` refers to the *topological* dimension of the element (not the *ambient* dimension in which it is embeded). The other attributes, like `nb_vertices` and `nb_edges` are geometric properties of a triangle and allow us to retrieve information about the primitive shape used for a particular mesh type. In `element.cpp`, you will see the following definition for the *canonical* edges of a triangle:

```c++
int Triangle::edges[6] = {0,1,1,2,2,0};
```

There are 6 values (3 edges, each with 2 endpoints). The first edge has vertex indices 0 and 1, the second edge has vertex indices 1 and 2 and the third edge has vertex indices 2 and 0. You might notice other conventions used in other codes. Note that the "stride" when retrieving edge indices in `Triangle::edges` is 2.

You might notice another attribute called `faces`. For triangles and quadrilaterals, these are the same as the edges. In general, we will define a face as being a primitive type that has a topological dimension one less than the topological dimension of the current primitive. For example, a tetrahedron has triangular faces, and a hexahedron has quadrilateral faces. Both triangles and quadrilaterals have `Line` faces.

**Note:** for a `Triangle`, `Quad` and (counter)clockwise-oriented `Polygon`, we could have extracted the `i`-th edge as being connected to vertices `i` and `(i+1) % count(k)` (e.g. `(i+1) % 3` for a `Triangle`). That works for our two-dimensional elements, but the approach above works for 3d (tetrahedra, hexahedra) and 4d (pentatopes, tesseracts) as well.

#### extracting the edges and calculating neighbors

Let's say we want to extract the **unique** set of edges in the mesh. Maybe we also want to keep track of which triangles these edges are connected to. We might also want to keep track of which triangles are adjacent to each triangle.

We can do this by building up a `std::map` of edge indices, and which triangle(s) they are adjacent to.

```c++
// we can pass in a second parameter to initialize the number of elements
// the third parameter is the initial value of all the data
array2d<int> neighbors(3 , mesh.nb() , -1 );
std::map< std::pair<int,int> , std::pair<int,int> > edge2element;
std::vector<int> edges;

Triangle element;

for (int k = 0; k < mesh.nb(); k++) {

  for (int j = 0; j < element.nb_edges; j++) {

    int e0 = mesh( k , element.edges[2*j  ] );
    int e1 = mesh( k , element.edges[2*j+1] );

    if (e1 < e0) std::swap(e0,e1);

    std::map< std::pair<int,int> , std::pair<int,int> >::iterator it = edge2element.find( {e0,e1} );
    if (it == edge2element.end()) {
      // this edge does not exist yet
      edge2element.insert( {{e0,e1} , {k,j}} );

      edges.push_back(e0);
      edges.push_back(e1);
    }
    else {
      // this edge exists, update the edge information
      int k1 = it->second.first;
      int j1 = it->second.second;

      neighbors(k1,j1) = k;
      neighbors(k,j)   = k1;
    }
  }
}
neighbors.print();
```

#### searching for a triangle with some property

Given some search criterion, how would you find a triangle that satisfies some property? This is where our neighbor (adjacency) information will be useful. Given some initial "guess element", we can traverse the neighbors (being careful not to step into boundaries) in order to find an element that satisfies our search property. We can use Breadth-First-Search (BFS) or Depth-First-Search (DFS) to find such an element - our guess element will be the root of the tree that is constructed as we traverse the adjacency graph of the mesh.

For example, suppose we have the following problem:

*Find an element (a triangle) that has a vertex within a distance $`R`$ to some point $`\mathbf{q}`$.*

We will fill in the code to do this in class together, so please see the solution after class.

#### other ways of searching

Most searches you would do in the context of meshing will be geometric, for example, finding the closest vertex in a mesh to some query point (i.e. the nearest neighbor). In these cases, there are much more efficient search structures you should use, like [k-d trees](https://en.wikipedia.org/wiki/K-d_tree), [quadtrees](https://en.wikipedia.org/wiki/Quadtree), [octrees](https://en.wikipedia.org/wiki/Octree), etc. You can also use the result of these nearest neighbor queries as the root of the search we just implemented. For example, if you still want to solve the problem above, you can use a k-d tree to find the element whose centroid is closest to a query point, then use that element as the initial guess in a BFS algorithm. Alternatively, you can use a k-d tree to find the nearest vertex to a query point, then start with some element touching this vertex (assuming you have this vertex-to-element information) as the root of your search.

#### unit testing ideas

By the end of the semester, please unit test the files `adjacency.h` and `adjacency.cpp`. These provide a general interface for computing adjacency (neighbor) relations for Line, Triangle, Quad, and Tet meshes, but does not support Polygon or Polyhedron meshes. Please see the documentation (linked on the course website, which is created directly from the source code comments). Here are some ideas of what to test - I would suggest using a `Grid` in your tests:

- make sure the `Adjacency` object has the same `nb()` as the original mesh.
- make sure the `Adjacency` object has `nb_neighbors()` equal to the number of faces of the primitive shape type (3 for triangles, 4 for quads, etc.).
- loop through all the elements in the mesh, and (within a nested for-loop) loop through the neighbors of each element. If the neighbor is negative, then this element borders a boundary. Otherwise, retrieve the **face index** of this neighbor (using the `indexof` function of the `Adjacency` class) and make sure that neighbor of the element at the returned index is equal to the neighbor.
- count the number of boundary neighbors and check this is equal to some expected number.
- you might also want to implement a search structure that uses the `Adjacency` class and search the mesh for a triangle containing a query point. One idea would be to place the query point at the centroid of an element, seed the search somewhere far away from this element, and then make sure that the resulting element (found by the search) is indeed the one used to compute the centroid (as the query point).
- your unit tests should cover `Triangle`s and `Quad`s (don't worry about `Tet` elements).

Please see the slides for today's lecture for examples of some of these tests.
