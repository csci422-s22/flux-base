#include "element.h"
#include "grid.h"
#include "mesh.h"
#include "vec.hpp"
#include "vertices.h"
#include "webgl.h"

#include <cstdio>
#include <map>
#include <set>
#include <queue>
#include <vector>

using namespace flux;

// we will search for an element that has a vertex
// with a distance within R of a query point q
vec3d q = { 0.6, 0.6 , 0.0 };
double R = 0.01; // the search radius

/**
 * \brief Determines whether some element satisfies our search property.
 *        In this example, we are looking for an element with a vertex
 *        that is within a distance of R to some query vertex (q).
 *        The point 'q' and radius 'R' are defined above.
 *
 * \param[in] elem - the element to check
 * \param[in] mesh - a reference to the full triangle mesh object
 *
 * \return whether the element satisfies the property (true) or not (false).
 */
bool
element_property( int elem , const Mesh<Triangle>& mesh ) {

  for (int j = 0; j < 3; j++) {

    // create a vec3d to represent this point so we can use some
    // linear algebra tools in flux
    vec3d p( mesh.vertices()[ mesh(elem,j) ] , 3 );

    // compute the distance to the query point
    double distance = std::sqrt( dot(p - q, p - q) );

    // return if this point satisfies the search property
    if (distance < R) {
      printf("found vertex %d\n",mesh(elem,j));
      p.print();
      return true;
    }
  }
  return false;
}

/**
 * \brief Performs a breadth-first-search on the mesh adjacency graph
 *        described by the neighbours in order to find an element that
 *        satisfies the function element_property (above).
 *
 * \param[in] mesh - a triangle mesh
 * \param[in] neighbours - the triangle-to-triangle neighbour information
 *                         an entry of -1 denotes a boundary
 *
 * \return the element that satisfies the element_property function (above)
 *         returns -1 if no element was found
 */
int
search( const Mesh<Triangle>& mesh , const array2d<int>& neighbours ) {

  // mark which elements have been visited
  std::vector<bool> visited(mesh.nb(),false);

  // initialize the queue of elements to visit, and add some initial guess
  std::queue<int> elements;
  elements.push(0); // our initial guess is triangle 0

  // iterate through the queue, keeping track of the
  // number of elements that have been visited
  int nb_visited = 0;
  while (!elements.empty()) {

    // get the next triangle (t) in the queue
    int t = elements.front();
    elements.pop();
    nb_visited++;

    // check if this element satisfies the search criteria
    if (element_property(t,mesh)) {
      printf("visited %d triangles.\n",nb_visited);
      return t;
    }

    // add the neighbours to the queue (but not boundaries)
    // loop through the three edges (faces) of the triangle
    for (int j = 0; j < 3; j++) {
      int n = neighbours(t,j);
      if (n < 0) continue; // do not step into boundaries
      if (!visited[n]) {
        visited[n] = true;
        elements.push(n); // add the neighbour to the queue
      }
    }
  }

  return -1; // no element was found
}

int
main( int argc, char** argv ) {

  Triangle element;
  int dim = 3;

  // create a 10x10 triangle grid
  int n = 10;
  Grid<Triangle> mesh( {n,n} , dim );

  // initialize the neighbours
  // we can pass in a second parameter to array2d to initialize the number of elements
  // the third parameter is the initial value of all the data (here, -1, which we will interpret as being undefined)
  array2d<int> neighbours( element.nb_edges , mesh.nb() , -1 );

  // each edge will point to the element it came from, and the local edge index in that element
  std::map< std::pair<int,int> , std::pair<int,int> > edge2element;

  // the full list of unique edges we will populate
  std::vector<int> edges;

  for (int k = 0; k < mesh.nb(); k++) {

    for (int j = 0; j < element.nb_edges; j++) {

      int e0 = mesh( k , element.edges[2*j  ] );
      int e1 = mesh( k , element.edges[2*j+1] );

      if (e1 < e0) std::swap(e0,e1);

      std::map< std::pair<int,int> , std::pair<int,int> >::iterator it = edge2element.find( {e0,e1} );
      if (it == edge2element.end()) {

        // this edge does not exist yet
        edge2element.insert( {{e0,e1} , {k,j}} );

        edges.push_back(e0);
        edges.push_back(e1);
      }
      else {
        // this edge exists, update the edge information
        int k1 = it->second.first;
        int j1 = it->second.second;

        neighbours(k1,j1) = k;
        neighbours(k,j)   = k1;
      }
    }
  }
  //neighbours.print();

  int elem = search( mesh , neighbours );
  printf("found element %d\n",elem);

  // initialize the viewer
  Viewer viewer;

  // add the mesh to the viewer
  viewer.add(mesh);

  // send the data to the browser
  // this function will return once a connection is established
  //viewer.run();

  return 0;
}
