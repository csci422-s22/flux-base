### lecture 17: mesh parameterization

#### objectives

- compute the 3d coordinates of a surface from a given mesh parameterization,
- compute the parameterization of a triangulated surface using fixed-boundary techniques,
- count the number of connected boundaries in a mesh.

#### motivation

The goal of this lecture is to construct a map from some two-dimensional space $`\mathcal{U}`$ to a 3d surface $`\mathcal{S}`$. In fact, you already did something like this in Project 1, when we mapped our two-dimensional `Grid` from $`[0,1]^2`$ to the unit sphere $`\mathcal{S}^2`$.

Knowing the parameterization of a surface is useful for applications in computer graphics, particularly in *texture mapping*, in which an image is "pasted" onto a surface. For example, supposed we had the image of the Earth and want to paste it onto our sphere mesh (computed in Project 1) - see the image below.

![](../images/earth-parametrization.png)

(*Figure 3.1 in Polygon Mesh Processing, (c) 2007 ACM, Inc.*)

Rendering a particular triangle in a mesh consists of first figuring out which pixels are "covered" by the triangle (more on this when we talk about visualization techniques). For a particular point inside the  triangle (associated with a particular pixel), we can compute the $`u`$ and $`v`$ coordinates of this pixel if we know the $`(u,v)`$ coordinates of the three vertices in the triangle - this can be determined using barycentric interpolation (as in [Lecture 10](https://gitlab.com/csci422-s22/flux-base/-/tree/main/notes/class10)). We can then look up which color should be used for this pixel by looking up the color in the $`(u,v)`$ space associated with the image.

The latter was a rough description of how to paste an image onto a mesh - there are other aspects that need to be considered, such as aliasing effects and minification and magnification, but that is covered in more detail in a computer graphics course (e.g. [CSCI 461](https://philipclaude.gitlab.io/cs461f21/calendar.html)).

Our goal for today is not to actually paste images onto our meshes, but to determine a $`(u,v)`$ parametrization of a given 3d surface automatically so that we could paste an image if we wanted to. For very simple shapes, we might already know a parametrization of a surface, so let's practice with that in the next section.

#### from parameter space to a 3d surface

For simple shapes, we might know a parametrization of the surface, for example, the sphere in Project 1. Other shapes might include a plane, cylinder cone, or torus - see the table below:

| shape | parameterization | $`\mathcal{U}`$ | notes |
| :---: | :---: | :---: | :---: |
| plane | $`\mathbf{p}(u,v) = u\mathbf{t_1} + v\mathbf{t}_2`$ | $`\mathcal{U} \subseteq \mathbb{R}^2`$ | $`\mathbf{t}_1`$ and $`\mathbf{t}_2`$ are two linearly independent vectors on the plane |
| sphere | $`\mathbf{p}(u,v) = [r \sin u \cos v, r\sin u\sin v, r \cos u]`$ | $`\mathcal{U} = [0,\pi] \times [0,2\pi]`$ | $`r`$ is the sphere radius, $`r \in \mathbb{R}`$|
| cylinder | $`\mathbf{p}(u,v) = [r \cos u , r\sin u, v]`$ | $`\mathcal{U} = [0,2\pi] \times [0,h]`$ | $`r`$ is the cylinder radius, $`r \in \mathbb{R}`$ and $`h`$ is the cylinder height, $`h \in \mathbb{R}`$ |
| cone | $`\mathbf{p}(u,v) = [\frac{r(h-v)\cos u}{h} , \frac{r(h-v)\sin u}{h} , v ]`$ | $`\mathcal{U} = [0,2\pi] \times [0,h]`$ | $`r`$ is the base radius, $`r \in \mathbb{R}`$ and $`h`$ is the cone height, $`h \in \mathbb{R}`$
| torus | $`\mathbf{p}(u,v) = [(R+ r \cos v)\cos u, (R+ r\cos v) \sin u, r \sin v]`$ | $`\mathcal{U} = [0,2\pi] \times [0,2\pi)`$ | $`r`$ is the cylinder radius, $`r \in \mathbb{R}`$ and $`R`$ is the radius from the center of the hole to the center of the tube  |

Surfaces can also be parametrized using *Bézier surface patches*, which maps a rectangular parameter space to the 3d surface using *control points*. For example, the Utah teapot (a famous computer graphics model) is represented in the following figure using 32 cubic Bézier patches. The wireframe represents the connectivity between the control points. A nice property of Bézier surface patches is that the actual surface is entirely contained within the convex hull of the control points.

![](../images/utahteapot-oshea.png)

#### from a 3d surface to parameter space

But how do we get a parametrization from a general 3d surface? In other words, given a triangle mesh in 3d, how do we flatten it into some 2d space without causing the triangles to overlap?

The techniques we will focus on will be applicable to a single patch that has a a single boundary without holes. For example, we might want to do something like the following picture:

![](../images/parametrization-1.png)

Or maybe we want the mesh to look like this in the parameter space

![](../images/parametrization-2.png)

Notice that, assuming there no holes for the eyes, nose or mouth, this mesh can be stretched and squished so that it fills a circle. The two parametrizations above differ in how they treat boundaries, which we will discuss next.

#### fixed-boundary techniques

In the lower-right image above, the 3d mesh was mapped to 2d such that the boundary of the original mesh mapped to a square in the parameter space. This method is known as a *fixed-boundary* method, because the boundary of the original mesh is fixed to lie on some prescribed convex polygon. Also known as *barycentric mapping*, this method is made possible by Tutte's embedding theorem [Tutte, 1960]:

*Given a triangulated surface homeomorphic to a disk, if the $`(u, v)`$ coordinates at the boundary vertices lie on a convex polygon, and if the coordinates of the internal vertices are a convex combination of their neighbors, then the $`(u, v)`$ coordinates form a valid parameterization (without self-intersections).*

For a vertex $`i`$, the second part of the theorem can be described mathematically as

```math
-a_{i,i} \left(\begin{array}{c}u_i \\ v_i \end{array}\right) = \sum\limits_{j\neq i} a_{i,j}  \left(\begin{array}{c}u_j \\ v_j \end{array}\right).
```

There are different ways to compute the coefficients, but ultimately, we want the following properties:
* $`a_{i,j} > 0`$ if vertex $`i`$ and vertex $`j`$ are connected by an edge,
* $`a_{i,i} = -\sum\limits_{j\neq i} a_{i,j}`$,
* $`a_{i,j} = 0`$ if vertex $`i`$ and vertex $`j`$ are not connected by an edge.

This gives rise to two systems of equations, both of which involved the same matrix $`\mathbf{A}`$, which need to be solved independently for the $`u`$ and $`v`$ coordinates: $`\mathbf{A} \mathbf{u} = \bar{\mathbf{u}}`$ and $`\mathbf{A} \mathbf{v} = \bar{\mathbf{v}}`$. Here, $`\bar{\mathbf{u}}`$ and $`\bar{\mathbf{v}}`$ are vectors in which the rows corresponding to boundary vertices contain the prescribed boundary coordinates. In those cases, the coefficient of the row of the $`\mathbf{A}`$ should only contain a $`1`$ at $`a_{i,i}`$, and $`0`$ otherwise. This has the effect of setting the equation $`u_b = \bar{u_b}`$ (and also for $`v_b`$) for all boundary vertices with index $b$. Here is some pseudocode which describes this method:

```
1. Pre-process the mesh and make sure there is only 1 connected boundary.
2. Identify all boundary vertices.
3. Calculate the desired parameter space coordinates of each boundary vertex (ubar and vbar)
3. Initialize a sparse matrix A (all entries are zero).
4. For each vertex p:
     a. If p is on the boundary:
          i. set aii = 1
     b. If p is not on the boundary:
          i. extract the one-ring of p
          ii. set sum = 0
          iii. for each vertex q in the one-ring:
            - set aij = some weight, and add to the sum.
          iv. set aii to sum.
5. solve A * u = ubar and A * v = vbar.
```

Now, we need to figure out how to calculate the coefficients of the matrix $`\mathbf{A}`$ in step `4.b.iii`. A simple choice would be to set $`a_{i,j} = 1`$ (thus, $`a_{i,i} = - |\mathcal{R}|`$ where $`\mathcal{R}`$ is the one-ring of the vertex). However, this does not take the mesh geometry into account (edge lengths, angles, areas), so the result can be quite distorted. Two popular alternatives are described below (see the figure below for a description of the angles):

1. **Discrete cotangent Laplacian**: determines the coefficients using the Laplace-Beltrami operator (a generalization of the Laplacian for curved surfaces):
```math
  a_{i,j} = \frac{1}{2A_i} \left( \cot \alpha_{i,j} + \cot\beta_{i,j} \right) \quad A_i\ \mathrm{is\ the\ Voronoi\ area\ of\ vertex\ }v_i
```
2. **Mean value weights**:
```math
  a_{i,j} = \frac{1}{\lvert|\mathbf{x}_i - \mathbf{x}_j\rvert|}  \left( \tan\left(\frac{\delta_{i,j}}{2}\right) + \tan\left(\frac{\gamma_{i,j}}{2}\right) \right)
```
<center>
<img src="../images/parametrization-weights.png" width=500/>
</center>

(*Figure 5.6 in Polygon Mesh Processing*)

Again, remember that $`a_{i,i} = -\sum\limits_{j\neq i} a_{i,j}`$. The *mean value weights* have the advantage that the weights are always positive, which ensures that these weights will always generate a one-to-one mapping (according to Tutte's theorem). The *discrete cotangent Laplacian* can, in fact, produce negative weights for meshes with very obtuse angles which could create an invalid parameterization.

In Step `3`, the coordinates of the boundary vertices (in the parameter space) can be set to whatever convex polygon you like - popular choices include a circle or a square. Some examples of using a different choice for the boundary are shown below for the mesh of a face:

![](../images/parameterization-circle.png)
![](../images/parameterization-square.png)

[source](https://istemi-bahceci.github.io/blog/digital%20geometry%20&%20mesh%20processing/2019/05/31/Mesh-Paramterization.html)

##### project idea
A nice project idea would be to implement and compare (one, or both of) the methods described above for computing the weights. You may also want to investigate the use of different boundary shapes, like a map to a circle versus a map to a square. If you chose this project idea, you should restrict your project to meshes which are homeomorphic to a disk. Please see some additional meshes posted [here](https://gitlab.com/csci422-s22/sample-meshes). Here is an example of a mesh parameterization of the mesh of a lion face using mean-value weights:

<img src="../images/lion0.png" width="300" height="300" /><img src="../images/lion1.png" width="300" height="300" />

#### other methods

Other methods for computing the parameterization of a mesh involve letting the boundary go free instead of fixing it to a prescribed polygon shape. Some of these methods are beyond the scope of the course. Ultimately, they are derived by minimizing an "energy" that quantifies the distortion in the parameterization, using conformal and harmonic maps. A popular method is known as *Least-Squares Conformal Maps* - for more information, please see Section 5.4 in *Polygon Mesh Processing*.

#### exercise

Since the methods we have developed only work for meshes with a single connected boundary, we will write a function to determine how many connected boundaries there are in a mesh. We will again get some practice with using our `HalfEdge` data structure. The idea will be to keep track of which half-edges that have been visited and continuously look for a half-edge on the mesh boundary (that we haven't visited yet), and traverse the `next` halfedge (thus traversing the connected boundary) until we return to the first edge. Please see the solution to this exercise after class.

#### unit testing

A new function has been added to `flux-base/src/halfedges.[h,cpp]` called `nb_connected_boundaries()`, which returns the number of boundaries in the mesh. If you implement the schemes described above for your project, you should use this function to check that your mesh has 1 boundary. Note that a sphere has no boundaries. Please write some unit tests for the `nb_connected_boundaries()` function for at least three meshes: no boundary, 1 boundary and multiple boundaries (like the Beetle mesh).
