#include "element.h"
#include "grid.h"
#include "halfedges.h"
#include "mesh.h"
#include "readwrite.h"
#include "sphere.h"
#include "webgl.h"

#include <map>
#include <cstdio>

using namespace flux;

int
main( int argc, char** argv ) {

  std::unique_ptr<MeshBase> mesh_ptr = read_off("../../flux-base/data/beetle.off");
  Mesh<Triangle>& mesh = * static_cast<Mesh<Triangle>*>(mesh_ptr.get());

  HalfEdgeMesh<Triangle> half(mesh);

  // how many boundaries does this mesh have?
  // hint: think of keeping track of which edges have been visited
  // recall that a boundary edge has edge->face = nullptr
  // and that, for a boundary edge, edge->next is another boundary edge

  Viewer viewer;
  viewer.add(mesh);
  viewer.run();

  return 0;
}
