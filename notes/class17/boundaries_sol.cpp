#include "element.h"
#include "grid.h"
#include "halfedges.h"
#include "mesh.h"
#include "readwrite.h"
#include "sphere.h"
#include "webgl.h"

#include <map>
#include <cstdio>

using namespace flux;

int
main( int argc, char** argv ) {

  std::unique_ptr<MeshBase> mesh_ptr = read_off("../data/beetle.off");
  Mesh<Triangle>& mesh = * static_cast<Mesh<Triangle>*>(mesh_ptr.get());

  HalfEdgeMesh<Triangle> half(mesh);

  std::map<HalfEdge*,bool> visited;
  for (auto& e_ptr : half.edges()) {
    HalfEdge* e = e_ptr.get();
    if (e->face == nullptr) visited[e] = false;
    else visited[e] =  true;
  }

  int nb_connected_boundaries = 0;
  while (true) {

    // find the first edge on a boundary that has not been visited
    HalfEdge* b = nullptr;
    for (auto& e_ptr : half.edges()) {
      HalfEdge* e = e_ptr.get();
      if (e->face != nullptr) continue;
      if (visited[e]) continue;
      b = e;
      break;
    }
    if (b == nullptr) break;

    // mark edges on this boundary as visited until we return to the start
    nb_connected_boundaries++;
    HalfEdge* edge = b;
    do {
      visited[edge] = true;
      edge = edge->next;
    } while (edge != b);
  }
  printf("--> nb connected boundaries = %d\n",nb_connected_boundaries);

  Viewer viewer;
  viewer.add(mesh);
  viewer.run();

  return 0;
}
