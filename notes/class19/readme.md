### lecture 19 - visualization

#### objectives

- identify and describe how mesh data is passed through the graphics pipeline,
- describe the roles of each shader involved in the graphics pipeline,
- transform vertex coordinates from object space to screen space,
- calculate ambient, diffuse and specular terms in a fragment shader.

#### the graphics pipeline

Now that you have a mesh (maybe you generated it or modified it using `flux`), let's see what it looks like! The main goal for today is to give a high-level overview of how we can pass our meshes through a modern graphics pipeline (using `OpenGL` or `WebGL`) so that we can visualize our meshes. In fact, this is how `flux360` works. This will just be a high-level overview - for a more thorough discussion, I would strongly encourage you to take a Computer Graphics course, like [CSCI 461](https://philipclaude.gitlab.io/cs461f21/calendar.html).

Our ultimate goal is to turn our mesh (defined at the simplest level by coordinates and element connectivity), into pixels on a screen. The procedure for doing so is broken up into several stages, outlined below.

<img src="../images/pipeline.png" width=800/>

##### transformations
The gray boxes perform various functions, and the white boxes show which data is passed through the pipeline. Our mesh starts off in its own "object space". This consists of the coordinates of the mesh that you start with, for example, the coordinates you will read from a file. At a very high-level, the vertex coordinates are transformed into (1) world space, (2) camera space, (3) projected onto a canonical viewing volume and (4) into screen space. The transformation to world space consists of any translations or rotations that you will do in your scene (like when you click and drag in `flux360`). Then we specify where the "camera" will be located in world space to give a certain view. We then need to project what our camera sees into a "canonical viewing volume". Depending on your application, you may want a perspective projection (common in graphics) or an orthographic projection (common in engineering). Finally the "near"-plane of our canonical viewing volume is transformed into screen coordinates.

All of this is specified using 4x4 matrices. Specifically the transformation from object space to world space is called a "model matrix", the transformation from world space to camera space is called a "view matrix" and the transformation from camera space to the canonical view volume is called the "projection matrix". The "viewport matrix" or "screen matrix" then transforms the near-plane to the screen where pixels live. The transformation from the object space to the viewing volume is usually combined into a single 4x4 matrix, called the "model-view-projection" (MVP) matrix. The screen transformation is omitted from this matrix because graphics APIs like `WebGL` will do the screen transformation for us.

Transforming normal vectors (which we need for lighting calculations) is a bit different. For a particular transformation $`\mathbf{M}`$, normals are transformed to the same space via $`\mathbf{N} = (\mathbf{M}^{-1})^T`$, i.e. the "inverse-transpose" of the matrix that transforms points. We will perform our lighting calculations in the frame of reference of the camera (in camera space), so our points will be transformed via the "model-view" matrix, and the normals will be transformed by the inverse-transpose of the model-view matrix.

##### rasterization

After the vertex data is processed, the elements (triangles, edges, points) we want to visualize are "rasterized". This consists of figuring out which pixels a transformed triangle "covers" in the screen space. In the image above, the input triangle (outlined with black lines) covers the pixels that are highlighted in blue. Multiple triangles may cover the same pixel, and the projection transformation allows us to know the "depth" of each pixel covered by a particular primitive. We can then keep the fragment that stores the minimum depth, since it will be closest to the camera.

In the fragment processing stage, any vertex data associated with a primitive is interpolated (recall barycentric interpolation from [Lecture 10]()) to produce a value at a particular pixel, thus giving access to an interpolated form of the vertex data within each pixel so that we can perform lighting calculations (or something else at the pixel-level).

#### buffers, attributes, varyings and uniforms

To pass our mesh data through the graphics pipeline, we need to write some data to the GPU. This might be data that is an input to the various stages, like vertex coordinates or triangle connectivity. Or it is data that is constant, such as the model-view-projection matrix. We might also need to manipulate some data and then pass it into a later stage in the graphics pipeline.

In general, **buffers** are used to write data to the GPU. We can then bind an appropriate buffer as an input to a stage in the pipeline via **attributes**. The most common is to specify that we want our vertex coordinates to be an input attribute to the vertex processing stage. The fragment processing stage requires our transformed vertex coordinates (i.e. a multiplication by the MVP matrix). In `WebGL`, we need to write the `gl_Position` variable at some point in the vertex processing stage. A nice convention is to prefix all attributes with `a_`, for example `a_Position` contains the input coordinates of a 3d vertex.

But what if we want to pass some different kind of data from the vertex- to the fragment-processing stage? This can be done by using **varyings**. For example if you need the coordinates of a vertex in the camera space, you would multiply by the model-view matrix, write the result to a varying (e.g. `v_Position`), which will then be passed through the pipeline. Similarly, we can write the normal vectors (multiplying an input normal attribute by the inverse-transpose of the model-view matrix) to a varying (e.g. `v_Normal`), which then be passed as well. These varyings are then interpolated and passed to the fragment processing stage, so that we can perhaps evaluate a lighting model. Again, we will use a convention, specifically that `v_` will be a varying.

Finally, we can also write **uniforms**, which are constants that can be used in either the vertex- or fragment processing stage. For example, the MVP matrix is used for every vertex, so we can specify a `u_ModelViewProjectionMatrix` (using a convention that `u_` refers to a uniform). Uniforms can be integers, floats, vectors and matrices.

#### writing shaders & `GLSL`

Modern graphics pipelines allow us to control how each stage receives and produces data for subsequent stages. In general, things like rasterization are taken care of for us, but APIs like `WebGL` and `OpenGL` provide two types of shaders called a **vertex shader** and **fragment shader** (and others if you use newer versions of `OpenGL`). These shaders are programs that will be compiled and run on the GPU, inputting and outputting whatever attributes or varyings we specify for a particular stage. These shaders are written in a `C`-like language called `GLSL` (OpenGL Shading Language). Just like in `flux`, there are special types for representing vectors and matrices, for example `vec3` (3d vector), `vec4` (4d vector), `mat3` (3x3 matrix) and `mat4` (4x4 matrix). There are also built-in functions like `length` and `normalize`, as well as operators for adding, subtracting and multiplying.

In the vertex shader below, there are two attributes associated with every vertex, a position (the vertex coordinates) and the normal vector at the vertex. We are also specifying the MVP matrix via the `u_ModelViewProjectionMatrix` uniform as well as the transformation of the normal vectors via the `u_NormalMatrix` uniform. The varying `v_Color` will be assigned the normalized coordinates of the vertex. This isn't really a color, but this is just an example. Note the transformations are done using homogeneous coordinates, which is why there is a `1.0` in the transformation of the vertex coordinates. Note that `gl_Position` is a `vec4`, and is the result of the matrix-vector multiplication between the 4x4 MVP matrix and the 4d vector (3d position + homogeneous coordinate).

```GLSL
// here are the attributes passed to the vertex shader
attribute vec3 a_Position;
attribute vec3 a_Normal;

// here are the uniforms passed to the shader program
uniform mat4 u_ModelViewProjectionMatrix;
uniform mat4 u_NormalMatrix;

// we will pass the vertex color to the fragment shader
varying vec3 v_Color;
varying vec3 v_Normal;

void main() {
  // at the very least, you need to assign a special variable called gl_Position
  gl_Position = u_ModelViewProjectionMatrix * vec4( a_Position , 1.0 );

  v_Normal = mat3(u_NormalMatrix) * a_Normal;

  // we need to assign another variable called v_Color
  // since we plan to use it in the fragment shader
  v_Color = a_Position;
}
```

The fragment shader takes in the color written by the vertex shader and simply writes it to the color associated with the fragment. The fourth-component corresponds to the transparency of the fragment, which is set to 1.0 (opaque).

```GLSL
// the inteprolated v_Color varying assigned in the vertex shader gets sent here
varying vec3 v_Color;

void main() {
  // you always need to assign a special variable called gl_FragColor to color the pixel
  gl_FragColor = vec4(v_Color,1.0);
}
```

Before invoking a call through the graphics pipeline, these shaders need to be compiled. If there is a compiler error, it will be displayed as a pop-up in your browser in today's exercise. Please use this information to determine if there is an error in your shader code.

#### lighting

If you open up the [exercise for today](https://csci422-s22.gitlab.io/home/demos/visualization.html), you should see a mesh (with no edges), but the coloring is constant for every pixel in the mesh. This is because a constant color is currently assigned in the fragment shader. To give our mesh a more 3d-like appearance, we need to implement some kind of shading model.

The model we will implement is called the *Phong reflection model* and consists of three terms: ambient, diffuse and specular components. The color that we assign to a pixel will be computed as the sum of these terms:

```math
c = c_a + c_d + c_s.
```

![](../images/phongcomponents.png)

(source: [Wikipedia](https://en.wikipedia.org/wiki/Phong_shading))

The ambient lighting term gives a general "mood" for our scene and is computed as the multiplication of the (r,g,b) components of the ambient reflection coefficient of our surface $`k_a`$ and the ambient light in our scene $`L_a`$. For simplicity, we can take $`L_a`$ to be $`(1,1,1)`$ which represents a white light, and $`k_a`$ to be some fraction of the color associated with our surface. The ambient lighting term is then
```math
c_a = k_a L_a
```
Note that, in contrast to the inner or outer product, this is *component-wise* multiplication. When you invoke multiplication (via `*`) between two vectors in `GLSL`, it will perform component-wise multiplication.

The diffuse term can be computed using Lambert's law:

```math
c_d = k_d L_d \max(0,\mathbf{n}\cdot \mathbf{l})
```

where $`k_d`$ and $`L_d`$ are the diffuse components of the surface material and the light respectively. Note that we need the normal at the surface $`\mathbf{n}`$ and the direction to the light $`\mathbf{l}`$. In our exercise today, we will place the light directly at the position of the camera, so $`\mathbf{l}`$ is the vector from the surface point to the camera, which is simply the opposite direction of the position vector of the surface point. Note that the fragment shader in today's exercise has the `v_Normal` varying as an input, which is the normal vector of the surface, transformed to the camera reference frame.

The ambient and diffuse terms will make our material look matte-like. To make it look more plasticky, we can add a specular term, which will add a spotlight somewhere on the surface. This specular term can be computed using

```math
c_s = k_s L_s (\mathbf{r}\cdot\mathbf{n}) ^ p
```

where $`\mathbf{r}`$ is the reflection of the light direction across the normal:

```math
\mathbf{r} = -\mathbf{l} + 2(\mathbf{l}\cdot\mathbf{n})\mathbf{n}
```

and $`p`$ is the Phong exponent or *shininess*. A higher $`p`$ causes size of the highlight to decrease. The components of the diffuse and specular terms are displayed below (left = diffuse, right = specular):

<img src="../images/lambert-1.png" width=400/><img src="../images/specular.png" width=410/>
(source: *Fundamentals of Computer Graphics*, Shirley 2009)

In the exercise today, we will write some code in the fragment shader to give our meshes a 3d-like appearance with the Phong shading model.

##### cartoon-like shading

We can also do some cartoon-like shading by adding a **silhouette**, and by banding the color we obtain from the Phong model. A silhouette consists of adding a thin black strip in the model where $`\mathbf{e} \cdot \mathbf{n} \le \epsilon`$ where $`\epsilon`$ is some tolerance and $`\mathbf{e}`$ is the vector from the camera (eye) to the point on the surface. In our class exercise, this means that $`\mathbf{e}`$ is the opposite direction of the position vector (in the frame of reference of the camera, i.e. `-v_Position`). Using a value of $`\epsilon = 0.2`$ gives the reult below.

We can also toonify the result by banding the color from the Phong model. You can check the length (using the `length` function) of the color, and modify the result as follows. If the length of the color from the Phong model is greater than 2.0, set the color to 0.9 times the base color of the object (`color`). If it is greater than `1.75`, set the color to 0.7 times the base color. If it is greater than `1.5`, set the final color to 0.5 times the base color. If it is greater than `1.0` set the final color to 0.25 times the base color. Otherwise set the resultant color to 0.1 times the base color. Feel free to experiment with different ranges for the banded colors.

<img src="../images/phong.png" width=300/><img src="../images/phong-silhouette.png" width=300/><img src="../images/toon.png" width=300/>

From left to right: Phong shading, Phong shading + silhouette, Toon shading.

#### `flux360`

You now have a copy of `flux360`, which communicates with our `C++` `Viewer` via a websocket connection. Upon receiving mesh data (coordinates, triangles, edges, parameter coordinates, textures) from the server, a series of buffers are created and the interface provides access to control which buffers are enabled and used during a call through the graphics pipeline. From now on, you can directly open `src/flux-base/flux360/index.html` to connect to a `Viewer`, instead of clicking on the button on the course website.
