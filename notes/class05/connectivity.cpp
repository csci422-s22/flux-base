#include "element.h"
#include "mesh.h"
#include "vertices.h"
#include "webgl.h"

#include <cstdio>
#include <map>
#include <vector>

using namespace flux;

/**
 * \brief represents a structured triangle mesh within [0,1]^2
 */
class TriangleGrid : public Mesh<Triangle> {

public:

  /**
   * \brief Initializes a structured triangle mesh.
   *
   * \param[in] sizes - vector with two values:
   *                  sizes[0] is the number of divisions in x
   *                  sizes[1] is the number of divisions in y
   */
  TriangleGrid( const std::vector<int>& sizes ) :
    Mesh<Triangle>(3), // initialize to 3d even though it's really in 2d
    sizes_(sizes) // save the sizes in the x- and y-directions
  {
    flux_assert( sizes_.size() == 2 );
    build();
  }

  /**
   * \brief Builds the structured mesh, populating vertex coordinates and triangle connectivity.
   */
  void build();

private:
  const std::vector<int>& sizes_; // number of divisions in x- and y-directions
  // why use a vector of sizes? our general Grid class will be templated by element type
  // so Grid<Line> will take in a vector of 1 value, Grid<Tet> will take in a vector of three values, etc.
};

void
TriangleGrid::build() {

  // number of divisions in x- and y-directions
  int nx = sizes_[0];
  int ny = sizes_[1];

  // exercise 1
  flux_implement;

}

int
main( int argc, char** argv ) {

  // create a 10x10 triangle grid
  TriangleGrid mesh( {10,10} );

  // initialize the viewer
  Viewer viewer;

  // add the mesh to the viewer
  viewer.add(mesh);

  // send the data to the browser
  // this function will return once a connection is established
  viewer.run();

  return 0;
}
