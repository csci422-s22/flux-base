### lecture 05: mesh connectivity

#### objectives
- call the `flux` visualizer from your source code,
- represent meshes using a list of vertex coordinates and a list of element indices,
- compute a structured grid of triangles and quadrilaterals,

#### getting started

From now on, you should execute some variant of the following commands at the beginning of every class (from some build directory):

```bash
make update
cmake .
make template_classXY_target
```
For today, the second command will be `make template_class05_connectivity` and `make_template_class05_visualizer`. The `cmake .` command will re-run the `CMake` configuration, which will allow you to have access to the new targets for today's lecture.

#### visualizing meshes in `flux`

When developing meshing algorithms, it often helps to have a mesh visualizer. You may have noticed some files is `src/flux-base/core` called `websockets.cpp`, `webgl.h` and `webgl.cpp`. These files provide functionality for sending our mesh data (and possibly a solution field) to a client via a websocket connection. Our client will always be a web browser on your computer. This means we will be visualizing meshes in your browser!

Throughout the semester, I will maintain the HTML and JavaScript code for our visualizer, which you can access at the following link:

https://csci422-s22.gitlab.io/home/tools/flux360.html

I would highly recommend bookmarking this page! If you click on this page right now, nothing will happen. The reason is because we need to send something to the client first.

Open up `exercises/class05/visualizer.cpp`. To access the visualizer from the `C++` side, you need to include `webgl.h`. There is another include, `readwrite.h`, which gives access to some common functions for reading and writing meshes.

After typing
```bash
make class05_visualizer
```

You will notice a message:

```bash
--> waiting for client (please open https://csci422-s22.gitlab.io/home/tools/flux360.html)...
```
As soon as you open the the visualizer client (clicking on the link above), then you should see the following:

![](../images/flux-visualizer.png)

Take a moment to click around and familiarize yourself with the interface. Also, I'm happy to discuss why I chose to implement a browser-based visualization tool!


#### watertight meshes

For the rest of the class, we will develop in `exercises/class05/connectivity.cpp`. First, create the template:
```bash
make template_class05_connectivity
```
Okay, now it's time to get into representing meshes on a computer. Based on the mesh we just looked at (the collection of triangles defining Spot), how do you think we should represent this collection of triangles?

Storing the vertex coordinates for each element is not very good:
  - elements might tough at vertices, edges (or triangles if 3d), so we would be duplicating information
  - e.g. for a triangle mesh in 3d: that's 3 x 3 x 4 = 36 bytes per triangle using `float`'s' and 3 x 3 x 8 = 72 bytes using `double`'s.

Instead, we can store a single list of vertex coordinates (say, `vertices`) and a single list of element indices (say, `elements`)
  - e.g. coordinates if i-th vertex (for 3d vertices) are `xi = coordinates[3*i]`, `yi = coordinates[3*i+1]`, `zi = coordinates[3*i+2]`
  - e.g. vertex indices of k-th triangle are `i0 = elements[3*k]`, `i1 = elements[3*k+1]`, `i2 = elements[3*k+2]`

We can represent both `vertices` and `elements` using a templated class that represents this 2d array (flattened to 1d):

```c++
template<typename T>
class array2d {

public:
  array2d( int stride );

  int nb() const { return data_.size() / stride_; }

protected:
  int stride_;
  std::vector<T> data_;
};
```

The `stride` represents the offset between data elements. You can think of this as a rectangular matrix with dimensions `nb() x stride_`. We can then write a very basic `TriangleMesh` class:

```c++

class TriangleMesh : public array2d<int> {

public:
  TriangleMesh( int dim ) :
    array2d<int>(3),
    vertices_(dim)
  {}

  array2d<double>& vertices() { return vertices_; }

private:
  array2d<double> vertices_;
};

```

The `TriangleMesh` class inherits from the `array2d` class, and the `array2d` class only provides a constructor that takes in a stride. So we need to specify the make sure the `array2d` constructor is called with the appropriate stride (here, 3 since each triangle has 3 integer indices to the vertex array). Similarly, the vertices are an `array2d` where the stride is the dimension (either 2d or 3d).

How do we read/write data in our 2d array? Let's modify our `array2d` class definition:

```c++

template<typename T>
class array2d {

public:
  array2d( int stride );

  int nb() const { return data_.size() / stride_; }

        T* operator[] ( int i  )       { return &data_[stride_*i]; } // read/write
  const T* operator[] ( int i  ) const { return &data_[stride_*i]; } // read only

        T& operator() ( int i , int j )       { return data_[stride_*i + j]; } // read/write
  const T& operator() ( int i , int j ) const { return data_[stride_*i + j]; } // read only

  void add( const T* d ) {
    for (int i = 0; i < stride_; i++)
      data_.push_back(d[i]);
  }

protected:
  int stride_;
  std::vector<type> data_;
};

```

Sometimes, we might need pointer access, which is what the `[]` operator provides.

In `flux`, we can also represent a "jagged" 2d array by specifying a stride of `-1`. In this case, we need to keep track of two things: the `first` index of the data element, and the `count` of **each** data element. We can see this as a generalization of our original data structure. When the `stride_ > 0` then `first[i] = i*stride_` and `count[i] = stride_`.

I have also included an `add` method, which allows us to add a single element to our table. You should make sure that `d` points to a location in memory with `stride_` values (of type `T`).

#### creating a structured grid

Let's get some more practice and create a *structured* mesh.
What I mean by "structured", is that there is a very predictable way in which we can calculate the vertex coordinates and element indices.
These meshes will be defined in $`[0,1]^d`$ where $`d`$ is the dimension.

For a line mesh in $`3d`$, we could create a structured mesh as follows:

```c++
Mesh<Line> mesh(3); // we'll make a 3d mesh even though it's in 1d
std::vector<double> x(3);
for (int i = 0; i <= nx; i++) {
  x[0] = double(i) / double(nx);
  mesh.vertices().add( x.data() );
}

// now we define the elements
std::vector<int> edge(2);
for (int i = 0; i < nx; i++) {
  edge[0] = i;
  edge[1] = i+1;
  mesh.add( edge.data() );
}
```

Why the template parameter for the mesh?? Most of the algorithms we will develop are independent of the element type we are working with, so our meshes are templated by *element type*. However, these algorithms sometimes need low-level information about the mesh element, such as its edges. Please have a look at `src/element.h` for a definition of all the element types we might work with in this course. Notice that each of these `struct`s has a field called `nb_vertices`. This field is used to determine that `stride` when constructing the `array2d<int>` which represents our mesh connectivity.

That's it! We now have a line mesh. It's a bit more complicated in 2d when we want to create triangles or quads. In those cases, we need to keep track of two parameters: `nx` (the number of division in the x-direction) and `ny` (the number of divisions in the y-direction).

Creating vertices follows the same idea. To create the element indices, we will loop through the *divisions* in each x- and y-directions.

```c++
Mesh<Triangle> mesh(3);

std::vector<double> x(3);
for (int j = 0; j <= ny; j++) {
  for (int i = 0; i <= nx; i++) {
    x[0] = double(i)/double(nx);
    x[1] = double(j)/double(ny);
    x[2] = 0.0;
    mesh.vertices().add( x.data() );
  }
}

// now define the triangles
std::vector<int> triangle(3);
for (int j = 0; j < ny; j++) {
  for (int i = 0; i < nx; i++) {

    // first triangle
    triangle[0] = j * (nx + 1) + i;
    triangle[1] = triangle[0] + 1;
    triangle[2] = triangle[1] + nx + 1;
    mesh.add( triangle.data() );

    // second triangle
    // triangle[0] stays the same
    triangle[1] = triangle[2];
    triangle[2] = triangle[0] + nx + 1; // which is also triangle[1] - 1
    mesh.add( triangle.data() );
  }
}
```

If we wanted to create quads instead, we would have declared our mesh as `Mesh<Quad>`, and we would only create one element as we loop through the divisions:
```c++
quad[0] = j * (nx + 1 ) + i;
quad[1] = quad[0] + 1;
quad[2] = quad[1] + nx + 1;
quad[3] = quad[0] + nx + 1;
```

#### unit testing ideas

Files to unit test by the end of the semester: `grid.cpp`

- check the number of elements is equal to some expected number,
- check the number of vertices is equal to some expected number,
- check the area of our structured meshes is equal to 1 (see Project 1 for info on how to compute triangle areas).
