#include "element.h"
#include "mesh.h"
#include "vertices.h"
#include "webgl.h"

#include <cstdio>
#include <map>
#include <vector>

using namespace flux;

/**
 * \brief represents a structured triangle mesh within [0,1]^2
 *        (this will look a bit different than the template we worked on in class)
 */
class TriangleGrid : public Mesh<Triangle> {

public:
  /**
   * \brief Initializes a structured triangle mesh.
   *
   * \param[in] sizes - vector with two values:
   *                  sizes[0] is the number of divisions in x
   *                  sizes[1] is the number of divisions in y
   * \param[in] dim - dimension of the vertices
   *                  the default is -1 which means the mesh will be in 2d (sizes.size())
   *                  otherwise we can explicitly specifiy if the mesh is in 2d or 3d
   *                  in our in-class template, we assume dim was always 3
   */
  TriangleGrid( const std::vector<int>& sizes , int dim = -1 ) :
    Mesh<Triangle>( (dim < 0) ? sizes.size() : dim ),
    sizes_(sizes)
  {
    build();
  }

  /**
   * \brief Builds the structured mesh, populating vertex coordinates and triangle connectivity.
   */
  void build();

private:
  const std::vector<int>& sizes_; // number of divisions in x- and y-directions
  // why use a vector of sizes? our general Grid class will be templated by element type
  // so Grid<Line> will take in a vector of 1 value, Grid<Tet> will take in a vector of three values, etc.
};

void
TriangleGrid::build() {

  int nx = sizes_[0];
  int ny = sizes_[1];

  double dx = 1./double(nx);
  double dy = 1./double(ny);

  std::vector<double> x(vertices_.dim(),0.0);
  for (int j = 0; j < ny+1; j++) {
    for (int i = 0; i < nx+1; i++) {

      x[0] = i * dx;
      x[1] = j * dy;

      vertices_.add(x.data());
    }
  }

  int t[3];
  for (int j = 0; j < ny; j++) {
    for (int i = 0; i < nx; i++) {

      // this is a bit different than what we did in class
      // but results in the same mesh (the ordering is a bit different)
      //
      //        i3 --------- i2
      //        |   t1    /  |
      //        |       /    |
      //        |     /      |
      //        |   /  t0    |
      //        | /          |
      //        i0 --------- i1
      //      (i,j)
      int i0 = j*(nx+1) + i;
      int i1 = i0 + 1;
      int i2 = i1 + nx+1;
      int i3 = i2 - 1;

      t[0] = i0;
      t[1] = i1;
      t[2] = i2;
      add(t);

      t[0] = i0;
      t[1] = i2;
      t[2] = i3;
      add(t);
    }
  }
}

int
main( int argc, char** argv ) {

  // create the triangle grid
  TriangleGrid mesh( {10,10} ,3 );

  // initialize the viewer
  Viewer viewer;

  // add the mesh to the viewer
  viewer.add(mesh);

  // send the data to the browser
  // this function will return once a connection is established
  viewer.run();

  return 0;
}
