#include "mesh.h"
#include "readwrite.h"
#include "webgl.h"

using namespace flux;

int
main() {

  // read the mesh defined in an OBJ file
  // the working directory for running this program is in test
  // (so ../ goes back to the root and ../data goes into data/)
  std::unique_ptr<MeshBase> mesh_ptr = read_obj( "../data/spot.obj",true);
  MeshBase& mesh = *mesh_ptr.get();

  // initialize the viewer
  Viewer viewer;

  // add the mesh to the viewer
  viewer.add(mesh);

  // send the data to the browser
  // this function will return once a connection is established
  viewer.run();

  return 0;
}
