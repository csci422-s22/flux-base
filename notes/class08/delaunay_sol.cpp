#include "linear_algebra.h"
#include "mat.h"
#include "mat.hpp"
#include "predicates.h"

#include <cstdio>

using namespace flux;

/**
 * \brief Computes the determinant of the matrix used to determine if a point is inside a triangle's circumcircle.
 *
 * \param[in] p0, p1, p2 - the points defining a triangle in CCW order
 * \param[in] q - the query point
 *
 * \return > 0 if q is inside the circumcircle, 0 if it is coincident and < 0 if it is outside
 */
double
delaunay2d_det( const double* p0 , const double* p1 , const double* p2 , const double* q ) {

  mats<3,3,double> m;
  double dx, dy;

  dx = p0[0] - q[0];
  dy = p0[1] - q[1];
  m(0,0) = dx;
  m(0,1) = dy;
  m(0,2) = dx*dx + dy*dy;

  dx = p1[0] - q[0];
  dy = p1[1] - q[1];
  m(1,0) = dx;
  m(1,1) = dy;
  m(1,2) = dx*dx + dy*dy;

  dx = p2[0] - q[0];
  dy = p2[1] - q[1];
  m(2,0) = dx;
  m(2,1) = dy;
  m(2,2) = dx*dx + dy*dy;

  return det(m);
}

/**
 * \brief A helper function to display the result of the circumcircle determinant calculation.
 */
void
delaunay2d( const double* p0 , const double* p1 , const double* p2 , const double* q ) {
  double det = delaunay2d_det(p0,p1,p2,q);
  printf("[inside2d] det = %1.16g (",det);
  if (det > 0.0)
    printf("inside)\n");
  else if (det == 0.0)
    printf("coincident)\n");
  else printf("outside)\n");
}

/**
 * \brief A helper function to display the result of the exact circumcircle determinant calculation.
 */
void
delaunay2d_exact( const double* p0 , const double* p1 , const double* p2 , const double* q ) {
  double det = incircle(p0,p1,p2,q);
  printf("[inside2d_exact] det = %1.16g (",det);
  if (det > 0.0)
    printf("inside)\n");
  else if (det == 0.0)
    printf("coincident)\n");
  else printf("outside)\n");
}


int
main( int argc, char** argv ) {

  // always initialize the predicates!
  initialize_predicates();

  // define a right triangle
  double p0[2] = {0,0};
  double p1[2] = {1,0};
  double p2[2] = {0,1};
  double q[2];

  // test 1
  q[0] = 0.25;
  q[1] = 0.25;
  delaunay2d(p0,p1,p2,q);

  // test 2
  q[0] = 1.0;
  q[1] = 1.0;
  delaunay2d(p0,p1,p2,q);

  // test 3
  q[0] = 1.0;
  q[1] = 0.0;
  delaunay2d(p0,p1,p2,q);

  // test 4
  #if 1
  double L = 1;
  double d = 1e-5;
  #else
  // this allows the coordinates to be represented exactly
  double L = 1e5;
  double d = 1;
  #endif

  p0[0] = 0; p0[1] = 0;
  p1[0] = L; p1[1] = 0;
  p2[0] = L/2; p2[1] = d;

  q[0] =  L/2;
  q[1] = -0.25*L*L/d;

  // go to the next representable number in some desired direction (either towards or away from the circumcenter)
  q[1] = std::nextafter(q[1] , q[1] - 1 ); // +1 to go towards circumcenter, -1 to go away from it

  double* X[4] = {p0,p1,p2,q};
  for (int i = 0; i < 4; i++)
    printf("X(%d) = (%1.20g,%1.20g)\n",i,X[i][0],X[i][1]);
  delaunay2d(p0,p1,p2,q);
  delaunay2d_exact(p0,p1,p2,q);

  return 0;
}
