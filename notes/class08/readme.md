### lecture 08: Delaunay triangulations

#### objectives

- determine whether a point is inside some triangle's circumcircle,
- compute the Delaunay triangulation of a set of points using the Bowyer-Watson algorithm,
- define a few properties of Delaunay triangulations,
- identify issues with Delaunay triangulations.

The meshes we have looked at so far were either structured grids, or they came from external files. The goal for today is to investigate a technique for generating unstructured meshes given a set of points within some domain. We will mostly work in 2d for today!

#### the Delaunay property

The main idea behind the Delaunay triangulation is to create triangles such that no vertex in the mesh lies inside the circumcircle of any triangle it does not belong to. The circumcircle of a triangle is a circle which passes through all three vertices of the triangle. In the left-most figure below, vertex $`p`$ is inside the circumcircle of triangle $`u-v-w`$, so that triangle does not satisfy the Delaunay property. We could "flip" the edge between $`u-w`$ so that it connects $`p`$ and $`v`$, which then makes the triangles satisfy the Delaunay property (on the right).

![](../images/delaunay-0.png)![](../images/delaunay-1.png)

##### properties of the Delaunay triangulation

Delaunay triangulations have a few nice properties:
- they are **unique** (if no more than 3 vertices lie on the same circumcircle),
- they maximize the minimum angle of any interior angle of the triangles in the mesh.

However, one major drawback of Delaunay triangulations is their ability to conform to the boundary of some domain. This means that if you are trying to generate a mesh inside a domain bounded by a complex domain, then the Delaunay triangulation might miss edges that should be along the domain geometry. Ideally, we would like the boundary of our mesh to represent the boundary of our domain as much as possible. In 2d, there are ways to address this without creating additional vertices. There are also robust ways to conform to the domain geometry in 3d, but some domains may require additional vertices (called Steiner vertices). Geometry conformity in higher dimensions is still an open research question.

#### determining if a point is inside the circumcircle of a triangle

To construct Delaunay triangulations, the first thing we need is a way to calculate whether a point is inside a triangle. Given a triangle whose vertices have coordinates $`\mathbf{p}_0`$, $`\mathbf{p}_1`$ and $`\mathbf{p}_2 \in \mathbb{R}^2`$, we can calculate whether a point $`\mathbf{q}`$ is inside the circumcircle of the triangle by calculating the **sign** of the determinant of the following 3x3 matrix:

```math
\left|\begin{array}{ccc}
p_{0,x} - q_x & p_{0,y} - q_y & (p_{0,x} - q_x)^2 + (p_{0,y} - q_y)^2 \\
p_{1,x} - q_x & p_{1,y} - q_y & (p_{1,x} - q_x)^2 + (p_{1,y} - q_y)^2 \\
p_{2,x} - q_x & p_{2,y} - q_y & (p_{2,x} - q_x)^2 + (p_{2,y} - q_y)^2
 \end{array}\right|.
```

Let's now implement and test this calculation - we'll use some tools within `flux-base` for numerical linear algebra. First, have a look at `src/flux-base/src/core/linear_algebra.h` (you will also need to include `mat.hpp` which has the definition of the 3x3 matrix determinant, templated by entry type).

Now, inside `delaunay.cpp` we have defined a right triangle with vertices $`\mathbf{p}_0 = (0,0)`$, $`\mathbf{p}_1 = (1,0)`$ and $`\mathbf{p}_2 = (0,1)`$. Let's test the our determinant calculation with the following values for the point $`\mathbf{q}`$:
1. $`\mathbf{q} = (0.25,0.25)`$,
2. $`\mathbf{q} = (1,1)`$,
3. $`\mathbf{q} = (1,0)`$,

Test 1 should be `inside`. In tests 2 and 3, the point lies exactly on the circle (the determinant is $0$), so this should display as `coincident`.

What does your code return for a point lying on the following circle when $`\delta`$ gets really small relative to the chord length $`\lvert| p_1 - p_0\rvert|`$?
![](../images/delaunay-example.png)

In general, you should not try to compute this determinant yourself. The reason is because when the determinant is near zero, roundoff error in the calculations involved (multiplying, subtracting, adding) can lead to incorrect results. As a result, this could lead to incorrect decisions made in geometric algorithms, like the Delauay triangulation. For the given inputs, we would like to know *exactly* whether a point lies inside, outside or on a circumcircle.

Luckily, there are solutions to this problem, and we will use Jonathan Shewchuk's robust predicates which use adaptive precision to return the correct result. The predicates we will use most often are called `incircle` and `orient2d`. The `incircle` predicate robustly determines whether a point lies in the circumcircle of a triangle and the `orient2d` predicate determines the orientation of a triangle (which also gives twice it's area). For more information, please see [Jonathan Shewchuk's robust predicates](https://www.cs.cmu.edu/~quake/robust.html).

The call to these predicates is wrapped in `predicates.h`, so you will need to include that file whenever you want to use them. You will also need to call `initialize_predicates()` before using them - always remember to do this! In `flux`, the `incircle` predicate has the following signature:

```c++
double incircle( const double* pa , const double* pb , const double* pc , const double* pd );
```

where `pa`, `pb` and `pc` are the points defining the triangle (in CCW order), and `pd` is the query point. Similarly, the `orient2d` function looks like:

```c++
double orient2d( const double* pa , const double* pb , const double* pc );
```
Recall how we calculated area in Project 1. In fact the area of a single triangle with vertex coordinates `pa`, `pb` and `pc` can be robustly computed using `area = 0.5 * orient2d(pa,pb,pc)`.

#### the Bowyer-Watson algorithm

So we can robustly determine if a point is inside a triangle's circumcircle! Now, we can proceed to construct Delaunay triangulations. The algorithm we will use is called the Bowyer-Watson algorithm, which you will implement for your second project. The input to the algorithm is a set of points $`\mathcal{P}`$ in the plane, and the output is a triangulation $`\mathcal{T}`$. The algorithm is incremental, in that it inserts one vertex at a time into an existing Delaunay triangulation. It proceeds as follows:

1. Compute the bounding square (i.e. the minimum and maximum $`x`$ and $`y`$ values of the input points) and create ghost vertices for the points $`(x_{\min},y_{\min})`$, $`(x_{\max},y_{\min})`$, $`(x_{\max},y_{\max})`$ and $`(x_{\min},y_{\max})`$.
2. Create two triangles (called "supertriangles") using the minimum and maximum coordinates from step 1. This is our initial triangulation $`\mathcal{T}`$.
3. Add each point $`p`$ from the input set $`\mathcal{P}`$, one at a time:
  - a. Search the current triangulation for all triangles whose circumcircle encloses $`p`$, call these "broken" triangles $`\mathcal{C}`$.
  - b. Compute the boundary of $`\mathcal{C}`$, i.e. the set of edges that define the polygon boundary of the triangles in $`\mathcal{C}`$.
  - c. Remove all the triangles in $`\mathcal{C}`$ from $`\mathcal{T}`$.
  - d. Insert new triangles into $`\mathcal{T}`$, which are defined by connecting the edges in Step 3b to $`p`$.
4. (optional) Remove any triangles that touch one of the ghost vertices that were created in Step 1.

If your input points already contain vertices on a square, then you don't need to create ghost vertices in Step 1 or remove triangles touching these ghost vertices in Step 4.
The following diagram, from left to right, illustrates each step of the Bowyer-Watson algorithm, in which vertex $`\mathbf{p}_5`$ is being inserted into an existing Delaunay triangulation.

![](../images/meshes-1.svg)![](../images/meshes-2.svg)![](../images/meshes-3.svg)![](../images/meshes-4.svg)


Note that this algorithm has a complexity of $`\mathcal{O}(n_pn_t)`$ where $`n_p`$ is the number of points to insert $`\lvert \mathcal{P}\rvert`$ and $`n_t`$ is the number of triangles. There are ways to perform this algorithm in $`\mathcal{O}(n\log n)`$ time, which requires some additional data structures about triangle-triangle connectivity (as we saw last week). Ultimately, these structures improve the search for broken triangles in Step 3a, but they require attentive bookkeeping when removing and inserting triangles. Here is some data that illustrates the benefit of optimizing the search in the Bowyer-Watson algorithm:

| # points | unoptimized (sec.) | optimized (sec.) |
| :---: | :---: | :---: |
| 1k | 0.033 | 0.0083 |
| 10k | 2.98 | 0.090 |
| 100k | 397.65 | 1.12 |
| 1M | n/a | 17.26 |

Here are some pictures of Delaunay triangulations generated with the Bowyer-Watson algorithm with 1k and 10k random points as input:

![](../images/delaunay1k.png)![](../images/delaunay10k.png)

#### extension to higher dimensions

The Delaunay kernel extends beautifully to higher dimensions. It is a bit more difficult to handle polyhedral (and polytopal) cavity boundaries compared to handling polygonal ones in 2d, but the same idea of the Bowyer-Watson algorithm applies: insert each point by finding broken simplices (triangles, tetrahedra, pentatopes, etc.) and connect the boundary of these simplices to the inserted point. The determinant calculation above, of course, needs to be modified, which becomes a $`(d+1)\times(d+1)`$ determinant. In 3d, this can be computed robustly using the `insphere` predicate.

#### geometry conformity

Delaunay triangulations triangulate the convex hull of the input points and, hence, struggle to conform to the boundaries of a general domain. We won't talk too much about algorithms for dealing with this, but here are some techniques that are often used in practice:
- **Constrained Delaunay Triangulation**: modifies the Delaunay kernel so as to add "constraints" (edges of the input geometry description). A simplex (edge, triangle) is said to be *constrained Delaunay* if there is an open circumdisk that contains no vertex that is visible from a point in the relative interior of the simplex. See Chapter 2.10.2 in *Delaunay Mesh Generation* [Cheng et al. 2012] for further details.
- **Conforming Delaunay Triangulation**: adds additional points along the geometry (a.k.a. Steiner points) so that the resulting mesh is a true Delaunay triangulation. The difficulty lies in deciding where to place these points, and a disadvantage is that the user may not have wanted additional points to be added.

#### unit testing ideas

There is no code added to the `flux-base` directory today that requires testing.
