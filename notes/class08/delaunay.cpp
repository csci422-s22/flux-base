#include "linear_algebra.h"
#include "mat.h"
#include "mat.hpp"
#include "predicates.h"

#include <cstdio>

using namespace flux;

/**
 * \brief Computes the determinant of the matrix used to determine if a point is inside a triangle's circumcircle.
 *
 * \param[in] p0, p1, p2 - the points defining a triangle in CCW order
 * \param[in] q - the query point
 *
 * \return > 0 if q is inside the circumcircle, 0 if it is coincident and < 0 if it is outside
 */
double
delaunay2d_det( const double* p0 , const double* p1 , const double* p2 , const double* q ) {

  // fill in, and then compute the determinant of the following 3x3 matrix (of doubles)
  // provides entry read/write access like m(i,j) for row i, column j
  mats<3,3,double> m;

  flux_implement;
}

/**
 * \brief A helper function to display the result of the circumcircle determinant calculation.
 */
void
delaunay2d( const double* p0 , const double* p1 , const double* p2 , const double* q ) {
  double det = delaunay2d_det(p0,p1,p2,q);
  printf("[inside2d] det = %1.16g (",det);
  if (det > 0.0)
    printf("inside)\n");
  else if (det == 0.0)
    printf("coincident)\n");
  else printf("outside)\n");
}

int
main( int argc, char** argv ) {

  // always initialize the predicates!
  initialize_predicates();

  // define a right triangle
  double p0[2] = {0,0};
  double p1[2] = {1,0};
  double p2[2] = {0,1};
  double q[2];

  return 0;
}
