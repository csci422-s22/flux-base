#include <memory>
#include <set>
#include <cstdio>

/**
 * A simplified version of our HalfEdge structure.
 */
struct HalfEdge {
  double cost;
  ~HalfEdge() { printf("I am being deleted (cost = %g)!\n",cost); }
};

inline bool operator< ( const std::unique_ptr<HalfEdge>& lhs , const HalfEdge* rhs ) {
  return std::less<const HalfEdge*>()(lhs.get(),rhs);
}

inline bool operator< (const HalfEdge* lhs , const std::unique_ptr<HalfEdge>& rhs ) {
  return std::less<const HalfEdge*>()(lhs,rhs.get());
}

int
main() {

  // represents a simplified version of the HalfEdgeMesh container (only holds HalfEdge's)
  std::set< std::unique_ptr<HalfEdge> , std::less<> > edges;

  // add 5 edges to the container
  for (int i = 0; i < 5; i++)
    edges.insert( std::make_unique<HalfEdge>() );

  // assign the cost of all edges
  double cost = 100;
  for (auto& e : edges) {
    e->cost = cost++;
    printf("cost = %g\n",e->cost);
  }

  // comparison used to determine order in priority queue
  struct CompareCost {
    bool operator() ( const HalfEdge* e1 , const HalfEdge* e2) const {
      return e1->cost < e2->cost;
    }
  };
  std::multiset<HalfEdge*,CompareCost> pq;

  // add the first edge in the container to the priority queue
  HalfEdge* e = edges.begin()->get();
  pq.insert(e);

  // delete this edge from the simplified HalfEdgeMesh container
  auto it = edges.find(e);
  assert( it != edges.end() );
  edges.erase(it);

  // print the cost of the edge with the highest priority
  e = * pq.begin();
  printf("top edge cost = %g, queue size = %lu\n",e->cost,pq.size());
  //pq.erase(e); // uncomment to show even more memcheck errors

  return 0;
}
