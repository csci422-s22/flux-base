### constructive solid geometry

#### objectives

- represent complex shapes from simpler ones using binary set operations: union, difference, intersection,
- use a binary space partition to perform discrete solid modeling operations,
- intersect polygons with planes, creating two polygons on either side of the plane.

Constructive Solid Geometry (CSG) consists of building more complicated shapes from simpler shapes (like a box, cylinder or sphere). Each of these primitive shapes bounds the interior of some interior volume (subsets of $`\mathbb{R}^3`$), so we can perform set operations to build up these complicated shapes.

Our discussion today will be brief. We will only cover a high level overview of the algorithms and data structures involved, and implementing a solid modeler (discretely) will make for a great final project.

Here are some examples of the shapes you can achieve with a CSG modeler, and how they are represented as a tree of binary operations. As with sets, the notation $`\cup`$ is used to represent a union, $`\cap`$ is used to represent intersection, and $`-`$ is used to represent subtraction (set difference).

<img src="../images/csg-tree.png" width=400/>
<img src="../images/csg.jpeg" width=400/>


#### binary space partitions

Instead of using analytic descriptions of the primitive shapes, we can represent each shape using a mesh of polygons. Whenever performing binary operations between two solids, we can determine which region should be included in the final shape by determining the "side" of each polygon with respect to all the other polygons. For each polygon (which is planar), we can compute a plane passing through the polygon and then check which side of this plane the other polygons are. This sounds like an $`\mathcal{O}(n^2)`$ algorithm since we are checking each polygon's plane against every other polygon. Luckily we have a very efficient data structure for handling these kinds of "which side of the plane is a polygon on" queries: a binary space partition.

A binary space partition (BSP) represents our polygon soup as a binary tree. Each node in the tree (including the root) would be some polygon, and each of its (two) children would represent the set of polygons that are ahead or behind the plane defined by the node polygon. Building the tree involves determining which polygons are in front of, or behind this plane. The rough description of the algorithm would be:

1. Pick a starting polygon $p$ (picking the first polygon is fine) and define its plane $`\mathcal{P}`$. Denote the space ahead of $`\mathcal{P}`$ as $`\mathcal{P}^+`$ and the space behind as $`\mathcal{P}^-`$.
2. Initialize a "front" and "back" array of polygons (these will go into the two children of the node).
3. Loop through all the polygons $`q`$:
  a. Determine the side of each vertex in $`q`$
  b. If the side of each vertex is the same, place $`q`$ into the appropriate front, or back array.
  c. If the side differs, then $`\mathcal{P}`$ intersects $`q`$, so split $`q`$ into two polygons (a front and back polygon), and place them into the appropriate front and back arrays.
4. Build the "front" and "back" children from the arrays of "front" and "back" polygons.

Given a plane $`\mathcal{P}`$ with normal vector $`\mathbf{n}`$ and point $`\mathbf{c}`$, the "side" of a point in $`q`$ (we'll call it $`\mathbf{v}`$ since it is a vertex of $`q`$) with respect to $`\mathcal{P}`$ can be computed from the sign of:

```math
s = \mathbf{n} \cdot \mathbf{v} - \mathbf{n}\cdot \mathbf{c}.
```

If $`s < 0`$ then $`\mathbf{v}`$ is behind $`\mathcal{P}`$, otherwise it is in front of $`\mathcal{P}`$. Of course, this result alternates if we flip the direction of $`\mathbf{n}`$ (which we will do for certain CSG operations). You have to be careful with finite precision arithmetic, so it's good to check if $`|s| < \epsilon`$. A tolerance of $`\epsilon = 10^{-5}`$ works fine.


#### intersecting polygons with planes

When a plane intersects a polygon, we obtain two polygons that need to be added to the appropriate list of front and back polygons during the recursive building of the BSP tree. This situation is sketched below.

<img src="../images/csg-1.svg" width=500/>
<img src="../images/csg-2.svg" width=500/>

The intersection points (the red points in the diagram) can be determined by looping through the edges of the current polygon and determining if vertex `vi` and `vj` have opposite signs (determined by the equation above). If so, then we need to compute the coordinates of the intersection point. In  `flux` this can be done via:

```c++
double t = (dot(normal,c) - dot(normal,vi)) / dot(normal,vj - vi);
vec3d v = vi + t * (vj - vi);
```
where `normal` is the plane normal and `c` is a point on the plane. The points `vi` and `vj` are `vec3d` objects. The resulting intersection point `v` should then be added to both front (red) and back (blue) polygons.

A great example of using this BSP technique for CSG is given [here](https://github.com/evanw/csg.js). A good final project would be to implement this functionality within `flux`. The final step in implementing the BSP-CSG algorithm would be to triangulate the resultant polygons. This can be done with a similar technique we used when studying Voronoi diagrams. The triangulation won't be the nicest, but it'll be okay to visualize our complex geometries. Here is an example implemented in `flux` with and without edges enabled in the visualization:

<img src="../images/csg-flux.png" width=400/>
<img src="../images/csg-flux-edges.png" width=400/>

This example consists of $`( \mathcal{B} - \mathcal{S}_i ) \cap \mathcal{S}_o`$ where $`\mathcal{B}`$ is a box, $`\mathcal{S}_i`$ is an inner sphere and $`\mathcal{S}_o`$ is an outer sphere.

#### unit testing ideas

Nothing has been added to `flux-base` that requires testing today.
