### lecture 11: subdivision surfaces

#### objectives

- define a smooth curve or surface as the limit of a sequence of repeated divisions,
- subdivide meshes using Catmull-Clark (quads) and Loop (triangles) subdivision schemes.

You may not always be given a smooth description of 3d surface - you might only be given a coarse mesh with vertices and elements (triangles or quads), with no description of control vertices.

Unless your goal is to produce an artistic "low-poly" representation of the geometry, you might want to produce a smoother representation of this coarse mesh. In fact, there are ways of subdividing curves and surfaces in such a way that the limit curve/surface is smooth. This concept was first used in Pixar's "Geri's game" (1997), and introduces the concept of subdivision surfaces. Watch the Pixar short [here](https://www.youtube.com/watch?v=uMVtpCPx8ow) and observe the surfaces used to represent Geri!

<img src="../images/geris-hand.png" width="530"/> <img src="../images/geris-game.jpg" width="350"/>

Today's topic will make for an excellent final project! It was originally supposed to be one of our projects in the course and studies a very important problem in geometric modeling.

#### subdivision curves

Before we look at subdivision surfaces, let's first consider the case of subdividing discretized curves. Producing a smoother description of a discretized curve can be done using (1) corner cutting, (2) interpolation or (3) approximation schemes.

##### Chaikin's corner cutting

Chaikin's corner cutting scheme consists of removing old vertices (corners), and introducing new vertices along existing edges and then connecting the dots. In a nutshell,

1. insert two new vertices at 1/4  and 3/4 of the endpoints of each edge,
2. remove all old vertices,
3. connect the new vertices.

This procedure converges to a **quadratic** B-spline, which is absolutely magic!

#### interpolation

Alternatively, we can interpolate the two points on an edge along with the two points on it's two neighboring edges, interpolate these four points with a cubic curve, and evaluate the interpolated curve at the midpoint of the current edge. Special care is needed at boundary edges, i.e. you can use a quadratic interpolation for the three points (2 on the edge + 1 on the neighboring edge). The old vertices would then be retained and connected to the new points to produce a smoother representation of the curve.

#### approximations

Instead of either discarding old vertices or keeping them fixed, we can move them too. The idea of approximation schemes is to split each edge into two segments, thereby introducing new vertices, and then computing new coordinates for the original vertices from some simple rule. The **Catmull-Clark** subdivision rule consists of:

1. insert new points (gray) at the midpoints of the current edges,
2. compute new coordinates for every existing point (red) as
```math
  \mathbf{p}_i = \frac{1}{8} \mathbf{p}_{i-1} + \frac{3}{4}\mathbf{p}_i + \frac{1}{8}\mathbf{p}_{i+1},
```
3. connect all the gray dots with the red dots, roughly doubling the number of points.

The weights and contributing points used to compute the coordinates of each new (gray) and existing (red) point are shown in the figure below. This process converges to a **cubic** B-spline curve!

<center>
<img src="../images/subdivision-1.svg" width="400"/>
</center>

Please visit [this demo](https://csci422-s22.gitlab.io/home/demos/demos.html#subdivision) to investigate corner cutting and the Catmull-Clark subdivision scheme.

#### subdivision surfaces

##### Catmull-Clark subdivision (quads)

Surface meshes can be subdivided using simple rules too. The same idea applies: (1) start with a coarse mesh, (2) introduce new points and (3) use some rule to determine coordinates of all vertices. For quadrilateral meshes, we introduce 1 new vertex along every edge and 1 new vertex on the interior of every quad. The Catmull-Clark subdivision rule then consists of:

- new **interior quad** vertices (blue) receive the average of the four coordinates of the quad,
- new **interior edge** vertices (blue) receive a weighted average shown below,
- new **boundary edge** vertices (blue) receive the average of the edge endpoint coordinates,
- existing **boundary vertices** (red outline) receive new coordinates depending on the coordinates of the edge endpoints as in the figure below,
- existing **interior vertices** (red outline) receive new coordinates depending on the quadrilaterals that surround the vertex (see below).

Note that $`k`$ is the number of vertices surrounding an interior vertex; $`\alpha`$ and $`\beta`$ are parameters that depend on the vertex valency (see [here](https://en.wikipedia.org/wiki/Catmull%E2%80%93Clark_subdivision_surface) for a description of how to update vertex positions).

![](../images/subdivision-2.svg)

##### Loop subdivision (triangles)

In the case of triangle meshes, new vertices are introduced along all edges of the triangulation. Thus, no "interior" triangle vertices are introduced, and **every triangle is divided into four new triangles** with each subdivision level. The Loop subdivision rules consist of

- new interior edge vertices (blue) receive the coordinates as a weighted sum (weights shown on vertices) of surrounding vertices shown below,
- new boundary edge vertices receive the average coordinates of the edge endpoint coordinates,
- existing boundary vertices (red outline) receive the weighted average of the edge endpoint coordinates,
- existing interior vertices (red outline) receive the weighted average of the coordinates of neighboring vertices (below, on the right).

Note that $`k`$ is the number of triangles (equivalently neighboring vertices) surrounding an interior vertex and

```math
\beta = \left\{ \begin{array}{cl} 3/16 & k = 3, \\ 3/(8k) & \mathrm{else}. \end{array}\right.
```

Other options for $k$ are possible, but the above scheme works well.

![](../images/subdivision-3.svg)

#### exercise: Loop subdivision by hand

Please see the accompanying file `subdivision.cpp` in which we will do some exercises to practice with Loop subdivision by hand.

<center>
<img src="../images/loop.svg" width="600"/>
</center>

#### setting up data structures for Loop subdivision

It's possible to perform Loop subdivision using a basic mesh connectivity data structure. When repositioning the vertices, you will want a vertex-to-vertex data structure to compute the new coordinates using the one-ring vertices of each vertex - see the exercise from last time for practice on this. The more difficult parts are (1) computing the coordinates of the vertices inserted on the edges and (2) the indices of the four new triangles formed within each old triangle.

My suggestion is to build up an edge-based data structure which keeps track of (1) the indices of the edge endpoint vertices and (2) the indices of of the opposite vertices. You should accompany this data structure with a triangle-to-edge data structure. This is needed because if you perform insertions on edges (in the order of the sequence of edges) then you will know exactly the index of the point inserted along the edge.

![](../images/subdivision-4.svg)![](../images/subdivision-5.svg)![](../images/subdivision-6.svg)

Of course, you can use a half-edge based data structure if you like.

#### further reading

- Charles Loop's [Master's thesis](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/thesis-10.pdf) on subdivision surfaces,
- I really encourage you to download and play around with Pixar's open-source `OpenSubdiv` ([link](http://graphics.pixar.com/opensubdiv/docs/intro.html), [GitHub](https://github.com/PixarAnimationStudios/OpenSubdiv)), which allows you to explore subdivision surfaces. After compiling the code, try running the `glViewer` utility in which you can pick a surface mesh, a number of "levels" and use either the Catmull-Clark or Loop schemes to compute a smooth approximation to the surface.

#### unit testing

Nothing has been added to `flux-base` that requires testing today.
