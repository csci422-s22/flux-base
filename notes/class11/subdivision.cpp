#include "error.h"

#include <cstdio>
#include <iostream>
#include <vector>

using namespace flux;

std::vector<double> points = {
  0,-1, // v0
  1,0,  // v1
  0,1,  // v2
  0,2,  // v3
  2,1,  // v4
  2,0,  // v5
  1,-1, // v6
  1,1,  // v7
  0,0,  // v8
  -1,0, // v9
  -1,1, // v10
  1,2   // v11
};
const int nb_points = points.size() / 2; // = 12

std::vector<int> triangles = {
  0,6,1,  // t0
  8,7,2,  // t1
  1,7,8,  // t2
  4,7,1,  // t3
  1,5,4,  // t4
  11,2,7, // t5
  11,3,2, // t6
  9,8,2,  // t7
  0,1,8,  // t8
  9,2,10  // t9
};
const int nb_triangles = triangles.size() / 3; // = 10

std::vector<int> edges = {
  0,6,  // e0
  1,6,  // e1
  0,1,  // e2
  1,8,  // e3
  7,8,  // e4
  2,8,  // e5
  2,9,  // e6
  8,9,  // e7
  2,10, // e8
  9,10, // e9
  7,11, // e10
  3,11, // e11
  2,11, // e12
  2,7,  // e13
  4,7,  // e14
  1,4,  // e15
  4,5,  // e16
  1,5,  // e17
  1,7,  // e18
  2,3,  // e19
  0,8   // e20
};
const int nb_edges = edges.size() / 2; // = 21

/**
 * Returns the point opposite the edge in a triangle t.
 * In other words, for a triangle t with indices (i0,i1,i2) and an edge e with indices (p,q)
 * t should have indices p and q somewhere in i0, i1, i2
 * We want to know the which of (i0,i1,i2) is NOT equal to p or q.
 * Assumes (p,q) is indeed an edge of the triangle t (but no check is performed).
 *
 * \param[in] p, q - indices of the two vertices of the edge
 * \param[in] t - index of the triangle (in triangles array above)
 *
 * \return vertex index in t that is not equal to either p or q
**/
int
getOppositePoint( int p , int q , int t ) {
  return -1;
}

int
main( int argc, char** argv ) {

  // example 1: getting the opposite point of edge e0 = (0,6) that is in triangle 0
  flux_assert( getOppositePoint(0,6,0) == 1 );

  // exercise 1a: get both vertex indices "opposite" edge e6
  // and print them out

  // exercise 1b: get both vertex indices "opposite" edge e18
  // and print them out


  // exercise 2: compute the coordinates of the point to insert along edge e4
  // using the Loop subdivision scheme, and print the coordinates


  // exercise 3a: compute the indices of the four triangles obtained when  subdividing triangle t1
  // note: assume edge points have been added in order of edges
  // print the vertex indices of the four new triangles


  // exercise 3b: compute the indices of the four triangles obtained when subdividing triangle t2
  // note: assume edge points have been added in order of edges
  // print the vertex indices of the four new triangles

  return 0;
}
