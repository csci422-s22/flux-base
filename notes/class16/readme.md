### lecture 16 - linear solvers

#### objectives

- represent sparse matrices using triplets and CSR formats,
- implement the Jacobi and Gauss-Seidel algorithms to solve diagonally-dominant system of linear equations,
- implement the Conjugate Gradient algorithm to solve a system of equations involving sparse, symmetric matrices.

#### model problem

To motivate the manipulation of sparse matrices and corresponding linear solvers, it's a good idea to know how these types of matrices come up in practice. Some of the following is beyond the scope of the course (and requires some knowledge of differential equations), and simply serves as motivation.

Suppose we want to solve for $`u`$ in the $`[0,1]`$ domain, governed by the following equation:
```math
-\frac{\mathrm{d}^2u}{\mathrm{d}x^2} = f(x) \quad\mathrm{with}\quad  u(0) = 0,\ u(1) = 0
```

To solve this equation numerically, we can introduce a line grid (with $`n`$ elements) in which points are separated by some constant spacing $`h = 1/n`$ (representing $`\Delta x`$) and make a **finite-difference** approximation for the second derivative of $`u`$. With this approximation to the second derivative, we need then need to satisfy (at each internal point in our grid):

```math
- \frac{\mathrm{d}^2u}{\mathrm{d}x^2} \approx - \frac{u_{j+1} - 2u_j + u_{j-1}}{h^2} = f(x_j)
```
with $u_0 = 0$ and $u_{n} = 0$ (there are $`n+1`$ points in the grid). Don't worry where this equation comes from (but I'm happy to show you the derivation if you're interested!). The following figure displays the coupling between all the $`u`$ values:

![](../images/poisson.svg)

So we have $`n+1`$ equations and $`n+1`$ unknowns: there are $`(n-1)`$ interior equations and 2 *boundary conditions* (equations that describe what the solution should be on domain boundaries, here at $`x = 0`$ and $`x = 1`$). Technically, we *do* know the values for $`u_0`$ and $`u_n`$, but we will treat them as unknowns and use the equation $`u_0 = 0`$ to "solve" for $`u_0`$. This will give a more general solution procedure.

To solve for the unknown $`u`$ values, we will represent our system of equations in matrix form:

```math
\frac{1}{h^2} \left[ \begin{array}{rrrrrrrrr} 1 & 0 & 0 & 0 & \dots & 0 & 0 & 0 & 0 \\
-1 & 2 & -1 & 0 & \dots & 0 & 0 & 0 & 0 \\
0 & -1 & 2 & -1 & \dots & 0 & 0 & 0 & 0\\
0 & 0 & -1 & 2 & \dots & 0 & 0 & 0 & 0\\
\vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots & \vdots & \vdots \\
0 & 0 & 0 & 0 & \dots & -1 & 2 & -1 & 0 \\
0 & 0 & 0 & 0 & \dots & 0 & -1 & 2 & -1 \\
0 & 0 & 0 & 0 & \dots & 0 & 0 & 0 & 1
\end{array}\right]
\left[ \begin{array}{c}
u_0 \\ u_1 \\ u_2 \\ u_3 \\ \vdots \\ u_{n-2} \\ u_{n-1} \\ u_{n}
\end{array}\right] =
\left[ \begin{array}{c}
0 \\ f_1 \\ f_2 \\ f_3 \\ \vdots \\ f_{n-2} \\ f_{n-1} \\ 0
\end{array}\right]
```
where the right-hand-side values $`f_j`$ ($`1 \le j \le n-1`$) are obtained by evaluating the "forcing function" at each internal grid point: $`x_j = j h`$.

Great, so we have a matrix to work with for today! Let's look at ways of representing this matrix on a computer, and how we can solve the system of equations above.

#### representing sparse matrices

Notice how there are a lot of zeros in the matrix above. It would be wasteful to store all of these zero entries in memory. In fact, the matrix on the left-hand-side of the system above is **sparse**, which means there are only a handful of **nonzero** entries in each row. In our example, the maximum number of nonzero entries in each row is 3 because each equation couples the solution of (at most) 3 grid points. We would only like to store the non-zero entries. As an example, we will work with the following matrix:

```math
\mathbf{A} = \left[\begin{array}{llll} 0.0 & 1.1 & 0.0 & 0.0 \\
2.2 & 0.0 & 3.3 & 4.4 \\
0.0 & 5.5 & 0.0 & 6.6 \\
0.0 & 7.7 & 8.8 & 9.9
\end{array}\right]
```

##### storing triplets
The simplest way to keep track of the nonzero entries is to store triplets: `(row,column,value)`. In other words, we only store the row and column indices of the nonzero entries, as well as the nonzero value itself. In `flux`, there is a class called `spmat` (`src/flux-base/src/core/spmat.h`) which stores a `map` in which the key is a `pair` and the value is the matrix entry value. With this format, our example matrix $`\mathbf{A}`$ would be represented as:

![](../images/triplet.png)


##### compressed row storage (CRS)

The *Compressed Row Storage* (CRS) format is a popular data structure for representing sparse matrices. It uses three arrays to represent the nonzero entries in the matrix. The first array (`v`) contains all the nonzero values, stored in row-major order. The second array `colind` contains all the column indices of the entries and, hence, has the same length as `v`. The third array contains the start and end index of each row, as stored in `v` and `colind`. The CRS format for our example matrix $`\mathbf{A}`$ would be

![](../images/crs.png)


#### iterative solvers

Now that we can represent sparse matrices on a computer, let's look at how to solve system of equations involving sparse matrices, as in our model problem. The class of solvers we will study are called **iterative** solvers, because they make an initial guess at the solution to the linear system, and keep updating the current guess until some *convergence* criteria is met. In the following algorithms, we will assume we are solving a linear system of the form

```math
\mathbf{A}\mathbf{x} = \mathbf{b}.
```

We will start with some initial guess $`\mathbf{x}^0`$ - superscripts will denote the iteration, and subscripts will denote a particular component in the vector, e.g. $`x_i^k`$ is the `i`-th component of vector $`\mathbf{x}`$ at iteration `k`.

##### Jacobi algorithm

The Jacobi method iteratively makes the update:

```math
x_i^{(k+1)} = \frac{1}{a_{i,i}} \left( b_i - \sum\limits_{j \neq i} a_{i,j} x_j^{(k)}\right)
```

until the norm $`\lvert| \mathbf{A}\mathbf{x} - \mathbf{b}\rvert|`$ is less than some tolerance `tol` (or if a cap on the number of iterations is reached).

In pseudocode, this looks like:

```
k = 0
x = some initial guess
while || A x - b || > tol && k < max_iter
    y = vector of size n
    for i = 1 to n
        y_i = update from equation above
    end
    x = y
    k = k + 1
end
```

In the case of our model problem, the update is $`y_i = (f_i - (-x_{i-1} - x_{i+1})/h^2) (h^2/2)`$.

The Jacobi method converges if the matrix $`\mathbf{A}`$ is *diagonally-dominant*, that is $`\lvert a_{i,i}\rvert > \sum\limits_{j\neq i}\lvert a_{i,j}\rvert`$

##### Gauss-Seidel algorithm

The Gauss-Seidel algorithm makes a minor modification to the Jacobi algorithm - it directly updates $`\mathbf{x}`$ during the loop through the rows:

```math
x_i^{(k+1)} = \frac{1}{a_{i,i}} \left( b_i - \sum\limits_{j =1}^{i-1} a_{i,j} x_j^{(k+1)} +  \sum\limits_{j =i+1}^{n} a_{i,j} x_j^{(k)}\right).
```
Although the summations are expressed over the entire row, it should be understood that only the non-zero components are involved in the update. In this case, the Gauss-Seidel algorithm does not need a second vector `y` to update the solution:

```
k = 0
x = some initial guess
while || A x - b || > tol && k < max_iter
    for i = 1 to n
        x_i = update from equation above
    end
    k = k + 1
end
```

The Gauss-Seidel method will also converge if the matrix $`\mathbf{A}`$ is diagonally-dominant, or if it is symmetric and positive-definite. However, note that the algorithms will fail if there is a diagonal entry with a value of 0.

##### Conjugate Gradients

The Jacobi and Gauss-Seidel methods are unfortunately slow to converge. For symmetric matrices ($`\mathbf{A}^T = \mathbf{A}`$), we can converge much faster using the *Conjugate Gradients* (CG) algorithm. The theory behind this algorithm is beyond the scope of the course, and is something called a *Krylov subspace method*. The algorithm iteratively uses a search direction that is orthogonal to previous search directions in terms of the inner product induced by the matrix $`\mathbf{A}`$. The search direction is called $`\mathbf{p}`$, and the inner product of two vectors $`\mathbf{u}`$, $`\mathbf{v}`$ induced by $`\mathbf{A}`$ is $`\mathbf{u}^T\mathbf{A}\mathbf{v}`$. The algorithm is outlined below:

```
k = 0
x = some initial guess
p = b - A * x
r = p
while ||A * x - b|| > tol && k < max_iter
    alpha = dot(r,r) / dot( p ,  A * p )
    x     = x + alpha * p
    r_new = r - alpha * A * p
    beta  = dot(r_new,r_new) / dot(r,r)
    p     = r_new + beta * p
    r     = r_new
    k     = k + 1
```

Note that each iteration of the CG algorithm only requires the matrix-vector multiplication $`\mathbf{A}\mathbf{x}`$, which can be performed efficiently since we know that $`\mathbf{A}`$ is sparse.

### example

Let's now return to our model problem. In class, we will develop the Jacobi and Gauss-Seidel algorithms to solve our system and compare the solution with the exact solution. The `spmat` class provides a `solve_nl`function which can also be used to solve a sparse system of linear equations. This function uses the `OpenNL` library developed at INRIA (see [here](http://alice.loria.fr/software/geogram/doc/html/nl_8h.html#details)).

We will look at two examples:
1. $`f(x) = 1`$, which has an analytic solution of $`u(x) = \frac{1}{2}x(1-x)`$.
2. $`f(x) = \pi^2 \sin (\pi x)`$ which has an analytic solution of $`u(x) = \sin(\pi x)`$.

Note that both of these examples have solutions which satisfy the $`u(0) = 0`$ and $`u(1) = 0`$ boundary conditions.

#### unit testing

You don't need to write tests for the code added to `flux` today. Some code has been added to `flux-base/src/core`, but I have already provided the tests in `src/flux-base/test/core/spmat_ut.cpp`.
