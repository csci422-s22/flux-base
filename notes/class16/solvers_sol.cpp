#include "spmat.h"
#include "vec.h"

using namespace flux;

// define which test case to use
// setting RHS_SINE to 1 uses the u(x) = sin(pi*x), f(x) = pi^2 sin(pi*x) problem
// setting RHS_SINE to 0 uses the u(x) = 0.5*x*(1 - x), f(x) = 1 problem
#define RHS_SINE 1

/**
 * \brief Sets up the A matrix and RHS vector b for the solution of Poisson's equation u"(x) = -f(x)
 * with homogeneous Dirichlet boundary conditions at x = 0 and x = 1, i.e. to solve A * u = b.
 *
 * \param[in]    n - number of edges to use in the discretization
 * \param[inout] A - (n+1) x (n+1) sparse matrix resulting from the discretization
 * \param[inout] f - right-hand-side vector
 */
void
get_model_problem( int n , spmat<double>& A , vecd<double>& f ) {

  flux_assert( f.m() == n+1 );
  double h = 1.0/n;

  A(0,0) = 1;
  f(0)   = 0;
  for (int i = 1; i < n; i++) {
    A(i,i-1) =  -1 / (h*h);
    A(i,i)   = 2 / (h*h);
    A(i,i+1) = -1 / (h*h);

    #if RHS_SINE
    double x = double(i)/n;
    f(i) = M_PI * M_PI * sin( M_PI*x );
    #else
    f(i) = 1;
    #endif
  }
  A(n,n) = 1.0;
  f(n)   = 0.0;
}

/**
 * \brief Sets up and solves the model problem to some tolerance and maximum number of iterations,
 *        using either Jacobi or Gauss-Seidel updates.
 *
 * \param[in] n - number of edges to use in the discretization.
 * \param[in] tol - tolerance to use in convergence test.
 * \param[in] max_iter - maximum number of iterations to use in Jacobi/Gauss-Seidel algorithms.
 *
 * \return the solution to the model problem u"(x) = -f(x) in [0,1]
 */
vecd<double>
solve_model_problem(int n, double tol , int max_iter) {

  spmat<double> A(n+1,n+1);
  vecd<double> f(n+1);

  get_model_problem(n,A,f);
  if (n < 30) A.print_full();

  vecd<double> x(n+1);
  int iter = 0;
  double e = norm(A*x - f);
  while (e > tol && iter++ < max_iter) {

    #if 0
    // jacobi
    vecd<double> y(n+1);
    for (int i = 1; i < n; i++) {
      double sigma = A(i,i-1)*x(i-1) + A(i,i+1)*x(i+1);
      y(i) = (f(i) - sigma) / A(i,i);
    }
    x = y;
    #else
    // gauss-seidel
    for (int i = 1; i < n; i++) {
      double sigma = A(i,i-1)*x(i-1) + A(i,i+1)*x(i+1);
      x(i) = (f(i) - sigma) / A(i,i);
    }
    #endif

    // recompute and log the error
    e = norm(A*x - f);
    printf("iter %d, e = %1.6e\n",iter,e);
  }
  printf("--> converged to %g in %d iterations\n",e,iter);

  return x;
}

vecd<double>
solve_model_problem_cg( int n ) {

  spmat<double> A(n+1,n+1);
  vecd<double> f(n+1);
  vecd<double> x(n+1);

  get_model_problem(n,A,f);
  if (n < 30) A.print_full();

  // solve using OpenNL's linear solver
  // false to use NL_BICGSTAB since A is not symmetric because of the bc implementation
  // (although NL_CG still works here)
  A.solve_nl(f,x,false);

  return x;
}

int
main( int argc, char** argv ) {

  int n = 1e2;

  //vecd<double> u = solve_model_problem(n,1e-10,1e5);
  vecd<double> u = solve_model_problem_cg(n);

  double error = 0.0;
  for (int i = 0; i < n+1; i++) {
    double xi = double(i) / n;
    #if RHS_SINE
    double ua = sin( M_PI * xi );
    #else
    double ua = 0.5*xi*(1.0 - xi);
    #endif
    error += std::pow( ua - u(i) , 2.0 );
    printf("u[%d] = %g, ua = %g, error = %g\n",i,u(i),ua, std::fabs(ua - u(i)));
  }
  error = std::sqrt(error/n);
  printf("error = %g\n",error);

  return 0;
}
