### lecture 04: `flux`

#### objectives

- configure and compile the `flux` library, develop and run unit tests,
- compile and run executables where we will develop our class work,
- debug the code you write in the `flux` framework.

#### welcome to `flux`

Welcome to `flux`. This is where we will be working for the rest of the semester. We will develop our class exercises directly in this repository, and you will also work on projects (with a partner) in the same repository. You will further develop tests throughout the semester (which are due on the last day), to test the functionalities we will write together in class.

Here are the groups:

| group | members | repository |
| :---: | :---: | :---: |
| 01 | Adam & Issy | https://gitlab.com/csci422-s22/group01/flux |
| 02 | Annika & Nick | https://gitlab.com/csci422-s22/group02/flux |
| 03 | Ben & Peter | https://gitlab.com/csci422-s22/group03/flux |
| 04 | Cynthia & Nellie | https://gitlab.com/csci422-s22/group04/flux |
| 05 | Dan & Sam | https://gitlab.com/csci422-s22/group05/flux |
| 06 | Ian & Sammy | https://gitlab.com/csci422-s22/group06/flux |
| 07 | Jack & Steven Shi | https://gitlab.com/csci422-s22/group07/flux |
| 08 | Jay-U & Mead | https://gitlab.com/csci422-s22/group08/flux |
| 09 | Mihir & Otis | https://gitlab.com/csci422-s22/group09/flux |
| 10 | Noel & Steven Jin | https://gitlab.com/csci422-s22/group10/flux |
| 11 | Ruben & Youssef | https://gitlab.com/csci422-s22/group11/flux |
| 12 | Siri & Toby | https://gitlab.com/csci422-s22/group12/flux |

The way `flux` is organized is as follows. I will maintain a `flux-base` repository. This will be continuously updated throughout the semester. We will develop some functionalities together (as class exercises), but we won't have enough time to add all the features or write all functions together. Generally speaking, I will upload the full functionality of what we will implement together after our class session in addition to the solutions to the in-class exercises. Your version of `flux` uses this `flux-base` version as a "git submodule". You will need to update this submodule whenever I make a change. The three main times you will need to update your `flux-base` are:
- Before class, to get a copy of the template we will use for the in-class exercises.
- After class, with the full implementation of an algorithm/data structure we were working on.
- If I find a bug in my code (it happens, sorry) - I'll let you know in Slack if you should update the submodule.

To get started with your partner, you should both clone the starter repository:
```bash
git clone --recursive https://gitlab.com/csci422-s22/groupN/flux
```
Replace `N` with your group number (see above) and don't forget the `--recursive` flag! This will download the `flux-base` submodule too. Only you and your partner have access to this repository (so do I), and please keep it that way. In respecting Middlebury's honor code, do not add other members to your project.

Here are the directories you will see in the project:
- **data:** contains some sample meshes
- **exercises:** where we will develop in-class exercises
- **projects:** where you will implement your projects
- **src:** where you can implement source files (if you want)
- **test:** where you will implement all the unit tests for the semester
- **tools:** where some scripts will be be used to configure Continuous Integration (below).

**Important:** Within `src/` you might notice something called `flux-base @ eXYZ1234` (or some numbers and letters like that). This is that "git submodule" I was talking about earlier.

#### configuring `flux` with `CMake`

Building `flux` will follow the same pattern as we saw last time. Within the top-level directory, type:

```bash
$ mkdir build
$ mkdir build/debug
$ mkdir build/release
```

Let's just build in `debug` mode for now.
```bash
$ cd build/debug
```

Let's configure `flux`:
```bash
$ cmake ../../
```

And now, type `make`. You should see a bunch of files getting compiled, along with a bunch of executables.

#### compiling the library
If you just want to compile the library, then type:
```bash
$ make flux_lib
```
in your build directory.

#### developing & running exercises

Suppose we want to compile the exercises for a particular class, say for today's class (`class04`). First we need to copy over the template from `flux-base`:

```bash
$ make template_class04_flux
```

This will copy over whatever files we need for the exercise into the `exercises/class04` directory. For this class, you will notice the `flux.cpp` file placed in the aforementioned directory. You can now compile and run the template code:

```bash
$ make class04_flux
```
Directories are delimited with underscores, which is why have `class04_flux`. Note that the `exercises` directory is omitted from the target. In general we will have `classN_cppfilewithoutextension`.

If there is a solution file (usually uploaded after class), then you can compile and run it with:
```bash
$ make class04_flux_sol
```
The solution source will always be in `src/flux-base/exercises/classN/*_sol.cpp`, and will not be copied to your repository.

We will now (together) write some code together. Before doing so, you should set up VS Code to collaborate with your partner.

#### writing a unit test

As mentioned earlier, you will be writing tests for `flux-base` throughout the semester (due on the last day). These will all be in the `test` directory. Test files should end with `_ut.cpp`. The `CMake` configuration will look for any files that have this naming convention in order to create (1) the individual test targets and (2) the complete `unit` target (which runs all unit tests together).

There are many tests that are already provided for you. They mostly test the `core` directory of `flux-base`. You will not be responsible for testing this `core` directory (you will only need to test the meshing data structures and algorithms we will develop).
Have a look at `src/flux-base/test/core/mat_ut.cpp` which tests a lot of the matrix class (`mats` and `matd`). The name `mats` stands for "statically allocated" matrix, whereas `matd` refers to the dynamic allocation of the matrix entries.

You will notice a few components that should always be present in your unit test files:
- `UT_TEST_SUITE` and  `UT_TEST_SUITE_END` (once)
- `UT_TEST_CASE` and `UT_TEST_CASE_END` (multiple times)
- macros you can use to actually test stuff:
  - `UT_ASSERT`
  - `UT_ASSERT_EQUALS`
  - `UT_ASSERT_NEAR`
  - `UT_CATCH_EXCEPTION`

You can run this particular test by typing (in your build directory):
```bash
$ make core_mat_ut
```

The latter will both compile and run your test.

**Exercise:** create a new file in `test` and write a couple of tests:
- check that `1+1` is equal to `2`
- create an `std::vector` (with a type of your choice) and check that the size is equal to `0`
- check that `0.1*3` is close to `0.3` (within a tolerance of your choosing)
- write a function that throws an exception via `flux_assert(expr)` where `expr` evaluates to `false`.

The latter (`flux_assert`) is a macro defined in `core/error.h`. It will evaluate a boolean expression and throw an `std::exception` if that boolean is false. This is useful for checking things at runtime, like checking if inputs to your algorithm are correct, or if there is a developer-related error somewhere.

You can also use the macro `flux_assert_msg( expr , msg)` where `expr` is another expression that returns a boolean and `msg` is a string (with `C`-style formatting) that you would like to print if the expression is false.

#### running all the unit tests together

You can either run unit tests individually (as above) or all together. To compile and run all the unit tests together, type
```bash
$ make unit
```

You should see a description of all the test suites that were run, how many tests, how many assertions passed and some information about how much time each suite took. In general, you should keep the time it takes to run your unit tests very very short. You should initially see something like:

```bash
Consolidate compiler generated dependencies of target unit_exe
[100%] Built target unit_exe
running suite: linear_algebra_tests           with   4 tests ...  X assertions ... done [  0 s : Y  ms]
running suite: matrix_test_suite              with   3 tests ...  X assertions ... done [  0 s : Y  ms]
running suite: symmetric_matrix_test_suite    with   1 tests ...  X assertions ... done [  0 s : Y  ms]
running suite: vector_suite                   with   2 tests ...  X assertions ... done [  0 s : Y  ms]
running suite: websockets_test_suite          with   1 tests ...  X assertions ... done [  0 s : Y  ms]
running suite: readwrite_suite                with   1 tests ...  X assertions ... done [  0 s : Y  ms]
running suite: example_test_suite             with   2 tests ...  X assertions ... done [  0 s : Y  ms]
Summary:
X assertions passed out of Y with 0 failures and 0 exceptions.
[100%] Built target unit
```

#### checking for code coverage

Part of your **Unit Tests** grade (see [syllabus](https://csci422-s22.gitlab.io/home/index.html)) will depend on your code coverage (it also depends on how well you write your unit tests and how many assertions you perform). Code Coverage refers to how many Source Lines of Code (SLOC) are executed by your unit tests (i.e. the full `unit` executable). The higher the better. This is purely about executing lines, but you should write tests that also test values returned from our functions are correct, not just that you executed them.

To get started with code coverage, you'll first need to install [lcov](https://wiki.documentfoundation.org/Development/Lcov). Then create a new build type called `coverage`. In other words, in the top-level `flux` directory you will have
```bash
$ mkdir build/coverage
$ cd build/coverage
$ cmake ../../
```
Now type
```bash
$ make unit_coverage
$ make coverage_show
```
A browser window should open up with the displayed coverage information. You can navigate and see which lines are being executed.

#### continuous integration

You don't always have to run code coverage on your personal laptop if you don't want to. Alternatively (this is usually what I do), you can run the complete unit test suite on a separate computer whenever pushing to your repository. I have provided a `.gitlab-ci.yml` file for you in the top-level `flux` directory. This is the continuous integration file that GitLab looks for and will use to know what commands to use, and what kind of resources to use for each "job" within a "pipeline".

You don't have to know the details of how this is set up, but it will be helpful to know where you can find information from a pipeline. Navigate to your GitLab project page, something like:

[https://gitlab.com/csci422-s22/groupN/flux](https://gitlab.com/csci422-s22/groupN/flux)

![](../images/pipeline-1.png)

Now click on **CI/CD->Pipelines**. If no pipeline is shown (probably because you haven't pushed to the repository yet), you can also click **Run Pipeline** in the top right. Your pipeline will now run and appear in the list. Click on the Pipeline ID (something like #439661266). Once the pipline finishes running, you should see:

![](../images/pipeline-2.png)

There are three stages to the pipeline: Build, Test and Deploy.
- **Build**: The Build stage will simply compile the code using two compilers (`clang` and `g++`).
- **Test**: Then the Test stage will build and run the `unit` target. Two build types are used (`coverage` & `release`). The `release` tests will run faster and will let you know quicker if a unit test is broken. The `coverage` tests will also run our code coverage target (`unit_coverage`) and collect the HTML results that can then be fed into the Deploy stage.
- **Deploy**: Publishes a web page consisting of the code coverage HTML results.

Your website will then be "deployed" to:

https://csci422-s22.gitlab.io/groupN/flux

If you forget the URL, you can always find it in **Settings->Pages**. Please keep your website (hosted as a GitLab Page) private since code coverage results essentially show the source code.

You will then see the three main directories to test for coverage: (`src/`, `src/flux-base/` and `src/flux-base/core`). As mentioned earlier, your job will primarily be to test `src/flux-base` - I will provide tests for `src/flux-base/core`. This is what the coverage information for `src/flux-base/core` should look like (numbers will vary):

![](../images/coverage-1.png)

We can see information about each file: how many lines were executed (out of how many) and how many functions were executed (out of how many) and what is the overall line/function coverage in the top right. **Please focus on Lines coverage**. You can click on an individual file to see which lines are executed (in blue) - lines that are not executed by the `unit` target are in red.

#### debugging

Having access to a debugger is super helpful when developing larger software projects. We will generally use the GNU Debugger (GDB), or LLDB on Mac OS X. First create a `debug` build - from the top-level `flux` directory:

```bash
$ mkdir build/debug
$ cd build/debug
$ cmake ../../
$ make -j 4
```
The `-j 4` means we will compile everything with 4 threads (you can increase or decrease this as you wish) which will speed up compilation.

Now, let's add a bug to `src/example.cpp`:

```c++
void
segfault_function() {
  std::vector<double> x;
  x[10] = 2;
}
```

Now add a call to `segfault_function()` in `test/example_ut.cpp` and call:

```bash
$ make test_example_ut
```

You should see something like:
```bash
--> running case example_test:

hello from example function!
/bin/sh: line 1: 67821 Segmentation fault: 11  /Users/pcaplan/Codes/flux/flux-groupN/build/debug/test/bin/test_example_ut_exe 1
make[3]: *** [src/flux-base/test/CMakeFiles/test_example_ut] Error 139
make[2]: *** [src/flux-base/test/CMakeFiles/test_example_ut.dir/all] Error 2
make[1]: *** [src/flux-base/test/CMakeFiles/test_example_ut.dir/rule] Error 2
make: *** [test_example_ut] Error 2
```

Oh no, segmentation fault! Let's run this through GDB (or LLDB). All targets (for a debug build) have an additional target with the suffix `_gdb` which will run GDB on the executable for your particular test:

```bash
make test_example_ut_gdb
```

You will then enter an interactive debugging session:
```bash
100%] Built target test_example_ut_exe
(lldb) target create "/Users/pcaplan/Codes/flux/flux-groupN/build/debug/test/bin/test_example_ut_exe"
Current executable set to '/Users/pcaplan/Codes/flux/flux-groupN/build/debug/test/bin/test_example_ut_exe' (x86_64).
(lldb)
```
Now type `r` (which stands for "run") and press enter:
```bash
--> running case example_test:

hello from example function!
Process 67881 stopped
* thread #1, queue = 'com.apple.main-thread', stop reason = EXC_BAD_ACCESS (code=1, address=0x50)
    frame #0: 0x000000010019c66d libflux.dylib`flux::segfault_function() at example.cpp:16:9
   13   void
   14   segfault_function() {
   15     std::vector<double> x;
-> 16     x[10] = 2;
   17     printf("here\n");
   18   }
   19
Target 0: (test_example_ut_exe) stopped.
```
Ah! The arrow tells us where the Segmentation fault happened. Let's see why.

```bash
(lldb) p x.size()
(std::__1::vector<double, std::__1::allocator<double> >::size_type) $0 = 0
```
Here we are "printing" (using `p`) the size of the `vector` x. Hmm, it has a size of 0. The segfault occurs because we tried to access index 10 of a zero-length vector.

#### notes

If you are on OS X, you might want to install `bash-completion` (`brew install bash-completion`) so that you can use the `Tab` key to autocomplete `CMake` targets. The installation instructions will also give some information about which commands you should add to your `.bash_profile` file so the changes take effect. After adding those, restart your `Terminal`.
