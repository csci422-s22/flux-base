// tester.hpp must always be included to develop unit tests!
#include "tester.hpp"    // located in src/flux-base/test/

#include "readwrite.h"   // located in src/flux-base/src/
#include "mesh.h"        // located in src/flux-base/src/
#include "webgl.h"       // located in src/flux-base/src/core/
#include "external/nl.h" // not used in this test, but is included relative to src/flux-base/src/

// local includes
#include "example.h"     // located in src/

#include <memory>
#include <vector>

using namespace flux;

UT_TEST_SUITE(example_test_suite)

UT_TEST_CASE(visualizer_test)
{
  // you will need to synchronize your repository to copy the mesh file into the data/ directory
  // this can be done by running 'make update' from a build directory
  std::unique_ptr<MeshBase> mesh = read_obj( std::string(FLUX_SOURCE_DIR) + "/data/spot.obj",true);

  Viewer viewer;
  viewer.add( *mesh.get() );
  //viewer.run(); // uncomment if you would like to visualize the mesh
}
UT_TEST_CASE_END(visualizer_test)

void
raise_exception() {
  flux_implement;
}

void
segfault_function() {
  std::vector<int> x;
  //x[10] = 2; uncomment to create a segmentation fault
}

UT_TEST_CASE(example_test)
{
  example(); // this function is implemented in src/example.cpp

  UT_ASSERT_EQUALS( 1 + 1 , 2 );

  UT_ASSERT_NEAR( 0.1*3 , 0.3 , 1e-12 );

  UT_CATCH_EXCEPTION( raise_exception() );

  segfault_function();
}
UT_TEST_CASE_END(example_test)

UT_TEST_SUITE_END(example_test_suite)
