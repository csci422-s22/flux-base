#include "tools.h"

#include <iostream>

double
addOne( const double x ) {
  return x + 1;
}

int
main() {

  int x = 2;
  float y = 3.14;
  double z = 3.14;

  std::cout << "x = " << x << ", y = " << y << ", z = " << z << std::endl;

  double d = tools::distance(0.0,0.0,1.0,1.0);
  std::cout << "distance = " << d << std::endl;

  int* p = nullptr; // p points to nothing (a null pointer)

  std::cout << "before: x = " << x << std::endl;

  p  = &x; // now p points to the "address of x"
  *p = 5;  // dereference p (change the value of the variable p points to)

  std::cout << "after: x = " << x << std::endl;

  double X[5] = {1,2,3,4,5};

  tools::double_values(X,5);

  for (int i = 0; i < 5; i++)
    std::cout << "value[" << i << "] = " << X[i] << std::endl;

  return 0;
}
