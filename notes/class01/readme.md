## lecture 01 - anatomy of a `C++` program

### objectives

- develop and compile basic `C++` programs,
- write Makefiles to automate the build of `C++` programs,
- write your own functions and header files in `C++`,
- use pointers to store and manipulate the address of a variable,
- create arrays in `C++`.

We'll develop everything in `replit` today, which means you'll develop `c++` code using a web-based IDE, and will be able to collaborate with your peers. We'll start with developing and building native `c++` programs on your system next class, so please be sure to read the "before next class" section below.

Please follow along at the following repl: https://replit.com/@pcaplan/csci422-exercise01-template

Most of what we'll do today is very "`C`" - we'll get into more `C++`-specific techniques next class.

### the `main` function

This is the main entry point of any `c++` program you will write, and will always be called this. It's a function that returns an integer, which will either be 0 (successful completion of the program) or 1 (something failed).

#### braces
The body of a function, conditional, or class definition is delimited by curly braces: `{}`. Single-line conditional statements do not require them.

#### semi-colons
Every line must end in one. Class definitions too. The compiler will throw an error (with file & line number information) if you forget one.

### compiling and running

There are different ways to compile `c++` programs, which all come down to invoking the "compiler" on some `c++` source file.
Common compilers include `g++` (GNU), `clang++` (LLVM), `icpc` (Intel).

To compile our code for today, type the following in the shell:

```bash
$ g++ main.cpp -o program
```

Now you can run the program with

```bash
$ ./program
```

You won't see anything yet, but we'll write some code soon.

Another way to compile a program is by using a Makefile. This is a file that contains the commands we would like to use to compile our program (and do just about anything in the shell for that matter). Whenever we want to invoke those commands, we type `make something` where something is the name of the **target**. We can omit the target name, in which case `make` would run the `default` target. Here is an example:

```bash
default:
  g++ main.cpp program

run:
  ./program

clean:
  rm program
```

Typing `make` in the shell would compile our program, `make run` would then run it and `make clean` would remove the program. Writing Makefiles can be tricky when your codes get big (and use third-party libraries) so, later on, we'll use `CMake` to auomatically generate Makefiles for our `c++` programs.

### built-in types

You must declare the type of any variable you want to use. Common types we will use are:

| type | description | # bytes | values |
| :---: | :---: | :---: | :--: |
| `bool` | boolean | 1 |  0 or 1 |
|`int` | integer | 1 | -2147483648 to 2147483647 |
|`char`| character | 1 | -127 to 127 or 0 to 255 |
|`float`| single-precision real | 4 | depends |
|`double` | double-precision real | 8 | depends |

For other types, please see [this document](https://www.tutorialspoint.com/cplusplus/cpp_data_types.htm).

For example,

```c++
int x = 2;
float y = 3.14;
double z = 3.14;
char c = 'a';
bool b = true;
```

### built-in functions and namespaces

One of the most commonly used functions is `cout` which is in the `std` namespace. Namespaces enclose functions and classes so that we don't get them mixed up with similarly-named functions and classes. The Standard Library contains a lot of functions we will use this semester, which are accessible through various header files.

Let's print out the value stored in the variables we defined above. First we need to include the `iostream` header (you can also use `stdio` for standard input/output) at the top of our file:

```c++
#include <iostream>
```

Then, inside the `main` function:

```c++
std::cout << "x = " << x << "y = " << y << "z = " << z << std::endl;
```
The `<<` is used to combine all the printing together, and the `std::endl` will add a new line character.
If you don't want to type `std` all the time, you can "use" the namespace at the top of the file:
```c++
using namespace std;
```
and then just type:
```c++
cout << "x = " << x << ", y = " << y << ", z = " << z << endl;
```

Personally, I grew up with the `C`-style `printf` and often use it instead. You would need to `#include <cstdio>` instead of `#include <iostream>`. The equivalent `printf` call would be:
```c++
printf("x = %d, y = %g, z = %g\n", x, y, z);
```
The `%d` is used to print the value of an integer, `%g` for floating-point numbers and `\n` for a new line character.

### defining your own functions

Let's define our own function to add one to a number. Define the following above `main`:

```c++
int addOne( int x ) {
  x = x + 1;
  return x;
}
```

Now add the following in your `main` function:

```c++
std::cout << addOne(2) << std::endl;
```
Just like we need to declare the types of all variables, we also need to declare the return type of a function (here, an `int`).

#### a note about `const`-correctness
If you know that a function parameter will not change, then it's a good idea to add a `const` qualifier, which is a promise to the compiler that you will not change the variable within the function. Breaking your promise will cause a compiler error:
```c++
int addOne( const int x ) {
  x = x + 1;
  return x;
}
```
Which we can fix, by rewriting the function:
```c++
int addOne( const int x ) {
  return x + 1;
}
```

### header files

As we have seen, we can access functions in the standard library by including an appropriate header (we'll see some more examples next class). You can also define your own header files, which is useful if a function or class definition will be used in many parts of your code. Whereas `c++` implementation files have the `.cpp` or `.cxx` suffix, header files have a `.h` suffix. We will also see files with a `.hpp` suffix, but we will establish the convention that these are reserved for templated functions (next week).

We need to make sure the stuff in the header file is only defined once, which we will do with an "include guard". One method involves declaring
```c++
#pragma once
```
at the top of your header file. Another method (which we will mostly use) involves wrapping everything in the header file within an `#ifdef`:

```c++
#ifdef MY_HEADER_FILE_H_
#define MY_HEADER_FILE_H_

// define your functions & classes here

#endif
```

### organizing your code

Since we will often write functions and class definitions that will be used in several parts of our code, it's a good idea to only **define** the signature of functions in a header file, but implement them in a separate `.cpp` file (likely with the same name before the file extension). For example, let's define some functions for doing some math in the header file (provided in your repl) `tools.h`.

```c++
#ifndef TOOLS_H_
#define TOOLS_H_

namespace tools {

double distance( double x0 , double y0 , double x1 , double y1 );

} // end of namespace tools

#endif
```

We could add the implementation for these functions directly in the header file, but let's implement it in a `.cpp` file. We can really define these functions in any file that gets compiled (even in `main.cpp`), but for now let's leave it out and see what the compiler says.

Add the following to the top of your `main.cpp` file:

```c++
#include "tools.h"
```
Note that we use quotations instead of angle brackets (`<>` like we did with the standard library). Now at the bottom of `main.cpp`, add the following:

```c++
double d = tools::distance( 0.0 , 0.0 , 1.0 , 1.0 );
```

Typing `make full` will try to create the `full` target, which also involves compiling the (empty) `tools.cpp` file into an "object" file, which we can then "link" to our main program (have a look at the `full` target in the `Makefile`):

```bash
Undefined symbols for architecture x86_64:
  "tools::distance(double, double, double, double)", referenced from:
      _main in main-94d05a.o
ld: symbol(s) not found for architecture x86_64
```

Although we defined what the `distance` function looks like, we haven't actually implemented it, so the compiler says it could not find the symbols ("Undefined symbols").

Let's fix this by defining the distance function in  `tools.cpp`:

```c++
#include "tools.h"

#include <cmath> // for std::sqrt

namespace tools {

double
distance( double x0 , double y0 , double x1 , double y1 ) {
  double dx = x1 - x0;
  double dy = y1 - y0;
  return std::sqrt( dx*dx + dy*dy );
}

} // tools namespace
```

Running the program gives:
```bash
$ ./program
x = 2, y = 3.14, z = 3.14
distance = 1.41421
```

### pointers and arrays

Pointers are often tricky when starting with `C/C++` programming, so we will introduce them today, and continue our discussion next class. Ultimately, a pointer is a variable that "points" to a memory location. For example, this memory location might be the address of some other variable. Having access to these memory locations is useful for modifying variables within functions and for dealing with chunks of memory, like you would need for arrays. Since pointers are associated with a particular type of variable, they are declared as integer-pointers, double-pointers, char-pointers, etc.:

```c++
int x = 2;
int* p = nullptr; // p points to nothing (a null pointer) - it's good practice to initialize your pointers to null to start off

std::cout << "before: x = " << x << std::endl;

p  = &x; // now p points to the "address of x"
*p = 5;  // dereference p and change the value of the variable p points to

std::cout << "after: x = " << x << std::endl;
```

You should see
```bash
before: x = 2
after: x = 5
```

Arrays can be declared in a few ways. For now, let's consider statically-sized arrays. If we want an integer array with 10 values, we would use:

```c++
int values[10];

for (int i = 0; i < 10; i++)
  values[i] = i*i;
```

Arrays have a relationship with pointers. In fact the variable `values` is an integer pointer. So setting `*value` is equivalent to setting the first value in the array (`values[0]`). For a different index `i` in the array, we would use `*(value + i)`:

```c++
// equivalent to the code above
for (int i = 0; i < 10; i++)
  (*values+i) = i*i;
```

If we want to call a function that will change the values in an array, we can just pass the pointer to the first value (a memory address). Let's define a function (in `tools.h`) that doubles the values in an array. Add the following definition in `tools.h` within the `tools` namespace:

```c++
void double_values( double* x , int n );
```

And now in `tools.cpp` add:

```c++
void
double_values( double* x , int n) {
  for (int i = 0; i < n; i++)
    x[i] *= 2.0;
}
```

Finally, add the following to `main.cpp` (at the bottom):
```c++
double X[5] = {1,2,3,4,5};

tools::double_values(X,5);

for (int i = 0; i < 5; i++)
  std::cout << "value[" << i << "] = " << X[i] << std::endl;
```
Running the program should print:
```bash
value[0] = 2
value[1] = 4
value[2] = 6
value[3] = 8
value[4] = 10
```

### comments

You may have noticed some comments in the code above. We could write single line comments using `//` or multiline comments enclosed within `/* */`. Another trick (useful for debugging) is to comment out large sections of code (that may contain comments themselves) by enclosing code you want the compiler to ignore within `#if 0` and `#endif`. Anything that starts with a `#` is a "preprocessor directive". So if the compiler sees the following:

```c++
#if 0

void i_want_to_comment_this_function() {
  because this is a syntax error.
}

#endif
```
The compiler will see this as "if false", so it won't even bother compiling the code.

### before next class

* Install the following tools on your system.
  - **Mac OS X:**
    * open a Terminal:
    ```bash
    $ xcode-select --install
    ```
    * install Homebrew by following: [https://brew.sh/](https://brew.sh/)
    * Install `git`:
    ```bash
    brew install git
    ```
  - **Linux:**
    * open a terminal:
    ```bash
      sudo apt-get install g++ git
    ```
  - **Windows**
    * download [Docker Desktop](https://www.docker.com/products/docker-desktop).
    * download [this Dockerfile](https://csci422-s22.gitlab.io/home/docs/Dockerfile) (maybe to your Desktop, it doesn't really matter).
    * Open the PowerShell and navigate to where you saved the Dockerfile from the previous step.
    * build your container:
    ```bash
      docker build -t flux .
    ```
    This is a one-time operation, so you won't need to do this every time. This step might take a while because some packages need to be installed.
    * begin developing:
    ```bash
    docker run \
        --entrypoint /bin/bash \
        -p 7681:7681 \
        --rm \
        -v `pwd`:/opt/flux \
        -it \
        flux
    ```
    Certain parts of this command will not be necessary until we get into `flux` next week. You may want to create a script that runs this command automatically so you don't have to remember it every time you want to start developing.
    You can run this command anywhere, but it will make the current directory available inside the container, which means you can edit files using your native operating system, and your container will directly have access to those files.
    * install `git`: https://git-scm.com/download/win

* Install Microsoft Visual Studio Code: [https://code.visualstudio.com/](https://code.visualstudio.com/).
* Install the VS Code Live Share Extension [here](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare).
* [Join our Slack workspace](https://join.slack.com/t/csci422-s22/shared_invite/zt-12us80i4n-Xo6UxKaZ9bfjQ7VPKsSI9Q).
* Please complete the following [background form](https://forms.gle/mmjyQ3JkLwjycyCw9).
