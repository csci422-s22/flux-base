#include "tools.h"

#include <cmath> // for std::sqrt

namespace tools {

double
distance( double x0 , double y0 , double x1 , double y1 ) {
  double dx = x1 - x0;
  double dy = y1 - y0;
  return std::sqrt( dx*dx + dy*dy );
}

void
double_values( double* x , int n ) {
  for (int i = 0; i < n; i++)
    x[i] *= 2.0;
}

} // tools namespace
