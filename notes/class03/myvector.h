#ifndef MYVECTOR_H_
#define MYVECTOR_H_

template<int N>
class Vector {
public:
  Vector();                     // constructor that will set vector to 0
  Vector( double* x );          // constructor from an array
  Vector( const Vector<N>& v ); // copy constructor

  double  operator() (int i ) const;
  double& operator() (int i );

  void print() const;

private:
  double data_[N];
};

template<int N>
Vector<N>
operator+ ( const Vector<N>& u , const Vector<N>& v ) {

  Vector<N> w;

  for (int i = 0; i < N; i++)
    w(i) = u(i) + v(i);

  return w;
}

#endif
