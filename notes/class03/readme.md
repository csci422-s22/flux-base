## lecture 03 - templates & more practice

### objectives

- use `CMake` to automatically generate Makefiles for `C++` projects,
- use templates to re-use functions & classes in which the computation, or underlying data is the same, but with different types,
- inherit from a base class,
- practice, practice, practice!

### getting started with `CMake`

`CMake` is a program that automatically generates Makefiles (with customizable targets that you specify) for you. This is particularly convenient when supporting software for different operating systems, or when compiling your software with different compilers (and different versions of compilers), and also if your software relies on third-party libraries (and versions of those libraries).

I will mostly provide the `CMake` configuration files for you, but it's important to know how it works. Ultimately, the configuration commands are defined in a `CMakeLists.txt` file. Your top-level project directory should have one. To make things cleaner, you can also have `CMakeLists.txt` files in subdirectories, and use the `add_subdirectory` command.

We will work from another mini repository today - clone with:

```bash
$ git clone https://gitlab.com/csci422-s22/class03.git class03
```
And then please set up VS Code like we did last class (with a new partner this time).

Navigate into the `class03` directory.
```bash
$ cd class03
```
`CMake` will generate a bunch of files when we configure our project. You should **always always always** run `CMake` from within a "build" directory. You can have multiple builds created for the same project, so it's often a good idea to create subdirectories for `debug` and `release`. This won't matter too much right now, but when we get into `flux` next time, our "debug" buils will have debugging symbols so that we can use a debugger, whereas a "release" build will add some optimization flags for the compiler so that you code will run faster.
```bash
$ mkdir build
$ cd build
```

To invoke `CMake` on our project, we need to type
```bash
$ cmake ../
```
In other words, we are telling the `cmake` program that we want to configure the project with the `CMakeLists.txt` one directory back. This last part will change based on where the top-level `CMakeLists.txt` file lives.

If everything works correctly, you should see (at the bottom):
```bash
-- Configuring done
-- Generating done
-- Build files have been written to: /some/path/to/repository/class03/build/
```
`CMake` has now automatically generated a `Makefile`! We can build our project by typing `make`.

### inheritance

Last class, we introduced the `public`, `private` and `protected` keywords for specifying the visibility of member variables and functions. Anything declared `public` will be visible to another portion of code that is manipulating an instance of the class. Anything that is declared `private` will be strictly hidden from external functions or objects. The `protected` keyword is handy when you are developing a base class and you want member variables/functions to be accessible to derived classes. For example, if you have the following definition of a class `Shape`:

```c++
#include <cmath>
#include <string>

class Shape {

public:

  // shape is an abstract class because it has a purely virtual function
  virtual double area() const = 0;

  const std::string& name() const { return name_; }

protected:
  std::string name_;
};

class Rectangle : public Shape {
public:
  Rectangle( double length , double width ) :
    length_(length),
    width_(width) {

    name_ = "rectangle";
  }

  // this function needs to be implemented
  double area() const {
    return length_*width_;
  }

private:
  double length_;
  double width_;
};

class Circle : public Shape {
public:
  Circle( double radius ) :
    radius_(radius) {

    name_ = "circle";
  }

  // this functions needs to be implemented
  double area() const {
    return M_PI * radius_ * radius_;
  }

private:
  double radius_;
};
```

### templates

Remember those angle brackets we had to use to define the type of a `vector` or `map` from last class? In fact, these data structures are templated by *type*. The overall implementation of a `vector` for an integer is the same as the implementation of a vector of floating-point numbers. The only thing that changes is the type. Our primary goal for today is to look at how templates can be used to add an additional layer of abstraction when designing functions and classes.

#### templating a function

Suppose you want to write a function called `add` which will add two numbers together. This function works the same regardless of whether we are adding integers or floating point numbers.

For example, in the case of integers, we could have

```c++
int
add( int x , int y ) {
  return x + y;
}
```
And in the case of `double`'s, we could have
```c++
double
add( double x , double y ) {
  return x + y;
}
```
It's wasteful to rewrite the same code for two different types - it's also very prone to bugs when the functions become really complicated. Instead we will use *templates*:

```c++
template<typename T>
T
add( T x , T y ) {
  return x + y;
}
```
We can then call the add function as follows:

```c++
int z_i = add<int>(1,2);
double z_f = add<double>(1.5,2.25);
```
In some cases, the compiler will be able to figure out which type to use and you would not need to write `add<type>`.

#### templating a class

Now suppose that we would like to write our own class that represents an `N`-dimensional vector (a mathematical vector). We could template this by `N` as well as the type of the components that we will store but let's keep things simple for now and just template this class definition by the number of elements in the vector `N`. We'll just assume all the entries are `double`'s. The following is included for you in `myvector.h`.

```c++
template<int N>
class Vector {
public:
  Vector();                     // constructor that will set vector to 0
  Vector( double* x );          // constructor from an array
  Vector( const Vector<N>& v ); // copy constructor

  double  operator() (int i ) const;
  double& operator() (int i );

private:
  double data_[N];
};
```

---

##### a note about `operator` overloading
`C++` allows you to "overload" common operators (like `+`, `-`, `*`, `/`, `()`, `[]`) which gives us the ability to write expressions in a more readable form. What we have done in declaring `double operator() (int i) const;` means "overload" the `()` operator (parentheses). When doing so, we will pass in a single integer (the `i`). This function will be `const` (i.e. it won't modify any member variable of the class), and it will return a `double`.

---

Back to our `Vector` class. We could implement some of these member functions directly within the class definition, but it's more common to implement them within a separate file. We'll implement these in `myvector.cpp`. Here is an example for retrieving a reference to the value at the `i`-th index:

```c++
template<int N>
double&
Vector<N>::operator() (int i) {
  assert( i >= 0 && i < N );
  return data_[i];
}
```

Okay great! But now if you try (in `main.cpp`):

```c++
Vector<3> v;

v(0) = 2;
```
You will get an `undefined reference` error. What's going on here? We remembered to include the header `myvector.h`, and we are compiling `myvector.cpp`. What is happening?!

#### template instantiation

We forgot to *instantiate* the templated implementation of our `Vector` class. If we had implemented everything in the header file, we would have been fine, but we didn't. We can instantiate our `Vector` class at the bottom of the `myvector.cpp` since the templated implementation is provided there:

```c++
template class Vector<3>;
```
Everything should work now. Note that if you try to use a `Vector<2>` then you'll also have to add a `template class Vector<2>` instantiation as well.

#### template specialization

Sometimes, a templated definition works for most types we will use, but maybe there's one particular type in which we need a slightly different version of a function. Enter *template specialization*. Ultimately, this means that we will "specialize" (essentially, override) our templated implementation with one that is specific for a particular type. For example, let's write the `print` function for our `Vector` class, which will display each entry on a new line:

```c++
template<int N>
void
Vector<N>::print() const {
  for (int i = 0; i < N; i++)
    std::cout << "entry[" << i << "] = " << data_[i] << std::endl;
}
```
This is fine, but maybe we want to this function to look a bit different for 2d or 3d vectors, so we'll specialize those cases:

```c++
template<>
void
Vector<2>::print() const {
  std::cout << "vec2d = (" << data_[0] << "," << data_[1] << ")" << std::endl;
}
```
We can also specialize `Vector<3>::print` but I'm omitting it here for brevity.

#### multiple template arguments

What if we want to add yet another layer of abstraction and template our vector class by the data type as well? We can have multiple template arguments!

```c++
template<int N, typename T>
class Vector {

public:
  // the rest of the definition goes here

private:
  T data_[N]; // we store a T-array of size N
};
```

### implementing a matrix

The rest of your task for today consists of implementing the class definition for a matrix, which is templated by 3 parameters: the type, as well as the number of rows `M` and the number of columns `N`. You're free to store the entries however you like (such as row-major or column-major order).

Some starter code is provided in `matrix.h` and I would recommend implementing everything in that file - you will not need to instantiate anything if you implement everything directly in the header file. It is common (as we will see in `flux`) to use a `.hpp` file extension to indicate that this file contains a lot of template *implementations* (`.tpp` and `*_impl.h` are also used).

You should also write some code in `main.cpp` to test out your implementation.

#### tasks

- write a function to retrieve/modify a value at indices `(i,j)`.
- write a function to print out the entire matrix.
- **challenge:** overload the `+` operator to add two matrices together.
- **advanced challenge:** overload the `*` operator to multiply two matrices together.
