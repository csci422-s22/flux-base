#ifndef MATRIX_H_
#define MATRIX_H_

#include <iostream>
#include <cstdlib>

// a matrix with M rows and N columns, stored in column-major order
template<int M, int N, typename T>
class Matrix {

public:
  // initializes MxN matrix to zero
  Matrix() {
    for (int i = 0; i < M*N; i++)
      data_[i] = 0;
  }

  // creates a random MxN matrix
  void random() {
    for (int i = 0; i < M*N; i++) {
      data_[i] = T(rand()) / T(RAND_MAX);
    }
  }

  // entry access
  const T& operator() (int i , int j) const { return data_[j*M+i]; }
        T& operator() (int i , int j)       { return data_[j*M+i]; }

  // prints the contents of the matrix, one row per line
  void print() const;

private:
  T data_[M*N];
};

template<int M,int N,typename T>
void
Matrix<M,N,T>::print() const {
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
      std::cout << (*this)(i,j);
      if (j < N-1) std::cout << " , ";
      else std::cout << std::endl;
    }
  }
}

// matrix addition
template<int M, int N, typename T>
Matrix<M,N,T>
operator+( const Matrix<M,N,T>& A , const Matrix<M,N,T>& B ) {
  Matrix<M,N,T> C;
  for (int i = 0; i < M; i++)
  for (int j = 0; j < N; j++)
    C(i,j) = A(i,j) + B(i,j);
  return C;
}

// matrix multiplication
// ML: number of rows in left-matrix
// NL: number of columns in left-matrix
// NR: number of rows in right-matrix
// MR is not needed since MR = NL
template<int ML, int NL, int NR, typename T>
Matrix<ML,NR,T>
operator*( const Matrix<ML,NL,T>& A , const Matrix<NL,NR,T>& B ) {

  Matrix<ML,NR,T> C;
  for (int i = 0; i < ML; i++) {
    for (int j = 0; j < NR; j++) {
      C(i,j) = 0;
      for (int k = 0; k < NL; k++)
        C(i,j) += A(i,k)*B(k,j);
    }
  }
  return C;
}

#endif
