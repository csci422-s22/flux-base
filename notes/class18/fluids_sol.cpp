#include "element.h"
#include "halfedges.h"
#include "linear_algebra.h"
#include "mat.hpp"
#include "mesh.h"
#include "readwrite.h"
#include "spmat.h"
#include "vec.hpp"
#include "webgl.h"

#include <cstdio>
#include <fstream>
#include <map>

using namespace flux;

class FlowSolver {

public:
  FlowSolver( const Mesh<Triangle>& mesh ) :
    mesh_(mesh)
  {}

  void solve( const std::map< std::pair<int,int> , int >& bcs );

  const std::vector<double>& phi() const { return phi_; }
  const std::vector<double>& speed() const { return speed_; }

private:
  const Mesh<Triangle>& mesh_;
  std::vector<double> phi_;
  std::vector<double> speed_;
};

void
FlowSolver::solve( const  std::map< std::pair<int,int> , int >& bcs ) {

  // this is the 3x2 dpsi_dxi matrix
  mats<3,2,double> psi_xi;
  psi_xi(0,0) = -1;
  psi_xi(0,1) = -1;
  psi_xi(1,0) = 1;
  psi_xi(2,1) = 1;

  // initialize the sparse n x n matrix (n = # vertices)
  spmat<double> K( mesh_.vertices().nb() , mesh_.vertices().nb() );

  printf("--> building stiffness matrix...\n");
  for (int k = 0; k < mesh_.nb(); k++) {

    int i0 = mesh_(k,0);
    int i1 = mesh_(k,1);
    int i2 = mesh_(k,2);

    const double* p0 = mesh_.vertices()[i0];
    const double* p1 = mesh_.vertices()[i1];
    const double* p2 = mesh_.vertices()[i2];

    // calculate element jacobian
    mats<2,2,double> J;
    for (int d = 0; d < 2; d++) {
      J(d,0) = p1[d] - p0[d];
      J(d,1) = p2[d] - p0[d];
    }
    double area = 0.5 * det(J);

    // calculate element matrix
    mats<2,2,double> Jinv = inverse(J);
    mats<3,2,double> G = psi_xi * Jinv;
    mats<3,3,double> M = G * transpose(G) * area;

    // add the coefficients to the global system
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        K( mesh_(k,i) , mesh_(k,j) ) += M(i,j);
      }
    }
  }
  printf("done!\n");

  // compute the right-hand-side
  printf("--> applying boundary conditions...\n");
  double uinf = 2.0;
  vecd<double> f( mesh_.vertices().nb() );
  for (auto& edge : bcs) {

    int e0 = edge.first.first;
    int e1 = edge.first.second;

    vec3d x0( mesh_.vertices()[e0],2);
    vec3d x1( mesh_.vertices()[e1],2);
    double ds = norm(x1 - x0);

    // which bc is this on?
    int bc = edge.second;
    if (bc == 1 || bc == 3) { // inlet = 3, outlet = 1
      int s = (bc == 3) ? -1 : 1;
      f[e0] += s * 0.5 * uinf * ds;
      f[e1] += s * 0.5 * uinf * ds;
    }
  }

  // solve K * x = f and save the solution into phi_
  phi_.resize( mesh_.vertices().nb()  );
  vecd<double> x( f.m() );
  K.solve_nl(f,x,true);
  for (int k = 0; k < x.m(); k++)
    phi_[k] = x[k];

  // post-process: calculate magnitude of velocity
  speed_.resize( mesh_.nb() );
  for (int k = 0; k < mesh_.nb(); k++) {

    int i0 = mesh_(k,0);
    int i1 = mesh_(k,1);
    int i2 = mesh_(k,2);

    const double* p0 = mesh_.vertices()[i0];
    const double* p1 = mesh_.vertices()[i1];
    const double* p2 = mesh_.vertices()[i2];

    // calculate element jacobian
    mats<2,2,double> J;
    for (int d = 0; d < 2; d++) {
      J(d,0) = p1[d] - p0[d];
      J(d,1) = p2[d] - p0[d];
    }

    // gradient of basis functions
    mats<3,2,double> G = psi_xi * inverse(J);

    // retrieve the solution at the vertices of this triangle
    vecs<3,double> phi_k;
    for (int j = 0; j < 3; j++)
      phi_k(j) = phi_[mesh_(k,j)];

    // calculate velocity and magnitude
    vecs<2,double> v = transpose(G) * phi_k;
    speed_[k] = std::sqrt( v[0]*v[0] + v[1]*v[1] );
  }

}

int
main( int argc, char** argv ) {
  Viewer viewer;

  std::string name = "airfoil";

  std::unique_ptr<MeshBase> mesh_ptr = read_obj("../data/" + name + ".obj" );
  Mesh<Triangle>& mesh = *static_cast<Mesh<Triangle>*>(mesh_ptr.get());

  std::map<std::pair<int,int>,int> bc;
  std::ifstream file("../data/" + name + ".bc");
  std::string line;
  while (!file.eof()) {
    std::getline(file,line);
    int e0,e1,b;
    int nitems = sscanf( line.c_str() , "%d %d %d\n",&e0,&e1,&b);
    if (nitems <= 0) continue;
    flux_assert( nitems > 0 );
    bc.insert( {{e0,e1},b} );
  }

  FlowSolver fem(mesh);
  fem.solve( bc );
  mesh.create_vertex_field("phi" , fem.phi());
  mesh.create_cell_field("velocity", fem.speed() );

  viewer.add(mesh);
  viewer.run();

}
