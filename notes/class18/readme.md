### lecture 18 - finite element methods

#### objectives

The main objective of today's lecture is to study an application of meshes. In particular, we will derive and implement the finite element method (FEM) to solve partial differential equations that govern the flow of an incompressible, inviscid, irrotational fluid around some geometry. By the end of this lecture, you will be able to:

- derive the finite element method to solve Poisson's equation,
- implement a finite element method to solve for the potential flow around an object.

Several details of what we will cover today is beyond the scope of the course, but I'm presenting them here in case it sparks an interest that you'd like to pursue later on.

#### model problem

As with the lecture on sparse linear solvers, we will motivate our lecture by considering a model problem. Specifically, we would like to predict the velocity of a fluid as it moves through some domain. This domain will be represented by a mesh, so our lecture today will focus on what we can *do* with meshes (not so much how we generate or modify them).

We will restrict our attention to *incompressible*, *inviscid* and *irrotational* fluids. An incompressible fluid is one in which the density is constant, inviscid means there is no viscosity, and irrotational means that the *vorticity* is zero. The vorticity is defined as the curl of the velocity ($`\nabla\times \mathbf{v}`$), so if this is zero, then the velocity can be written as the gradient of some *potential* function, which we will call $`\phi`$. Under these assumptions, our fluid can be modeled by the following equation:

```math
\nabla^2 \phi = \frac{\partial^2\phi}{\partial x^2} + \frac{\partial^2\phi}{\partial y^2} =  0, \quad \forall\,\mathbf{x} \in \Omega\ \ (\mathrm{our\ domain})
```

where the velocity is written as the gradient of $`\phi`$: $`\mathbf{u} = \nabla\phi = (\partial\phi/\partial x,\ \partial\phi/\partial y)`$. You can think of these partial derivatives ($`\partial`$) as the derivative of the function with respect to some variable, while keeping all other varibles constant. For example for $`\phi(x,y) = x^3 y^4`$, we have $`\partial\phi/\partial x = 3x^2y^4`$ and $`\partial\phi/\partial y = 4x^3y^3`$.

This equation alone is not enough to solve for the flow field. We need something called *boundary conditions* (BCs). These are extra equations on the domain boundaries that specify the behavior of the fluid at those boundaries.

For our model problem, we will specify Neumann boundary conditions, which amounts to specifying the value of the derivative of $`\phi`$ along the domain boundary. Why? Well, for this problem, we know what the fluid velocity should be really far away from the object, which we will call $`U_\infty`$ and will only have an x-component. This means $`\partial \phi/\partial x = U_\infty`$ and $`\partial\phi/\partial y =0`$ on the outer domain boundary. We also know that there is no fluid going *through* the object, which means that there cannot be any component of the velocity going *into* the surface. In other words, the rate of change of $`\phi`$ in the normal direction ($`\mathbf{n}`$) is zero: $`\partial\phi/\partial\mathbf{n} = 0`$,  which we can write as $`\nabla\phi \cdot \mathbf{n} = 0`$. These boundary conditions are summarized below:

| BC | applied to | explanation |
| :---: | :---: | :---: |
| $`\nabla \phi \cdot \mathbf{n} = 0`$ | interior object | fluid does not go through the object |
| $`\partial \phi /\partial x = U_\infty`$ | outer boundary | x-component of velocity is equal to $`U_\infty`$  far away from the object |
| $`\partial \phi /\partial y = 0`$ | outer boundary | y-component of velocity is equal to zero far away from the object |

In other problems, the PDE or boundary conditions might be different. You can even have a right-hand side in the PDE, but that will complicate our implementation a little bit.

![](../images/circle-fem.png)

#### the finite element method

The finite element method is derived by considering a "weak" form of our governing equation. This weak form is obtained by taking some "test" function $`v`$ and multiplying it with our PDE and then integrating over the domain $`\Omega`$:

```math
\int_\Omega v\ \nabla^2 \phi = 0
```
Now we integrate by parts. Remember that an integral can be integrated by parts using $`\int u\ \mathrm{d}v = uv - \int v\ \mathrm{d}u`$. In our case, $`u = v`$ and $`\mathrm{d}v = \nabla\cdot \nabla\phi`$ (another way to write $`\nabla^2\phi`$). So our $`v = \nabla\phi`$:

```math
\int\limits_{\partial \Omega} v \nabla \phi \cdot \mathbf{n} - \int_\Omega \nabla \phi \cdot \nabla v = 0
```

Okay, where do we go from here? We need to make an assumption about what the solution $`\phi`$ will look like in our domain. In general, we won't have an analytic expression for $`\phi`$, but let's have $`\phi`$ defined at the vertices of our mesh $`\mathcal{M}`$ that represents the domain $`\Omega`$. Further, let's assume that $`\phi`$ varies *linearly* within each element (a triangle) of the mesh. In other words, for some triangle $`\kappa`$, we have:

```math
\phi_\kappa(\xi,\eta) = (1 - \xi - \eta) \phi_{\kappa,1} + \xi \phi_{\kappa,2} + \eta \phi_{\kappa,3} = \sum\limits_{i=1}^3 \psi_i(\xi,\eta) \phi_{\kappa,i}.
```
The $`\psi(\xi,\eta)`$ functions are called *basis functions*. Here, we are assuming *linear* basis functions (i.e. linear functions of $`\xi`$ and $`\eta`$) - also notice that $`\phi`$ is *piecewise continuous* throughout the domain (but its derivative is not necessarily).

The last step we need to take in our finite element derivation is to assume what our "test functions" $`v`$ look like. In the *Galerkin* finite element method, we will take these test functions to be the basis functions we just assumed for $`\phi`$. We then have:

```math
\int\limits_{\partial \Omega} \psi \nabla \phi \cdot \mathbf{n} - \int_\Omega \nabla \phi \cdot \nabla \psi = \sum\limits_{e \in \mathcal{B}} \int_e \psi_e \nabla\phi_e \cdot \mathbf{n} - \sum\limits_{\kappa \in \mathcal{M}}\int_\kappa \nabla\phi_\kappa \cdot \nabla \psi_\kappa =   0.
```

In other words, we have broken up the integrals over the domain boundary, and the domain itself into integrals over the the boundary edges (in the boundary $`\mathcal{B}`$) and the elements in the mesh $`\mathcal{M}`$.

Let's start with the boundary terms. We can break this up over each of the five boundaries in our domain:

```math
\sum\limits_{e \in \mathcal{B}} \int_e \psi_e \nabla\phi_e \cdot \mathbf{n} =
\sum\limits_{e \in \mathcal{B}_{\mathrm{inflow}}} \int_e \psi_e \nabla\phi_e \cdot \mathbf{n} +
\sum\limits_{e \in \mathcal{B}_{\mathrm{outflow}}} \int_e \psi_e \nabla\phi_e \cdot \mathbf{n} +
\sum\limits_{e \in \mathcal{B}_{\mathrm{surface}}} \int_e \psi_e \nabla\phi_e \cdot \mathbf{n} +\\
\sum\limits_{e \in \mathcal{B}_{\mathrm{top}}} \int_e \psi_e \nabla\phi_e \cdot \mathbf{n} +
\sum\limits_{e \in \mathcal{B}_{\mathrm{bottom}}} \int_e \psi_e \nabla\phi_e \cdot \mathbf{n}
```

The last three terms are all zero in our problem! This is because of the boundary condition $`\nabla\phi\cdot \mathbf{n} = 0`$ on these boundaries. Also, remember that for the "inflow" and "outflow" boundaries, we have the constant $`\nabla\phi\cdot\mathbf{n} = U_\infty`$. The outflow term is then

```math
\sum\limits_{e \in \mathcal{B}_{\mathrm{outflow}}} \int_e \psi_e \nabla\phi_e \cdot \mathbf{n} =
\sum\limits_{e \in \mathcal{B}_{\mathrm{outflow}}} \int_e \psi_e U_\infty.
```

Now, $`\psi_e`$ is also a linear function across the edge. The basis functions for the first and second vertices are $`s`$ and $`1-s`$, respectively ($`0 \le s \le 1`$), so this integral simplifies (for both vertices) to
```math
\sum\limits_{e \in \mathcal{B}_{\mathrm{outlow}}} \int_e \psi_e U_\infty =
\sum\limits_{e \in \mathcal{B}_{\mathrm{outflow}}} \int_0^1 s\ U_\infty\ \lvert|e\rvert|\ \mathrm{d}s = \frac{1}{2}U_\infty \lvert|e\rvert|.
```
($`\vert| e \rvert|`$ is the length of edge $`e`$). This adds a right-hand-side term for the equations of any vertex that touches the edge $`e`$. The same derivation applies to the inflow boundary $`\mathcal{B}_{\mathrm{inflow}}`$, but the sign is different because the normal vector points to the left.

Let's now derive the volume terms. We don't directly know the gradient of the solution within an element (which we need to compute $`\nabla \phi_{\kappa}`$ and $`\nabla \psi_{\kappa}`$), but we do know the gradient in the "reference" space of the element. This is the space where the basis functions are defined. Therefore, to calculate the gradient $`\nabla\psi_\kappa`$, we can use the chain rule (subscripts denote differentiation with respect to those variables):

```math
\nabla\psi_\kappa =  \nabla_{\boldsymbol{\xi}} \psi \nabla_{\mathbf{x}} \mathbf{\xi} = \nabla_{\boldsymbol{\xi}} \psi\ J_\kappa^{-1} .
```
where we have denoted $`J_{\kappa}`$ to be the *Jacobian* of the transformation from the canonical right triangle (see `src/flux-base/src/element.h`) to the physical triangle (we'll revisit how to calculate this matrix later on). Note that we analytically know the derivative of the basis functions in the reference space (i.e. with respect to $`\xi`$ and $`\eta`$):

  ```math
  \nabla_{\boldsymbol{\xi}}\psi_\kappa = \left[ \begin{array}{rr} -1 & - 1 \\ 1 & 0 \\ 0 & 1 \end{array}\right].
  ```

  Each row of this 3x2 matrix represents a particular basis function, and each colum represents differentiation with respect to $`\boldsymbol{\xi} = (\xi,\eta)`$.

Let us represent the solution within triangle $`\kappa`$ in vector form:
```math
  \phi_\kappa = \left[ \begin{array}{ccc} (1-\xi-\eta) & \xi & \eta \end{array}\right] \left[ \begin{array}{c} \phi_{\kappa,1} \\ \phi_{\kappa,2} \\ \phi_{\kappa,3} \end{array}\right].
```
The gradient of the solution can be written as:

```math
\nabla \phi_{\kappa} = (\nabla \psi_{\kappa})^T \left[ \begin{array}{c} \phi_{\kappa,1} \\ \phi_{\kappa,2} \\ \phi_{\kappa,3} \end{array}\right].
```

To summarize, there are two things we need to calculate: volume terms and boundary terms. The finite element discretization leads to a system of equations in the unknown $`\phi`$ values. We need to place the coefficients of all the matrices we calculate into the appropriate location in a sparse matrix and vector, ultimately leading to a big (sparse) system of linear equations:

```math
\mathbf{K} \boldsymbol{\phi} = \mathbf{b},
```

where $`\boldsymbol{\phi}`$ is a vector with the number of components equal to the number of vertices in the mesh. The right-hand-side vector $`\mathbf{b}`$ contains the boundary terms (in the appropriate entries), and the big matrix $`\mathbf{K}`$ is sometimes called a *stiffness matrix*, especially when solving structural deformation problems.

##### uniqueness

If you have taken a course on differential equations, you may notice that the solution to our problem is not unique. This is because we have only specified the values of the derivatives on the domain boundaries. We can add any constant value to whatever solution we obtain, and it will still be a solution to the problem (so it's not unique). In fact, the matrix $`\mathbf{K}`$ is *singular*, so we can't directly compute $`\mathbf{K}^{-1}`$. One possibility to make the system unique is to "pin" down one of the values or to add an extra condition on the entire solution. However, we will use the conjugate gradient method to solve our system and, in fact, the CG method will find *a* solution. In the end, we will take the gradient of the solution to visualize the velocity, anyway.

#### implementing the finite element method

If we order the unknown $`\phi`$ values in the same order as they appear as vertices in the mesh, then we will know where to place the volume and boundary term coefficients when building up our system of equations. Here is how to implement the finite element method to solve our model problem:

1. Initialize a sparse matrix: call this `K`.
2. Loop through every triangle `k` in the mesh:
  a.) Calculate the 2x2 Jacobian of triangle `k` which has vertex coordinates $`\mathbf{x}_{\kappa,1}`$, $`\mathbf{x}_{\kappa,2}`$, $`\mathbf{x}_{\kappa,3}`$:
    ```math
    J_{\kappa} = \left[\begin{array}{cc} (x_{\kappa,2} - x_{\kappa,1}) & (x_{\kappa,3} - x_{\kappa,1})  \\
      (y_{\kappa,2} - y_{\kappa,1}) & (y_{\kappa,3} - y_{\kappa,1})
     \end{array} \right]
    ```
    b.) Calculate the 2x2 inverse of the Jacobian: $`J_{\kappa}^{-1}`$.
    c.) Calculate the 3x2 matrix representing the gradient of $`\psi`$: $`G = \nabla_{\boldsymbol{\xi}} \psi\ J_\kappa^{-1}`$.
    d.) Calculate the 3x3 matrix: $`M = \frac{1}{2} G G^T \det(J_\kappa) `$.
    e.) Place the coefficients of $`M`$ into `K`. The indices of where these should be placed into `K` correspond to the first, second and third vertex indices of the triangle.
3. Initialize a vector `b` with a size equal to the number of vertices in the mesh.
4. Loop through every edge `e` on the boundary of the mesh:
    a.) If this is not an edge on the inflow or outflow boundaries, continue.
    b.) Calculate the length of the edge $`\ell`$
    c.) Calculate the term: $`r = \frac{1}{2}U_\infty \ell`$ - be careful with the sign! The normal vector points out of the domain. This term is negative for inflow boundaries and positive for outflow boundaries.
    d.) Add $`r`$ to the current value of `b` at the two vertex indices of the edge.

Note that the term $`\frac{1}{2}\det(J_\kappa)`$ represents the area of triangle $`\kappa`$ which comes from the fact that we integrated a constant term across the triangle.

#### exercise

Please open the file `notes/class18/fluids.cpp`. In class, we will complete the two loops that compute the volume and boundary terms. When you run `make update` there will be two additional meshes that are copied to your `data/` directory. The first represents the domain that is shown above. The second one is a very similar domain but contains an airfoil (a slice of an airplane wing) instead of a circular object. You will also notice two files called `circle.bc` and `airfoil.bc` which represent the association between the boundary edges and the domain boundaries. The first two columns in the `.bc` file represent the first and second indices of the vertices of a boundary edge (one edge per line in the file). The third value represents which boundary this edge is on. An outflow boundary has `bc = 1` and an inflow boundary has `bc = 3` as in the image above.

When visualizing the solution, you can also click the "gradient" button in `flux360` which will calculate the velocity $`\mathbf{u} = \nabla\phi`$ and plot it as a vector field. This calculates the gradient of the currently selected field (be careful which one you select). You should select `phi` to calculate the gradient. You can then overlay the `velocity` field and view the velocity field near the leading edge of the airfoil as in the following figure. Note that the velocity is zero at the "stagnation point" near the leading edge of the airfoil.

![](../images/airfoil-fem.png)

#### unit testing

Nothing has been added to `flux-base` that requires testing today.
