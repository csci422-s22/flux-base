#include "element.h"
#include "halfedges.h"
#include "linear_algebra.h"
#include "mat.hpp"
#include "mesh.h"
#include "readwrite.h"
#include "spmat.h"
#include "vec.hpp"
#include "webgl.h"

#include <cstdio>
#include <fstream>
#include <map>

using namespace flux;

/**
 * \brief Solves for the flow field of an incompressible, irrotational, invisicd fluid
 *        using linear finite elements.
 */
class FlowSolver {
public:
  /**
   * \brief Initialize the flow solver, saving the mesh on which to solve.
   *
   * \param[in] mesh - reference to the mesh for the solution process
   */
  FlowSolver( const Mesh<Triangle>& mesh ) :
    mesh_(mesh)
  {}

  /**
   * \brief Solves the fluid flow equations given the edges on the boundary.
   *
   * \param[in] bcs - keys are edges (e0,e1) and values are the boundary labels for boundary conditions (bc).
   *            bc = 0 is the bottom of the domain (dphi/dn = 0)
   *            bc = 1 is the right of the domain (dphi/dx = uinf, dphi/dy = 0)
   *            bc = 2 is the top of the domain (dphi/dn = 0)
   *            bc = 3 is the left of the domain (dphi/dx = uinf, dphi/dy = 0)
   *            bc = 4 is the interior boundary (dphi/dn = 0)
   */
  void solve( const std::map< std::pair<int,int> , int >& bcs );

  /**
   * \brief Returns the velocity potential values (size = # vertices).
   */
  const std::vector<double>& phi() const { return phi_; }

  /**
   * \brief Returns the velocity magnitude (size = # triangles)
   */
  const std::vector<double>& speed() const { return speed_; }

private:
  const Mesh<Triangle>& mesh_;
  std::vector<double> phi_;
  std::vector<double> speed_;
};

void
FlowSolver::solve( const  std::map< std::pair<int,int> , int >& bcs ) {

  // this is the 3x2 dpsi_dxi matrix
  mats<3,2,double> psi_xi;
  psi_xi(0,0) = -1;
  psi_xi(0,1) = -1;
  psi_xi(1,0) = 1;
  psi_xi(2,1) = 1;

  // iniitalize the sparse n x n matrix (n = # vertices)
  spmat<double> K( mesh_.vertices().nb() , mesh_.vertices().nb() );

  printf("--> building stiffness matrix...\n");
  for (int k = 0; k < mesh_.nb(); k++) {

    int i0 = mesh_(k,0);
    int i1 = mesh_(k,1);
    int i2 = mesh_(k,2);

    const double* p0 = mesh_.vertices()[i0];
    const double* p1 = mesh_.vertices()[i1];
    const double* p2 = mesh_.vertices()[i2];

    // calculate element jacobian
    mats<2,2,double> J;
    for (int d = 0; d < 2; d++) {
      J(d,0) = p1[d] - p0[d];
      J(d,1) = p2[d] - p0[d];
    }

    // a) calculate the inverse of J

    // b) calculate 3x2 matrix G = psi_x = psi_xi * J^(-1)

    // c) calculate M = 0.5 * G * transpose(G) * det(J)

    // d) add components of M into appropriate rows/columns of K

    flux_implement;
  }
  printf("done!\n");

  // compute the right-hand-side
  printf("--> applying boundary conditions...\n");
  vecd<double> f( mesh_.vertices().nb() );
  for (auto& edge : bcs) {

    int e0 = edge.first.first;
    int e1 = edge.first.second;

    // coordinates of the edge endpoints (2d)
    vec3d x0( mesh_.vertices()[e0] , 2 );
    vec3d x1( mesh_.vertices()[e1] , 2 );

    // which bc is this on?
    int bc = edge.second;
    if (bc == 1 || bc == 3) { // inlet = 3, outlet = 1

      // a) calculate length of edge ||e||

      // b) calculate 0.5 * uinf * ||e||

      // c) add result to right-hand-side fector f (with appropriate sign, whether bc = 1 or 3)
      flux_implement;
    }
  }

  // solve K * x = f and save the solution into phi_
  phi_.resize( mesh_.vertices().nb()  );
  vecd<double> x( f.m() );
  K.solve_nl(f,x,true);
  for (int k = 0; k < x.m(); k++)
    phi_[k] = x[k];

  // post-process: calculate magnitude of velocity
  speed_.resize( mesh_.nb() );
}

int
main( int argc, char** argv ) {

  Viewer viewer;

  // either "circle" or "airfoil"
  std::string name = "circle";

  // read the mesh
  std::unique_ptr<MeshBase> mesh_ptr = read_obj("../data/" + name + ".obj" );
  Mesh<Triangle>& mesh = *static_cast<Mesh<Triangle>*>(mesh_ptr.get());

  // read the edges on the boundary, and the appropriate boundary label
  std::map<std::pair<int,int>,int> bc;
  std::ifstream file("../data/" + name + ".bc");
  std::string line;
  while (!file.eof()) {
    std::getline(file,line);
    int e0, e1, b;
    int nitems = sscanf( line.c_str() , "%d %d %d\n",&e0,&e1,&b);
    if (nitems <= 0) continue;
    flux_assert( nitems > 0 );
    bc.insert( {{e0,e1},b} );
  }

  // set up the flow solver and solve
  FlowSolver fem(mesh);
  fem.solve( bc );

  // attach the solution to the mesh
  mesh.create_vertex_field("phi" , fem.phi());
  mesh.create_cell_field("speed", fem.speed() );

  viewer.add(mesh);
  viewer.run();

}
