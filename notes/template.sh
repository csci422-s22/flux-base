#!/bin/sh

echo "--> creating template from $2/$1 to $3/$1"

if [ -d "$2/$4" ]; then
  echo "--> directory $4 does not exist ..."
  mkdir "$3/$4"
fi

if [ -f "$3/$1" ]; then
  read -p 'file exists: overwrite? [yes/no] ' overwrite
  echo "responded ${overwrite}"
  if [ "${overwrite}" == "yes" ]; then
    echo "--> overwriting file"
  else
    echo "not copying"
    exit 0
  fi
else
  echo "file does not exist"
fi

cp "$2/$1" "$3/$1"
#cp $2/$4/*.md $3/$4/
