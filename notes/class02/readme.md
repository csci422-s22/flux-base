## lecture 02 - object-oriented programming & the STL

### objectives

- clone a repository with `git`
- define classes and create objects,
- follow the conventions we will use throughout the semester,
- practice some more with header files,
- use common data structures and functions from the Standard Template Library (STL).

### getting the code template

Before proceeding, please ensure you installed a `c++` compiler and `git` on your system using the instructions from last class. You can check if the installation was successful by opening a Terminal (within a Docker container if you are on Windows) and typing:
```bash
$ g++
error: no input files
$ git --version
git version 2.34.1
```
Version numbers will vary.

Use the console to navigate to a directory where you want to save your work for today and clone the template repository:
```bash
git clone https://gitlab.com/csci422-s22/class02.git class02
```

You should now open up VS Code and click on the Live Share button at the bottom-left of the VS Code application. You will be prompted to sign in.

![](../images/liveshare-01.png)

Please sign in with your Middlebury email address **even if you have a GitHub account**.

After signing in, your name should appear in the bottom-left area where it previously said Live Share. Click on your name, and you should see some sharing options (below).

![](../images/liveshare-02.png)

Form groups of two (with the person sitting next to you). Then, you only need one person to click on  "Invite Others (Copy Link)" and send it to the second person (a private message in our Slack workspace would be a good idea for sharing the link). The second person should then open the invitation and ensure that you can see each other's cursor as you move around the code template (open up `main.cpp`).

### passing by reference versus passing by value

Last time, we looked at modifying the elements of an array by passing a pointer to the first element in the array. Alternatively, we can pass *references* to our variables and modify those variables directly within the function, and the modified value will be accessible in the caller of our function.

Add the following above `main` (in `main.cpp`):

```c++
void
multiply_by( double& x , double y ) {
  x *= y;
}
```

Now add the following inside the body of `main`:

```c++
double a = 2;
double b = 3;

std::cout << "a = " << a << std::endl;
multiply_by( a , b );
std::cout << "a = " << a << std::endl;
```

You should see:
```bash
a = 2
a = 6
```

The `&` right after the `double` (and before the `x`) means that `x` is passed *by reference*, which means we are passing an alias to the variable instead of a copy of the variable. The address of this alias is the same as the address of the original variable.

Aside from being able to modify the variable within the function this is also important for making sure that large chunks of data (stored in an object, for example) are not copied when we pass an object to a function. Even if you don't want to modify the object, passing parameters by reference is preferred. If you want to write really good code, you can use `const` to promise the compiler that you won't change the variable. For example, we can modify our `multiply_by` function to be

```c++
void
multiply_by( double& x , const double& y ) {
  x *= y;
}
```

For built-in types like integers and floating points numbers, we usually won't add this `const` qualifier, but you should use it when passing objects that will remain constant during the execution of the function.


### defining a class & creating objects

Here is a very simple class that represents a 3d point (defined in `point.h`):

```c++

class Point {

public:
  Point( double x , double y , double z ); // constructor 1
  Point( double *coords );                 // constructor 2

  double  x() const { return x_; } // return the x-coordinate
  double& x()       { return x_; } // return a reference to x-coordinate so we can modify it

  // complete y() and z() similar to above

  double  x( int d ) const;
  double& x( int d );

private:
  double x_, y_, z_;  
};
```

This is just a class declaration, and only some of the methods are implemented. You can implement some of the functions directly in the class definition (as was done for `x()`), or you can implement them elsewhere, as we'll do with some of the other functions in `point.cpp`. We have two options (based on the constructors we've declared) to create an object of type `Point`:

```c++
Point p(0,1,2);

double coords[3] = {3,4,5};
Point q(coords);
```

#### `public` versus `private` (and also `protected`)

Anything after `public` will be publicly available to any use of an object. For example, with an object `p` of type `Point`, we could access `p.x()`. However, anything we declare after `private` will only be accessible internally within the class, either as a member variable (as we have defined here, `x_`, `y_`, and `z_`) or as other functions that you only want to call from functions within your class. Using the same example object, we would get a compiler error if we tried to access `p.x_`.

As a general convention, we will use trailing underscores to denote member variables, whatever type they may be. You might notice other conventions in other `C++` projects, but this is the one we'll stick with.

There's another way to declare whether a function is public or private, called `protected`. This has to do with inheritance, which we'll talk about next class.

#### relationship to `struct`'s
If you've used `C` before, you may have created `struct`'s, which you can think of as a class in which everything is `public`. For example, we could have

```c++
struct Point {

  Point(double x , double y , double z );
  // etc.

  double x_, y_, z_; // these are all public
};
```

----

### the Standard Template Library (STL)

#### `vector` ([doc](https://www.cplusplus.com/reference/vector/vector/))

Most of the time, we won't know the size of arrays at compile time - we'll only be able to determine them at runtime. Although there are ways to handle this with pointers (using `new` or `malloc`), you should almost never use these methods. The STL provides a very commonly-used data structure for representing arrays, called a `vector` (not to be confused with vectors from linear algebra). To use `vector`'s, you'll need to `#include <vector>`.

You can have a `vector` of anything - this anything is specified by whatever you put in between the angle brackets. Here are a few common operations with vectors:
```c++

std::vector<double> X; // creates an empty vector of doubles
std::vector<int> Y(5); // creates a vector of 5 integers
std::vector<float> Z(10,1); // creates a vector of 10 floats, each equal to 1

X.push_back(10.); // X is now {10.} and has size 1
Y.resize(8); // Y is now {0,0,0,0,0,0,0,0}
Z.clear(); // Z is now {} (empty)

std::cout << "|X| = " << X.size() << ", |Y| = " << Y.size() << ", |Z| = " << Z.size() << std::endl;
```

There are other functions for inserting or erasing elements at particular locations, which you can read about [here](https://www.cplusplus.com/reference/vector/vector/).

A natural way to iterate through the items in a `vector` `X` is:

```c++
for (int i = 0; i < X.size(); i++) {
  std::cout << "X[" << i << "] = " << X[i] << std::endl;
}
```

To obtain a pointer to the beginning of the vector, you can use `X.data()` or `&X[0]`. The former is safer in case the array has a size of 0.

#### iterators

Another way to iterate through the items in a `vector` is by using *iterators*. They are accessible via the ```std::vector<T>``` namespace (where `T` is the type you are storing in the `vector`). For a vector `X`, `X.begin()` returns an iterator to the beginning of the `vector`. We can go to the next item using the `++` operator on our iterator. The value at the iterator can be obtained by dereferencing it.

```c++
std::vector<double>::iterator it;
for (it = X.begin(); it != X.end(); ++it) {
  std::cout << *it << std::endl;
}
```
Note, however, that we don't have access to the index, which makes this a little unnatural for vectors. Iterators are much more useful for `map`'s and `set`'s (below).

You can also have a `const_iterator`. This is useful in case you pass a `vector` to a function and the function promises to treat the vector as `const`.

#### `string` ([doc](https://www.cplusplus.com/reference/string/string/))

The STL has a built-in data structure to represent strings. To use it, you'll need to `#include <string>`. For example,

```c++
std::string s = "hello";
std::cout << "length of s = " << s.size() << std::endl;

s.clear();
std::cout << "length of s = " << s.size() << std::endl;

std::cout << "3rd character = " << s[2] << std::endl;
```

To convert a number to a string, you can use `std::to_string`.

#### `map` ([doc](https://www.cplusplus.com/reference/map/map/))

A map is like a dictionary: there are keys and values. Thus, instead of only having a single type to pass inside the angle-brackets, we need to pass two things. For example,

```c++
std::map<int,double> m;

m[1] = 3.2;
m[9] = 0.6;

std::cout << "m.size() = " << m.size() << std::endl; // should be 2

std::map<int,double>::const_iterator it;
for (it = m.begin(); it != m.end(); ++it) {
  std::cout << "value at key " << it->first << " = " it->second << std::endl;
}
```

The `iterator` for  `map` is essentially a pointer to a key-value `pair`. The `first` attribute accesses the first item in the pair (the key) and the `second` attribute access the second item (the value). Just like a pointer, we need to access these attributes using `->`, in contrast to if it was not a pointer (a value, or object), in which we would use dot-notation (`.`).

#### `set` ([doc](https://www.cplusplus.com/reference/set/set/))

We won't use `set`'s a whole lot because most of what we'll do in this course will require a `map` between stuff, but they're good to know about.

A set is just a collection of objects (of any type) - again, this type is specified by whatever you put in between angle-brackets.

```c++
std::set<Point> points;

// p and q were defined above
points.insert(p);
points.insert(q);

std::set<Point>::const_iterator sit;
for (sit = points.begin(); sit != points.end(); ++sit) {
  std::cout << "point = (" << sit->x() << "," << sit->y() << "," << sit->z() << ")" << std::endl;
}

std::cout << "number of points = " << points.size() << std::endl;
```

This won't work immediately, because a `set` needs to know how to compare two items in the `set`. To make this work, we need to define the `<` (less-than) comparison `operator`. We'll talk about operator overloading next time, but for now, just copy the following to the top of `main.cpp` (but below the includes).

```c++
bool
operator<(const Point& p , const Point& q ) {
  for (int d = 0; d < 3; d++) {
    if (p.x(d) < q.x(d)) return true;
  }
  return false;
}
```

#### smart pointers

For most of the algorithms we will develop, you'll know at compile time which objects need to be created. But sometimes, we don't know and we need to create objects with certain properties, depending on certain conditions.

`C++` allows you to create objects dynamically using the `new` operator. But like I said previously, you should be careful about deciding when to use it. The reason is because as soon as you allocate memory with `new`, you're responsible for freeing the memory when your program terminates, which can be done using `delete`. But if your program crashes before you expect it to, that memory won't get released back to the operating system.

This is where smart pointers come in. They are "smart" because they know if they are being used, and when they are no longer used, then they release their memory. Once upon a time, I had fun implementing my own smart pointer class, but luckily the STL provides various types of smart pointers, which you should always use if you need to create objects dynamically.

The ones we will use the most are called `unique_ptr` and `shared_ptr` and can be used with `#include <memory>`. Like other STL stuff, we need to specify the type via angle brackets.

```c++
std::shared_ptr<Point> point_ptr = std::make_shared<Point>(coords);

Point* raw_point_ptr = point_ptr.get();
Point& point = *raw_point_ptr;

std::cout << "p = (" << point_ptr->x() << "," << point_ptr->y() << "," << point_ptr->z() << ")" << std::endl;
```

The example above uses a shared pointer, which maintains a reference counter and deletes its memory resources when the reference counter goes to zero. We could have also used a "unique pointer" in this example (via `std::unique_ptr` and `std::make_unique`) since we weren't going to share the memory used for a `Point` with another smart pointer. If you try this, you will need to change the `C++` standard in the Makefile to `c++14`. When you can, try to use a `unique_ptr` over a `shared_ptr`.

#### algorithms

The `algorithm` header provides several functions for commonly used algorithms, such as sorting:

```c++
std::vector<int> values = {6,8,3,2,0,-2,5,5};

std::sort( values.begin() , values.end() );

for (int i = 0; i < values.size(); i++)
  std::cout << "values[" << i << "] = " << values[i] << std::endl;
```

### before next class

Install `CMake` on your system:
- **Mac OS X:**
  ```bash
  $ brew install cmake
  ```
- **Linux (including WSL):**
  * open a terminal:
  ```bash
    sudo apt-get install cmake
  ```
- **Windows**
  * You should be all set if you set up Docker last time!
  * One thing to note: if git is not letting you clone or commit to a repository, you might need to do:
  ```bash
  git config --global http.sslVerify false
  ```
  However, it would be better to use `Git Bash` (within your native Windows operating system) to clone, commit and push to your repositories.
