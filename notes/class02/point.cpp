#include "point.h"

#include <exception>

Point::Point( double x , double y , double z ) :
	x_(x),
	y_(y),
	z_(z)
{}

Point::Point( const double* x ) :
	x_(x[0]),
	y_(x[1]),
	z_(x[2])
{}

double
Point::x( int d ) const {
  if (d == 0) return x_;
  if (d == 1) return y_;
  if (d == 2) return z_;
  throw std::exception();
}

double&
Point::x( int d ) {
  if (d == 0) return x_;
  if (d == 1) return y_;
  if (d == 2) return z_;
  throw std::exception();
}

