#include "point.h"

#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <vector>

#define PASS_POINTERS 1 // set either to 0 or 1

#if PASS_POINTERS

// pass in pointers to the variables
void
multiply_by( double* x , double* y ) {
  *x *= *y;
}

#else

// pass in x by reference
void
multiply_by( double& x , double y ) {
  x *= y;
}

#endif

bool
operator<(const Point& p , const Point& q ) {
  for (int d = 0; d < 3; d++) {
    if (p.x(d) < q.x(d)) return true;
  }
  return false;
}

int
main() {

  double a = 2;
  double b = 3;

  std::cout << "a = " << a << std::endl;
  #if PASS_POINTERS
  multiply_by( &a , &b );
  #else
  multiply_by( a , b );
  #endif
  std::cout << "a = " << a << std::endl;

  Point p(0,1,2);

  double coords[3] = {3,4,5};
  Point q(coords);

  std::cout << "p = (" << p.x() << "," << p.y() << "," << p.z() << ")" << std::endl;

  p.x() = 2;
  std::cout << "p = (" << p.x() << "," << p.y() << "," << p.z() << ")" << std::endl;

  p.x(2) = 5;
  std::cout << "p = (" << p.x() << "," << p.y() << "," << p.z() << ")" << std::endl;

  try {
    p.x(5) = 10;
    std::cout << "p = (" << p.x() << "," << p.y() << "," << p.z() << ")" << std::endl;
  }
  catch(...) {
    std::cout << "execption!" << std::endl;
  }

  // vectors
  std::vector<double> X; // creates an empty vector of doubles
  std::vector<int> Y(5); // creates a vector of 5 integers
  std::vector<float> Z(10,1); // creates a vector of 10 floats, each equal to 1

  X.push_back(10.); // X is now {10.} and has size 1
  Y.resize(8); // Y is now {0,0,0,0,0,0,0,0}
  Z.clear(); // Z is now {} (empty)

  std::cout << "|X| = " << X.size() << ", |Y| = " << Y.size() << ", |Z| = " << Z.size() << std::endl;

  for (int i = 0; i < X.size(); i++) {
    std::cout << "X[" << i << "] = " << X[i] << std::endl;
  }

  std::vector<double>::iterator vit;
  for (vit = X.begin(); vit != X.end(); ++vit) {
    std::cout << *vit << std::endl;
  }

  // maps
  std::map<int,double> m;

  // assign key-value pairs using square brackets
  m[1] = 3.2;
  m[9] = 0.6;

  std::cout << "m.size() = " << m.size() << std::endl; // should be 2

  // notice that we are declaring it const-iterator, which means we won't modify the map
  std::map<int,double>::const_iterator mit;
  for (mit = m.begin(); mit != m.end(); ++mit) {
    //mit->second = 2; // compiler error
    std::cout << "value at key " << mit->first << " = "<< mit->second << std::endl;
  }

  // exercise: make a map from strings to prices for items in a grocery store
  // add three items and print out the contents
  std::map<std::string,double> price;
  price["apple"]  = 0.99;
  price["banana"] = 0.25;
  price["tofu"]   = 1.99;

  std::map<std::string,double>::const_iterator pit;
  for (pit = price.begin(); pit != price.end(); ++pit) {

    // either of the following two methods would work, the third would not
    std::string key = pit->first;
    //const std::string& key = pit->first;
    //std::string& key = pit->first;

    // retrieve the value using the .at function
    double value = price.at(key); // or can use pit->second;

    // the iterator for a map behaves like a pointer to a pair (dereference using *)
    const std::pair<std::string,double>& p = *pit;

    std::cout << "value at key " << key << " = "<< value << std::endl;
    std::cout << "also,    key " << p.first << " = "<< p.second << std::endl;
  }

  // set of points
  std::set<Point> points;
  points.insert(p);
  points.insert(q);

  std::set<Point>::const_iterator sit;
  for (sit = points.begin(); sit != points.end(); ++sit) {
    std::cout << "point = (" << sit->x() << "," << sit->y() << "," << sit->z() << ")" << std::endl;
  }
  std::cout << "number of points = " << points.size() << std::endl;

  // shared pointers
  std::shared_ptr<Point> point_ptr = std::make_shared<Point>(-1,5,8);

  Point* raw_point_ptr = point_ptr.get();
  Point& point = *raw_point_ptr;

  std::cout << "p = (" << point_ptr->x() << "," << point_ptr->y() << "," << point_ptr->z() << ")" << std::endl;

  // algorithms
  std::vector<int> values = {6,8,3,2,0,-2,5,5};

  std::sort( values.begin() , values.end() );

  for (int i = 0; i < values.size(); i++)
    std::cout << "values[" << i << "] = " << values[i] << std::endl;

  return 0;
}
