### lecture 12 - surface reconstruction

#### objectives

- define and visualize an isosurface on a 3d grid,
- reconstruct a surface from a point cloud and an implicit function using the Marching Tetrahedra algorithm.

#### motivating exercise

Directly, rendering a function defined in 3d is difficult. It is common to have a volumetric data set (points defined in 3d with a scalar value attachd to them) and we would like to determine constant values of this data set. For example, aerodynamicists may be interested in visualizing regions of constant pressure or density when simulating the flow field around an aircraft. Similarly, medical images (e.g. MRI) might contain the density of tissue defined at a 3d point. This density allows us to visualize a particular organ or bone, so the goal would be to reconstruct the surface defined by a constant density value. This surface-of-constant-value is called an *isosurface*.

As a warm-up exercise, please open `isosurface.cpp` in the `notes/class12` directory. We will practice defining and visualizing an isosurface on a structured grid. This isosurface will be defined as the distance from a sphere. `flux360` provides an option for "clipping" the volume mesh so that we can visualize the interior. This doesn't fully allow us to visualize the isosurface, so we will introduce the "Marching Cubes" and "Marching Tetrahedra" algorithms to do so.

<img src="../images/sphere-isosurface-clip.png" width="400"/>

#### Marching Cubes

A famous algorithm for surface reconstruction is called the Marching Cubes algorithm. Interestingly, the Marching Cubes algorithm was patented (filed in 1985 by the authors who worked at General Electric), and [no one was allowed to use it without royalties for several years](https://www.liquisearch.com/marching_cubes/patent_issues). Luckily, the patent has now expired, so we can study it. The basic idea is to (1) visit cells in a structured grid on which our scalar function is defined and (2) determine the geometry of the isosurface within the cell by "polygonizing" the interior of the cell. For a single hexahedral cell, there are 256 possible polygon cases (some shown below). I won't present further details of how the algorithm works here, because we will focus on the Marching Tetrahedra algorithm, which is very similar but simpler to implement.

![](../images/marching-cubes.png)

#### Marching Tetrahedra

The basic inputs to our problem are (1) a tetrahedral grid (2) some way of figuring out whether an arbitrary 3d point is inside or outside of the surface (or have a function value less than or greater than the desired isosurface value). Again, the basic idea is to march through the cells of the grid, determining what the isosurface looks like within each cell, and continuously build up a surface triangulation depending on how the surface "cuts" the current tetrahedron.

The first step in the algorithm consists of initializing a tetrahedralization (yes, tetrahedra, not triangles). As in the exercise, you can use the `Grid<Tet>` mesh defined in `grid.cpp`. The more divisions you use in this input mesh, the better resolution you'll have of the surface (but the more expensive it will be).

Next we need to evaluate our function at the grid points of the tetrahedralization. We will loop through the tetrahedra and determine which of its six edges contain endpoint vertices that have different function signs. This will dictate how we should triangulate the surface that intersects our tetrahedra. We will be doing a linear interpolation, and whenever a plane cuts a tetrahedron, we will always be in one of three cases: (1) no intersection at all, (2) three edges are intersected thus defining a single triangle or (3) four edges are intersected thus defining a quad (two triangles).

There are 16 possible cases we could be in (see image below), which isn't that bad compared to the 256 possible cases for the original Marching Cubes algorithm. The 16 comes from the fact that there are 4 vertices per tetrahedron, and 2 possible signs of the function each vertex could have (and $2^8$ in the case of a cube).

#### determining intersection points and polygonizing

For any edge that is intersected, we can create a point along the edge that uses the endpoint function values to weight the location of the intersection point. Let $ `f_0`$ and $`f_1`$ denote the function values at the endpoints of the edge with coordinates $`\mathbf{e}_0`$ and $`\mathbf{e}_1`$, respectively. Let $`d_0 = \lvert f_0 \rvert`$ and $`d_1 = \lvert f_1 \rvert`$. The weights on the endpoint vertices coordinates can be computed as
```math
w_0 = \frac{d_1}{d_0+d_1}, \quad w_1 = \frac{d_0}{d_0+d_1}
```
The intersection point $`\mathbf{x}`$ is then
```math
\mathbf{x} = w_0 \mathbf{e}_0 + w_1 \mathbf{e}_1
```

The last step is to triangulate the intersection points. A more elegant way of handling the intersections is to preprocess the side of every vertex and then use binary arithmetic to figure out which case we are in. There is symmetry between the different cases. For example, the geometry obtained if vertex 1 has a function value less than the isovalue is symmetric to the case when vertex 1 has a function value greater than the isovalue, but vertices 0, 2 and 3 have a function value less than the isovalue. See the image below which describes the bits that encode each case:

![](../images/marching-tets.png)

#### implementation notes

If you decide to implement the Marching Cube/Tet algorithm for your final project, I would recommend having a look at [this really great website](http://paulbourke.net/geometry/polygonise/) for polygonizing shapes using a scalar field. The source code for polygonizing a tetrahedron is [here](http://paulbourke.net/geometry/polygonise/source1.c). You should also use the `Grid<Tet>` class to create the initial volume mesh.


You should also make sure that the resulting triangulation is watertight. This means that any vertex shared by several triangles is unique (not duplicated). You can do this by keeping track of which edges have been inserted along, so that any tetrahedron in the shell of an edge (which is intersected), will first check if the intersection point exists (by symbolically checking if the edge has been inserted upon) and either (1) returning the index of the point previously created or (2) creating the point and returning the index of the newly created point.

If you decide to implement this algorithm for your final project, you should first start out by defining an analytic function on your tetrahedral grid. I would recommend using a sphere to start off. Your implicit function could be the distance to the sphere, which is negative for points inside the sphere and positive for points outside the sphere.

Another option would be to compute the distance to an existing surface mesh. This probably seems pointless since you already have a surface mesh, but it allows you to obtain a different mesh (maybe with more, or fewer triangles) by controlling the size of the original tetrahedral grid. Again, you'll need to compute the distance to your input surface mesh to calculate the implicit function values. Have a look at Section 1.5 in *Polygon Mesh Processing* for some ideas on how to compute this distance. I would also recommend using a kd-tree for finding closest points.

In the left figure below, a tetrahedral grid with 20 divisions in each direction was used to reconstruct the surface of a sphere. On the right, a tetrahedral grid with 50 divisions in each direction was used to reconstruct our mesh of Spot.

<img src="../images/sphere-reconstructed.png" width="400"/>
<img src="../images/spot-reconstructed.png" width="400"/>

I also recommend checking out this really cool demo: [https://d3x0r.github.io/MarchingTetrahedra/](https://d3x0r.github.io/MarchingTetrahedra/)


#### unit testing ideas

Nothing has been added to `flux-base` that requires testing today.
