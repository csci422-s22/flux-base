#include "element.h"
#include "grid.h"
#include "vec.hpp"
#include "webgl.h"

using namespace flux;

int
main( int argc , char** argv ) {

  int n = 50;
  Grid<Tet> grid( {n,n,n} );

  vec3d center = {0.5,0.5,0.5};
  double radius = 0.25;

  // create a vertex field
  std::vector<double> distance( grid.vertices().nb() );
  for (int i = 0; i < grid.vertices().nb(); i++) {
    vec3d p( grid.vertices()[i] );
    distance[i] = norm( p - center ) - radius;
  }
  grid.create_vertex_field("distance" , distance );

  Viewer viewer;
  viewer.add(grid);
  viewer.run();

}
