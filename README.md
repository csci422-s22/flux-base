### flux-base

`flux` is a framework for learning unstructured meshing. This repository provides a foundation for the higher-level `flux` repository which is where exercises and projects should be implemented. This repository should be used as a git submodule within the higher-level `flux` repository, although it could be used as a standalone project as well.

#### dependencies

- `git`
- `CMake`
- `C` and `C++` compilers (supporting the `C++14` standard)
- (optional) `gdb valgrind lcov`

Please see the installation instructions at the end of `notes/class01/readme.md`.

#### downloading and building

You should only follow these steps if you intend to develop directly within the `flux-base` repository, and not from the higher-level `flux` repository template.

-  `$ git clone git@gitlab.com:csci422-s22/flux-base.git`
- `$ cd flux-base`
- `$ mkdir build`
- `$ mkdir build/release`
- `$ cd build/release`
- `$ cmake ../../`
- `$ make`

The latter command will build the `flux` library as well as all unit tests.

#### running unit tests

Within `flux-base/build/release`:

`$ make core_array2d_ut`

This will run the suite of unit tests in the file `flux-base/test/core/array2d_ut.cpp`.

To run all the unit tests together:

`$ make unit`

#### debugging

To debug, you must install the `gdb` and `valgrind` dependencies.

Instead of creating a `build/release` directory, create a `build/debug directory`:

- `$ mkdir build/debug`
- `$ cd build/debug`
- `$ cmake ../../`

The `CMake` configuration files will automatically detect that this build is intended for debugging, and will add debugging symbols when compiling the source.

To run `gdb` on a particular unit test:

`$ make core_array2d_ut_gdb`

and then enter `r` to run.

To run the unit test through `valgrind`:

`$ make core_array2d_ut_memcheck`

which will report any memory leaks.


#### checking for code coverage

To check for code coverage, you must install the `lcov` dependency.

Now, we will create a `build/coverage` directory (again, `CMake` will detect this build is intended for checking code coverage):

- `$ mkdir build/coverage`
- `$ cd build/coverage`
- `$ cmake ../../`

To check for code coverage, we will run the `unit` executable (all the unit testing suites compiled together) with a specific target for code coverage:

`$ make unit_coverage`

This will take a while to run and there will be temporary files generated in your repository, which will be deleted when execution is complete.

To display the code coverage information:

`make coverage_show`

which should open a tab in your browser with code coverage information. Current code coverage information is displayed at https://csci422-s22.gitlab.io/flux-base/.

#### license

`flux` is distributed under the MIT License (https://mit-license.org/), however some of the dependencies such as `Triangle` and `TetGen` are only free for academic use (please see the accompanying licenses in `src/external` for details).

MIT License

Copyright (c) 2022 Philip Caplan

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
