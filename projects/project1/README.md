### project 1 report

**Names:**

(part 4)

| n | error |
| :---: | :---: |
| 10 | e0 |
| 100 | e1 |
| 1000 | e2 |

Compute $`s = \log(e_2/e_1) / \log(n_2/n_1) = `$
