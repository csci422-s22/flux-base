#include "element.h"
#include "grid.h"
#include "vec.h"
#include "vec.hpp"
#include "webgl.h"

using namespace flux;

template<typename type>
class Sphere : public Grid<type> {

public:
  Sphere( int nphi , int ntheta , double radius = 1 ) :
    Grid<type>({nphi,ntheta},3),
    nphi_(nphi),
    ntheta_(ntheta),
    radius_(radius) {

    // build the sphere
    build();
  }

private:

  void build();

  int nphi_;
  int ntheta_;
  double radius_;

  using Grid<type>::vertices_;

};

template<typename type>
void
Sphere<type>::build() {

  flux_assert( (nphi_+1)*(ntheta_+1) == vertices_.nb() );

  // part 1
  flux_implement;

}


int main() {

  Viewer viewer;

  // you should also test n = 100, 1000
  int n = 10;
  Sphere<Triangle> mesh( 10 , 10 );

  // TODO part 3

  // add the mesh to the visualizer and then run the server
  viewer.add(mesh);
  viewer.run();

}
