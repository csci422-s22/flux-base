### project 2 report

**Names:**

| N | time |
| :---: | :---: |
| 100  | t0 |
| 1k   | t1 |
| 10k  | t2 |
| 100k | t3 |

Please include pictures of your Delaunay triangulations and Voronoi diagrams as described in the assignment.
