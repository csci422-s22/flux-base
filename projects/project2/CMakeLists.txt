# add the project2 executable and link it to flux, build with:
# $ make project2_exe
add_executable( project2_exe EXCLUDE_FROM_ALL main.cpp delaunay.cpp )
target_link_libraries( project2_exe flux_shared )

# the visualizer needs to know it can run (since this is not a full unit test)
target_compile_definitions( project2_exe PUBLIC -DFLUX_FULL_UNIT_TEST=false )

# add a custom target for building and running project2, using:
# $ make project2
# the working directory is set to the project2 directory in case you want to read/write files relative to that path
add_custom_target( project2 command $<TARGET_FILE:project2_exe> 1 WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/projects/project2 )

# add debug targets with the working directory as project2
ADD_DEBUG_TARGETS( project2 ${CMAKE_SOURCE_DIR}/projects/project2/ )
