#include "delaunay.h"
#include "mesh.h"
#include "predicates.h"
#include "readwrite.h"
#include "vertices.h"
#include "webgl.h"

#include <cmath>
#include <time.h>

using namespace flux;

int main( int argc , char** argv ) {

  // initialize the exact predicates
  initialize_predicates();

  // initial time
  clock_t t0 = clock();

  // total number of points
  int N = 1e2;

  // the set of Delaunay vertices (in 2d)
  Vertices vertices(2);

  // create the corners
  double c[4][2] = { {0,0} , {1,0} , {1,1} , {0,1} };
  for (int i = 0; i < 4; i++)
    vertices.add( c[i] );

  // randomly create interior points reasonably spaced from the [0,1] boundary
  int n = std::sqrt(N);
  double dx = 1.0/n;
  double x[2] = {0,0};
  int m = N - vertices.nb();
  for (int k = 0; k < m; k++) {
    for (int d = 0; d < 2; d++)
      x[d] = dx + (1.0 - 2.0*dx)*double(rand())/double(RAND_MAX);
    vertices.add(x);
  }

  // final and elapsed time in seconds
  clock_t t1 = clock();
  double elapsed_time = double(t1 - t0) / double(CLOCKS_PER_SEC);
  printf("--> initialization time = %g sec.\n",elapsed_time);

  // generate Delaunay triangulation, Voronoi diagram, and implement vertex smoothing

}
