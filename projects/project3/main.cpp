#include "element.h"
#include "linear_algebra.h"
#include "mat.hpp"
#include "mesh.h"
#include "sphere.h"
#include "readwrite.h"
#include "simplifier.h"
#include "vec.hpp"
#include "webgl.h"

using namespace flux;

// set the following to 0 to test a mesh of your choice
#define TEST_SPHERE 1

int main( int argc , char** argv ) {

  // setup the initial mesh
  #if TEST_SPHERE
  int N = 100;
  Sphere<Triangle> mesh(N,N);
  #else
  // use read_obj or read_off to read a mesh from a file
  // the working directory of this executable is "projects/project3"
  std::unique_ptr<MeshBase> mesh_ptr = read_obj("../../data/spot.obj" , false );
  Mesh<Triangle>& mesh = *static_cast< Mesh<Triangle>* >(mesh_ptr.get());
  #endif

  // add the initial mesh to the viewer
  Viewer viewer;
  viewer.add(mesh);

  // simplify your mesh to some target number of faces (triangles)
  // extract the simplified mesh and add it to the viewer
  int nf_target = 1000;

  // run the visualizer
  viewer.run();

}
