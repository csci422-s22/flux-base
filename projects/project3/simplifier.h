#ifndef FLUX_PROJECT3_SIMPLIFIER_H_
#define FLUX_PROJECT3_SIMPLIFIER_H_

#include "element.h"
#include "error.h"
#include "halfedges.h"
#include "mesh.h"

#include <set>

namespace flux {

/**
 * \brief Provides a comparison operator to order edges in a set/multiset
 */
struct CompareCost {
  bool operator() ( const HalfEdge* e1 , const HalfEdge* e2) const {
    flux_assert( e1 != nullptr && e2 != nullptr);
    return e1->cost < e2->cost;
  }
};

// defines a set of HalfEdge pointers ordered by their cost
// (allows for multiple edges with the same cost)
//
// examples:
//
// CollapsePriorityQueue edges; // declare a priority queue of edges to collapse
//
// edges.insert( e ); // e is some HalfEdge pointer
//
// HalfEdge* top = *edges.begin(); // get the HalfEdge* with the highest priority (lowest cost)
//
// edges.erase( top ); // remove a HalfEdge* from the priority queue
//
typedef std::multiset<HalfEdge*,CompareCost> CollapsePriorityQueue;

} // flux

#endif
