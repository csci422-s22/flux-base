#!/bin/bash

workspace=$(git rev-parse --show-toplevel)

echo "synchronizing ${workspace}"
cp "$workspace"/src/flux-base/.gitlab-ci.yml "$workspace"/.gitlab-ci.yml
cp "$workspace"/src/flux-base/tools/commit.sh "$workspace"/tools/
cp "$workspace"/src/flux-base/tools/docker.sh "$workspace"/tools/
cp "$workspace"/src/flux-base/tools/Dockerfile "$workspace"/tools/
cp "$workspace"/src/flux-base/data/*.obj "$workspace"/data/
cp "$workspace"/src/flux-base/data/*.off "$workspace"/data/
cp "$workspace"/src/flux-base/data/*.png "$workspace"/data/
cp "$workspace"/src/flux-base/data/*.jpg "$workspace"/data/
cp "$workspace"/src/flux-base/data/*.bc "$workspace"/data/
cp "$workspace"/src/flux-base/tools/suppressions/* "$workspace"/tools/suppressions/


if [ -f "$workspace"/tools/Dockerfile.local ]; then
  cp "$workspace"/tools/Dockerfile.local "$workspace"/tools/Dockerfile
fi
