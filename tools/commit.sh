#!/bin/bash

workspace=`pwd`
config=$1
nproc=$2

# files might linger if a build was aborted
echo "Removing any lingering untracked files"
for f in `git ls-files --others --exclude-standard`; do
  rm -f $f
done

# update the flux-base submodule
if [ -d "src/flux-base" ]; then
	git submodule init
	git submodule update --remote src/flux-base
fi

# create the build directory if it does not exist
cmakedir=build/$config
mkdir -p $cmakedir
cd $cmakedir

CMAKE_ARGS=""

if [[ $config == *"coverage"* ]]; then
  CMAKE_ARGS="$CMAKE_ARGS"
fi

# there is a bug in gcc 4.8 which prevents from using some features needed by mpi wrapper
if [[ $config == *"gnu48"* ]]; then
  CMAKE_ARGS="$CMAKE_ARGS"
fi

# configure the project
time cmake $CMAKEARGS \
           $@ \
           $workspace

# copy files & data from base
if [ -d "src/flux-base" ]; then
  time make update
fi

if [[ $config == *"coverage"* ]]; then

  # coverage information always needs to be completely recompiled
  time make coverage_clean
fi

# build all libraries and executables
time make -j $nproc

if [[ $config == *"coverage"* ]]; then

  Xvfb :5 -screen 0 1280x1280x24 &
  export DISPLAY=:5

  time make unit_coverage
  time make coverage_info
fi

# warning if any files were generated.
# count the number of files
cd $workspace
tmpfiles=`git ls-files --others --exclude=build | wc -l`

if [ $tmpfiles -ne 0 ]; then
  echo "warning: files should not be generated in code pushed to the repository."
  echo "warning: files found :"
  for f in `git ls-files --others --exclude=build`; do
    echo "warning: $f"
  done
fi
