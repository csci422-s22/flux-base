/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
function WebGL_Renderer( url , canvasID , vertexShaderSource , fragmentShaderSource ) {

  this.resizeWindow(canvasID);
  this.textCanvas = document.getElementById('text');
  this.meshes = new Array();
  this.colormap = 'giraffe';
  this.vertexNumbers   = false;
  this.triangleNumbers = false;
  this.initGL(vertexShaderSource,fragmentShaderSource);

  this.buffers = [];

  let webgl = this;
  this.connect = function(addr,vis) {

    let websocket = window['MozWebSocket'] ? window['MozWebSocket'] : window['WebSocket'];

    let ws = new websocket(addr);
    ws.onopen  = function(evt) { console.log('connection opened'); }
    ws.onclose = function(evt) { console.log('connection closed'); }
    ws.onmessage = function(evt) {

      //console.log(evt.data);
      let message;
      try {
        message = JSON.parse(evt.data);
      }
      catch(e) {
        console.log(e);
        console.log(evt.data);
        console.error('error reading JSON');
      }
      let buffers = message['buffers'];

      console.log('unpacking ' + buffers.length + ' buffers' );
      for (let i = 0; i < buffers.length; i++) {

        let buffer = buffers[i];
        let data = buffer['data'];
        let targ = buffer['target'];
        let type = buffer['type'];
        let tag  = buffer['tag'];
        let indx = buffer['index'];

        webgl.addBuffer(data,targ,type,tag,indx);
      }
      vis.initialize(webgl);
    }
  }
}

WebGL_Renderer.prototype.resizeWindow = function(canvasID) {
  this.canvas     = document.getElementById(canvasID);
  this.textCanvas     = document.getElementById('text');
  this.canvas.width = 0.9*$(window).width();
  this.canvas.height = 0.6*$(window).height();
  this.textCanvas.width = 0.9*$(window).width();
  this.textCanvas.height = 0.6*$(window).height();
  document.getElementById('input-height').value = this.canvas.height;
}

WebGL_Renderer.prototype.setHeight = function() {
  let h = document.getElementById('input-height').value;
  if (h < 100) {
    document.getElementById('input-height').value = 100;
    return;
  }
  this.canvas.height = h;
  this.textCanvas.height = h;
  flux.initializeView();
  flux.draw();
}

WebGL_Renderer.prototype.addBuffer = function(data,target,type,tag,index) {

  let gl = this.gl;

  let buffer = gl.createBuffer();

  // bind the buffer
  if      (target == "ARRAY_BUFFER")         gl.bindBuffer( gl.ARRAY_BUFFER , buffer);
  else if (target == "ELEMENT_ARRAY_BUFFER") gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER , buffer);
  else {
    console.log('unknown buffer target ' + target);
    return;
  }

  // buffer the data
  if (type == "Float32Array") {
    gl.bufferData( gl.ARRAY_BUFFER , new Float32Array(data) , gl.STATIC_DRAW );
    buffer.type = gl.FLOAT;
  }
  else if (type == "Uint16Array") {
    gl.bufferData( gl.ELEMENT_ARRAY_BUFFER , new Uint16Array(data) , gl.STATIC_DRAW );
    buffer.type = gl.UNSIGNED_SHORT;
  }
  else if (type == "Uint32Array") {
    gl.bufferData( gl.ELEMENT_ARRAY_BUFFER , new Uint32Array(data) , gl.STATIC_DRAW );
    buffer.type = gl.UNSIGNED_INT;
  }
  else if (type == "Uint8Array") {
    buffer.type = gl.UNSIGNED_BYTE;
    console.log('--> assuming Uint8Array is an image, not buffering...');
  }
  else {
    console.log('unknown buffer type' + type);
    return;
  }

  // add the buffer to our list
  buffer.nb   = data.length;
  buffer.tag  = tag;
  buffer.data = data;
  buffer.min  = getMin(data);
  buffer.max  = getMax(data);
  this.buffers.push(buffer);
}

function getMax(arr) {
    let len = arr.length;
    let max = -Infinity;

    while (len--) {
        max = arr[len] > max ? arr[len] : max;
    }
    //console.log(max);
    return max;
}

function getMin(arr) {
    let len = arr.length;
    let min = Infinity;

    while (len--) {
        min = arr[len] < min ? arr[len] : min;
    }
    //console.log(min);
    return min;
}

WebGL_Renderer.prototype.initGL = function(vertexShaderSource,fragmentShaderSource) {

  // initialize the webgl context
  this.gl = this.canvas.getContext('webgl2') || this.canvas.getContext('experimental-webgl');
  if (this.gl == undefined) {
    alert( 'it looks like WebGL2 is not available in your browser, please enable WebGL2!' );
  }
  console.assert( this.gl != undefined );

  // create the shader program and save the attribute locations
  this.program = compileProgram( this.gl , vertexShaderSource , fragmentShaderSource );
}

function fluxWebGL() {
  this.active_vao = 0;
  this.lighting   = true;
}

fluxWebGL.prototype.initialize = function(webgl) {

  this.gl = webgl.gl;
  let gl = this.gl;
  let buffers = webgl.buffers;

  console.log('parsing ' + buffers.length + ' buffers' );

  // split up the buffer information into coordinates, triangle indices, edge indices, normals, colors, etc.
  let nb_vao = 0;
  let ids = new Set();
  for (let i = 0; i < buffers.length; i++) {
    // flux will have tagged a buffer with the convention name-index where index is the index of the vao
    const tag = buffers[i].tag;
    const s = tag.split("-vao");
    idx = parseInt(s[1]);
    ids.add(idx);
  }
  nb_vao = ids.size;
  console.log('creating ' + nb_vao + ' meshes');

  this.vao = [];
  for (let i = 0; i < nb_vao; i++)
    this.vao.push( new VertexAttributeObject() );

  for (let i = 0; i < buffers.length; i++) {

    const tag = buffers[i].tag;
    const s = tag.split("-vao");
    idx = parseInt(s[1]);

    const name = s[0];
    if (name.indexOf('coordinates') !== -1) {
      this.vao[idx].points = buffers[i];
      this.vao[idx].points.nb_indices = buffers[i].nb;
    }
    else if (name.indexOf('edges') !== -1) {
      this.vao[idx].edges = buffers[i];
      this.vao[idx].edges.nb_indices = buffers[i].nb;
      this.vao[idx].edges.visible = true;

    }
    else if (name.indexOf('triangles') !== -1) {
      this.vao[idx].triangles = buffers[i];
      this.vao[idx].triangles.nb_indices = buffers[i].nb;
      this.vao[idx].triangles.visible = true;
    }
    else if (name.indexOf('parameters') !== -1) {
      // process parameter coordinates
      this.vao[idx].parameters = buffers[i];
    }
    else if (name.indexOf('normals') !== -1) {
      // process normal vector
      this.vao[idx].normals = buffers[i];
    }
    else if (name.indexOf('field') !== -1) {
      // creating scalar attribute
      this.vao[idx].fields.push(buffers[i]);
      this.vao[idx].field_names.push(name.split('field_')[1]);
    }
    else if (name.indexOf('texture') != -1) {
      // create texture
      let s = tag.split("-size-")[1];
      s = s.split("x");
      const w = parseInt(s[0]);
      const h = parseInt(s[1]);
      const c = parseInt(s[2]);

      console.log('import texture from ',w,'x',h,'x',c,' image');
      console.assert( c == 3 );

      gl.activeTexture(gl.TEXTURE0 + 2);
      let tex = gl.createTexture();
      gl.bindTexture(gl.TEXTURE_2D, tex);   // TEXTURE0+2 is now `tex`
      gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
      gl.texImage2D(                        // upload a checkerboard pattern to `tex`
        gl.TEXTURE_2D,
        0,                // level
        gl.RGB,          // internal format
        w,                // width
        h,                // height
        0,                // border
        gl.RGB,          // format
        gl.UNSIGNED_BYTE, // type
        new Uint8Array(buffers[i].data),
      );
      /*
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
      */
      // generate the mipmap and filters
      gl.generateMipmap( gl.TEXTURE_2D );
      gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR );
      gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST );


      gl.pixelStorei( gl.UNPACK_ALIGNMENT , 1 );

      tex.width = w;
      tex.height = h;
      this.vao[idx].texture = tex;
    }
    else {
      console.log('unknown primitive type');
    }
  }

  // parameters may not be defined for some meshes
  for (let i = 0; i < this.vao.length; i++) {
    if (this.vao[i].parameters == undefined) {
      let u = new Array( 2*this.vao[i].points.nb/3 );
      let buffer = gl.createBuffer();
      gl.bindBuffer( gl.ARRAY_BUFFER , buffer );
      gl.bufferData( gl.ARRAY_BUFFER , new Float32Array(u) , gl.STATIC_DRAW );
      buffer.type = gl.FLOAT;

      // add the buffer to our list
      buffer.nb   = u.length;
      buffer.tag  = "unused";
      buffer.data = u;
      buffer.min  = getMin(u);
      buffer.max  = getMax(u);

      this.vao[i].parameters = buffer;
      console.log(buffer.nb);
    }
  }

  this.setup(webgl);
  this.draw();
}

fluxWebGL.prototype.setup = function(webgl) {

  this.gl      = webgl.gl;
  this.canvas  = webgl.canvas;
  this.program = webgl.program;
  this.textCanvas = webgl.textCanvas;

  // save the uniform locations
  this.u_ModelMatrix       = this.gl.getUniformLocation(this.program,'u_ModelMatrix');
  this.u_ViewMatrix        = this.gl.getUniformLocation(this.program,'u_ViewMatrix');
  this.u_PerspectiveMatrix = this.gl.getUniformLocation(this.program,'u_PerspectiveMatrix');
  this.u_NormalMatrix      = this.gl.getUniformLocation(this.program,'u_NormalMatrix');

  this.program.u_edges = this.gl.getUniformLocation(this.program,'u_edges');
  this.program.u_alpha = this.gl.getUniformLocation(this.program,'u_alpha');
  this.program.u_scalar = this.gl.getUniformLocation(this.program,'u_scalar');
  this.program.u_parameter = this.gl.getUniformLocation(this.program,'u_parameter');
  this.program.u_min       = this.gl.getUniformLocation(this.program,'u_min');
  this.program.u_max       = this.gl.getUniformLocation(this.program,'u_max');
  this.program.u_clip_dir  = this.gl.getUniformLocation(this.program,'u_clip_dir');
  this.program.u_lighting  = this.gl.getUniformLocation(this.program,'u_lighting');
  this.program.u_clip_center = this.gl.getUniformLocation(this.program,'u_clip_center');
  this.program.u_clip_sign = this.gl.getUniformLocation(this.program,'u_clip_sign');
  this.program.u_points    = this.gl.getUniformLocation(this.program,'u_points');

  // setup the callbacks for both canvases (webgl and text)
  this.dragging = false;
  this.rotating = false;
  let controller = this;
  this.canvas.addEventListener( 'mousemove' ,  function(event) { mouseMove(event,controller); } );
  this.canvas.addEventListener( 'mousedown' ,  function(event) { mouseDown(event,controller); } );
  this.canvas.addEventListener( 'mouseup' ,    function(event) { mouseUp(event,controller); } );
  this.canvas.addEventListener( 'mousewheel' , function(event) { mouseWheel(event,controller);} );
  this.canvas.addEventListener( 'wheel' , function(event) { mouseWheel(event,controller);} );


  this.textCanvas.addEventListener( 'mousemove' ,  function(event) { mouseMove(event,controller); } );
  this.textCanvas.addEventListener( 'mousedown' ,  function(event) { mouseDown(event,controller); } );
  this.textCanvas.addEventListener( 'mouseup' ,    function(event) { mouseUp(event,controller); } );
  this.textCanvas.addEventListener( 'mousewheel' , function(event) { mouseWheel(event,controller);} );
  this.textCanvas.addEventListener( 'wheel' , function(event) { mouseWheel(event,controller);} );

  document.addEventListener('contextmenu' , event => event.preventDefault() );

  // setup the model and perspective matrices
  this.modelMatrix = mat4.create();

  // initialize the texture for the colormap
  let gl = this.gl;
  let texture = this.gl.createTexture();
  this.gl.bindTexture( this.gl.TEXTURE_2D , texture );

  const data = new Float32Array(colormap['viridis']);
  const level = 0;
  const internalFormat = gl.RGB32F;
  const width = data.length/3;
  const height = 1;
  const border = 0;
  const type = gl.FLOAT;
  const alignment = 1;

  gl.pixelStorei( gl.UNPACK_ALIGNMENT , alignment );
  gl.texImage2D( gl.TEXTURE_2D , level , internalFormat , width , height , border , gl.RGB , type , data );

  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

  this.colormap_texture = texture;

  gl.activeTexture(gl.TEXTURE0 + 1);
  var tex = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, tex);   // TEXTURE0+1 is now `tex`
  gl.texImage2D(                        // upload a checkerboard pattern to `tex`
    gl.TEXTURE_2D,
    0,                // level
    gl.RGBA,          // internal format
    2,                // width
    2,                // height
    0,                // border
    gl.RGBA,          // format
    gl.UNSIGNED_BYTE, // type
    new Uint8Array([
      0  , 0  , 0  , 255,
      255, 255, 255, 255,
      255, 255, 255, 255,
      0  , 0  , 0  , 255,
    ])
  );
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  this.checkerboard_texture = tex;

  // initialize some drawing parameters
  this.points    = false;
  this.edges     = true;
  this.triangles = true;
  this.gradient  = false;
  this.numbers   = false;

  for (let i = 0; i < this.vao.length; i++) {
    this.vao[i].visible = false;
    this.vao[i].points_on    = false;
    this.vao[i].edges_on     = true;
    this.vao[i].triangles_on = true;
    this.vao[i].gradient_on  = false;
    this.vao[i].numbers_on   = false;
  }

  // build up the user interface
  this.buildUI();

  // select the first mesh (this also sets up the mesh-dependent view)
  this.vao[0].visible = true;
  this.active_vao = 0;
  this.selectMesh(true);

}

fluxWebGL.prototype.colormap = function() {

  const dropdown = document.getElementById('colormap-dropdown');
  const idx = dropdown.selectedIndex;

  cmap = 'giraffe';
  if (idx == 0 || idx == 7) cmap = 'viridis';
  if (idx == 1) cmap = 'bwr';
  if (idx == 2) cmap = 'bgr';
  if (idx == 3) cmap = 'parula';
  if (idx == 4) cmap = 'jet';
  if (idx == 5) cmap = 'hsv';
  if (idx == 6) cmap = 'hot';
  if (idx == 7) cmap = 'viridis';
  if (idx == 8) cmap = 'giraffe';

  let gl = this.gl;

  let texture = this.colormap_texture;
  this.gl.bindTexture( this.gl.TEXTURE_2D , texture );

  const data = new Float32Array(colormap[cmap]);
  const level = 0;
  const internalFormat = gl.RGB32F;
  const width = data.length/3;
  const height = 1;
  const border = 0;
  const type = gl.FLOAT;
  const alignment = 1;

  gl.pixelStorei( gl.UNPACK_ALIGNMENT , alignment );
  gl.texImage2D( gl.TEXTURE_2D , level , internalFormat , width , height , border , gl.RGB , type , data );

  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

}

fluxWebGL.prototype.initializeView = function() {
  this.viewMatrix = mat4.create();
  mat4.lookAt( this.viewMatrix, this.eye, this.center , vec3.fromValues(0,1,0) );
  this.perspectiveMatrix = mat4.create()
  mat4.perspective( this.perspectiveMatrix, Math.PI/4.0, this.canvas.width/this.canvas.height, 0.001, 10000.0 );
  this.gl.viewport(0,0,this.canvas.width,this.canvas.height);
}

fluxWebGL.prototype.buildUI = function() {

  // TODO, add any meshes to the dropdown at the top of the HTML page
  let dropdown = document.getElementById('mesh-dropdown');
  for (let i = 0; i < this.vao.length; i++) {

    let option = document.createElement('option');
    option.innerHTML = 'Mesh ' + i.toString();
    dropdown.appendChild(option);
  }
}

fluxWebGL.prototype.selectMesh = function( calculate_view = false ) {

  const dropdown = document.getElementById('mesh-dropdown');
  this.active_vao = dropdown.selectedIndex;

  let field_dropdown = document.getElementById('field-dropdown');

  let option1 = document.createElement('option');
  option1.innerHTML = 'constant';

  let option2 = document.createElement('option');
  option2.innerHTML = 'texture';

  let option3 = document.createElement('option');
  option3.innerHTML = 'parameters';

  while (field_dropdown.hasChildNodes())
    field_dropdown.removeChild(field_dropdown.childNodes[0]);

  field_dropdown.appendChild(option1);
  field_dropdown.appendChild(option2);
  field_dropdown.appendChild(option3);
  for (let i = 0; i < this.vao[this.active_vao].fields.length; i++) {
    let option = document.createElement('option');
    option.innerHTML = this.vao[this.active_vao].field_names[i];
    field_dropdown.appendChild(option);
  }

  // initialize to a constant color
  this.vao[this.active_vao].active_field = -3;

  // compute the bounding box of the mesh
  let xmin = [ 1e20, 1e20, 1e20];
  let xmax = [-1e20,-1e20,-1e20];
  const p = this.vao[this.active_vao].points.data;
  for (let i = 0; i < p.length / 3; i++) {

    for (let d = 0; d < 3; d++) {
      let x = p[3*i+d];
      if (x < xmin[d]) xmin[d] = x;
      if (x > xmax[d]) xmax[d] = x;
    }
  }

  // calculate the maximum length scale, and the center of the object
  this.length = -1.0;
  let center = vec3.create();
  for (let d = 0; d < 3; d++) {
    const L = xmax[d] - xmin[d];
    if (L > this.length) this.length = L;
    center[d] = 0.5*(xmax[d] + xmin[d]);
  }
  console.assert( this.length > 0.0 );

  // setup the view to look at the object center
  if (calculate_view) {
    this.center = center.slice();
    this.eye    = center.slice();//vec3.create();
    this.eye[2] = this.center[2] + 2.0*this.length;
    this.initializeView();
  }
  this.object_center = center.slice();
  this.vao[this.active_vao].center = center.slice();

  // setup the clip distance slider
  let clip_distance = document.getElementById('clip-distance');
  clip_distance.setAttribute('min' , -this.length/2.0);
  clip_distance.setAttribute('max' ,  this.length/2.0);
  clip_distance.setAttribute('step' , this.length/100.0 );

  // check if there is no mesh visible
  let nb_visible = 0;
  for (let i = 0; i < this.vao.length; i++) {
    if (this.vao.visible) nb_visible++;
  }
  if (nb_visible == 0) this.vao[this.active_vao].visible = true;

  // set the checkboxes for this mesh
  let visible = document.getElementById('checkbox-visible');
  visible.checked = this.vao[this.active_vao].visible;
  let points = document.getElementById('checkbox-points');
  points.checked = this.vao[this.active_vao].points_on;
  let edges = document.getElementById('checkbox-edges');
  edges.checked = this.vao[this.active_vao].edges_on;
  let triangles = document.getElementById('checkbox-triangles');
  triangles.checked = this.vao[this.active_vao].triangles_on;


}

fluxWebGL.prototype.selectField = function() {
  const vao_idx = document.getElementById('mesh-dropdown').selectedIndex;
  const fld_idx = document.getElementById('field-dropdown').selectedIndex;
  this.vao[vao_idx].active_field = fld_idx - 3;
}

fluxWebGL.prototype.toggleVisibility = function() {
  const vao_idx = document.getElementById('mesh-dropdown').selectedIndex;
  this.vao[vao_idx].visible = !this.vao[vao_idx].visible;
}

fluxWebGL.prototype.toggleTriangles = function() {
  const vao_idx = document.getElementById('mesh-dropdown').selectedIndex;
  this.vao[vao_idx].triangles_on = !this.vao[vao_idx].triangles_on;
}

fluxWebGL.prototype.toggleEdges = function() {
  const vao_idx = document.getElementById('mesh-dropdown').selectedIndex;
  this.vao[vao_idx].edges_on = !this.vao[vao_idx].edges_on;
}

fluxWebGL.prototype.togglePoints = function() {
  const vao_idx = document.getElementById('mesh-dropdown').selectedIndex;
  this.vao[vao_idx].points_on = !this.vao[vao_idx].points_on;
}

fluxWebGL.prototype.toggleNumbers = function() {
  const vao_idx = document.getElementById('mesh-dropdown').selectedIndex;
  this.vao[vao_idx].numbers_on = !this.vao[vao_idx].numbers_on;
}

fluxWebGL.prototype.draw = function() {

  // make life easier by dereferencing this.gl
  let gl = this.gl;
  gl.useProgram(this.program);

  // clear the canvas color
  gl.clearColor(1,1,1,1); // white background
  gl.clear( gl.DEPTH_BUFFER_BIT );
  gl.clear( gl.COLOR_BUFFER_BIT );
  gl.enable(this.gl.DEPTH_TEST);
  gl.enable(gl.POLYGON_OFFSET_FILL);
  gl.polygonOffset(2, 3);

  // clear the text canvas
  let textCanvas = document.getElementById('text');
  let ctx = textCanvas.getContext('2d');
  ctx.clearRect(0,0,textCanvas.width,textCanvas.height);

  // set the camera and perspective matrices (which may have changed)
  gl.uniformMatrix4fv( this.u_ViewMatrix , false , this.viewMatrix );
  gl.uniformMatrix4fv( this.u_PerspectiveMatrix , false , this.perspectiveMatrix );
  gl.uniformMatrix4fv( this.u_ModelMatrix , false , this.modelMatrix );

  let normalMatrix = mat4.create();
  mat4.multiply( normalMatrix , this.viewMatrix , this.modelMatrix );
  mat4.invert( normalMatrix , normalMatrix );
  mat4.transpose( normalMatrix , normalMatrix );
  gl.uniformMatrix4fv( this.u_NormalMatrix , false , normalMatrix );

  // activate the colormap texture and set the uniform
  gl.activeTexture( gl.TEXTURE0 + 0 );
  gl.bindTexture( gl.TEXTURE_2D , this.colormap_texture );
  gl.uniform1i(gl.getUniformLocation(this.program, 'u_colormap'),0);

  // activate the checkerboard texture and set the uniform
  gl.activeTexture( gl.TEXTURE0 + 1 );
  gl.bindTexture( gl.TEXTURE_2D , this.checkerboard_texture );
  gl.uniform1i(gl.getUniformLocation(this.program, 'u_checker'),1);

  gl.uniform1i( this.program.u_lighting , this.lighting ? 1 : -1 );

  gl.clearColor(1,1,1,1); // white background
  gl.clear( gl.DEPTH_BUFFER_BIT );
  gl.clear( gl.COLOR_BUFFER_BIT );

  // activate the appropriate texture if there are any fields associated with a particular patch of triangles
  //this.vao[this.active_vao].draw( this.gl , this.program , this.triangles , this.edges , this.points );
  for (let i = 0; i < this.vao.length; i++) {

    // do not draw this vao if it is not visible
    if (!this.vao[i].visible) continue;

    this.vao[i].draw( this.gl , this.program , this.vao[i].triangles_on , this.vao[i].edges_on , this.vao[i].points_on );

    if (this.vao[i].numbers_on) {
      // draw vertex and or triangle numbers
      ctx.font = '20px Arial';

      // compute the transformation matrix from camera -> screen
      const w = textCanvas.width;
      const h = textCanvas.height;

      let Mcs = mat4.fromValues(
          w/2 , 0 , 0 , 0 ,  // first column
          0 , -h/2 , 0 , 0 , // second column
          0 , 0 , 1 , 0 ,    // third column
          w/2 , h/2 , 0 , 1  // fourth column
      );
      mat4.multiply( Mcs , Mcs , this.perspectiveMatrix );
      mat4.multiply( Mcs , Mcs , this.viewMatrix );

      const vertices = this.vao[this.active_vao].points.data;

      // transformation from world to screen
      let Mws = mat4.create();
      mat4.multiply( Mws , Mcs , this.modelMatrix );

      for (let j = 0; j < vertices.length/3; j++) {

        let p = vec4.fromValues( vertices[3*j] , vertices[3*j+1] , vertices[3*j+2] , 1 );
        let q = vec4.create();
        vec4.transformMat4( q , p , Mws );

        // divide by w-coordinate (needed because of perspective projection)
        q[0] /= q[3];
        q[1] /= q[3];
        ctx.fillText( j.toString() , q[0] , q[1] );
      }
    }
  }

  if (this.gradient) this.drawGradient();

}

fluxWebGL.prototype.toggleGradient = function() {

  this.gradient = !this.gradient;
  if (!this.gradient) return;

  // compute the gradient for the field of the active vao (or the normals?)
  let vao = this.vao[this.active_vao];
  if (vao.active_field < 0) return;

  let fld = vao.fields[vao.active_field];

  const t = vao.triangles.data;
  const p = vao.points.data;
  const u = fld.data;

  const nt = t.length / 3;
  const np = p.length / 3;
  const nu = u.length;

  console.log(nt,np,nu);

  //let phi_xi = [ [-1,-1], [1,0], [0,1] ];

  console.assert( u.length == p.length/3 );

  // calculate the arrow dimensions
  let x = new Float32Array( 3*4*nt );
  let h = new Uint32Array( 3*nt );
  let e = new Uint32Array( 2*nt );
  let I = 0;
  let J = 0;
  let K = 0;
  const zp = 0.01;
  for (let k = 0; k < nt; k++) {

    const i0 = t[3*k];
    const i1 = t[3*k+1];
    const i2 = t[3*k+2];

    // calculate the centroid of this triangle
    let c = vec2.create();
    for (let d = 0; d < 2; d++)
      c[d] = (p[3*i0+d] + p[3*i1+d] + p[3*i2+d]) / 3.0;

    let lmax = -1;
    for (let i = 1; i < 3; i++)
    for (let d = 0; d < 2; d++) {
      let hd = Math.abs( p[3*t[3*k+i]+d] - p[3*i0+d] );
      if (hd > lmax) lmax = hd;
    }

    // calculate the gradient of the field
    let x_xi = mat2.create();
    for (let d = 0; d < 2; d++) {
      x_xi[d  ] = p[ 3*i1 +d ] - p[3*i0 +d];
      x_xi[2+d] = p[ 3*i2 +d ] - p[3*i0 +d];
    }
    let xi_x = mat2.create();
    mat2.invert( xi_x , x_xi );
    mat2.transpose( xi_x , xi_x );

    let v = vec2.fromValues( u[i1] - u[i0] , u[i2] - u[i0] );

    let du = vec2.create();
    vec2.transformMat2( du , v , xi_x );
    vec2.normalize( du , du );
    vec2.scale( du , du , 1e-3/lmax ); // normalize the gradient to be within the triangle

    let alpha = Math.atan2(du[1],du[0]);

    let ha = 0.25 * lmax;

    h[3*J  ] = I+1;
    h[3*J+1] = I+2;
    h[3*J+2] = I+3;
    J++;

    e[2*K  ] = I;
    e[2*K+1] = I+1;
    K++;

    // point I
    x[3*I  ] = c[0];
    x[3*I+1] = c[1];
    x[3*I+2] = zp;
    I++;

    // point I+1
    let T = [ c[0] + lmax * Math.cos(alpha) ,  c[1] + lmax * Math.sin(alpha)];
    x[3*I  ] = T[0];
    x[3*I+1] = T[1];
    x[3*I+2] = zp;
    I++;

    // point I+2
    let theta = Math.PI / 8.0;
    x[3*I  ] = T[0] + ha * Math.cos( Math.PI - theta + alpha );
    x[3*I+1] = T[1] + ha * Math.sin( Math.PI - theta + alpha );
    x[3*I+2] = zp;
    I++;

    // point I+3
    x[3*I  ] = T[0] + ha * Math.cos( alpha - (Math.PI - theta) );
    x[3*I+1] = T[1] + ha * Math.sin( alpha - (Math.PI - theta) );
    x[3*I+2] = zp;
    I++;

  }
  console.assert( I == 4*nt );
  console.assert( J == nt );
  console.assert( K == nt );

  console.assert( x.length == 4*3*nt );
  console.assert( e.length == 2*nt );
  console.assert( h.length == 3*nt );

  let gl = this.gl;
  let vertex_buffer = gl.createBuffer();
  gl.bindBuffer( gl.ARRAY_BUFFER , vertex_buffer);
  gl.bufferData( gl.ARRAY_BUFFER , new Float32Array(x) , gl.STATIC_DRAW );

  let triangle_buffer = gl.createBuffer();
  gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER , triangle_buffer);
  gl.bufferData( gl.ELEMENT_ARRAY_BUFFER , new Uint32Array(h) , gl.STATIC_DRAW );

  let edge_buffer = gl.createBuffer();
  gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER , edge_buffer);
  gl.bufferData( gl.ELEMENT_ARRAY_BUFFER , new Uint32Array(e) , gl.STATIC_DRAW );

  this.gradient = {};
  this.gradient.points    = vertex_buffer;
  this.gradient.triangles = triangle_buffer;
  this.gradient.triangles.nb = h.length;
  this.gradient.edges     = edge_buffer;
  this.gradient.edges.nb  = e.length;
}

fluxWebGL.prototype.drawGradient = function() {

  // compute the gradient for the field of the active vao (or the normals?)
  let vao = this.vao[this.active_vao];
  if (vao.active_field < 0) return;

  let gl = this.gl;
  let program = this.program;

  gl.useProgram(program);

  gl.uniform1i( program.u_parameter , -1 );

  gl.bindBuffer( gl.ARRAY_BUFFER , this.gradient.points );
  const a_Position = gl.getAttribLocation(program,'a_Position');
  gl.vertexAttribPointer( a_Position , 3 , gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(a_Position);

  const a_Normal = gl.getAttribLocation(program,'a_Normal');
  gl.vertexAttribPointer( a_Normal , 3 , gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(a_Normal);

  const a_Parameter = gl.getAttribLocation(program,'a_Parameter');
  gl.vertexAttribPointer( a_Parameter , 2 , gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(a_Parameter);

  const a_Scalar = gl.getAttribLocation(program,'a_Scalar');
  gl.vertexAttribPointer( a_Scalar , 1 , gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(a_Scalar);

  gl.uniform1i( program.u_edges , 1 );
  gl.uniform1i( program.u_scalar , -1 );
  gl.uniform1i( program.u_parameter , -1 );

  gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER , this.gradient.triangles );
  gl.drawElements( gl.TRIANGLES , this.gradient.triangles.nb , gl.UNSIGNED_INT , 0 );

  gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER , this.gradient.edges );
  gl.drawElements( gl.LINES , this.gradient.edges.nb , gl.UNSIGNED_INT , 0 );
}

function VertexAttributeObject() {

  this.triangles  = undefined;
  this.edges      = undefined;

  this.points     = undefined;
  this.parameters = undefined;
  this.normals    = undefined;

  this.active_field = 0;
  this.fields = new Array();
  this.field_names = new Array();
  this.texture = undefined;
}

VertexAttributeObject.prototype.enable = function( gl, program ) {

  // enable the position attribute
  const a_Position = gl.getAttribLocation(program,'a_Position');
  gl.bindBuffer( gl.ARRAY_BUFFER , this.points );
  gl.vertexAttribPointer( a_Position , 3 , gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(a_Position);


  // enable parameter coordinates
  //gl.uniform1i( program.u_parameter , -1 );
  if (this.parameters != undefined) {
    gl.bindBuffer( gl.ARRAY_BUFFER , this.parameters );
    const a_Parameter = gl.getAttribLocation(program,'a_Parameter');
    gl.vertexAttribPointer( a_Parameter , 2 , gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(a_Parameter);
    //gl.uniform1i( program.u_parameter , 1 );
  }

  // enable normals
  if (this.normals != undefined) {
    gl.bindBuffer( gl.ARRAY_BUFFER , this.normals);
    const a_Normal = gl.getAttribLocation(program,'a_Normal');
    gl.vertexAttribPointer( a_Normal , 3 , gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(a_Normal);
  }

  // option to enable scalar attribute
  this.draw_texture = false;
  gl.uniform1i(gl.getUniformLocation(program, 'u_image_width'),-1);
  if (this.active_field >= 0) {
    gl.uniform1i( program.u_scalar , 1 );
    gl.uniform1i( program.u_parameter , -1 );
    gl.uniform1f( program.u_min , this.fields[this.active_field].min );
    gl.uniform1f( program.u_max , this.fields[this.active_field].max );

    gl.bindBuffer( gl.ARRAY_BUFFER , this.fields[this.active_field] );
    const a_Scalar = gl.getAttribLocation(program,'a_Scalar');
    gl.vertexAttribPointer( a_Scalar , 1 , gl.FLOAT , false , 0 , 0 );
    gl.enableVertexAttribArray(a_Scalar);
  }
  else if (this.active_field == -1) {
    // no scalar, use a constant color
    gl.uniform1i( program.u_scalar , -1 );
    gl.uniform1i( program.u_parameter , 1 );
  }
  else if (this.active_field == -2 && this.texture != undefined) {
    // activate the image texture and set the uniform
    gl.uniform1i( program.u_scalar , -1 );
    gl.uniform1i( program.u_parameter , 1 ); // still need to pass parameters
    this.draw_texture = true;
    gl.activeTexture( gl.TEXTURE0 + 2 );
    gl.bindTexture( gl.TEXTURE_2D , this.texture );
    gl.uniform1i(gl.getUniformLocation(program, 'u_image'), 2);
    gl.uniform1i(gl.getUniformLocation(program, 'u_image_width'),this.texture.width);
    gl.uniform1i(gl.getUniformLocation(program, 'u_image_height'),this.texture.height);
  }
  else {
    gl.uniform1i( program.u_scalar , -1 );
    gl.uniform1i( program.u_parameter , -1 );
  }

}

VertexAttributeObject.prototype.draw = function( gl, program, with_triangles, with_edges, with_points) {

  this.enable(gl,program);

  // set the clip direction
  const clip_dropdown = document.getElementById('clip-dropdown');
  let dir = parseInt((clip_dropdown.selectedIndex-1) / 2);
  if (clip_dropdown.selectedIndex == 0) dir = -1;
  const sign = (clip_dropdown.selectedIndex-1) % 2;
  gl.uniform1i( program.u_clip_dir , dir );

  // set the clip distance from the object center
  const clip_distance = parseFloat(document.getElementById('clip-distance').value);
  let clip_center = this.center.slice();
  if (clip_dropdown.selectedIndex > 0)
    clip_center[dir] += clip_distance;
  gl.uniform3fv( program.u_clip_center , clip_center );

  gl.uniform1f( program.u_clip_sign , (sign == 0) ? 1.0 : -1.0 );

  // draw the triangles
  if (with_triangles) {
    gl.uniform1i( program.u_edges , -1 );
    gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER , this.triangles );
    gl.drawElements( gl.TRIANGLES , this.triangles.nb_indices , this.triangles.type , 0 );
  }

  // draw the edges
  gl.uniform1i( program.u_edges , -1 );
  if (with_edges) {
    gl.uniform1i( program.u_edges , 1 );
    gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER , this.edges );
    gl.drawElements( gl.LINES , this.edges.nb_indices , this.edges.type , 0 );
  }
  gl.uniform1i( program.u_edges , -1 );

  // draw the points
  gl.uniform1i( program.u_points , -1 );
  if (with_points) {
    gl.uniform1i( program.u_points , 1 );
    gl.bindBuffer( gl.ARRAY_BUFFER , this.points );
    gl.drawArrays( gl.POINTS , 0 , this.points.nb_indices/3 );
  }
  gl.uniform1i( program.u_points , -1 );

}

function setupImageTexture( gl , program , image , uniformName , index , flip ) {

  // create the texture and activate it
  let texture = gl.createTexture();
  gl.activeTexture(gl.TEXTURE0 + index );
  gl.bindTexture( gl.TEXTURE_2D, texture );

  // define the texture to be that of the requested image
  gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, flip);
  gl.texImage2D( gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image );

  // generate the mipmap and filters
  gl.generateMipmap( gl.TEXTURE_2D );
  gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR );
  gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST );

  // tell webgl which texture index to use for the uniform sampler2D in the shader
  // you will need a 'uniform sampler2D uniformName' to use the texture within your shader
  gl.uniform1i( gl.getUniformLocation(program, uniformName) , index );

  return texture;
}

function compileShader(gl, shaderSource, type) {
  let shader = gl.createShader(type);
  gl.shaderSource(shader, shaderSource);
  gl.compileShader(shader);

  if (! gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    var error = gl.getShaderInfoLog(shader);
    gl.deleteShader(shader);
    throw "unable to compile " + (type === gl.VERTEX_SHADER ? 'vertex': 'fragment') + " shader: " + error;
  }
  return shader;
}

function compileProgram( gl , vertexShaderSource , fragmentShaderSource ) {

  let vertexShader = compileShader(gl, vertexShaderSource, gl.VERTEX_SHADER);
  let fragmentShader = compileShader(gl, fragmentShaderSource, gl.FRAGMENT_SHADER);

  let program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);

  gl.linkProgram(program);

  if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
    throw "unable to compile the shader program: " + gl.getProgramInfoLog(program);
  }
  gl.useProgram(program);
  return program;
}

// computes a rotation matrix of a vector (X,Y) on the unit sphere using quaternions
function rotation(X, Y) {
  let X2 = X*X, Y2 = Y*Y,
    q  = 1 + X2 + Y2,
    s  = 1 - X2 - Y2,
    r2 = 1/(q*q), s2 = s*s,
    A = (s2 + 4*(Y2 - X2))*r2,
    B = -8*X*Y*r2,
    C = 4*s*X*r2,
    D = (s2 + 4*(X2 - Y2))*r2,
    E = 4*s*Y*r2,
    F = (s2 - 4*(X2 + Y2))*r2;
  return mat4.fromValues(
    A, B, C, 0,
    B, D, E, 0,
    -C,-E, F, 0,
    0, 0, 0, 1
  );
}

let mouseMove = function(event,webgl) {

  if (!webgl.dragging) return;
  event.preventDefault();
  event = event || window.event;

  // compute the rotatation matrix from the last point on the sphere to the new point
  let T = mat4.create();
  if (webgl.rotating) {

    let T0 = mat4.create();
    mat4.fromTranslation( T0 , webgl.object_center );
    let T1 = mat4.create();
    mat4.invert( T1 , T0 );
    let R = rotation( -(event.pageX-webgl.lastX)/webgl.canvas.width , (event.pageY-webgl.lastY)/webgl.canvas.height );

    mat4.multiply( T , R , T1 );
    mat4.multiply( T , T0 , T );

  }
  else {
    mat4.fromTranslation( T ,
      vec3.fromValues(  2*(event.pageX-webgl.lastX)/webgl.canvas.width ,
                       -2*(event.pageY-webgl.lastY)/webgl.canvas.height,
                       0.0 ) );

    // move the center as well
    vec3.transformMat4( webgl.object_center , webgl.object_center , T );
  }

  mat4.multiply( webgl.modelMatrix , T , webgl.modelMatrix );

  // redraw and set the last state as the new one
  webgl.draw();
  webgl.lastX = event.pageX;
  webgl.lastY = event.pageY;
}

let mouseDown = function(event,webgl) {

  event.preventDefault();

  // set that dragging is true and save the last state
  webgl.dragging = true;

  // determine if we are translating or rotating
  //if (event.button == 2) webgl.rotating = true; // left button
  //else webgl.rotating = false; // middle or right buttons
  if (event.ctrlKey) webgl.rotating = true;
  else webgl.rotating = false;

  webgl.lastX    = event.pageX;
  webgl.lastY    = event.pageY;
}

let mouseUp = function(event,webgl) {
  webgl.dragging = false;
}

let mouseWheel = function(event,webgl) {

  event.preventDefault();

  let scale = 1.0;
  if (event.deltaY > 0) scale = 0.9;
  else if (event.deltaY < 0) scale = 1.1;

  // scale the direction from the model center to the eye
  let direction = vec3.create();
  vec3.subtract( direction , webgl.eye , webgl.center );
  vec3.scaleAndAdd( webgl.eye , webgl.center , direction , scale );

  mat4.lookAt( webgl.viewMatrix , webgl.eye , webgl.center , vec3.fromValues(0,1,0) );
  webgl.draw();
}
