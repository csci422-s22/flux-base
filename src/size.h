/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#ifndef FLUX_SIZE_H_
#define FLUX_SIZE_H_

#include "element.h"
#include "grid.h"
#include "kdtree.h"

#include <memory>
#include <string>
#include <vector>

namespace flux {

struct HalfVertex;

/**
 * \brief Base class for representing a target element size.
 *
 *        Derived classes must implement the call () operator.
 */
template<int dim>
class SizingField {
public:
  /**
   * \brief Returns the target size at a point x.
   */
  virtual double operator()( const double* x ) const = 0;

  /**
   * \brief virtual destructor.
   */
  virtual ~SizingField() {}

  /**
   * \brief Returns the length of an edge between half-vertices p and q.
   */
  double length( const HalfVertex* p , const HalfVertex* q ) const;

  /**
   * \brief Returns the expected number of triangles if a mesh were generated
   *        such that each edge had a length of 1 under the target size.
   *
   * \param[in] mesh - domain on which integration is performed to estimate the number of triangles
   *                   For an ImageSizingField, use the "grid" (see below).
   */
  template<typename T>
  int nb_expected_triangles( const Mesh<T>& mesh ) const;
};

/**
 * \brief Represents a target element size throughout a rectangular domain
 *        derived from the grayscale values of an input image.
 *
 * \details
 *        The domain is [0,1] x [0,ymax] where ymax is 1/(aspect_ratio)
 *        where aspect_ratio is the width / height of the image.
 *        Given an "hmin" and "hmax" and some exponent "p", the target size h
 *        is determined as h = hmin + s^p *(hmax - hmin) where s
 *        is the grayscale value (average of input pixel r,g,b values).
 */
class ImageSizingField : public SizingField<2> {

public:
  /**
   * \brief Constructs a sizing field from an input image and sizing parameters.
   *
   * \param[in] filename - image from which the size field will be constructed.
   * \param[in] hmin - minimum size in the mesh
   * \param[in] hmax - maximum size in the mesh
   * \param[in] p - exponent to use in size calculation (default = 2.0)
   */
  ImageSizingField( const std::string& filename , double hmin , double hmax , double p = 2.0 );

  /**
   * \brief Returns the size at coordinates x. Uses a kdtree to search for the nearest pixel.
   */
  double operator()( const double* x ) const;

  /**
   * \brief Returns the aspect ratio of the input image.
   *        Useful for determining how the initial mesh should be mapped
   *        to maintain the aspect ratio of the image.
   */
  double aspect_ratio() const { return aspect_ratio_; }

  /**
   * \brief Returns the grid of pixels representing the image.
   */
  const Grid<Quad>& grid() const { return *grid_.get(); }
        Grid<Quad>& grid()       { return *grid_.get(); }

  /**
   * \brief Returns the size at pixel k. Should only be used for unit testing.
   */
  double size( int k ) const {
    flux_assert( k < grid_->nb() && k < size_.size() );
    return size_[k];
  }

private:
  /**
   * \brief Initializes the size values at each pixel.
   */
  void initialize( const std::string& filename , double hmin , double hmax , double p );

  std::unique_ptr<kdtree<2>> search_;
  std::unique_ptr<Grid<Quad>> grid_;
  std::vector<double> size_;
  double aspect_ratio_;
};

} // flux

#endif
