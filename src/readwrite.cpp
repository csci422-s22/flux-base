/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/

#include "element.h"
#include "readwrite.h"
#include "mesh.h"

#include "external/tiny_obj_loader.h"

#include <cassert>
#include <cstring>
#include <iostream>

namespace flux {

static std::string
get_base_dir(const std::string& filepath) {
  if (filepath.find_last_of("/\\") != std::string::npos)
    return filepath.substr(0, filepath.find_last_of("/\\"));
  return "";
}

std::unique_ptr<MeshBase>
read_obj( const std::string& filename , bool allow_duplicating ) {

  tinyobj::attrib_t attrib;
  std::vector<tinyobj::shape_t> shapes;
  std::vector<tinyobj::material_t> materials;

  std::string base_dir = get_base_dir(filename.c_str());
  if (base_dir.empty()) {
    base_dir = ".";
  }
#ifdef _WIN32
  base_dir += "\\";
#else
  base_dir += "/";
#endif

  std::string warn;
  std::string err;
  bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, filename.c_str(), base_dir.c_str() , false );
  if (!warn.empty()) {
    std::cout << "WARN: " << warn << std::endl;
  }
  if (!err.empty()) {
    std::cerr << err << std::endl;
  }

  if (!ret) {
    std::cerr << "Failed to load " << filename << std::endl;
    return nullptr;
  }

  printf("# of vertices  = %d\n", (int)(attrib.vertices.size()) / 3);
  printf("# of normals   = %d\n", (int)(attrib.normals.size()) / 3);
  printf("# of texcoords = %d\n", (int)(attrib.texcoords.size()) / 2);
  printf("# of materials = %d\n", (int)materials.size());
  printf("# of shapes    = %d\n", (int)shapes.size());

  if (attrib.texcoords.size()/2 <= attrib.vertices.size()/3) {
    //flux_implement;
    allow_duplicating = false;
  }

  // initialize the mesh
  std::unique_ptr<MeshBase> mesh = std::make_unique<Mesh<Polygon>>(3);

  std::vector<double> buffer(3);

  // find the map between texture coordinates and vertices (loop trough indices of each shape)
  std::map<int,int> tex2vert;
  std::map<int,int> vertex_map;
  for (size_t i = 0; i < shapes.size(); i++) {

    size_t index_offset = 0;

    flux_assert(shapes[i].mesh.num_face_vertices.size() == shapes[i].mesh.material_ids.size());
    flux_assert(shapes[i].mesh.num_face_vertices.size() == shapes[i].mesh.smoothing_group_ids.size());

    // for each face
    for (size_t f = 0; f < shapes[i].mesh.num_face_vertices.size(); f++) {

      size_t fnum = shapes[i].mesh.num_face_vertices[f];

      // for each vertex in the face
      for (size_t v = 0; v < fnum; v++) {
        tinyobj::index_t idx = shapes[i].mesh.indices[index_offset + v];
        if (idx.texcoord_index < 0) continue;
        tex2vert.insert( {idx.texcoord_index,idx.vertex_index} );
      }
      index_offset += fnum;
    }
  } // loop over shape

  // read the vertices
  double uv[2] = {0.,0.};
  if (allow_duplicating) {

    // loop through the texture coordinates
    for (size_t t = 0; t < attrib.texcoords.size() / 2; t++) {

      // get the vertex coordinates
      int v = tex2vert.at(t);
      buffer.assign( &attrib.vertices[3*v] , &attrib.vertices[3*v] + 3 );
      mesh->vertices().add(buffer.data());

      for (int d = 0; d < 2; d++)
        uv[d] = attrib.texcoords[2*t +d];
      mesh->vertices().uv().add(uv);
    }

  }
  else {
    tex2vert.clear();

    for (size_t v = 0; v < attrib.vertices.size() / 3; v++) {
      buffer.assign( &attrib.vertices[3*v] , &attrib.vertices[3*v] + 3 );
      mesh->vertices().add(buffer.data());
      tex2vert.insert( {v,v} );
      mesh->vertices().uv().add(uv);
    }
  }

  // read the normals
  array2d<double> normals(3);
  for (size_t v = 0; v < attrib.normals.size() / 3; v++) {
    buffer.assign( &attrib.normals[3*v] , &attrib.normals[3*v] + 3 );
    mesh->vertices().normals().add( buffer.data() );
  }

  // for each shape
  std::vector<int> indices;
  for (size_t i = 0; i < shapes.size(); i++) {

    size_t index_offset = 0;

    flux_assert(shapes[i].mesh.num_face_vertices.size() == shapes[i].mesh.material_ids.size());
    flux_assert(shapes[i].mesh.num_face_vertices.size() == shapes[i].mesh.smoothing_group_ids.size());

    // for each face
    for (size_t f = 0; f < shapes[i].mesh.num_face_vertices.size(); f++) {

      size_t fnum = shapes[i].mesh.num_face_vertices[f];
      indices.resize(fnum);

      if (!allow_duplicating) {
        for (size_t j = 0; j < fnum; j++) {
          tinyobj::index_t idx = shapes[i].mesh.indices[index_offset + j];
          indices[j] = idx.vertex_index;

          if (idx.texcoord_index < 0) continue;

          for (int d = 0; d < 2; d++)
            mesh->vertices().uv()[idx.vertex_index][d] = attrib.texcoords[2*idx.texcoord_index+d];
        }
      }
      else {
        for (size_t j = 0; j < fnum; j++) {
          tinyobj::index_t idx = shapes[i].mesh.indices[index_offset + j];
          flux_assert( idx.texcoord_index >= 0);
          indices[j] = idx.texcoord_index;
        }
      }

      mesh->add( indices.data() , indices.size() );

      index_offset += fnum;
    }
  } // loop over shape

  int nv = mesh->count(0);
  for (int k = 0; k < mesh->nb(); k++) {
    if (nv != mesh->count(k)) {
      nv = -1;
      break;
    }
  }

  // convert the mesh to triangles or quads
  if (nv == 3) {
    std::unique_ptr<MeshBase> mesh_tri = std::make_unique<Mesh<Triangle>>(3);
    for (int k = 0; k < mesh->nb(); k++)
      mesh_tri->add( (*mesh)[k] );
    mesh_tri->vertices().copy( mesh->vertices() );
    return mesh_tri;
  }
  else if (nv == 4) {
    std::unique_ptr<MeshBase> mesh_quad = std::make_unique<Mesh<Quad>>(3);
    for (int k = 0; k < mesh->nb(); k++)
      mesh_quad->add( (*mesh)[k] );
    mesh_quad->vertices().copy( mesh->vertices() );
    return mesh_quad;
  }

  return mesh;
}

void
write_obj(const MeshBase& mesh, const std::string& filename ) {

  FILE* out = fopen(filename.c_str(), "w");
  flux_assert_msg( out  , "failed to open file: %s" , filename.c_str() );

  // header
  fprintf(out, "# obj mesh from flux\n");

  // write vertices
  for (int k = 0; k < mesh.vertices().nb(); k++) {
    const double* p = mesh.vertices()[k];
    double z = (mesh.vertices().dim() == 2) ? 0.0 : p[2];
    fprintf(out, "v %.10f %.10f %.10f\n", p[0], p[1], z);
  }

  // write faces
  for (int k = 0; k < mesh.nb(); k++) {
    fprintf(out, "f ");
    for (int j = 0; j < 3; j++) {
      int idx = mesh(k,j);
      fprintf(out, "%d ",idx+1);
    }
    fprintf(out,"\n");
  }

  fclose(out);
}


void
read_off_ascii( MeshBase& mesh , FILE* in ) {

    char line[1000], *lp;
    int nc;
    unsigned int i, j, items, idx;
    unsigned int nv, nf, ne;
    float x, y, z;

    items = fscanf(in, "%d %d %d\n", (int*)&nv, (int*)&nf, (int*)&ne);
    flux_assert(items);

    // read vertices: pos [normal] [color] [texcoord]
    for (i = 0; i < nv && !feof(in); ++i) {

        // read line
        lp = fgets(line, 1000, in);
        lp = line;

        // position
        items = sscanf(lp, "%f %f %f%n", &x, &y, &z, &nc);
        assert(items == 3);

        double point[3] = {x,y,z};

        mesh.vertices().add(point);
        lp += nc;
    }

    // read faces: #N v[1] v[2] ... v[n-1]
    std::vector<int> vertices;
    for (i = 0; i < nf; ++i) {
        // read line
        lp = fgets(line, 1000, in);
        lp = line;

        // #vertices
        items = sscanf(lp, "%d%n", (int*)&nv, &nc);
        assert(items == 1);
        vertices.resize(nv);
        lp += nc;

        // indices
        for (j = 0; j < nv; ++j) {
          items = sscanf(lp, "%d%n", (int*)&idx, &nc);
          assert(items == 1);
          vertices[j] = idx;
          lp += nc;
        }
        mesh.add(vertices.data(),nv);
    }
}

void
read_off_binary( MeshBase& mesh, FILE* in) {

    unsigned int nv(0), nf(0), ne(0);
    int nb_items = 0;
    float x[3];

    // read
    flux_assert( fread((char*)&nv, 1, sizeof(unsigned int), in) > 0 );
    flux_assert( fread((char*)&nf, 1, sizeof(unsigned int), in) > 0 );
    flux_assert( fread((char*)&ne, 1, sizeof(unsigned int), in) > 0 );

    // read vertices
    for (int i = 0; i < int(nv) && !feof(in); i++) {

        // position
        nb_items = fread((char*)&x, 3, sizeof(float), in);
        flux_assert( nb_items > 0 );

        double point[3] = {x[0],x[1],x[2]};
        mesh.vertices().add(point);
    }

    // read faces
    std::vector<unsigned int> indices;
    std::vector<int> vertices;
    for (int i = 0; i < int(nf); i++) {

      // read the number of vertices
      nb_items = fread((char*)&nv, 1, sizeof(unsigned int), in);
      flux_assert(nb_items > 0);

      // read the indices
      indices.resize(nv);
      nb_items = fread((char*)indices.data(), nv, sizeof(unsigned int), in);
      flux_assert(nb_items > 0);

      // copy the vertices from unsigned to signed integer for the mesh
      vertices.resize(nv);
      vertices.assign( indices.begin() , indices.end() );
      mesh.add(vertices.data(),nv);
    }
}

std::unique_ptr<MeshBase>
read_off(const std::string& filename) {

  char line[200];
  bool is_binary = false;

  // open file (in ASCII mode)
  FILE* in = fopen(filename.c_str(), "r");
  flux_assert_msg( in , "could not open %s",filename.c_str());

  // read header: [ST][C][N][4][n]OFF BINARY
  char* c = fgets(line, 200, in);
  flux_assert(c != nullptr);
  c = line;

  flux_assert_msg ( strncmp(c, "OFF", 3) == 0 , "failed to read OFF header" );

  if (strncmp(c + 4, "BINARY", 6) == 0)
    is_binary = true;

  // if binary: reopen file in binary mode
  if (is_binary) {
    fclose(in);
    in = fopen(filename.c_str(), "rb");
    c = fgets(line, 200, in);
    assert(c != nullptr);
  }

  // read as ASCII or binary
  std::unique_ptr<MeshBase> mesh_ptr = std::make_unique<Mesh<Polygon>>(3);
  MeshBase& mesh = *mesh_ptr.get();
  if (is_binary)
    read_off_binary(mesh, in);
  else
    read_off_ascii(mesh, in);
  fclose(in);

  // determine if we should convert to triangle or quads
  if (mesh.nb() == 0) {
    printf("[warning] mesh has no elements\n");
    return mesh_ptr;
  }

  int nv = mesh.count(0);
  for (int k = 0; k < mesh.nb(); k++) {
    if (nv != mesh.count(k)) {
      nv = -1;
      break;
    }
  }

  // convert the mesh to triangles or quads
  if (nv == 3) {
    std::unique_ptr<MeshBase> mesh_tri = std::make_unique<Mesh<Triangle>>(3);
    for (int k = 0; k < mesh.nb(); k++)
      mesh_tri->add( mesh[k] );
    mesh_tri->vertices().copy( mesh.vertices() );
    return mesh_tri;
  }
  else if (nv == 4) {
    std::unique_ptr<MeshBase> mesh_quad = std::make_unique<Mesh<Quad>>(3);
    for (int k = 0; k < mesh.nb(); k++)
      mesh_quad->add( mesh[k] );
    mesh_quad->vertices().copy( mesh.vertices() );
    return mesh_quad;
  }

  return mesh_ptr;
}

} // flux
