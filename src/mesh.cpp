/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/

#include "element.h"
#include "field.h"
#include "mesh.h"
#include "vec.hpp"

#include <map>
#include <set>
#include <tuple>

namespace flux {

template<typename type>
void
get_edges_from_element( const Mesh<type>& mesh , std::vector<int>& edges ) {

  edges.clear();
  std::set< std::pair<int,int> > E;

  for (int k = 0; k < mesh.nb(); k++) {

    for (int j = 0; j < mesh.element().nb_edges; j++) {

      int e0 = mesh(k, mesh.element().edges[2*j  ] );
      int e1 = mesh(k, mesh.element().edges[2*j+1] );

      if (e0 > e1) std::swap(e0,e1);

      if (E.find({e0,e1}) == E.end()) {

        E.insert({e0,e1});
        edges.push_back(e0);
        edges.push_back(e1);
      }
    }
  }
}

template<typename type>
void
get_triangles_from_element( const Mesh<type>& mesh , std::vector<int>& triangles , bool check_uniqueness = true ) {

  triangles.clear();
  std::set< std::tuple<int,int,int> > T;

  for (int k = 0; k < mesh.nb(); k++) {
    for (int j = 0; j < mesh.element().nb_triangles; j++) {

      int t0 = mesh(k , mesh.element().triangles[3*j  ] );
      int t1 = mesh(k , mesh.element().triangles[3*j+1] );
      int t2 = mesh(k , mesh.element().triangles[3*j+2] );

      if (!check_uniqueness) {
        triangles.push_back(t0);
        triangles.push_back(t1);
        triangles.push_back(t2);
        continue;
      }

      if (t0 > t1) std::swap(t0,t1);
      if (t0 > t2) std::swap(t0,t2);
      if (t1 > t2) std::swap(t1,t2);
      if (T.find({t0,t1,t2}) == T.end()) {

        T.insert( {t0,t1,t2} );
        triangles.push_back(t0);
        triangles.push_back(t1);
        triangles.push_back(t2);
      }
    }
  }
}

void
MeshBase::create_vertex_field( const std::string& name , const std::vector<double>& data ) {
  std::shared_ptr<Field> f = std::make_shared<Field>(FieldType_VertexBased,name,data);
  fields_.push_back(f);
}

void
MeshBase::create_cell_field( const std::string& name , const std::vector<double>& data ) {
  flux_assert( int(data.size()) == nb() );
  std::shared_ptr<Field> f = std::make_shared<Field>(FieldType_CellBased,name,data);
  fields_.push_back(f);
}

const Field&
MeshBase::field( int k ) const {
  flux_assert( k < (int)fields_.size() );
  return *fields_[k].get();
}

template<typename type>
Mesh<type>::Mesh( int dim ) :
  MeshBase(dim,type::nb_vertices)
{}

template<typename type>
Mesh<type>::Mesh( int dim , int np , const double* vertices , int ne , const int* elements ) :
  MeshBase(dim,element_.nb_vertices)
{
  flux_assert( element_.nb_vertices > 0 );
  for (int k = 0; k < np; k++)
    vertices_.add( vertices + dim*k );

  for (int k = 0; k < ne; k++)
    add( elements + element_.nb_vertices * k );
}

template<typename type>
void
get_normal( const Vertices& vertices , const int* E , int nv , std::vector<double>& normal ) {
  //flux_implement;
}

template<>
void
get_normal<Tet>( const Vertices& vertices , const int* E , int nv , std::vector<double>& normal ) {
  //printf("[warning] not calculating normals for tetrahedra\n");
}

template<>
void
get_normal<Line>( const Vertices& vertices , const int* L , int nv , std::vector<double>& normal ) {

  vecs<3,double> u;

  for (int d = 0; d < vertices.dim(); d++)
    u[d] = vertices[L[1]][d] - vertices[L[0]][d];

  normal[0] = -u[1];
  normal[1] =  u[0];
  normal[2] = 0.0;

}

template<>
void
get_normal<Triangle>( const Vertices& vertices , const int* T , int nv , std::vector<double>& normal ) {

  vecs<3,double> u;
  vecs<3,double> v;

  for (int d = 0; d < 3; d++) {
    u[d] = vertices[T[1]][d] - vertices[T[0]][d];
    v[d] = vertices[T[2]][d] - vertices[T[0]][d];
  }

  vecs<3,double> w = cross(u,v);
  normalize(w);

  for (int d = 0; d < 3; d++)
    normal[d] = w[d];
}

template<>
void
get_normal<Quad>( const Vertices& vertices , const int* T , int nv , std::vector<double>& normal ) {

  vecs<3,double> u;
  vecs<3,double> v;

  for (int d = 0; d < 3; d++) {
    u[d] = vertices[T[1]][d] - vertices[T[0]][d];
    v[d] = vertices[T[3]][d] - vertices[T[0]][d];
  }

  vecs<3,double> w = cross(u,v);
  normalize(w);

  for (int d = 0; d < 3; d++)
    normal[d] = w[d];
}

template<>
void
get_normal<Polygon>( const Vertices& vertices , const int* T , int nv , std::vector<double>& normal ) {

  vecs<3,double> u;
  vecs<3,double> v;

  for (int d = 0; d < 3; d++) {
    u[d] = vertices[T[1]][d] - vertices[T[0]][d];
    v[d] = vertices[T[2]][d] - vertices[T[0]][d];
  }

  vecs<3,double> w = cross(u,v);
  normalize(w);

  for (int d = 0; d < 3; d++)
    normal[d] = w[d];
}

template<typename type>
void
Mesh<type>::calculate_face_normals( array2d<double>& normals ) const {

  std::vector<double> normal(3);

  for (int k = 0; k < nb(); k++) {
    get_normal<type>( vertices_ , (*this)[k] , count(k) , normal );
    normals.add( normal.data() );
  }

}

template<typename type>
void
Mesh<type>::calculate_vertex_normals( array2d<double>& normals ) const {

  // first calculate the face normals
  array2d<double> face_normals( 3 );
  calculate_face_normals( face_normals );

  // initialize the vertex normals
  std::vector<double> normal(3,0.0);
  for (int k = 0; k < vertices_.nb(); k++)
    normals.add( normal.data() );

  // add the normals to each vertex
  std::vector<int> valency( vertices_.nb() , 0 );
  for (int k = 0; k < nb(); k++) {
    for (int j = 0; j < count(k); j++) {

      int p = (*this)(k,j);
      for (int d = 0; d < 3; d++)
        normals(p,d) += face_normals(k,d);
      valency[p]++;
    }
  }

  for (int k = 0; k < vertices_.nb(); k++) {
    if (valency[k] == 0) continue;
    for (int d = 0; d < 3; d++)
      normals(k,d) /= double(valency[k]);
  }

}

template<>
void
Mesh<Line>::get_edges( std::vector<int>& edges ) const {
  edges = data();
}

template<>
void
Mesh<Line>::get_triangles( std::vector<int>& triangles , std::vector<int>* parents ) const {
  // no triangles!
}

template<>
void
Mesh<Triangle>::get_edges( std::vector<int>& edges ) const {
  get_edges_from_element(*this,edges);
}

template<>
void
Mesh<Triangle>::get_triangles( std::vector<int>& triangles , std::vector<int>* parents ) const {
  triangles = data();
  if (parents != nullptr) {
    parents->resize( triangles.size()/3 );
    for (size_t k = 0; k < triangles.size()/3; k++)
      (*parents)[k] = k;
  }
}

template<>
void
Mesh<Quad>::get_edges( std::vector<int>& edges ) const {
  get_edges_from_element(*this,edges);
}

template<>
void
Mesh<Quad>::get_triangles( std::vector<int>& triangles , std::vector<int>* parents ) const {
  get_triangles_from_element(*this,triangles,false);
  if (parents != nullptr) {
    parents->resize( triangles.size()/3 );
    for (size_t k = 0; k < triangles.size()/3; k++)
      (*parents)[k] = k/2; // each quad is divided into two triangles
  }
}

template<>
void
Mesh<Tet>::get_edges( std::vector<int>& edges ) const {
  get_edges_from_element(*this,edges);
}

template<>
void
Mesh<Tet>::get_triangles( std::vector<int>& triangles , std::vector<int>* parents ) const {
  get_triangles_from_element(*this,triangles);
}

template<>
void
Mesh<Polygon>::get_edges( std::vector<int>& edges ) const {
  // assume the vertices in each polygon are ordered CCW
  for (int k = 0; k < nb(); k++) {
    int nv = count(k);
    for (int j = 0; j < nv; j++) {
      edges.push_back( (*this)(k,j) );
      edges.push_back( (*this)(k,(j+1)%nv) );
    }
  }
}

template<>
void
Mesh<Polygon>::get_triangles( std::vector<int>& triangles , std::vector<int>* parents ) const {

  // assume the vertices in each polygon are ordered CCW
  int triangle[3];
  for (int k = 0; k < nb(); k++) {

    triangle[0] = (*this)(k,0);
    for (int j = 1; j < count(k)-1; j++) {
      triangle[1] = (*this)(k,j);
      triangle[2] = (*this)(k,j+1);

      for (int i = 0; i < 3; i++)
        triangles.push_back(triangle[i]);
      if (parents != nullptr) parents->push_back(k);
    }
  }

}

template class Mesh<Line>;
template class Mesh<Triangle>;
template class Mesh<Quad>;
template class Mesh<Polygon>;
template class Mesh<Tet>;

}
