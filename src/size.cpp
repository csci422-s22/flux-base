/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#include "grid.h"
#include "halfedges.h"
#include "kdtree.h"
#include "predicates.h"
#include "size.h"
#include "texture.h"
#include "vec.hpp"

namespace flux {

template<int dim>
double
SizingField<dim>::length( const HalfVertex* p , const HalfVertex* q ) const {
  vec3d e = q->point - p->point;
  flux_assert_msg( norm(e) > 0.0 , "|e| = %g" , norm(e) );
  double lp = norm(e) / (*this)( p->point.data() );
  double lq = norm(e) / (*this)( q->point.data() );
  double r, lm;
  if (lp > lq) {
    r  = lp / lq;
    lm = lp;
  }
  else {
    r  = lq / lp;
    lm = lq;
  }
  if (std::fabs(r - 1.0) < 1e-12) return lp;
  return lm * (r - 1.0) / ( r * std::log(r) );
}

template<>
template<typename T>
int
SizingField<2>::nb_expected_triangles( const Mesh<T>& mesh ) const {

  const Vertices& p = mesh.vertices();

  double area = 0.0;
  for (int k = 0; k < mesh.nb(); k++) {

    vec2d c = {0,0};
    for (int j = 0; j < mesh.count(k); j++)
    for (int d = 0; d < 2; d++)
      c[d] += p[mesh(k,j)][d];
    c = c / double(mesh.count(k));

    double h = (*this)(c.data());

    double area_k = 0.0;
    area_k = 0.5 * orient2d( p[mesh(k,0)] , p[mesh(k,1)] , p[mesh(k,2)] );
    if (mesh.count(k) == 4) {
      area_k += 0.5 * orient2d( p[mesh(k,0)] , p[mesh(k,2)] , p[mesh(k,3)] );
    }
    if (mesh.count(k) > 4) flux_implement;

    area_k /= (h*h);
    area += area_k;
  }
  return area * 4 / std::sqrt(3); // a unit equilateral triangle has an area of sqrt(3)/4
}

ImageSizingField::ImageSizingField( const std::string& filename , double hmin , double hmax , double p ) {
  initialize(filename,hmin,hmax,p);
}

void
ImageSizingField::initialize( const std::string& filename, double hmin, double hmax , double p ) {

  Texture texture(filename);

  int w = texture.width();
  int h = texture.height();
  aspect_ratio_ = double(w) / double(h);

  std::vector<int> dims = {w,h};
  grid_ = std::make_unique<Grid<Quad>>(dims);

  // correct the aspect ratio: keep the width = 1 and adjust the height
  for (int k = 0; k < grid_->vertices().nb(); k++) {
    grid_->vertices()[k][1] /= aspect_ratio_;
  }

  size_.resize( grid_->nb() );

  int k = 0;
  const unsigned char* pixels = texture.data();
  for (int j = h-1; j >= 0; j--) {
    for (int i = 0; i < w; i++) {

      int idx = 3 * j * w + 3 * i;

      // read the pixel (r,g,b) values in [0,1]
      double r = pixels[idx    ] / 255.0;
      double g = pixels[idx + 1] / 255.0;
      double b = pixels[idx + 2] / 255.0;

      double s = (r + g + b) / 3.0;

      double h = hmin + std::pow(s,p) * (hmax - hmin);

      size_[k++] = h;
    }
  }

  std::vector<double> coordinates( grid_->nb() * 2 );
  for (int k = 0; k < grid_->nb(); k++) {
    vec3d p0( grid_->vertices()[(*grid_)(k,0)] , 2 );
    vec3d p1( grid_->vertices()[(*grid_)(k,1)] , 2 );
    vec3d p2( grid_->vertices()[(*grid_)(k,2)] , 2 );
    vec3d p3( grid_->vertices()[(*grid_)(k,3)] , 2 );

    vec3d c = (p0 + p1 + p2 + p3) / 4.0;
    for (int d = 0; d < 2; d++)
      coordinates[2*k+d] = c[d];
  }
  search_ = std::make_unique<kdtree<2>>(coordinates);
}

double
ImageSizingField::operator()(const double* x) const {
  // for now just look up the closest point
  int n = search_->nearest_neighbor(x);
  return size_[n];
}

template class SizingField<2>;
template int SizingField<2>::nb_expected_triangles( const Mesh<Triangle>& ) const;
template int SizingField<2>::nb_expected_triangles( const Mesh<Quad>& ) const;

} // flux
