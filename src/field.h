/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#ifndef FLUX_FIELD_H_
#define FLUX_FIELD_H_

#include <string>
#include <vector>

namespace flux {

/**
 * \brief Field types where a single value is specified at each vertex or cell.
 */
enum FieldType {
  FieldType_VertexBased,
  FieldType_CellBased,
  FieldType_None
};

/**
 * \brief Represents a generic field (vertex- or cell-based) that is attached to a mesh.
 */
class Field {

public:
  /**
   * \brief Field constructor.
   *
   * \param[in] type (FieldType) - either vertex- or cell-based
   * \param[in] name - a name that will be used to identify this field in the visualizer
   * \param[in] data - the vertex- or cell-based data that will be stored
   */
  Field( FieldType type , const std::string& name , const std::vector<double>& data ) :
    type_(type),
    name_(name),
    data_(data)
  {}

  /**
   * \brief Returns the type of the field, either vertex- or cell-based
   *
   * \return the type of the field
   */
  FieldType type() const { return type_; }

  /**
   * \brief Returns the name that is used to identify this field.
   *
   * \return the name of the field
   */
  const std::string& name() const { return name_; }

  /**
   * \brief Returns the data (vertex- or cell-based) that is stored in this field
   *        which can be used to send data to the visualizer
   *
   * \return a vector with the field data
   */
  const std::vector<double>& data() const { return data_; }

private:
  FieldType type_;
  std::string name_;
  std::vector<double> data_;
};


}

#endif
