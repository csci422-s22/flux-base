/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#ifndef FLUX_SPHERE_H_
#define FLUX_SPHERE_H_

#include "mesh.h"

namespace flux {

/**
 * \brief Represents the mesh of a sphere.
 *        The boundaries of the grid at theta = 0 and 2\pi are stitched together
 *        and the row of triangles at the north and south poles are
 *        treated to avoid duplicatation and zero-area triangles.
 */
template<typename type>
class Sphere : public Mesh<type> {

public:
  Sphere( int nphi , int ntheta , double radius = 1 ) :
    Mesh<type>(3),
    nphi_(nphi),
    ntheta_(ntheta),
    radius_(radius) {
    create_vertices();
    create_elements();
  }

private:

  void create_vertices();
  void create_elements();

  int nphi_;
  int ntheta_;
  double radius_;

  int north_pole_;
  int south_pole_;

  using Mesh<type>::vertices_;

};

} // flux

#endif
