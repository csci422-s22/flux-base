/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/

#include "mesh.h"
#include "readwrite.h"
#include "texture.h"
#include "webgl.h"

#include <string>

using namespace flux;

static std::string
get_file_ext( const std::string& filename ) {
  std::string::size_type idx;
  idx = filename.rfind('.'); // find the '.' in reverse order
  if (idx!=std::string::npos)
    return filename.substr(idx+1);
  return "";
}

int
main( int argc , const char** argv ) {

  if (argc < 2) {
    printf("usage: flux360 [filename] [imagefile] [port]\n");
    printf("\twhere filename can be a .obj or .off file\n");
    printf("\timagefile is an image used to texture the mesh\n");
    printf("\tport number is optional, default to 7681\n");
    printf("\texamples:\n");
    printf("\t\tflux360 spot.obj\n");
    printf("\t\tflux360 fox.obj fox.png\n");
    printf("\t\tflux360 spot.obj --noimage 7681\n");
    return 0;
  }

  int port = 7681;
  std::unique_ptr<Texture> texture_ptr = nullptr;
  if (argc > 2) {
    std::string imagefile(argv[2]);
    if (imagefile != "--noimage") texture_ptr = std::make_unique<Texture>(imagefile);
    if (argc == 4) port = atoi(argv[3]);
  }

  // get the file extension
  std::string filename( argv[1] );
  std::string ext = get_file_ext(filename);

  std::unique_ptr<MeshBase> mesh;
  if (ext == "obj" || ext == "OBJ") {
    mesh = read_obj(filename,true);
  }
  else if (ext == "off" || ext == "OFF") {
    mesh = read_off(filename);
  }
  else {
    printf("unsupported file type: %s\n",ext.c_str());
  }

  Viewer viewer;
  viewer.add( *mesh.get() , texture_ptr.get() );
  viewer.run( port );

  return 0;
}
