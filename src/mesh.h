/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/

#ifndef FLUX_MESH_H_
#define FLUX_MESH_H_

#include "vertices.h"
#include "array2d.h"

#include <memory>
#include <string>

namespace flux {

// forward declaration of Field
class Field;

/**
 * \brief Generic mesh class that does not know which element type it represents.
 *        This is an abstract class. Derived classes should implement:
 *         - get_edges
 *         - get_triangles
 *         - calculate_face_normals
 *         - calculate_vertex_normals
 */
class MeshBase : public array2d<int> {

protected:

  /**
   * \brief Constructs the base mesh.
   *
   * \param[in] dim - ambient dimension where the mesh is defined
   * \param[in] stride - how many vertices are stored per element? 3 for triangles, 4 for quads, -1 for polygons/polyhedra
   */
  MeshBase( int dim , int stride ) :
    array2d(stride),
    vertices_(dim)
  {}

public:

  /**
   * \brief Destructs the base mesh.
   */
  virtual ~MeshBase() {}

  /**
   * \brief Retrieves the edges of the mesh, useful for visualization.
   *
   * \param[out] edges (2 x # edges)
   */
  virtual void get_edges( std::vector<int>& edges ) const = 0;

  /**
   * \brief Retrieves the triangles in the mesh, useful for visualization.
   *
   * \param[out] triangles (3 x # triangles)
   */
  virtual void get_triangles( std::vector<int>& triangles , std::vector<int>* parents = nullptr ) const = 0;

  /**
   * \brief Calculates the per-face normals (constant across each face).
   *        Often the result is averaged to define per-vertex normals (below).
   *
   * \param[out] the normal vectors at each face (# faces x 3)
   */
  virtual void calculate_face_normals( array2d<double>& normals ) const = 0;

  /**
   * \brief Calculates the per-vertex normals, useful for visualization.
   *
   * \param[out] the normal vectors at each face (# faces x 3)
   */
  virtual void calculate_vertex_normals( array2d<double>& normals ) const = 0;

  /**
   * \brief Retrieves a const reference to the vertices.
   */
  const Vertices& vertices() const { return vertices_; }

  /**
   * \brief Retrieves a non-const reference to the vertices.
   */
  Vertices& vertices() { return vertices_; }

  /**
   * \brief Creates a vertex field with a particular name.
   *
   * \param[in] name - the name of the field that will be used in the visualizer
   * \param[in] data - the data to attach to the vertices
   */
  void create_vertex_field( const std::string& name , const std::vector<double>& data );

  /**
   * \brief Creates a cell field with a particular name.
   *
   * \param[in] name - the name of the field that will be used in the visualizer
   * \param[in] data - the data to attach to the cells
   */
  void create_cell_field( const std::string& name , const std::vector<double>& data );

  /**
   * \brief Returns the number of fields stored in the mesh
   *
   * \return number of fields
   */
  int nb_fields() const { return fields_.size(); }

  /**
   * \brief Retrieves a const reference to a field stored in this mesh.
   *
   * \param[int] k - the index of the field to retrieve.
   */
  const Field& field( int k ) const;


protected:
  Vertices vertices_;           // vertices of the mesh
  std::vector< std::shared_ptr<Field> > fields_; // fields stored in the mesh
};

/**
 * \brief Represents a mesh with a particular element type (Triangle, Quad, etc.)
 */
template<typename type>
class Mesh : public MeshBase {

public:
  /**
   * \brief Constructs a mesh in a particular ambient dimension
   *
   * \param[in] dim - ambient dimension where the mesh vertices are defined
   */
  Mesh( int dim );

  /**
   * \brief Constructs a mesh from initial point and element index data.
   *
   * \param[in] dim - ambient dimension where the mesh vertices are defined
   * \param[in] np - number of points
   * \param[in] points - vertex coordinates
   * \param[in] ne - number of elements (triangles, quads, etc.)
   * \param[in] elements - the connectivity of the mesh elements (indices of the vertices in each element).
   */
  Mesh( int dim , int np , const double* points , int ne , const int* elements );

  void get_edges( std::vector<int>& edges ) const;
  void get_faces_with_cells( std::vector<int>& faces , std::vector<int>& cells ) const;
  void get_triangles( std::vector<int>& triangles , std::vector<int>* parents = nullptr ) const;
  void calculate_face_normals( array2d<double>& normals ) const;
  void calculate_vertex_normals( array2d<double>& normals ) const;

  /**
   * \brief Returns a const reference to the element type which knows geometric properties about its shape
   *        (e.g. Triangle, Quad, or another element defined in element.h)
   */
  const type& element() const { return element_; }

private:
  type element_; // fundamental element type for this mesh (e.g. Triangle, Quad, or another element defined in element.h)
};

/**
 * \brief A utility function that returns the vector normal to an element.
 *
 * \param[in] vertices - const reference to the vertices used to retrieve the vertex coordinates of the element.
 * \param[in] E - the indices of the vertices in this element.
 * \param[in] nv - the number of vertices in the element.
 * \param[out] normal - the resultant normal vector orthogonal to the element.
 */
template<typename type> void get_normal( const Vertices& vertices , const int* E , int nv , std::vector<double>& normal );

} // flux

#endif
