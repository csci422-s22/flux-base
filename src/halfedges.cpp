/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#include "element.h"
#include "halfedges.h"
#include "mesh.h"

#include <map>

namespace flux {

template<typename type>
HalfEdgeMesh<type>::HalfEdgeMesh( const Mesh<type>& mesh ) :
  mesh_(mesh),
  nb_boundary_(0) {
  build();
}

template<typename type>
void
HalfEdgeMesh<type>::build() {

  int dim = mesh_.vertices().dim();

  // first create the vertices
  std::map<int,HalfVertex*> index2vertex;
  for (int k = 0; k < mesh_.vertices().nb(); k++) {

    std::pair<vertex_itr,bool> ret = vertices_.insert( std::make_unique<HalfVertex>(dim,mesh_.vertices()[k]) );
    flux_assert( ret.second );

    HalfVertex* v = ret.first->get();
    v->index = k;
    index2vertex.insert( {k,v} );
  }

  // loop through the elements
  std::map< std::pair<int,int> , HalfEdge* > twins;
  for (int k = 0; k < mesh_.nb(); k++) {

    // loop through each edge of the element
    std::pair<face_itr,bool> retf = faces_.insert( std::make_unique<HalfFace>() );
    flux_assert( retf.second );

    HalfFace* face = retf.first->get();

    HalfEdge* first = nullptr;
    HalfEdge* prev  = nullptr;
    for (int j = 0; j < type::nb_edges; j++) {

      // construct the oriented edge
      int e0 = mesh_(k, type::edges[2*j  ]);
      int e1 = mesh_(k, type::edges[2*j+1]);

      // create the half edge
      std::pair<edge_itr,bool> rete = edges_.insert( std::make_unique<HalfEdge>() );
      flux_assert( rete.second );

      HalfEdge* edge = rete.first->get();

      // define the vertex for this half edge
      HalfVertex* vertex = index2vertex.at(e0);

      edge->vertex = vertex;
      edge->face   = face;
      vertex->edge = edge; // this gets written a bunch of times

      // determine the half-edge twin
      if (twins.find({e1,e0}) == twins.end())
        edge->twin = nullptr;
      else {
        HalfEdge* twin = twins.at({e1,e0});
        edge->twin = twin;
        twin->twin = edge;
      }
      twins.insert( { {e0,e1} , edge } );

      // assign the prev and next
      if (prev != nullptr) {
        edge->prev = prev;
        prev->next = edge;
      }
      else {
        first = edge;
      }

      prev = edge;
    }

    // connect the first and last edges
    flux_assert( first != nullptr );
    first->prev = prev;
    prev->next  = first;

    // assign the last edge to the face (it doesn't matter which one)
    face->edge  = first;
    face->index = k;
  }

  // go back through the edges and find those that do not have a twin
  nb_boundary_ = 0;
  for (edge_itr it = edges_.begin(); it != edges_.end(); ++it) {

    HalfEdge* iedge = it->get();
    if (iedge->twin != nullptr) continue; // not a boundary

    nb_boundary_++;

    // create a new fictitious half edge
    // it is more useful to have this boundary half edge point to a null face
    std::pair<edge_itr,bool> rete = edges_.insert( std::make_unique<HalfEdge>() );
    flux_assert( rete.second );

    HalfEdge* bedge = rete.first->get();

    bedge->vertex = iedge->next->vertex;
    bedge->twin   = iedge;
    iedge->twin   = bedge;
    bedge->face   = nullptr;

    // we cannot set the next and prev yet as they may not be constructed
    bedge->next = nullptr;
    bedge->prev = nullptr;

  }

  // go through the edges again, this time we will construct the next and prev
  // information for the boundary half edges
  for (edge_itr it = edges_.begin(); it != edges_.end(); ++it) {

    HalfEdge* he = it->get();
    if (he->face != nullptr) continue; // not a boundary edge

    flux_assert( he->next == nullptr );

    // keep looking for another boundary edge
    HalfEdge* he_next = he->twin;
    while (he_next->face != nullptr) {
      he_next = he_next->prev->twin;
    }

    he->next      = he_next;
    he_next->prev = he;
  }
}

template<typename type>
bool
HalfEdgeMesh<type>::check() const {

  bool ok = true;

  for (const_vertex_itr it = vertices_.begin(); it != vertices_.end(); ++it) {

    HalfVertex* v0 = it->get();
    HalfVertex* v = v0;
    if (v->edge == nullptr) ok = false;

    int count = 1;
    HalfEdge* e = v->edge;
    while (true) {
      e = e->next;
      v = e->vertex;
      if (v == v0) break;
      count++;
    }
    if (count != type::nb_edges) ok = false;
  }

  for (const_face_itr it = faces_.begin(); it != faces_.end(); ++it) {
    if (it->get()->edge == nullptr) ok = false;
  }
  return ok;
}

template<typename type>
void
HalfEdgeMesh<type>::get_onering( const HalfVertex* v , std::vector<HalfVertex*>& onering ) const {

  HalfEdge* first = v->edge;
  HalfEdge* edge  = first;
  do {

    // add the edge
    onering.push_back(edge->twin->vertex);

    // go to the next edge
    edge  = edge->twin->next;

  } while (edge != first);
}

template<typename type>
void
HalfEdgeMesh<type>::get_onering( const HalfVertex* v , std::vector<HalfFace*>& onering ) const {
  HalfEdge* first = v->edge;
  HalfEdge* edge  = first;
  int nb_faces = 0;
  do {

    // add the face
    flux_assert( edge != nullptr );
    if (edge->face != nullptr)
      onering.push_back(edge->face);

    // go to the next edge
    edge  = edge->twin->next;

    nb_faces++;
    if (nb_faces > int(faces_.size())) flux_assert_not_reached;

  } while (edge != first);
}

template<typename type>
void
HalfEdgeMesh<type>::get_onering( const HalfVertex* v , std::vector<HalfEdge*>& onering ) const {

  HalfEdge* first = v->edge;
  HalfEdge* edge  = first;
  int nb_edges = 0;
  do {

    // add the opposite vertex
    onering.push_back(edge);

    // go to the next edge
    edge  = edge->twin->next;

    nb_edges++;
    if (nb_edges > int(edges_.size())) flux_assert_not_reached;

  } while (edge != first);
}

template<typename type>
void
HalfEdgeMesh<type>::extract( Mesh<type>& mesh ) const {

  mesh.vertices().clear();
  mesh.clear();

  mesh.vertices().set_dim(mesh_.vertices().dim()); // use the same dimension as the original mesh
  mesh.set_stride(type::nb_vertices);

  // create the vertices
  int count = 0;
  for (const_vertex_itr it = vertices_.begin(); it != vertices_.end(); ++it) {
    HalfVertex* v = it->get();
    v->index = count++;
    mesh.vertices().add( v->point.data() );
  }

  // create the faces
  for (const_face_itr it = faces_.begin(); it != faces_.end(); ++it) {

    HalfFace* f = it->get();
    HalfEdge* e = f->edge;
    int elem[type::nb_vertices];
    for (int j = 0; j < type::nb_vertices; j++) {
      elem[j] = e->vertex->index;
      e = e->next;
    }

    mesh.add(elem);
  }
}

template<typename type>
HalfEdge*
HalfEdgeMesh<type>::get_boundary_edge() const {

  for (const_edge_itr it = edges_.begin(); it != edges_.end(); ++it) {
    HalfEdge* e = it->get();
    if (e->face == nullptr) return e;
  }
  return nullptr;
}

template<typename type>
void
HalfEdgeMesh<type>::count_boundary_edges() const {
  nb_boundary_ = 0;
  for (const_edge_itr it = edges_.begin(); it != edges_.end(); ++it) {
    HalfEdge* e = it->get();
    if (e->face == nullptr) nb_boundary_++;
  }
}

template<>
void
HalfEdgeMesh<Triangle>::flip( HalfEdge* edge ) {

  HalfVertex* p = edge->vertex;
  HalfVertex* q = edge->twin->vertex;

  flux_assert( p != q );

  HalfVertex* vL = edge->next->next->vertex;
  HalfVertex* vR = edge->twin->next->next->vertex;

  HalfEdge* twin = edge->twin;

  // get the faces adjacent to this edge
  HalfFace* fL = edge->face;
  HalfFace* fR = twin->face;

  // we are assuming a closed mesh
  flux_assert( fL != nullptr );
  flux_assert( fR != nullptr );

  // get the lower edges
  HalfEdge* eLR = twin->next;
  HalfEdge* eLL = edge->next->next;

  // get the upper edges
  HalfEdge* eUR = twin->next->next;
  HalfEdge* eUL = edge->next;

  // top
  edge->next = eUR;
  edge->vertex = vL;
  edge->face = fL;
  edge->twin = twin;

  eUR->next = eUL;
  eUR->vertex = vR;
  eUR->face = fL;
  //eUR->twin (same)

  eUL->next = edge;
  eUL->vertex = q;
  eUL->face = fL;
  //eUL->twin (same)

  // bottom
  twin->next = eLL;
  twin->vertex = vR;
  twin->face = fR;
  twin->twin = edge;

  eLL->next = eLR;
  eLL->vertex = vL;
  eLL->face = fR;
  //eLL->twin (same)

  eLR->next = twin;
  eLR->vertex = p;
  eLR->face = fR;
  //eLR->twin (same)

  // vertices
  vL->edge = edge;
  vR->edge = eUR;
  p->edge = eLR;
  q->edge = eUL;

  // faces
  fL->edge = edge;
  fR->edge = twin;
}

template<typename type>
int
HalfEdgeMesh<type>::nb_connected_boundaries() const {
  std::map<HalfEdge*,bool> visited;
  for (auto& e_ptr : edges_) {
    HalfEdge* e = e_ptr.get();
    if (e->face == nullptr) visited[e] = false;
    else visited[e] =  true;
  }

  int n = 0;
  while (true) {

    // find the first edge on a boundary that has not been visited
    HalfEdge* b = nullptr;
    for (auto& e_ptr : edges_) {
      HalfEdge* e = e_ptr.get();
      if (e->face != nullptr) continue;
      if (visited[e]) continue;
      b = e;
      break;
    }
    if (b == nullptr) break;

    // mark edges on this boundary as visited until we return to the start
    n++;
    HalfEdge* edge = b;
    do {
      visited[edge] = true;
      edge = edge->next;
    } while (edge != b);
  }
  return n;
}

template class HalfEdgeMesh<Triangle>;
template class HalfEdgeMesh<Quad>;

} // flux
