/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/

#include "element.h"
#include "error.h"

namespace flux {

// vertex/node
int* Vertex::edges = nullptr;
int* Vertex::triangles = nullptr;

// line
int Line::edges[2] = {0,1};
int Line::faces[2] = {0,1};
int* Line::triangles = nullptr;

// triangle
int Triangle::edges[6] = {0,1,1,2,2,0};
int Triangle::faces[6] = {0,1,1,2,2,0};
int Triangle::triangles[3] = {0,1,2};

// quad
int Quad::edges[8] = {0,1,1,2,2,3,3,0};
int Quad::faces[8] = {0,1,1,2,2,3,3,0};
int Quad::triangles[6] = {0,1,2,0,2,3};

// tet
int Tet::edges[12] = {0,1,0,2,0,3,1,2,1,3,2,3};
int Tet::triangles[12] = {1,2,3,2,0,3,0,1,3,0,2,1};
int Tet::faces[12] = {1,2,3,2,0,3,0,1,3,0,2,1};

template<>
void
random_barycentric<Triangle>( std::vector<double>& alpha ) {

  alpha.resize(3);
  alpha[0] = double(rand()) / double(RAND_MAX);
  alpha[1] = double(rand()) / double(RAND_MAX);
  alpha[2] = 1.0 - alpha[0] - alpha[1];

  if (alpha[2] < 0.0) {
    double v1 = 1 - alpha[0];
    double v2 = 1 - alpha[1];
    alpha[0] = v1;
    alpha[1] = v2;
    alpha[2] = 1.0 - v1 - v2;
  }
}

} // flux
