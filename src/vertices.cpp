/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/

#include "vertices.h"

namespace flux {

void
Vertices::copy( const Vertices& vertices ) {

  // clear the current data and copy the dimension
  clear();
  set_dim( vertices.dim() );

  // copy the coordinate data
  for (int k = 0; k < vertices.nb(); k++)
    add( vertices[k] );

  // TODO copy normals, incidence?
  for (int k = 0; k < vertices.uv().nb(); k++)
    uv_.add( vertices.uv()[k] );
}

void
Vertices::clear() {
  // do not clear the stride (dimension)
  array2d<double>::clear(false);
  uv_.clear(false);
  normals_.clear(false);
  incidence_.clear();
}

} // flux
