/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#include "element.h"
#include "grid.h"
#include "system.h"

#if defined(FLUX_WINDOWS)
#define _USE_MATH_DEFINES // needed before including cmath for M_PI
#endif
#include <cmath>

namespace flux {

template<typename type>
Grid<type>::Grid( const std::vector<int>& sizes , int dim ) :
  Mesh<type>( (dim < 0) ? sizes.size() : dim ),
  sizes_(sizes)
{
  build();
}

template<>
void
Grid<Line>::build() {

  int nx = sizes_[0];

  double dx = 1./double(nx);
  std::vector<double> x(vertices_.dim(),0.0);
  for (int i = 0; i < nx+1; i++) {
    x[0] = i * dx;
    vertices_.add(x.data());
  }

  int e[2];
  for (int i = 0; i < nx; i++) {
    e[0] = i;
    e[1] = i + 1;
    add(e);
  }
}

template<>
void
Grid<Triangle>::build() {

  int nx = sizes_[0];
  int ny = sizes_[1];

  double dx = 1./double(nx);
  double dy = 1./double(ny);

  std::vector<double> x(vertices_.dim(),0.0);
  for (int j = 0; j < ny+1; j++) {
    for (int i = 0; i < nx+1; i++) {

      x[0] = i * dx;
      x[1] = j * dy;

      vertices_.add(x.data());
    }
  }

  int t[3];
  for (int j = 0; j < ny; j++) {
    for (int i = 0; i < nx; i++) {

      int i0 = j*(nx+1) + i;
      int i1 = i0 + 1;
      int i2 = i1 + nx+1;
      int i3 = i2 - 1;

      t[0] = i0;
      t[1] = i1;
      t[2] = i2;
      add(t);

      t[0] = i0;
      t[1] = i2;
      t[2] = i3;
      add(t);
    }
  }
}

template<>
void
Grid<Quad>::build() {

  int nx = sizes_[0];
  int ny = sizes_[1];

  double dx = 1./double(nx);
  double dy = 1./double(ny);

  std::vector<double> x(vertices_.dim(),0.0);
  for (int j = 0; j < ny+1; j++) {
    for (int i = 0; i < nx+1; i++) {
      x[0] = i * dx;
      x[1] = j * dy;
      vertices_.add(x.data());
    }
  }

  int q[4];
  for (int j = 0; j < ny; j++) {
    for (int i = 0; i < nx; i++) {

      int i0 = j*(nx+1) + i;
      int i1 = i0 + 1;
      int i2 = i1 + nx+1;
      int i3 = i2 - 1;

      q[0] = i0;
      q[1] = i1;
      q[2] = i2;
      q[3] = i3;
      add(q);

    }
  }
}

template<>
void
Grid<Tet>::build() {

  int nx = sizes_[0];
  int ny = sizes_[1];
  int nz = sizes_[2];

  double dx = 1./double(nx);
  double dy = 1./double(ny);
  double dz = 1./double(nz);

  // create the vertices
  std::vector<double> x(vertices_.dim(),0.0);
  for (int k = 0; k < nz+1; k++) {
    for (int j = 0; j < ny+1; j++) {
      for (int i = 0; i < nx+1; i++) {
        x[0] = i * dx;
        x[1] = j * dy;
        x[2] = k * dz;
        vertices_.add(x.data());
      }
    }
  }

  const int hextets[6][4] = { {0, 1, 2, 4},
                            {1, 4, 3, 2},
                            {6, 2, 3, 4},

                            {1, 4, 5, 3},
                            {4, 6, 5, 3},
                            {7, 6, 3, 5} };

  const int joffset = (nx+1);
  const int koffset = (nx+1)*(ny+1);

  int t[4];
  for (int k = 0; k < nz; k++) {
    for (int j = 0; j < ny; j++) {
      for (int i = 0; i < nx; i++) {

        const int n0 = k*koffset + j*joffset + i;

        // all the vertex indices that make up an individial hex
        const int hexnodes[8] = { n0 + 0,
                                  n0 + 1,
                                  n0 + joffset + 0,
                                  n0 + joffset + 1,

                                  n0 + koffset + 0,
                                  n0 + koffset + 1,
                                  n0 + koffset + joffset + 0,
                                  n0 + koffset + joffset + 1 };

        // loop over all tets that make up a hex
        for (int tet = 0; tet < 6; tet++) {

          // map the nodes from the hex for each tet
          for (int n = 0; n < 4; n++)
            t[n] = hexnodes[hextets[tet][n]];

          // add the tet indices
          add(t);
        }
      }
    }
  }
}

template class Grid<Line>;
template class Grid<Triangle>;
template class Grid<Quad>;
template class Grid<Tet>;

} // flux
