/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#include "element.h"
#include "sphere.h"

#include <cmath>
#include <map>

namespace flux {

template<typename type>
void
Sphere<type>::create_vertices() {

  double dphi = M_PI / nphi_;
  double dtheta = 2.0 * M_PI / ntheta_;

  for (int j = 0; j < ntheta_; j++) {

    for (int i = 1; i < nphi_; i++) {

      double phi = i * dphi;
      double theta = j * dtheta;

      double x = radius_ * cos(theta) * sin(phi);
      double y = radius_ * sin(theta) * sin(phi);
      double z = radius_ * cos(phi);

      double p[3] = {x,y,z};
      vertices_.add(p);
    }
  }

  // add the north and south poles
  double pn[3] = {0,0,radius_};
  double ps[3] = {0,0,-radius_};
  north_pole_ = vertices_.nb();
  vertices_.add(pn);
  south_pole_ = vertices_.nb();
  vertices_.add(ps);
}

template<>
void
Sphere<Triangle>::create_elements() {

  // create the main portion of the sphere
  int t[3];
  for (int j = 0; j < ntheta_-1; j++) {
    for (int i = 0; i < nphi_-2; i++) {

      int i0 = j*(nphi_-1) + i;
      int i1 = i0 + 1;
      int i2 = i1 + (nphi_-1);
      int i3 = i2 - 1;

      t[0] = i0;
      t[1] = i1;
      t[2] = i2;
      this->add(t);

      t[0] = i0;
      t[1] = i2;
      t[2] = i3;
      this->add(t);
    }
  }

  // connect the theta and 2\pi boundaries
  int j = ntheta_ - 1;
  for (int i = 0; i < nphi_-2; i++) {
    int i0 = j*(nphi_-1) + i;
    int i1 = i0 + 1;
    int i2 = i + 1;
    int i3 = i2 -1;

    t[0] = i0;
    t[1] = i1;
    t[2] = i2;
    this->add(t);

    t[0] = i0;
    t[1] = i2;
    t[2] = i3;
    this->add(t);
  }

  // create the triangles on the layer near the north pole
  for (int j = 0; j < ntheta_; j++) {
    int i0 = north_pole_;
    int i1 = j*(nphi_-1);
    int i2 = i1 + (nphi_-1);
    if (j == ntheta_-1) i2 = 0;

    t[0] = i0;
    t[1] = i1;
    t[2] = i2;
    this->add(t);
  }

  // create the triangles on the layer near the south pole
  for (int j = 0; j < ntheta_; j++) {
    int i0 = j*(nphi_-1) + (nphi_-2);
    int i1 = south_pole_;
    int i2 = i0 + (nphi_-1);
    if (j == ntheta_-1) i2 = nphi_-2;

    t[0] = i0;
    t[1] = i1;
    t[2] = i2;
    this->add(t);
  }

  // we lose ntheta triangles at each of the two poles
  // since they are collapsed
  flux_assert( this->nb() == 2 * nphi_ * ntheta_  - 2*ntheta_ );
}

template class Sphere<Triangle>;

} // flux
