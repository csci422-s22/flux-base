/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#ifndef FLUX_READWRITE_H_
#define FLUX_READWRITE_H_

#include <memory>
#include <string>

namespace flux {

// forward declaration of MeshBase
class MeshBase;

/**
 * \brief Reads a mesh from an OBJ file using external/tiny_obj_loader.[h,cc]
 *
 * \param[in] filename - the name of the mesh file (include the .obj extension, e.g. "spot.obj")
 * \param[in] allow_duplicating - determines whether the vertices can be duplicated if there
 *                                are multiple parameter coordinates defined per vertex
 *
 *                                Set to false (default) which means the mesh will be watertight
 *                                assuming the vertices in the mesh file are not duplicated.
 *
 *                                Set to true if you want to visualize parameter coordinates.
 *
 * \return pointer to a mesh
 */
std::unique_ptr<MeshBase> read_obj( const std::string& filename , bool allow_duplicating = false );

/**
 *
 * \brief Writes a mesh to an OBJ file.
 *
 * \param[in] mesh - a generic mesh (no info about element type) to write
 * \param[in] filename - the name of the file that will be written (include the .obj extension)
 */
void write_obj( const MeshBase& mesh , const std::string& filename );

/**
 * \brief Reads a mesh from an OFF file
 *
 * \param[in] filename - the name of the mesh file (include the .off extension (e.g. "bunny.off")
 *
 * \return shared pointer to a mesh
 */
std::unique_ptr<MeshBase> read_off( const std::string& filename );


} // flux

#endif
