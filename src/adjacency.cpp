/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
 
#include "adjacency.h"
#include "element.h"
#include "mesh.h"

#include <algorithm>
#include <set>

namespace flux {

/**
 * \brief Represents a unique face of an element as a sorted list
 *        of the vertex indices.
 */
struct UniqueFace {
  std::vector<int> indices; // the list of indices that can be sorted (below)
  void sort() {
    std::sort( indices.begin() , indices.end() );
  }
  int elem = -1; // which element did this face come from?
  int indx = -1; // which face index in 'elem' did this face come from?
};

/**
 * \brief An operator necessary to create a std::set of UniqueFace's.
 *        Compares the topological dimension of the faces (which is usually the same)
 *        and then compares the sorted indices of the faces in lexicographic order.
 *
 * \param[in] ex, ey - UniqueFace objects with 'indices' that are assumed to be sorted.
 *
 * \return whether ex has an index less than ey in lexicographic order.
 */
bool
operator< (const UniqueFace& ex , const UniqueFace& ey ) {

  // check if the number of indices is different (different topological dimension)
  if (ex.indices.size() < ey.indices.size()) return true;
  if (ex.indices.size() > ey.indices.size()) return false;

  // since the unique face indices are sorted, compare each index in lexicographic order
  return std::lexicographical_compare( ex.indices.begin() , ex.indices.end() , ey.indices.begin() , ey.indices.end() );
}

template<typename type>
Adjacency<type>::Adjacency( const Mesh<type>& mesh ) :
  array2d<int>(type::nb_faces,mesh.nb(),-1),
  mesh_(mesh) {

  static_assert( ! std::is_same<type,Polygon>::value , "polygons not supported!" );
  static_assert( ! std::is_same<type,Polyhedron>::value , "polyhedra not supported!" );

  build();
}

template<typename type>
int
Adjacency<type>::indexof( int k0 , int k1 ) const {
  for (int j = 0; j < type::nb_faces; j++) {
    if ((*this)(k0,j) == k1) return j; // k1 is a neighbour of k0 along face j
  }
  return -1; // k1 is not a neighbour of k0
}

template<typename type>
void
Adjacency<type>::build() {

  const int nb_face_per_elem = mesh_.element().nb_faces;
  const int nb_vert_per_face = type::face_type::nb_vertices;
  const int* elem_faces = mesh_.element().faces;
  std::vector<int> indices( nb_vert_per_face );

  std::set<UniqueFace> faces;

  // loop through each element in the mesh
  for (int k = 0; k < mesh_.nb(); k++) {

    // loop through the facets of this element
    for (int j = 0; j < nb_face_per_elem; j++) {

      // retrieve the facet indices
      for (int i = 0; i < nb_vert_per_face; i++)
        indices[i] = mesh_( k , elem_faces[ nb_vert_per_face * j + i] );

      // create the unique face
      UniqueFace face;
      face.indices = indices;
      face.sort();

      // assign the element and face index of this face
      face.elem = k;
      face.indx = j;

      // check if this face exists
      if (faces.find(face) == faces.end()) {
        // the unique face does not exist, so add it to the faces
        faces.insert( face );
      }
      else {
        // the unique face exists, connect the two elements
        const UniqueFace& n = *faces.find(face);
        (*this)(k,j) = n.elem;
        (*this)(n.elem,n.indx) = k;
      }
    }
  }
}

template class Adjacency<Triangle>;
template class Adjacency<Quad>;
template class Adjacency<Tet>;

} // flux
