/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#ifndef FLUX_WEBGL_H_
#define FLUX_WEBGL_H_

#include <memory>
#include <vector>
#include <stdio.h>
#include <cassert>

#include <iostream>

namespace websockets {

/**
 * \brief Starts a WebSocket server, and listens for a connection from a client, ultimately sending
 *        a message to the client when a connection is established.
 *        Implemented in websockets.cpp
 *
 * \param[in] port - port to be used in the connection
 * \param[in] messages - array of messages to send to the client (browser)
 * \param[in] testing - controls whether we are running a test, and should avoid executing certain parts
 */
void write( int port , const std::vector<std::string>& messages , bool testing = false );

} // websockets

/**
 * \brief Labels for identifying buffer types that are passed to the client.
 *        Useful for writing things in a WebGL-like manner, such as gl::ARRAY_BUFFER
 */
namespace gl {
  static const int ARRAY_BUFFER = 0;
  static const int ELEMENT_ARRAY_BUFFER = 1;
  static const int TEXTURE_BUFFER = 2;
  static const int STATIC_DRAW = 3;
}

namespace flux {

class Texture;

#define ERROR(msg) printf("%s (line %d of file %s\n",msg,__LINE__,__FILE__);

/**
 * \brief Represents a vertex buffer object that stores some arbitrary type of data,
 *        while keeping track of the bytes used to store each piece of data.
 */
class BufferObject {

public:

  /**
   * \brief Saves the bytes stored in the incoming array into this buffer object.
   *
   * \param[in] data - array of data to store
   * \param[in] nbytes - number of bytes of the incoming array to store.
   */
  template<typename T>
  void write( const T* data , int nbytes ) {
    bytes_per_elem_ = sizeof(T);
    bytes_.resize(nbytes);
    const char* x = (const char*)data;
    for (int i = 0; i < nbytes; i++) {
      bytes_[i] = x[i];
    }

    // assign the type
    type_ = typeid(T).name();
  }

  /**
   * \brief Returns the number of bytes stored in this buffer object
   */
  int nbytes() const { return bytes_.size(); }

  /**
   * \brief Retrieves the data stored in this buffer object.
   *
   * \param[out] data - where the buffer data will be written
   */
  template<typename T>
  void get( std::vector<T>& data ) const {
    int nelem = bytes_.size() / bytes_per_elem_;
    data.resize(nelem);
    //printf("nbytes per elem = %d, nelem = %d\n",bytes_per_elem_,nelem);
    for (int i = 0; i < nelem; i++) {
      data[i] = *reinterpret_cast<const T*>(&bytes_[i*bytes_per_elem_]);
    }
  }

  /**
   * \brief Assigns a tag to this buffer so it can be identified in the client.
   *
   * \param[in] name - label to be assigned to this buffer
   */
  void set_tag( const std::string& name ) {
    tag_ = name;
  }

  /**
   * \brief Assigns which buffer type (ARRAY_BUFFER, ELEMENT_ARRAY_BUFFER) to be used by this buffer object.
   *
   * \param[in] target - ARRAY_BUFFER or ELEMENT_ARRAY_BUFFER (for now)
   */
  void set_target( int target ) {
    target_ = target;
  }

  /**
   * \brief Converts and returns the data stored in this buffer object to a Float32 array (single-precision float)
   */
  std::vector<float> Float32Array() const {
    assert( type_ == typeid(float).name() );
    std::vector<float> data;
    get(data);
    return data;
  }

  /**
   * \brief Converts and returns the data stored in this buffer object to a Uint16 (unsigned short)
   */
  std::vector<unsigned short> Uint16Array() const {
    assert( type_ == typeid(unsigned short).name() );
    std::vector<unsigned short> data;
    get(data);
    return data;
  }

  /**
   * \brief Converts and returns the data stored in this buffer object to a Uint32 (unsigned int)
   */
  std::vector<unsigned int> Uint32Array() const {
    assert( type_ == typeid(unsigned int).name() );
    std::vector<unsigned int> data;
    get(data);
    return data;
  }

  /**
   * \brief Converts and returns the data stored in this buffer object to a Uint8 (unsigned char)
   */
  std::vector<unsigned char> Uint8Array() const {
    assert( type_ == typeid(unsigned char).name() );
    std::vector<unsigned char> data;
    get(data);
    return data;
  }

  /**
   * \brief Returns the buffer data type.
   */
  const std::string& type() const { return type_; }

  /**
   * \brief Returns the label assigned to this buffer.
   */
  const std::string& tag() const { return tag_; }

  /**
   * \brief Returns the target for this buffer (ARRAY_BUFFER, ELEMENT_BUFFER)
   */
  int target() const { return target_; }

private:
  int bytes_per_elem_;
  std::vector<unsigned char> bytes_;
  std::string tag_;
  std::string type_;
  int target_;
};

/**
 * \brief Represents a WebGL context (on the server side) for storing buffers that
 *        are all eventually sent to the client.
 *        Roughly exhibits the same state-machine properties of a real WebGL context.
 */
class WebGL_Context {
public:

  /**
   * \brief Initializes a WebGL context
   */
  WebGL_Context() {}

  /**
   * \brief Create a new buffer and return the index in the list of buffer objects.
   */
  int createBuffer() {
    int idx = buffers_.size();
    buffers_.push_back( std::make_shared<BufferObject>() );
    return idx;
  }

  /**
   * \brief Create a vertex array (associated with a particular set of buffer objects)
   *        and return the index in the list of arrays
   */
  int createVertexArray();

  /**
   * \brief Binds the index of some buffer to a certain target.
   *
   * \param[in] type - the target for the buffer
   * \param[in] buffer - index of the buffer to bind (index from createBuffer())
   */
  void bindBuffer( int type , int buffer ) {
    if (type == gl::ARRAY_BUFFER) {
      bound_array_buffer_ = buffers_[buffer].get();
    }
    else if (type == gl::ELEMENT_ARRAY_BUFFER) {
      bound_element_buffer_ = buffers_[buffer].get();
    }
    else {
      ERROR("unimplemented buffer type");
    }
  }

  /**
   * \brief Binds a texture. This is not implemented but something I would
   *        like to support in the future.
   */
  void bindTexture( int type , int texture );

  /**
   * \brief Saves some data (with type T) into the **currently bound buffer** (with a particular type)
   *
   * \param[in] type - target to save the buffer data to (ARRAY_BUFFER or ELEMENT_ARRAY_BUFFER)
   * \param[in] data - data to save to the currently bound buffer
   * \param[in] nbytes - number of bytes in the data array to save
   */
  template<typename T>
  void bufferData( int type , const T* data , int nbytes ) {
    if (type == gl::ARRAY_BUFFER) {
      bound_array_buffer_->write<T>(data,nbytes);
    }
    else if (type == gl::ELEMENT_ARRAY_BUFFER) {
      bound_element_buffer_->write<T>(data,nbytes);
    }
    else {
      ERROR("unimplemented buffer type");
    }
  }

  /**
   * \brief Tag the currently bound buffer (of some type) with some name.
   *        This is not in the WebGL spec, but is needed so users can know which
   *        buffers they want to use to draw stuff in the browser
   *        It operates within the same model as GL - you need to bind a buffer in order to tag it.
   */
  void tagBuffer( int type , const std::string& name ) {
    if (type == gl::ARRAY_BUFFER) {
      bound_array_buffer_->set_tag(name);
      bound_array_buffer_->set_target(type);
    }
    else if (type == gl::ELEMENT_ARRAY_BUFFER) {
      bound_element_buffer_->set_tag(name);
      bound_element_buffer_->set_target(type);
    }
    else {
      ERROR("unimplemented buffer type");
    }
  }

  /**
   * \brief Returns a string representation of some buffer type.
   */
  std::string
  target_name( int type ) {
    if      (type == gl::ARRAY_BUFFER) return "ARRAY_BUFFER";
    else if (type == gl::ELEMENT_ARRAY_BUFFER) return "ELEMENT_ARRAY_BUFFER";
    else ERROR("unimplemented buffer type");
    return "";
  }

  /**
   * \brief Sends all the data stored in the buffers to a client.
   *
   * \param[in] port - port used in the connection
   * \param[in] testing -  whether we are in testing mode and should omit executation of some functionality
   */
  void send( int port , bool testing );

private:
  std::vector< std::shared_ptr<BufferObject> > buffers_;

  BufferObject* bound_array_buffer_;
  BufferObject* bound_element_buffer_;
  BufferObject* bound_texture_buffer_;
};

class MeshBase;

/**
 * \brief This is the main server-side graphics application.
 *         It maintains a WebGL context that is updated whenever a mesh is added to the visualizer.
 *         When the run() function is called, all the data stored in the WebGL context is sent to the client.
 */
class Viewer {

public:
  /**
   * \brief Initializes the visualizer, and sets the current VAO (mesh) index to zero.
   */
  Viewer() :
    current_vao_index_(0)
  {}

  /**
   * \brief Adds a mesh to the WebGL context, incrementing the VAO index
   *
   * \param[in] vao - a generic mesh with vertex coordinates, element connectivity, normals, parameter coordinates, fields, etc.
   *                  any data detected in the mesh will be saved to the WebGL context.
   */
  void add( const MeshBase& vao , const Texture* texture = nullptr );

  /**
   * \brief Sends all the data stored in the WebGL context to the client.
   *        The reason this is implemented in the header file is so that we can
   *        determine whether we are testing (see testing flags above) so that
   *        the server-side wait can be cancelled in order to run the full suite of unit tests
   *        without manually modifying any unit testing code.
   *
   * \param[in] port - port to be used in the connection.
   */
  void run(int port = 7681) {
    webgl_.send(port,FLUX_FULL_UNIT_TEST);
  }

private:
  WebGL_Context webgl_;
  int current_vao_index_;
};

} // flux

#endif
