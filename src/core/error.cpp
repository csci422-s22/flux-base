/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#include "error.h"

#if !defined(__CYGWIN__) && !defined(__MINGW32__) && !defined(_MSC_VER)

#define BACKTRACE_AVAILABLE 1

#include <execinfo.h>
#include <cxxabi.h>
#include <dlfcn.h>
#include <sys/types.h>
#include <unistd.h>
#include <cstdlib>
#include <cstdarg>
#include <csignal>

/**
 * \brief print the backtrace of the function calls leading to the error.
 *
 * \param[in] start - frame number to start the trace
 * \param[in] end - frame number to end the trace (up to # frames - end)
 */
inline void
call_backtrace(const int start = 1 ,const int end = 2) {
  int 	i;
  enum 	{MAX_DEPTH=10};
  void 	*trace[MAX_DEPTH];
  char 	*demangled;
  int 	trace_size,status=0;
  Dl_info dlinfo;
  const char *symname;

  trace_size = backtrace(trace,MAX_DEPTH);

  for (i = start; i < trace_size-end; i++) {

    if (!dladdr(trace[i],&dlinfo)) continue;
    symname = dlinfo.dli_sname;

    demangled = abi::__cxa_demangle(symname, NULL, 0, &status);
    if (status == 0 && demangled)
      symname = demangled;

    if (symname)
      printf("%s\n",symname);

    if (demangled) {
      free(demangled);
    }
  }
	printf("\n");
}

#else

#define BACKTRACE_AVAILABLE 0

#endif // __CYGWIN__

namespace flux {

Exception::Exception(const char *file, int line,const char *fmt, ...) {
  char buffer[512];
  char info[512];

  #if BACKTRACE_AVAILABLE
  va_list argp;
  va_start(argp,fmt);
  vsprintf(buffer,fmt,argp);
  va_end(argp);
  #endif

  sprintf(info," (file %s, line %d) ===\n",file,line);

  message = "\n=== caught flux::Exception";
  message += std::string(info);
  message += std::string(buffer);

  #if BACKTRACE_AVAILABLE
  printf("%s\n\n=== Backtrace ===\n",message.c_str());
  call_backtrace(1);
  #endif
}

} // flux
