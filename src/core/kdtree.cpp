/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#include "kdtree.h"

namespace flux {

template class kdtree<2>;
template class kdtree<3>;

} // flux
