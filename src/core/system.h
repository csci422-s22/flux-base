/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#ifndef FLUX_SYSTEM_H_
#define FLUX_SYSTEM_H_

// determine if we are on windows
#if defined(__CYGWIN__) || defined(__MINGW32__) || defined(_MSC_VER)

#define FLUX_WINDOWS

// check for visual studio
#if defined(_MSC_VER)
#define FLUX_MVS
#define __PRETTY_FUNCTION__ __FUNCSIG__
#define M_PI 3.141592654
#endif

#else

// we are on a unix system
#define FLUX_UNIX

#endif

#endif
