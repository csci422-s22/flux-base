/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
 
#include "array2d.h"

namespace flux {

// explicitly instantiate the array2d class for required types
template class array2d<int>;
template class array2d<double>;

} // flux
