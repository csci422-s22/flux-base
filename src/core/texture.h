/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#ifndef FLUX_TEXTURE_H_
#define FLUX_TEXTURE_H_

#include <string>
#include <vector>

namespace flux {

/**
 * \brief Holds (r,g,b) pixel data that represents an image.
 */
class Texture {

public:
  /**
   * \brief Reads an image from a file.
   *        Any image supported by stb_image.h can be read.
   *
   * \param[in] filename - image file to read
   */
  Texture( const std::string& filename );

  /**
   * \brief Returns a pointer to the bytes used to store the image.
   */
  const unsigned char* data() const { return data_.data(); }

  /**
   * \brief Returns the width of the image.
   */
  int width() const { return width_; }

  /**
   * \brief Returns the height of the image
   */
  int height() const { return height_; }

  /**
   * \brief Returns the number of channels stored in each pixel (should be 3).
   */
  int channels() const { return channels_; }

  /**
   * \brief Returns the total number of bytes stored for the image.
   */
  int nb_bytes() const { return width_*height_*channels_; }

private:

  /**
   * \brief Reads the image in 'filename', storing the (r,g,b) pixel data
   *        in data_, and saving the dimensions.
   */
  void read( const std::string& filename );

  int width_;
  int height_;
  int channels_;
  std::vector<unsigned char> data_;

};

}

#endif
