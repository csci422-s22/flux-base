/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#ifndef FLUX_CRYPTO_H_
#define FLUX_CRYPTO_H_

#include <cstdint>
#include <cstdlib>

/**
 * \brief Implements the Base64 Content-Transfer-Encoding standard described in RFC1113,
 *        needed for the WebSocket handshake.
 *
 * \param[in] in - string to encode
 * \param[in] in_len - number of characters to encode
 * \param[out] out - encoded string
 * \param[out] out_len - number of characters in encoded string
 */
int b64_encode_string(const char *in, int in_len, char *out, int out_size);

/**
 * \brief Implements the Secure Hash Algorithm, needed for the WebSocket handshake.
 *
 * \param[in] d - characters to hash
 * \param[in] n - number of characters
 * \param[out] md - hash value
 */
unsigned char* SHA1(const unsigned char *d, size_t n, unsigned char *md);

#endif
