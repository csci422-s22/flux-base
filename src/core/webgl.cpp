/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/

#include "field.h"
#include "mesh.h"
#include "texture.h"
#include "webgl.h"

#include <iostream>
#include <map>
#include <vector>
#include <math.h>

namespace flux {

void
add_json_field( std::string& J , const std::string& name , const int& i , bool end = false) {
  J += "\u0022" + name + "\u0022:" + std::to_string(i);
  if (end) J += "}";
  else J += ",";
}

void
add_json_field( std::string& J , const std::string& name , const std::string& s , bool end = false) {
  J += "\u0022" + name + "\u0022:\u0022" + s + "\u0022";
  if (end) J += "}";
  else J += ",";
}

template<typename T>
void
add_json_field( std::string& J , const std::string& name , const std::vector<T>& x , bool end = false) {
  J += "\u0022" + name + "\u0022:[";
  if (x.size() == 0)
    J += "]";
  else {
    for (int k = 0; k < (int)x.size(); k++) {
      if (x[k] != x[k]) {
        printf("NaN in data for field %s!\n",name.c_str());
        flux_assert_not_reached;
      }
      J += std::to_string(x[k]);
      if (k < int(x.size())-1) J += ",";
      else J += "]";
    }
  }
  if (end) J += "}";
  else J += ",";
}

void
WebGL_Context::send( int port , bool testing ) {

  std::string message = "{ \"buffers\": [";
  for (int i = 0; i < (int)buffers_.size(); i++) {

    const BufferObject& buffer = *buffers_[i].get();

    // retrieve the data
    std::string J = "{";
    if (buffer.type() == typeid(float).name()) {
      std::vector<float> x = buffer.Float32Array();
      add_json_field(J,"data",x);
      add_json_field(J,"type","Float32Array");
    }
    else if (buffer.type() == typeid(unsigned short).name()) {
      std::vector<unsigned short> x = buffer.Uint16Array();
      add_json_field(J,"data",x);
      add_json_field(J,"type","Uint16Array");
    }
    else if (buffer.type() == typeid(unsigned int).name()) {
      std::vector<unsigned int> x = buffer.Uint32Array();
      add_json_field(J,"data",x);
      add_json_field(J,"type","Uint32Array");
    }
    else if (buffer.type() == typeid(unsigned char).name()) {
      std::vector<unsigned char> x = buffer.Uint8Array();
      add_json_field(J,"data",x);
      add_json_field(J,"type","Uint8Array");
    }
    else {
      ERROR("unimplemented buffer type");
    }
    add_json_field(J,"tag",buffer.tag());
    add_json_field(J,"index",i);
    add_json_field(J,"target",target_name(buffer.target()),true);

    message += J;
    if (i < (int)buffers_.size()-1) message += ",";
    else message += "]}";
  }

  // at the end we write the data to the client
  websockets::write( port , {message} , testing );
}

typedef unsigned int gl_index;
typedef float gl_float;

class VertexAttributeObject {

public:
  VertexAttributeObject( const MeshBase& mesh ) {

    // option to calculate vertex normals
    // check the number of vertices, because it's possible to have multiple
    // normals per vertex (which would mean the vertex should have been
    // duplicated in the obj loader)

    array2d<double> vertex_normals(3);
    if (mesh.vertices().dim() == 3) {
      if (mesh.vertices().normals().nb() != mesh.vertices().nb()) {
        mesh.calculate_vertex_normals( vertex_normals );
      }
      else {
        for (int k = 0; k < mesh.vertices().nb(); k++)
          vertex_normals.add( mesh.vertices().normals()[k] );
      }
    }
    else {
      flux_assert( mesh.vertices().dim() == 2 );
      double normal[3] = {0,0,1};
      for (int k = 0; k < mesh.vertices().nb(); k++)
        vertex_normals.add( normal );
    }

    // initialize the triangles and edges
    std::vector<int> T;
    std::vector<int> parents;
    mesh.get_triangles( T , &parents );
    triangles_.assign( T.begin() , T.end() );

    std::vector<int> E;
    mesh.get_edges( E );
    edges_.assign( E.begin() , E.end() );

    // first check if there are any cell-based fields
    bool cell_based = false;
    field_names_.resize( mesh.nb_fields() );
    fields_.resize( mesh.nb_fields() );
    for (int k = 0; k < mesh.nb_fields(); k++) {

      field_names_[k] = mesh.field(k).name();

      const Field& field = mesh.field(k);
      if (field.type() == FieldType_CellBased) {
        cell_based = true;
      }
    }

    if (cell_based) {

      // copy the points and normals, and determine a map from the
      // original vertices to the duplicates
      std::map<int,int> point_map;
      int count = 0;
      points_.clear();
      parameters_.clear(); // we will not populate parameters for now
      normals_.clear();

      for (int k = 0; k < triangles_.size(); k++) {
        point_map.insert( {triangles_[k],count++} );
        for (int d = 0; d < mesh.vertices().dim(); d++)
          points_.push_back( mesh.vertices()[triangles_[k]][d] );
        for (int d = mesh.vertices().dim(); d < 3; d++) // in case the vertices are in 2d
          points_.push_back( 0.0 );
        for (int d = 0; d < 3; d++) {
          normals_.push_back( vertex_normals[triangles_[k]][d] );
        }
      }

      // adjust the indices for the edges
      for (int k = 0; k < edges_.size(); k++)
        edges_[k] = point_map.at( edges_[k] );

      // adjust the indices for the triangles
      triangles_.resize( points_.size()/ 3 );
      for (int k = 0; k < triangles_.size(); k++)
        triangles_[k] = k;

      // adjust the fields
      for (int k = 0; k < mesh.nb_fields(); k++) {

        const Field& field = mesh.field(k);
        if ( field.type() == FieldType_VertexBased ) {
          for (int j = 0; j < mesh.nb(); j++) {
            for (int i = 0; i < mesh.count(j); i++) {
              fields_[k].push_back( field.data()[ mesh(j,i) ] );
            }
          }
        }
        else if (field.type() == FieldType_CellBased ) {
          for (int j = 0; j < parents.size(); j++) {
            for (int i = 0; i < 3; i++)
              fields_[k].push_back( field.data()[parents[j]] );
          }
          flux_assert_msg( fields_[k].size() == triangles_.size() , "|fields[%d]| = %lu, nb_tri = %lu\n",k,fields_[k].size(),triangles_.size());
        }
        else
          flux_assert_not_reached;
      }
    }
    else {

      // no need to adjust triangle or edge indices
      // copy over the data for coordinates, parameters and normals
      if (mesh.vertices().dim() == 3) {
        const std::vector<double>& coordinates = mesh.vertices().data();
        points_.assign( coordinates.begin() , coordinates.end() );
      }
      else {
        flux_assert( mesh.vertices().dim() == 2 );
        std::vector<double> coords( mesh.vertices().nb() * 3 , 0.0 );
        for (int k = 0; k < mesh.vertices().nb(); k++) {
          for (int d = 0; d < 2; d++)
            coords[3*k+d] = mesh.vertices()(k,d);
          //printf("(%g,%g,%g)\n",coords[3*k],coords[3*k+1],coords[3*k+2]);
        }
        points_.assign( coords.begin() , coords.end() );
      }

      const std::vector<double>& parameters = mesh.vertices().uv().data();
      parameters_.assign( parameters.begin() , parameters.end() );

      const std::vector<double>& normals = vertex_normals.data();
      normals_.assign( normals.begin() , normals.end() );

      // copy the scalar (vertex-based) fields
      for (int k = 0; k < mesh.nb_fields(); k++) {

        const Field& field = mesh.field(k);
        flux_assert( field.type() == FieldType_VertexBased );

        const std::vector<double>& data = field.data();
        fields_[k].assign( data.begin() , data.end() );
      }

    }

  }

  const std::vector<gl_index>& triangles()  const { return triangles_; }
  const std::vector<gl_index>& edges()      const { return edges_; }
  const std::vector<gl_float>& points()     const { return points_; }
  const std::vector<gl_float>& parameters() const { return parameters_; }
  const std::vector<gl_float>& normals()    const { return normals_; }
  const std::vector<gl_float>& field( int k ) const {
    flux_assert( k < (int)fields_.size() );
    return fields_[k];
  }

  const std::string& field_name( int k ) const {
    flux_assert( k < (int)field_names_.size() );
    return field_names_[k];
  }

  int nb_fields() const { return fields_.size(); }

private:

  std::vector<gl_index> triangles_;
  std::vector<gl_index> edges_;

  std::vector<gl_float> points_;
  std::vector<gl_float> parameters_;
  std::vector<gl_float> normals_;

  std::vector< std::vector<gl_float> > fields_;
  std::vector< std::string > field_names_;
};

void
Viewer::add( const MeshBase& mesh , const Texture* texture ) {

  WebGL_Context& gl = webgl_; // I like to write it like in JavaScript :)

  int idx = current_vao_index_;

  VertexAttributeObject vao(mesh);

  // buffer the triangles
  int triangle_buffer = gl.createBuffer();
  gl.bindBuffer( gl::ELEMENT_ARRAY_BUFFER , triangle_buffer );
  gl.bufferData( gl::ELEMENT_ARRAY_BUFFER , vao.triangles().data() , sizeof(gl_index) * vao.triangles().size() );
  gl.tagBuffer( gl::ELEMENT_ARRAY_BUFFER , "triangles0-vao" + std::to_string(idx) );

  // buffer the vertex coordinates
  int vertex_buffer = gl.createBuffer();
  gl.bindBuffer( gl::ARRAY_BUFFER , vertex_buffer );
  gl.bufferData( gl::ARRAY_BUFFER , vao.points().data() , sizeof(gl_float) * vao.points().size() );
  gl.tagBuffer( gl::ARRAY_BUFFER , "coordinates-vao" + std::to_string(idx) );

  // buffer the parameter coordinates
  if (vao.parameters().size() > 0) {
    int parameter_buffer = gl.createBuffer();
    gl.bindBuffer( gl::ARRAY_BUFFER , parameter_buffer );
    gl.bufferData( gl::ARRAY_BUFFER , vao.parameters().data() , sizeof(gl_float) * vao.parameters().size() );
    gl.tagBuffer( gl::ARRAY_BUFFER , "parameters-vao" + std::to_string(idx) );
  }

  // buffer the normal vectors
  if (vao.normals().size() > 0) {
    int normal_buffer = gl.createBuffer();
    gl.bindBuffer( gl::ARRAY_BUFFER , normal_buffer );
    gl.bufferData( gl::ARRAY_BUFFER , vao.normals().data() , sizeof(gl_float) * vao.normals().size() );
    gl.tagBuffer( gl::ARRAY_BUFFER , "normals-vao" + std::to_string(idx) );
  }

  // buffer the scalar fields
  for (int k = 0; k < vao.nb_fields(); k++) {

    int field_buffer = gl.createBuffer();
    gl.bindBuffer( gl::ARRAY_BUFFER , field_buffer );
    gl.bufferData( gl::ARRAY_BUFFER , vao.field(k).data() , sizeof(gl_float) * vao.field(k).size() );
    gl.tagBuffer( gl::ARRAY_BUFFER , "field_" + vao.field_name(k) + "-vao" + std::to_string(idx) );

  }

  // buffer the edges
  int edge_buffer = gl.createBuffer();
  gl.bindBuffer( gl::ELEMENT_ARRAY_BUFFER , edge_buffer );
  gl.bufferData( gl::ELEMENT_ARRAY_BUFFER , vao.edges().data() , sizeof(gl_index) * vao.edges().size() );
  gl.tagBuffer( gl::ELEMENT_ARRAY_BUFFER , "edges0-vao" + std::to_string(idx) );

  // buffer the texture if there is one
  if (texture) {
    int texture_buffer = gl.createBuffer();
    gl.bindBuffer( gl::ARRAY_BUFFER , texture_buffer );
    gl.bufferData( gl::ARRAY_BUFFER , texture->data() , texture->nb_bytes() );
    int w = texture->width();
    int h = texture->height();
    int c = texture->channels();
    gl.tagBuffer( gl::ARRAY_BUFFER , "texture-vao" + std::to_string(idx) + "-size-" + std::to_string(w) + "x" + std::to_string(h) + "x" + std::to_string(c) );
  }

  current_vao_index_++;
}

} // flux
