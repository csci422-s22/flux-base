/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#ifndef FLUX_ERROR_H_
#define FLUX_ERROR_H_

#include "system.h"

#include <cstdio>
#include <exception>
#include <string>

namespace flux {

#ifndef unlikely
#ifdef __GNUC__
#define unlikely(x) __builtin_expect(!!(x), 0)
#else
#define unlikely(x) x
#endif
#endif


#ifndef FLUX_MVS
#define flux_throw(args ...) throw(flux::Exception(__FILE__,__LINE__,args));
#else
#define flux_throw(args, ...) throw(flux::Exception(__FILE__,__LINE__,args));
#endif

/** @file */
/**
 * \brief Assert the input is true, causing a crash if false.
 *
 * \param[in] X - boolean expresssion to be evaluated in the assertion.
 */
#define flux_assert(X) if(unlikely(!(X))) { printf("\nfailed to assert %s in file %s line %d\n",(#X),__FILE__,__LINE__);flux_throw("assertion error");}

/**
 * \brief Assert the input is true, causing a crash if false and printing some info.
 *
 * \param[in] X - boolean expresssion to be evaluated in the assertion.
 * \param[in] ... stuff to print if the expression is false (formatted like printf), useful for debugging.
 */
#define flux_assert_msg(X,...) { try{ flux_assert(X); } catch(...) { printf(__VA_ARGS__); flux_throw("assertion error"); } }

/**
 * \brief Crash the program because something was reached which requires implementation.
 */
#define flux_implement { flux_throw("not implemented"); }

/**
 * \brief Crash the program because something was reached which should not be possible.
 */
#define flux_assert_not_reached { flux_throw("\nthis should not have been reached!\n"); }

/**
 * \brief Represents an exception that is thrown with some custom message.
 */
class Exception : public std::exception {

public:
  /**
   * \brief Creates an exception with file, line number information, as well
   *        as a custom message
   */
  explicit Exception(const char *file, int line,const char *fmt, ...);
  virtual ~Exception() throw() {}
  //virtual const char* what() const throw() { return message.c_str(); }

protected:
  std::string message;
};

} // flux

#endif
