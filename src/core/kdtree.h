/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#ifndef FLUX_KDTREE_H_
#define FLUX_KDTREE_H_

#include "error.h"

#include "external/nanoflann.hpp"

#include <memory>
#include <vector>

namespace flux {

/**
 * \brief Represents a kd-tree that wraps around the nanoflann external library.
 *        See flux-base/test/core/kdtree_ut.cpp for examples.
 */
template<int dim>
class kdtree {

private:

  /**
   * \brief Private point cloud class used to represent points in nanoflann.
   */
  class PointCloud {
  public:
    /**
     * \brief PointCloud constructor, saves the coordinates of the points.
     */
    PointCloud( const std::vector<double>& coordinates ) :
      coordinates_(coordinates)
    {}

    /**
     * \brief Returns the number of points (needed by nanoflann).
     */
  	size_t kdtree_get_point_count() const { return coordinates_.size() / dim; }

    /**
     * \brief Returns the d'th coordinate of point k (needed by nanoflann).
     */
  	double kdtree_get_pt(const size_t k, int d) const { return coordinates_[dim*k + d]; }

    /**
     * \brief Returns whether there is a bounding box associated with the point cloud.
     *        (needed by nanoflann).
     */
  	template <class BBOX>	bool kdtree_get_bbox(BBOX&) const { return false; }

  private:
    std::vector<double> coordinates_;
  };

public:
  /**
   * \brief kd-tree constructor from a list of point coordinates.
   *        Compatible with the way points are stored in the Vertices class.
   *        For example,
   *        Grid<Triangle> mesh( {10,10} );
   *        kdtree<2> tree( mesh.vertices().data() );
   */
  kdtree( const std::vector<double>& coordinates ) :
    cloud_(coordinates) {
    tree_ = std::make_unique<nanoflann::KDTreeSingleIndexAdaptor<
              nanoflann::L2_Simple_Adaptor<double,PointCloud>,
              PointCloud,dim> >
              (dim,cloud_,nanoflann::KDTreeSingleIndexAdaptorParams(10));
    tree_->buildIndex();
  }

  /**
   * \brief Computes the indices of the nearest points (in the PointCloud) to a query point x.
   *
   * \param[in] x - coordinates of the query point
   * \param[inout] neighbors - indices of the nearest neighbors.
   *               The size of the incoming neighbors vector is used to determine how many
   *               nearest neighbors should be computed.
   */
  void get_nearest_neighbors( const double* x , std::vector<int>& neighbors ) const {
    std::vector<double> distance( neighbors.size() );
    std::vector<unsigned int> indices( neighbors.size() );
    int nu = tree_->knnSearch( x , neighbors.size(), indices.data() , distance.data() );
    neighbors.resize(nu);
    neighbors.assign( indices.begin() , indices.end() );
  }

  /**
   * \brief Returns the index of a single nearest neighbor (in the PointCloud) to a query point x.
   */
  int nearest_neighbor( const double* x ) const {
    double distance;
    unsigned int neighbor;
    tree_->knnSearch( x , 1 , &neighbor , &distance );
    return neighbor;
  }

private:
  PointCloud cloud_;
  std::unique_ptr<nanoflann::KDTreeSingleIndexAdaptor
      <nanoflann::L2_Simple_Adaptor<double,PointCloud>,PointCloud,dim>> tree_;
};

} // flux

#endif
