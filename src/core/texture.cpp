/**********************************************************************
 * flux: framework for learning unstructured meshing
 * Copyright (c) 2022 Philip Caplan. All rights reserved.
 * Licensed under the MIT License (https://mit-license.org/)
 **********************************************************************/
#include "texture.h"

#define STB_IMAGE_IMPLEMENTATION
#include "external/stb_image.h"

namespace flux {

Texture::Texture( const std::string& filename ) :
  channels_(3) {
  read(filename);
}

void
Texture::read( const std::string& filename ) {

  int n;
  unsigned char* pixels = stbi_load( filename.c_str() , &width_ , &height_ , &n , channels_ );

  if (n != channels_) printf("[warning] image has %d channels -> using %d\n",n,channels_);

  data_.resize( width_*height_*channels_ );
  for (int i = 0; i < width_*height_*channels_; i++)
    data_[i] = pixels[i];

  printf("--> read %d x %d image\n",width_,height_);

  stbi_image_free(pixels);
}

}
